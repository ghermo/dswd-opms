$(document).ready(function() {

	function removeItem () {
		var tr = $(this).closest('tr');

		switch(tr.data('type'))
		{
			case "office":
				$("#selectedTeam").append("<option value="+tr.find("input").val()+">"+tr.data('text')+"</option");
				$("#selectedCoChampion").append("<option value="+tr.find("input").val()+">"+tr.data('text')+"</option");
				break;
			case "measure":
				$("#selectedMeasure").append("<option value="+tr.find("input").val()+">"+tr.data('text')+"</option");
				break;
		}

		tr.remove();
	}

	$(".removeItem").click(removeItem);

	$("#addMeasure").click(function() {

		if($('#selectedMeasure option:selected').length == 0) return;

		measure_id = $('#selectedMeasure').val();
		measure_name = $('#selectedMeasure option:selected').text();

		$('#selectedMeasure option:selected').remove();

		$('#measures').find('.empty').remove();
		
		var newMeasure = $('<tr data-text="'+measure_name+'" data-type="measure"> <td width="0%"> <input type="hidden" name="measures[]" value="'+measure_id+'"> &nbsp; </td><td width="75%"> '+measure_name+' </td> <td width="25%"> <a class="btn removeItem" data-id="'+measure_id+'" title="Remove"><i class="fa fa-times" style="color:red;"></i></a></td></tr>');

		newMeasure.find("a").click(removeItem);
		$('#measures').append(newMeasure);
	});

	$("#addCoChampion").click(function() {

		if($('#selectedCoChampion option:selected').length == 0) return;

		office_id = $('#selectedCoChampion').val();
		office_name = $('#selectedCoChampion option:selected').text();

		$('#selectedCoChampion option:selected').remove();
		$('#selectedTeam').find('[value='+office_id+']').remove();

		$('#coChampions').find('.empty').remove();

		var office = $('<tr data-text="'+office_name+'" data-type="office"> <td width="0%"> <input type="hidden" name="coChampions[]" value="'+office_id+'"> &nbsp; </td><td width="75%"> '+office_name+' </td> <td width="25%"> <a class="btn removeItem" data-id="'+office_id+'" data-original-title="Delete"><i class="fa fa-times" style="color:red;"></i></a></td></tr>');

		office.find('a').click(removeItem);
		$("#coChampions").append(office);
	});

	$("#addTeam").click(function() {

		if($('#selectedTeam option:selected').length == 0) return;

		office_id = $('#selectedTeam').val();
		office_name = $('#selectedTeam option:selected').text();

		$('#selectedTeam option:selected').remove();
		$('#selectedCoChampion').find('[value='+office_id+']').remove();

		$('#teams').find('.empty').remove();

		var office = $('<tr data-text="'+office_name+'" data-type="office"> <td width="0%"> <input type="hidden" name="teams[]" value="'+office_id+'"> &nbsp; </td><td width="75%"> '+office_name+' </td> <td width="25%"> <a class="btn removeItem" data-id="'+office_id+'" data-original-title="Delete"><i class="fa fa-times" style="color:red;"></i></a></td></tr>');


		office.find('a').click(removeItem);
		$("#teams").append(office);
	});

	var PMs = 0;
	function onlyNumbers(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }

	$(".input-numbers").keydown(onlyNumbers);
	function addPM() {

		var startDate = $('#input-start').val();
		var endDate = $('#input-end').val();
		var milestone = $('#input-milestone').val();
		var amount = $('#input-amount').val();
		var source = $('#input-source').val();
		var owner = $('#input-owner').val();
		var owner_name = $('#input-owner option:selected').text();
		var status = $('#input-status').val();

		if($.trim(startDate)&&$.trim(endDate)&&$.trim(milestone)&&$.trim(amount)&&$.trim(source)&&$.trim(owner)&&$.trim(owner_name))
		{
			//$('.validatemsg').hide();
			$('#input-start').val('');
			$('#input-end').val('');
			$('#input-milestone').val('');
			$('#input-amount').val('');
			$('#input-source').val('');
			$('#input-status').val('');

			$('#input-start').focus();

			var newPM = $('<tr> <td> <input class="form-control" placeholder="'+startDate+'" name="PM'+ PMs +'a" type="text" value="'+startDate+'"> </td> <td><input class="form-control" placeholder="'+endDate+'" name="PM'+ PMs +'b" type="text" value="'+endDate+'"></td> <td><input class="form-control" placeholder="'+milestone+'" name="PM'+ PMs +'c" type="text" value="'+milestone+'"></td> <td> <div class="input-group"> <span class="input-group-addon">Php</span> <input class="form-control input-numbers" placeholder="'+amount+'" name="PM'+ PMs +'d" type="text" value="'+amount+'"> </div> </td> <td><input class="form-control" placeholder="'+source+'" name="PM'+ PMs +'e" type="text" value="'+source+'"></td> <td><select class="form-control" name="PM'+ PMs +'f"><option value="'+owner+'" selected="selected">'+owner_name+'</option></select></td> <td> <div class="status"> <div class="head"> 4th quarter: </div> <textarea class="form-control body" rows="3" data-current="0" placeholder="" name="PM'+ PMs +'j" cols="50" style="display: none;"></textarea> </div> <div class="status"> <div class="head"> 3rd quarter: </div> <textarea class="form-control body" rows="3" placeholder="" data-current="0" name="PM'+ PMs +'i" cols="50" style="display: none;"></textarea> </div> <div class="status"> <div class="head"> 2nd quarter: </div> <textarea class="form-control body" rows="3" placeholder="" data-current="1" name="PM'+ PMs +'h" cols="50" style="display: none;"></textarea> </div> <div class="status"> <div class="head"> 1st quarter: </div> <textarea class="form-control body" rows="3" placeholder="'+status+'" data-current="0" name="PM'+ PMs +'g" cols="50">'+status+'</textarea> </div><td><button class="btn btn-danger delete-pm" type="button"><i class="fa fa-trash-o"></i></button></td></tr>');

			newPM.find(".input-numbers").keydown(onlyNumbers);
			newPM.find('.delete-pm').click(removePM);
			$('#project-milestones').find('.empty').remove();
			$("#project-milestones").append(newPM);
			
			PMs++;
		}	
	}

	function removePM() {
		if(confirm("Are you sure you want to delete?"))
		{
			$(this).closest('tr').find('input').val('');
			$(this).closest('tr').find('.PM-id').remove();
			$(this).closest('tr').hide();
		}
	}

	function pressEnter(event) {
		if(event.which == 13) {
			addPM();
			event.preventDefault();
		}
	}

	$('#input-start').keypress(pressEnter);
	$('#input-end').keypress(pressEnter);
	$('#input-milestone').keypress(pressEnter);
	$('#input-amount').keypress(pressEnter);
	$('#input-source').keypress(pressEnter);
	$('#input-owner').keypress(pressEnter);
	$('#input-status').keypress(pressEnter);

	$("#addPM").click(addPM);
	$(".delete-pm").click(removePM);
});
// go back to the previous page when the back button is clicked
function goBack() {
  history.back()
}

$('.datepicker').datepicker();

//show and then hide alert notification
$(document).ready(function(){
  $(".notif").hide(0);

  if($("#dirty_page").val() != 1)
  {
    $(".notif").delay(300).fadeIn(600).delay(2000).fadeOut(300);
    $("#dirty_page").val(1);
  }

  $('.sub-nav2').delay(300).animate({ top: '31' }, 400, 'swing');

  //autosize textarea
  $('textarea').autosize();

  // $("tbody").delegate(".animated", "focus", function()) {
  //   $( this ).autosize();
  // }
  $("tbody").delegate(".animated", "focus", function() {
    $(this).autosize();
  });

  $('span, h2 > a, a.btn, button.btn').tooltipster ({
    animation: 'fade',
    delay: 0,
    theme: 'black',
    touchDevices: false,
    trigger: 'hover'
  });

  //Initialize WYSIWYG editor
  $('.summernote').summernote({ 
    toolbar: [
      ['style', ['bold', 'italic', 'underline', 'clear']],
      ['font', ['strikethrough']],
      ['fontsize', ['fontsize']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['table', ['table']],
      ['height', ['height']]
    ],
    airPopover: [
      ['style', ['bold', 'italic', 'underline', 'clear']],
      ['font', ['strikethrough']],
      ['fontsize', ['fontsize']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['table', ['table']],
      ['height', ['height']]
    ]
  });

  $('.hide-toolbar').siblings().children('.note-toolbar').css('display', 'none');

  // check if user is using a mobile
  if(WURFL.is_mobile){

    // if a table has a table-hover class, remove it.
    if($('table').hasClass('table-hover')){
      $('table').removeClass('table-hover'); 
    }

    if($('table').hasClass('table-select')){
      $('.table-select').on('click', 'tbody tr', function(event) {
        $(this).addClass('highlight').siblings().removeClass('highlight');
        $(this).find("td:last-child").children('.actions').addClass('show');
        $(this).find('td:last-child').children('.actions').children('.action-btn').removeClass('hide-button-style');
        $(this).siblings().find("td:last-child").children('.actions').removeClass('show').addClass('unselected');
      });
    }
    else {
      $('table > tbody').children().find('td:last-child').children('.actions').addClass('show');
      $('table > tbody').children().find('td:last-child').children('.actions').children().removeClass('hide-button-style');
    }

  } else {
    if($('table').hasClass('table-hover')){
      $(".actions").on("mouseenter",function(){
        $(this).children('.action-btn').removeClass('hide-button-style');
      });

      $(".actions").on("mouseleave",function(){
        $(this).children('.action-btn').addClass('hide-button-style');
      });

      $('.table-hover').on('mouseenter', 'tbody tr', function(event) {
        $(this).find("td:last-child").children('.actions').addClass('show');
        $(this).siblings().find('td:last-child').children('.actions').removeClass('show');
      });

      $('.table-hover').on('mouseleave', 'tbody tr', function(event) {
        $(this).find("td:last-child").children('.actions').removeClass('show');
      });
    }
    else {
      $('table > tbody').children().find('td:last-child').children('.actions').addClass('show');

      $(".actions").on("mouseenter",function(){
        $(this).children('.action-btn').removeClass('hide-button-style');
      });

      $(".actions").on("mouseleave",function(){
        $(this).children('.action-btn').addClass('hide-button-style');
      });
    }
  }

});
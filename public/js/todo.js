function TodoCtrl($scope) {
  var id = 65;
    $scope.perspective_id = 0;

  $scope.todos = [
    {id: id++, perspective_id: 1, objective:'learn angular', measure:'test' },
    {id: id++, perspective_id: 2, objective:'build an angular app', measure:'test'}];
  
  $scope.getId = function(id) {
    return String.fromCharCode(id);
  }
  $scope.addTodo = function() {
    $scope.todos.push({id: id++, perspective_id: parseInt($scope.perspective_id)+1, objective:$scope.objectiveText, measure:$scope.measureText});
    $scope.objectiveText = '';
    $scope.measureText = '';
    $scope.perspective_id = 0;
  };
 
  $scope.remaining = function() {
    var count = 0;
    angular.forEach($scope.todos, function(todo) {
      count += todo.done ? 0 : 1;
    });
    return count;
  };
 
  $scope.archive = function() {
    var oldTodos = $scope.todos;
    $scope.todos = [];
    angular.forEach(oldTodos, function(todo) {
      if (!todo.done) $scope.todos.push(todo);
    });
  };
}
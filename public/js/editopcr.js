$(document).ready(function() {
	$('#selected-office').text($('#office-select option:selected').text());
	$('#office-select').change(function() {
		$('#selected-office').text($('#office-select option:selected').text());
	});

	$('#period-select').keyup(function() {
		$('#selected-period').text($('#period-select').val());
	});
	
	var A_count = 0;
	var B_count = 0;
	var C_count = 0;
	var D_count = 0;
	
	function addIndicator() {

		var form = $(this).closest(".opcr-add");
		var tbody = form.find('.add-button').data('tbody');

		if( ! tbody) {
			tbody = "B-"+form.find('.input1').val();
			id = B_count++;
			$("#form").append($("<input type='hidden' name='"+id+"-B-1' value='"+form.find('.input1').val()+"'>"));
		}
		else {
			var id;
			switch(tbody.charAt(0))
			{
				case 'A': id = A_count++; break;
				case 'C': id = C_count++; break;
				case 'D': id = D_count++; break;
			}
		}
		
		$('#'+tbody).find('.empty').remove();
		$("#"+tbody).append("<tr><td></td><td>"+form.find('.input2').val()+"</td><td>"+form.find('.input3').val()+"</td><td>"+form.find('.input4').val()+"</td><td>"+form.find('.input5').val()+"</td><td>"+form.find('.input6').val()+"</td><td>"+((parseFloat(form.find('.input4').val())+parseFloat(form.find('.input5').val())+parseFloat(form.find('.input6').val()))/3)+"</td><td>&nbsp;</td><td>&nbsp;</td></tr>");

		
		if(tbody.charAt(0) == 'A') {
			if(tbody.charAt(2) == 'B') {
				$("#form").append($("<input type='hidden' name='"+id+"-A-1' value='BG'>"));
			}
			else {
				$("#form").append($("<input type='hidden' name='"+id+"-A-1' value='LM'>"));
			}
			tbody = 'A';
		}
		else if(tbody.charAt(0) == 'B')
			tbody = 'B';

		$("#form").append($("<input type='hidden' name='"+id+"-"+tbody+"-2' value='"+form.find('.input2').val()+"'>"));
		$("#form").append($("<input type='hidden' name='"+id+"-"+tbody+"-3' value='"+form.find('.input3').val()+"'>"));
		$("#form").append($("<input type='hidden' name='"+id+"-"+tbody+"-4' value='"+form.find('.input4').val()+"'>"));
		$("#form").append($("<input type='hidden' name='"+id+"-"+tbody+"-5' value='"+form.find('.input5').val()+"'>"));
		$("#form").append($("<input type='hidden' name='"+id+"-"+tbody+"-6' value='"+form.find('.input6').val()+"'>"));
		//alert(tbody.charAt(0));
		if(tbody.charAt(0) == 'A'||tbody.charAt(0) == 'B')
		{
			
			var initial=parseFloat($('#strat').val());
			document.getElementById('strat').value=initial+((parseFloat(form.find('.input4').val())+parseFloat(form.find('.input5').val())+parseFloat(form.find('.input6').val()))/3);
			
	  		document.getElementById('strategic').value = (parseFloat($('#strat').val())/(A_count+B_count))*.50;

			//alert((initial+(parseFloat(form.find('.input4').val())+parseFloat(form.find('.input5').val())+parseFloat(form.find('.input6').val()))/(A_count+B_count))*.50);
		}
		else if(tbody.charAt(0) == 'C')//the last one
		{
			
			//var initial=parseFloat($('#other').val());
	  		//document.getElementById('other').value = (initial+(parseFloat(form.find('.input4').val())+parseFloat(form.find('.input5').val())+parseFloat(form.find('.input6').val()))/(C_count))*.10;
			var initial=parseFloat($('#oth').val());
			document.getElementById('oth').value=initial+((parseFloat(form.find('.input4').val())+parseFloat(form.find('.input5').val())+parseFloat(form.find('.input6').val()))/3);
			
	  		document.getElementById('other').value = (parseFloat($('#oth').val())/(C_count))*.10;

		}
		else if(tbody.charAt(0) == 'D')
		{
			//alert("WHHHAHAHAHAHAHH");
			//var initial=parseFloat($('#leader').val());
	  		//document.getElementById('leader').value = (initial+(parseFloat(form.find('.input4').val())+parseFloat(form.find('.input5').val())+parseFloat(form.find('.input6').val()))/(D_count))*.40;
			var initial=parseFloat($('#lead').val());
			document.getElementById('lead').value=initial+((parseFloat(form.find('.input4').val())+parseFloat(form.find('.input5').val())+parseFloat(form.find('.input6').val()))/3);
			
	  		document.getElementById('leader').value = (parseFloat($('#lead').val())/(D_count))*.40;

		}
		var initial=$('#final').val();

	  	document.getElementById('final').value = parseFloat($('#leader').val())+parseFloat($('#other').val())+parseFloat($('#strategic').val());
		
		form.find('.input2').val('').focus();
		form.find('.input3').val('');
		form.find('.input4').val('');
		form.find('.input5').val('');
		form.find('.input6').val('');
	}
	

	function pressEnter(event) {
		if(event.which == 13) {
			event.preventDefault();
			$(this).closest(".opcr-add").find('.add-button').click();
		}
	}

	$('.input1').keypress(pressEnter);
	$('.input2').keypress(pressEnter);
	$('.input3').keypress(pressEnter);
	$('.input4').keypress(pressEnter);
	$('.input5').keypress(pressEnter);
	$('.input6').keypress(pressEnter);
	$('.add-button').click(addIndicator);
});
$(document).ready(function() {
	$('#selected-office').text($('#office-select option:selected').text());
	$('#office-select').change(function() {
		$('#selected-office').text($('#office-select option:selected').text());
	});

	$('#period-select').keyup(function() {
		$('#selected-period').text($('#period-select').val());
	});
	
	var A_count = 0;
	var B_count = 0;
	var C_count = 0;
	var D_count = 0;
	//stack  overflow start
	$(".input4").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $(".input5").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $(".input6").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    //stack overflow end
	$('.input4').keyup(function(){
		var form = $(this).closest(".opcr-add");
		$input4=form.find('.input4').val();
		$input5=form.find('.input5').val();
		$input6=form.find('.input6').val();
		if($input4=='')
			$input4=0;
		if($input5=='')
			$input5=0;
		if($input6=='')
			$input6=0;

		form.find('.stratAve').val((parseFloat($input4)+parseFloat($input5)+parseFloat($input6))/3);
    }); 
    $('.input5').keyup(function(){
    	var form = $(this).closest(".opcr-add");
		$input4=form.find('.input4').val();
		$input5=form.find('.input5').val();
		$input6=form.find('.input6').val();
		if($input4=='')
			$input4=0;
		if($input5=='')
			$input5=0;
		if($input6=='')
			$input6=0;

		form.find('.stratAve').val((parseFloat($input4)+parseFloat($input5)+parseFloat($input6))/3);
    }); 
    $('.input6').keyup(function(){
    	var form = $(this).closest(".opcr-add");
		$input4=form.find('.input4').val();
		$input5=form.find('.input5').val();
		$input6=form.find('.input6').val();
		if($input4=='')
			$input4=0;
		if($input5=='')
			$input5=0;
		if($input6=='')
			$input6=0;

		form.find('.stratAve').val((parseFloat($input4)+parseFloat($input5)+parseFloat($input6))/3);
    }); 

    function removeIndicator() {
    	$(this).closest('tr').hide();
    }
	
	function addIndicator() {

		var form = $(this).closest(".opcr-add");
		var tbody_ = form.find('.add-button').data('tbody');
		if($.trim(form.find('.input2').val())&&$.trim(form.find('.input3').val())&&$.trim(form.find('.input4').val())&&$.trim(form.find('.input5').val())&&$.trim(form.find('.input6').val()!=''))
		{
			if( ! tbody_) {
				tbody_ = "B-"+form.find('.input1').val();
				id = B_count++;
				$("#form").append($("<input type='hidden' name='"+id+"-B-1' value='"+form.find('.input1').val()+"'>"));
			}
			else {
				var id;
				switch(tbody_.charAt(0))
				{
					case 'A': id = A_count++; break;
					case 'C': id = C_count++; break;
					case 'D': id = D_count++; break;
				}
			}

			if(tbody_.charAt(0) == 'A') {
				if(tbody_.charAt(2) == 'B') {
					$("#form").append($("<input type='hidden' name='"+id+"-A-1' value='BG'>"));
				}
				else {
					$("#form").append($("<input type='hidden' name='"+id+"-A-1' value='LM'>"));
				}
				tbody = 'A';
			}
			else if(tbody_.charAt(0) == 'B')
				tbody = 'B';
			else tbody = tbody_;
			
			var newIndicator = $("<tr>&nbsp;<td></td><td><input class='form-control' type='text' name='"+id+"-"+tbody+"-2' value='"+form.find('.input2').val()+"'></td><td><input class='form-control' type='text' name='"+id+"-"+tbody+"-3' value='"+form.find('.input3').val()+"'></td><td><input class='form-control' type='text' name='"+id+"-"+tbody+"-4' value='"+form.find('.input4').val()+"'></td><td><input class='form-control' type='text' name='"+id+"-"+tbody+"-5' value='"+form.find('.input5').val()+"'></td><td><input class='form-control' type='text' name='"+id+"-"+tbody+"-6' value='"+form.find('.input6').val()+"'></td><td>"+((parseFloat(form.find('.input4').val())+parseFloat(form.find('.input5').val())+parseFloat(form.find('.input6').val()))/3)+"</td><td>&nbsp;</td><td>&nbsp;</td><td> <a class='removeItem btn btn-danger'> <i class='fa fa-trash-o'> </i> </a></tr>");

			$('#'+tbody_).find('.empty').remove();
			$("#"+tbody_).append(newIndicator);

			newIndicator.find('a').click(removeIndicator);

			//alert(tbody.charAt(0));
			if(tbody.charAt(0) == 'A'||tbody.charAt(0) == 'B')
			{
				
				var initial=parseFloat($('#strat').val());
				document.getElementById('strat').value=initial+((parseFloat(form.find('.input4').val())+parseFloat(form.find('.input5').val())+parseFloat(form.find('.input6').val()))/3);
				
		  		document.getElementById('strategic').value = (parseFloat($('#strat').val())/(A_count+B_count))*.50;

				//alert((initial+(parseFloat(form.find('.input4').val())+parseFloat(form.find('.input5').val())+parseFloat(form.find('.input6').val()))/(A_count+B_count))*.50);
			}
			else if(tbody.charAt(0) == 'C')//the last one
			{
				
				//var initial=parseFloat($('#other').val());
		  		//document.getElementById('other').value = (initial+(parseFloat(form.find('.input4').val())+parseFloat(form.find('.input5').val())+parseFloat(form.find('.input6').val()))/(C_count))*.10;
				var initial=parseFloat($('#oth').val());
				document.getElementById('oth').value=initial+((parseFloat(form.find('.input4').val())+parseFloat(form.find('.input5').val())+parseFloat(form.find('.input6').val()))/3);
				
		  		document.getElementById('other').value = (parseFloat($('#oth').val())/(C_count))*.10;

			}
			else if(tbody.charAt(0) == 'D')
			{
				//alert("WHHHAHAHAHAHAHH");
				//var initial=parseFloat($('#leader').val());
		  		//document.getElementById('leader').value = (initial+(parseFloat(form.find('.input4').val())+parseFloat(form.find('.input5').val())+parseFloat(form.find('.input6').val()))/(D_count))*.40;
				var initial=parseFloat($('#lead').val());
				document.getElementById('lead').value=initial+((parseFloat(form.find('.input4').val())+parseFloat(form.find('.input5').val())+parseFloat(form.find('.input6').val()))/3);
				
		  		document.getElementById('leader').value = (parseFloat($('#lead').val())/(D_count))*.40;

			}
			var initial=$('#final').val();

		  	document.getElementById('final').value = parseFloat($('#leader').val())+parseFloat($('#other').val())+parseFloat($('#strategic').val());
			
			form.find('.input2').val('').focus();
			form.find('.input3').val('');
			form.find('.input4').val('0');
			form.find('.input5').val('0');
			form.find('.input6').val('0');
			form.find('.stratAve').val('0');
		}
	}
	

	function pressEnter(event) {
		if(event.which == 13) {
			event.preventDefault();
			$(this).closest(".opcr-add").find('.add-button').click();
		}
	}

	$('.input1').keypress(pressEnter);
	$('.input2').keypress(pressEnter);
	$('.input3').keypress(pressEnter);
	$('.input4').keypress(pressEnter);
	$('.input5').keypress(pressEnter);
	$('.input6').keypress(pressEnter);
	$('.add-button').click(addIndicator);
});
$(document).ready(function() {
	$(".remarks").find(".show-more").hide();

	$(".remarks").find(".remarks-collapse").click(function(event) {
		if($(this).closest('.remarks').find('.show-more').is(":visible"))
			$(this).text("See All");
		else
			$(this).text("Collapse");

		$(this).closest(".remarks").find(".show-more").slideToggle('slow');
	});

	$(".remarks").find(".filter").change(function(event) {
		switch($(this).val())
		{
			case '0':
				$(this).closest('.remarks').find('.remark0').show();
				$(this).closest('.remarks').find('.remark1').hide();
				break;
			case '1':
				$(this).closest('.remarks').find('.remark0').hide();
				$(this).closest('.remarks').find('.remark1').show();
				break;
			case '2':
				$(this).closest('.remarks').find('.remark0').show();
				$(this).closest('.remarks').find('.remark1').show();
				break;
		}
	});
});
var dswdApp = angular.module('dswdApp', [], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

dswdApp.controller('OpcrController', ['$scope', function ($scope) {
	$scope.indicators1 = [];
	$scope.indicators2 = [];
	$scope.indicators3 = [];
	$scope.indicators4 = [];

	$scope.newIndicator1 = [];
	$scope.newIndicator2 = [];

	$scope.addIndicator1 = function(flag) {
		if($scope.newIndicator1[flag].indicator && $scope.newIndicator1[flag].actualAccomplishment)
		{
			$scope.newIndicator1[flag].flag = flag;
			$scope.indicators1.push($scope.newIndicator1[flag]);
			$scope.newIndicator1[flag] = {};
		}
	}

	$scope.addIndicator2 = function(objective) {
		if($scope.newIndicator2[objective].indicator && $scope.newIndicator2[objective].actualAccomplishment)
		{
			$scope.newIndicator2[objective].objective = objective;
			$scope.indicators2[objective].push($scope.newIndicator2[objective]);
			$scope.newIndicator2[objective] = {};
		}
	}

	$scope.addIndicator3 = function() {
		if($scope.newIndicator3.indicator && $scope.newIndicator3.actualAccomplishment) 
		{
			$scope.indicators3.push($scope.newIndicator3);
			$scope.newIndicator3 = {};
		}
	}

	$scope.addIndicator4 = function() {
		if($scope.newIndicator4.indicator && $scope.newIndicator4.actualAccomplishment) 
		{
			$scope.indicators4.push($scope.newIndicator4);
			$scope.newIndicator4 = {};
		}
	}

	$scope.delIndicator1 = function(indicator) {
		$scope.indicators1.splice($scope.indicators1.indexOf(indicator), 1);
	}

	$scope.delIndicator2 = function(id, indicator) {
		$scope.indicators2[id].splice($scope.indicators2[id].indexOf(indicator), 1);
	}

	$scope.delIndicator3 = function(indicator) {
		$scope.indicators3.splice($scope.indicators3.indexOf(indicator), 1);
	}

	$scope.delIndicator4 = function(indicator) {
		$scope.indicators4.splice($scope.indicators4.indexOf(indicator), 1);
	}
}]);
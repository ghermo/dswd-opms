<?php

class CoreFunctions extends Eloquent {

	protected $table = 'core_functions';
	public $fillable = ['opcr_id', 'objective_id', 'indicator', 'accomplishment', 'description', 'qn','q1','t'];

	function opcr() 
	{
		return $this->belongsTo('Opcr');	
	}

	public function averageRating()
	{
		return number_format((($this->qn+$this->q1+$this->t)/3), 2);
	}
}
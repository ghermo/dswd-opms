<?php

class CsbRemark extends Eloquent 
{
	public $fillable = ['user_id', 'remark', 'type', 'scoreboard_id'];
	protected $table = 'csb_remarks';

	function scoreboard()
	{
		return $this->belongsTo('Scoreboard');
	}
	function user()
	{
		return $this->belongsTo('User');
	}
	
}
<?php

class InitRemark extends Eloquent 
{
	public $fillable = ['user_id', 'remark', 'type', 'initiative_id'];
	protected $table = 'init_remarks';

	function initiative()
	{
		return $this->belongsTo('Initiatives', 'initiative_id');
	}
	function user()
	{
		return $this->belongsTo('User');
	}
}
<?php

class OtherFunctions extends Eloquent {

	protected $table = 'other_functions';
	public $fillable = ['opcr_id', 'indicator', 'accomplishment','qn','q1','t'];

	function opcr() 
	{
		return $this->belongsTo('Opcr');	
	}
	
	public function averageRating()
	{
		return number_format((($this->qn+$this->q1+$this->t)/3), 2);
	}
}
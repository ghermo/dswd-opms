<?php

class Scoreboard extends Eloquent 
{
	protected $table = 'scoreboards';
	//public $fillable = ['goal','year','statusDate','status','type','baseline','month','date','author','lead_measure1','lead_measure2','responsible_office1','responsible_office2','status1','status2','narrative'];
	public $fillable = ['goal','goal_status','objective_id','scoreboards_holder_id','universe','year','flag','statusDate','type','baseline','month','date','author_id','lead_measure1','lead_measure2','responsible_office1','responsible_office2','status1','status2','narrative'];
	public  $errors;

	function author()
	{
		return $this->belongsTo('Office');
	}
	function challenge()
	{
		return $this->hasMany('Challenge');
	}
	function assignments()
	{
		return $this->hasMany('Assignments');
	}
	function objective()
	{
		return $this->belongsTo('Objective', 'objective_id');
	}
	function holder()
	{
		return $this->belongsTo('ScoreboardsHolder', 'scoreboards_holder_id');
	}
	
	public function accomplishments($month = null, $year = null)
	{
		if($month && $year)
			return $this->hasMany('Accomplishments', 'scoreboard_id')->where('year', $year)->where('month', $month)->get();
		else
			return $this->hasMany('Accomplishments');
	}

	public function name()
	{
		return $this->author->office_name.' Office Scoreboard';
	}

	public function remarks()
	{
		return $this->hasMany('CsbRemark');
	}

	public function notifiedUsers()
	{
		if($this->flag)
			return User::officers($this->author_id);
		else
			return User::adminAndOfficers($this->author_id);
	}
}

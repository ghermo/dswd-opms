<?php

class Objective extends Eloquent 
{
	public $fillable = ['objective', 'perspective_id', 'measure_id'];

	function perspective() 
	{
		return $this->belongsTo('Perspective');
	}

	function measure() 
	{
		return $this->hasOne('Measure');
	}

	public function scoreboard()
	{
		return $this->hasMany('Scoreboard');
	}

	function indicators()
	{
		return $this->hasMany('Indicators', 'goal_id')->orderBy('ordinal')->orderBy('type');
	}

	function indicatorsMovement()
	{
		return $this->hasMany('Indicators', 'goal_id')->whereType(2)->orderBy('ordinal');
	}
	function indicatorsUniverse()
	{
		return $this->hasMany('Indicators', 'goal_id')->whereType(1);
	}
	function indicatorsBaseline()
	{
		return $this->hasMany('Indicators', 'goal_id')->whereType(0);
	}
	function indicatorsTarget()
	{
		return $this->hasMany('Indicators', 'goal_id')->whereType(3);
	}
}
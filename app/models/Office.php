<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class Office extends Eloquent implements UserInterface, RemindableInterface {

	protected $table = 'offices';
	public $timestamps=false;
	protected $fillable=['office_type','office_name','office_abbreviation'];
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	
	public  $errors;
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function isValid()
	{
		 
		
		$rules = array('office_type' => 'required|alpha_spaces','office_name'=>'required|alpha_spaces|max:100','office_abbreviation'=>'required|alpha_dash|unique:offices|max:20');

		
		$validation=Validator::make($this->attributes,$rules);
		if($validation->passes())
		{
			return true;
		}
		$this->errors=$validation->messages();
			return false;
		
	}
	public function isValidEdit()
	{
		 
		
		$rules = array('office_name'=>'required|alpha_spaces|max:100','office_type' => 'required|alpha_spaces');

		
		$validation=Validator::make($this->attributes,$rules);
		if($validation->passes())
		{
			return true;
		}
		$this->errors=$validation->messages();
			return false;
		
	}

	public function getRememberToken()
	{
	    return $this->remember_token;
	}

	public function setRememberToken($value)
	{
	    $this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
	    return 'remember_token';
	}

	public function initiatives()
	{
		return $this->belongsToMany('Initiatives');
	}
	public function users()
	{
		return $this->hasMany('User');
	}

	public function opcr()
	{
		return $this->hasMany('Opcr');
	}

	public function opcrForApproval()
	{
		return $this->hasMany('Opcr')->whereFlag(0)->whereForApproval(1)->count();
	}

	public function scoreboardsHolder()
	{
		return $this->hasMany('ScoreboardsHolder', 'author_id')->with('Scoreboard');
	}

	public function scoreboards()
	{
		return $this->hasMany('Scoreboard', 'author_id');
	}

	public function scoreboardsForApproval()
	{
		return Role::access('3') ? 
				$this->scoreboardsHolder()->whereFlag(0)->whereForApproval(1)->count() : 
				$this->scoreboards()->whereFlag(0)->whereForApproval(1)->count();
	}
}
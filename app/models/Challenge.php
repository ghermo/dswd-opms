<?php

class Challenge extends Eloquent 
{
	protected $table = 'challenges';
	public $fillable = ['issue', 'action_taken', 'action_requested','scoreboard_id'];

	function scoreboard()
	{
		return $this->hasOne('Scoreboard');
	}
	
}
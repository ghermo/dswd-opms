<?php
class Opcr extends Eloquent 
{
	protected $table = 'opcr';
	public $fillable = ['id', 'scorecard_id', 'name', 'office_id', 'period','flag'];

	public function office()
	{
		return $this->belongsTo('Office');
	}

	public function strategicPriorities()
	{
		return $this->hasMany('StrategicPriorities');
	}

	public function coreFunctions()
	{
		return $this->hasMany('CoreFunctions');
	}

	public function otherFunctions()
	{
		return $this->hasMany('OtherFunctions');
	}

	public function leadershipCompetencies()
	{
		return $this->hasMany('LeadershipCompetencies');
	}

	public function remarks()
	{
		return $this->hasMany('OpcrRemark');
	}

	public function notifiedUsers()
	{
		if($this->flag)
			return User::officers($this->office_id);
		else
			return User::adminAndOfficers($this->office_id);
	}
}
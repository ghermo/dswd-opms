<?php

class Image extends Eloquent 
{
	protected $table = 'pictures';
	public $fillable = ['name','size','type','url','title','description'];

	
}


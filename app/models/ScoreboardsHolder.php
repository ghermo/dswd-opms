<?php
class ScoreboardsHolder extends Eloquent 
{
	protected $table = 'scoreboards_holder';
	public $fillable = ['flag', 'author_id'];

	function scoreboard()
	{
		return $this->hasMany('Scoreboard');
	}
	function remarks()
	{
		return $this->hasMany('FsbRemark');
	}

	public function author()
	{
		return $this->belongsTo('Office');
	}
	
	public function name()
	{
		return $this->scoreboard[0]->author->office_name.' Office Scoreboard for '.Date::$months[$this->scoreboard->first()->month].' '.$this->scoreboard->first()->year;
	}

	public function whereYear($value)
	{
		return ScoreboardsHolder::with('Scoreboard')->where('year', $value)->first();
	}

	public function whereMonth($value)
	{
		return ScoreboardsHolder::with('Scoreboard')->where('month', $value)->first();
	}

	public function notifiedUsers()
	{
		if($this->flag)
			return User::officers($this->author_id);
		else
			return User::adminAndOfficers($this->author_id);
	}
}
<?php

class FsbRemark extends Eloquent 
{
	public $fillable = ['user_id', 'remark', 'type', 'scoreboards_holder_id'];
	protected $table = 'fsb_remarks';

	function holder()
	{
		return $this->belongsTo('scoreboardsHolder', 'scoreboards_holder_id');
	}
	function user()
	{
		return $this->belongsTo('User');
	}
	
}
<?php

class Perspective extends Eloquent 
{
	public $fillable = ['perspective', 'scorecard_id', 'flag'];
	
	static function getHighest($scorecard) 
	{
		return Perspective::where('scorecard_id', '=', $scorecard)->where('flag', '=', '1')->first();
	}

	function scorecard() {
		return $this->belongsTo('Scorecard');
	}

	function objectives() {
		return $this->hasMany('Objective')->orderBy('ordinal');
	}
}
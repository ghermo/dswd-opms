<?php

class Scorecard extends Eloquent {

	protected $table = 'scorecard';
	public $fillable = ['name', 'description','flag'];

	static function current() 
	{
		return Scorecard::where('flag', '=', '1')->first();
	}

	function perspectives()
	{
		return $this->hasMany('Perspective')->orderBy('flag', 'desc');
	}

	function goals()
	{
		return $this->getHighest()->objectives;
	}

	function initiatives()
	{
		return $this->hasMany('Initiatives');
	}

	function targetYear()
	{

		return $this->hasMany('ScorecardYears');
	}
	function getHighest()
	{
		return $this->perspectives()->where('flag', '=', '1')->first();
	}

	function years()
	{
		return $this->hasMany('ScorecardYears')->orderBy('year');
	}

	public function opcr()
	{
		return $this->hasMany('Opcr');
	}
}
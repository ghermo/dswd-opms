<?php

class Indicators extends Eloquent 
{
	protected $table = 'indicators';
	public $fillable = ['goal_id', 'indicator', 'description', 'type', 'affect'];
	
	public function goal()
	{
		return $this->belongsTo('Objective');
	}

	public function accomplishments($month = null, $year = null)
	{
		if($this->goal->perspective->flag)
		{
			if($month !== null && $year !== null)
				return $this->hasMany('Accomplishments', 'indicator_id')->where('year', $year)->where('month', $month)->get();
			else
				return $this->hasMany('Accomplishments', 'indicator_id');
		}
		else
		{
			if($month !== null && $year !== null)
				return $this->goal->hasMany('CoreFunctions')->whereIndicator($this->indicator)->where('created_at', '>=', $year.'-01-01')->where('created_at', '<', ($year+1).'-01-01')->get();
			else
				return $this->goal->hasMany('CoreFunctions');
		}
	}
}
<?php

class StrategicPriorities extends Eloquent {

	protected $table = 'strategic_priorities';
	public $fillable = ['opcr_id', 'flag', 'indicator', 'accomplishment','qn','q1','t'];

	function opcr() 
	{
		return $this->belongsTo('Opcr');	
	}
	
	public function averageRating()
	{
		return number_format((($this->qn+$this->q1+$this->t)/3), 2);
	}
}
<?php

class Notifications extends Eloquent 
{
	protected $table = 'notifications';
	public $fillable = ['doer_id', 'notification', 'link'];

	public function doer()
	{
		return $this->belongsTo('User');
	}

	public function users()
	{
		return $this->belongsToMany('User', 'users_notifications', 'notification_id', 'user_id')->orderBy('created_at', 'desc');
	}

	public function timeCreated()
	{
		return $this->created_at->diffForHumans();
	}

	public static function notify($notification, $affected_users, $link = null)
	{ 
		$doer_id = Auth::user()->id;
		$notification = Notifications::create(compact('doer_id', 'notification', 'link'));

		foreach ($affected_users as $user)
		{
			if($user != Auth::user())
				$notification->users()->attach($user);
		}
	}
}

<?php

class ProjectMilestones extends Eloquent 
{
	public $fillable = ['initiative_id', 'start_date', 'end_date', 'milestones', 'budget_amount', 'budget_source', 'owner_id', 'status1', 'status2', 'status3', 'status4'];

	public function initiative()
	{
		return $this->belongsTo('Initiatives');
	}

	public function owner()
	{
		return $this->belongsTo('User');
	}

	public function currentStatus()
	{
		if($this->status4) return 5;
		if($this->status3) return 4;
		if($this->status2) return 3;
		if($this->status1) return 2;
		return 1;
	}
}
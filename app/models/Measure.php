<?php

class Measure extends Eloquent 
{
	public $fillable =['status','objective_id','description'];

	function objective() 
	{
		return $this->hasOne('Objective');
	}

	function owners()
	{
		return $this->userAccountableTarget;
	}

	function targets()
	{
		$targets = $this->hasMany('TargetAccomplishment');
		$filteredTargets = [];

		foreach($targets->get() as $target)
		{
			if( ! $target->year)
				$target->delete();
		}
		return $targets;
	}


	function targets2($years)
	{
		foreach($years as $year)
		{
			if( ! TargetAccomplishment::where('year_id', $year->id)->where('measure_id', $this->id)->exists())
			{
				TargetAccomplishment::create([
					'year_id'=>$year->id,
					'measure_id'=>$this->id]);
			}
		}

		return $this->targets();
	}

	function initiatives()
	{
		return $this->belongsToMany('Initiatives', 'measures_has_initiatives');
	}

	function listInitiatives()
	{
		$list = '';
		$first = true;
		foreach($this->initiatives as $initiative)
		{
			if(!$first)
				$list = $list.', ';
			
			$list = $list.$initiative;

			$first = false;
		}
		return $list;
	}
}
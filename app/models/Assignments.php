<?php

class Assignments extends Eloquent 
{
	protected $table = 'assignments';
	public $fillable = ['scoreboard_id','lead_measure','responsible_office','status'];
	public  $errors;

	
	function scoreboard()
	{
		return $this->belongsTo('Scoreboard', 'scoreboard_id');
	}
	
	
}
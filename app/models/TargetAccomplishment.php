<?php

class TargetAccomplishment extends Eloquent {

	protected $table = 'target_accomplishment';
	public $fillable = ['year_id', 'measure_id'];

	function year() 
	{
		return $this->belongsTo('ScorecardYears');	
	}

	function measure() 
	{
		return $this->belongsTo('Measure');	
	}
}
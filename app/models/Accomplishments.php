<?php

class Accomplishments extends Eloquent 
{
	protected $table = 'accomplishments';
	public $fillable = ['indicator_id', 'scoreboard_id', 'month', 'year', 'value'];

	public function indicator()
	{
		return $this->belongsTo('Indicators', 'indicator_id');
	}
	public function scoreboard()
	{
		return $this->belongsTo('Scoreboard', 'scoreboard_id');
	}
}
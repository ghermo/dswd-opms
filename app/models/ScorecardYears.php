<?php

class ScorecardYears extends Eloquent {

	protected $table = 'scorecard_years';
	public $fillable = ['scorecard_id', 'year'];

	function target_accomplishment()
	{
		return $this->hasMany('TargetAccomplishment');
	}

	function scorecard()
	{
		return $this->belongsTo('Scorecard');
	}
}
<?php

class OpcrRemark extends Eloquent 
{
	public $fillable = ['user_id', 'remark', 'type', 'opcr_id'];
	protected $table = 'opcr_remarks';

	function opcr()
	{
		return $this->belongsTo('Opcr');
	}
	function user()
	{
		return $this->belongsTo('User');
	}
	
}
<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	public $guarded = ['id'];
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */

	public static function admins()
	{
		return static::whereRole(0)->get();
	}

	public static function adminAndOpcr()
	{
		return static::where('role', '>=', 3)->orWhere('role', 0)->get();
	}

	public static function opcr()
	{
		return static::where('role', '>=', 3)->get();
	}

	public static function adminAndOfficers($office_id)
	{
		return static::whereRole(0)->orWhere('office_id', $office_id)->get();
	}

	public static function officers($office_id)
	{
		return static::where('office_id', $office_id)->get();
	}

	public function getAuthIdentifier()
	{
		return $this->getKey();
	}
	public function getUsername()
	{
		return $this->username;
	}
	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function getRememberToken()
	{
	    return $this->remember_token;
	}

	public function setRememberToken($value)
	{
	    $this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
	    return 'remember_token';
	}

	public function fullName()
	{
		return $this->firstName . ' ' . $this->lastName . ' (' . $this->username . ')';
	}

	public function office()
	{
		return $this->belongsTo('Office');
	}

	public function role()
	{
		switch ($this->role) {
			case 0:
				return 'OSM Admin';
			case 1:
				return 'Field Office User';

			case 2:
				return 'Central Office User';
			
			case 3:
				return 'Field Office Approving Authority';

			case 4:
				return 'Central Office Approving Authority';
				
			case 5:
				return 'OPCR admin';

			default:
				return 'Unknown';
		}
	}

	public function initiatives()
	{
		$initiatives = [];

		foreach ($this->projectMilestones()->get() as $projectMilestone) 
		{

			if($projectMilestone->initiative && ! in_array($projectMilestone->initiative, $initiatives))
			{
				array_push($initiatives, $projectMilestone->initiative);
			}
		}

		return $initiatives;
	}
	public function projectMilestones()
	{
		return $this->hasMany('ProjectMilestones', 'owner_id');
	}

	public function activities()
	{
		return $this->hasMany('Notifications', 'doer_id');
	}

	public function notifications()
	{
		return $this->belongsToMany('Notifications', 'users_notifications', 'user_id', 'notification_id')->where('doer_id', '!=', Auth::user()->id)->orderBy('created_at', 'desc');
	}
}	
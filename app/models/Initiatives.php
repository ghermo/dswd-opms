<?php

class Initiatives extends Eloquent 
{
	public $fillable = ['scorecard_id', 'projectName', 'projectDesc'];

	function scorecard()
	{
		return $this->belongsTo('Scorecard');
	}

	function perspective()
	{
		return $this->hasOne('Perspective');
	}

	function measures() 
	{
		return $this->belongsToMany('Measure', 'measures_has_initiatives', 'measure_id', 'initiative_id');
	}

	function measuresList()
	{
		$measures = '';
		$first = true;

		foreach($this->measures as $measure)
		{
			if(! $first) 
				$measures = $measures . '<br>';
			else 
				$first = false;

			$measures = $measures . $measure->description;
		}

		return $measures;
	}

	function offices()
	{
		return $this->belongsToMany('Office', 'offices_initiatives', 'initiative_id', 'office_id')->withPivot('flag');
	}

	function projectMilestones()
	{
		return $this->hasMany('ProjectMilestones', 'initiative_id');
	}

	function coChampions()
	{
		return $this->offices()->whereFlag(0);
	}

	function teams()
	{
		return $this->offices()->whereFlag(1);
	}

	function coChampionsList()
	{
		$coChampions = '';
		$first = true;

		foreach($this->offices()->where('flag', '=', 0)->get() as $coChampion)
		{
			if(! $first) 
				$coChampions = $coChampions . ', ';
			else 
				$first = false;

			$coChampions = $coChampions . $coChampion->office_abbreviation;
		}

		return $coChampions;
	}

	function teamsList()
	{
		$teams = '';
		$first = true;

		foreach($this->offices()->where('flag', '=', 1)->get() as $team)
		{
			if(! $first) 
				$teams = $teams . ', ';
			else 
				$first = false;

			$teams = $teams . $team->office_abbreviation;
		}

		return $teams;
	}

	public function users()
	{
		$users = [];

		foreach ($this->projectMilestones as $projectMilestone)
			$users[$projectMilestone->owner->id] = $projectMilestone->owner;

		return $users;
	}

	public function remarks()
	{
		return $this->hasMany('InitRemark', 'initiative_id');
	}

	public function notifiedUsers()
	{
		return User::admins()->merge($this->users());
	}
}
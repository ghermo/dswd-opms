<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('/', ['as' => 'dashboard.index', 'uses' => 'DashboardController@index']);
Route::get('/notifications', ['as' => 'dashboard.notifications', 'uses' => 'DashboardController@notifications']);

Route::get('login', 'AuthController@showLogin');
Route::post('login', 'AuthController@postLogin');
Route::get('logout', 'AuthController@getLogout');

Route::get('/scorecards/current', ['as' => 'scorecards.current', 'uses' => 'ScorecardsController@current']);
Route::post('accomplishments/', ['as' => 'accomplishments.index', 'uses' => 'AccomplishmentsController@index']);
Route::get('accomplishments/{year}', ['as' => 'accomplishments.year', 'uses' => 'AccomplishmentsController@year']);
Route::get('sample/{year}', ['as' => 'accomplishments.sample', 'uses' => 'AccomplishmentsController@sample']);
Route::get('accomplishments/{year}/{quarter}', ['as' => 'accomplishments.quarter', 'uses' => 'AccomplishmentsController@quarter']);

Route::get('/report/sg/{id}', ['as' => 'report.generate', 'uses' => 'ReportController@generateSG']);

Route::get('/visionbc', function()
{
	return View::make('masterSC.visionbc');
});

Route::get('/measure/{id}', ['as' => 'measure.show', 'uses' => 'MeasureController@show']);

Route::group(['before' => 'role:0'], function()
{
	Route::get('/scorecards', ['as' => 'scorecards.index', 'uses' => 'ScorecardsController@index']);
	Route::get('/scorecards/create', ['as' => 'scorecards.create', 'uses' => 'ScorecardsController@create']);
	Route::post('/scorecards', ['as' => 'scorecards.store', 'uses' => 'ScorecardsController@store']);
	Route::get('/scorecards/{id}/edit', ['as' => 'scorecards.edit', 'uses' => 'ScorecardsController@edit']);
	Route::post('/scorecards/{id}/edit', ['as' => 'scorecards.update', 'uses' => 'ScorecardsController@update']);
	Route::get('scorecard/setcurrent/{id}', ['as' => 'scorecards.setcurrent', 'uses' => 'ScorecardsController@setcurrent']);
	Route::get('scorecard/setarchive/{id}', ['as' => 'scorecards.setarchive', 'uses' => 'ScorecardsController@setarchive']);
	Route::get('scorecard/unsetarchive/{id}', ['as' => 'scorecards.unsetarchive', 'uses' => 'ScorecardsController@unsetarchive']);
	Route::get('scorecard/archives', ['as' => 'scorecards.archives', 'uses' => 'ScorecardsController@archives']);

	Route::get('/manageperspective', ['as' => 'perspectives.index', 'uses' => 'PerspectivesController@index']);
	Route::post('/manageperspective', ['as' => 'perspectives.store', 'uses' => 'PerspectivesController@store']);
	Route::get('perspective/sethighest/{id}', ['as' => 'perspectives.setHighest', 'uses' => 'PerspectivesController@setHighest']);
	Route::get('perspective/delete/{id}',['as' => 'perspectives.delete', 'uses' => 'PerspectivesController@delete']);
	Route::post('perspective/{id}/edit', ['as' => 'perspectives.update', 'uses' => 'PerspectivesController@update']);

	Route::get('/managestratobj',  ['as' => 'objectives.index', 'uses' => 'ObjectivesController@index'] );
	Route::post('/objectives', ['as' => 'objectives.store', 'uses' => 'ObjectivesController@store']);
	Route::post('/objectives/{o_id}/edit',['as' => 'objectives.update', 'uses' => 'ObjectivesController@update']);
	Route::get('/objectives/{o_id}/delete', ['as' => 'objectives.delete', 'uses' => 'ObjectivesController@delete']);
	Route::post('objectives/{id}/{pid}/move', ['as' => 'objectives.move', 'uses' => 'ObjectivesController@move']);

	Route::get('/measurelist', ['as' => 'measure.index', 'uses' => 'MeasureController@index']);
	Route::get('/measure/{id}/edit', ['as' => 'measure.edit', 'uses' => 'MeasureController@edit']);
	Route::get('/measure/setasfinal/{id}', ['as' => 'measure.setFinal', 'uses' => 'MeasureController@setFinal']);
	Route::get('/measure/unsetasfinal/{id}', ['as' => 'measure.setDraft', 'uses' => 'MeasureController@setDraft']);
	Route::post('/measure/save/{id}', ['as' => 'measure.update', 'uses' => 'MeasureController@update']);
	Route::get('/measure/delete/{id}', ['as' => 'measure.destroy', 'uses' => 'MeasureController@destroy']);

	Route::get('/initiative/create', ['as' => 'initiatives.create', 'uses' => 'InitiativesController@create']);
	Route::post('/initiative', ['as' => 'initiatives.store', 'uses' => 'InitiativesController@store']);
	Route::get('initiatives/setfinal/{id}', ['as' => 'initiatives.setFinal', 'uses' => 'InitiativesController@setFinal']);
	Route::get('initiatives/unsetfinal/{id}', ['as' => 'initiatives.setDraft', 'uses' => 'InitiativesController@setDraft']);
	Route::get('initiatives/delete/{id}', ['as' => 'initiatives.delete', 'uses' => 'InitiativesController@delete']);

	Route::get("/indicators/goal/", ['as' => 'indicators.index', 'uses' => 'IndicatorsController@index']);
	Route::post("/indicators/goal/store", ['as' => 'indicators.store', 'uses' => 'IndicatorsController@store']);
	Route::post("/indicators/goal/{id}/update", ['as' => 'indicators.update', 'uses' => 'IndicatorsController@update']);
	Route::get("/indicators/goal/{id}/delete", ['as' => 'indicators.destroy', 'uses' => 'IndicatorsController@destroy']);
	Route::get("/goals/{gid}/indicator/{id}/move/up", ['as' => 'indicators.moveUp', 'uses' => 'IndicatorsController@moveUp']);
	Route::get("/goals/{gid}/indicator/{id}/move/down", ['as' => 'indicators.moveDown', 'uses' => 'IndicatorsController@moveDown']);
	Route::post("/goals/{id}/indicators/aggregation/", ['as' => 'indicators.aggregation', 'uses' => 'IndicatorsController@aggregation']);

	Route::get("/indicators/obj/{id}", ['as' => 'indicators.index2', 'uses' => 'IndicatorsController@index2']);

	Route::get('/users', ['as' => 'users.index', 'uses' => 'UsersController@index']);
	Route::get('/createuser', ['as' => 'users.create', 'uses' => 'UsersController@create']);
	Route::post('/createuser', ['as' => 'users.store', 'uses' => 'UsersController@store']);
	Route::get('/edit/{id}', ['as' => 'users.edit', 'uses' => 'UsersController@edit']);
	Route::post('/edituser/{id}', ['as' => 'users.update', 'uses' => 'UsersController@update']);

	Route::resource('createoffice','OfficeController');
	Route::resource('offices','OfficeController');
	
	Route::get('/officesb/central/unsetasfinal/{id}', ['as' => 'sb.centralUnsetfinal', 'uses' => 'ScoreboardController@centralUnsetfinal']);
	
	Route::get('/officesb/field/unsetasfinal/{id}', ['as' => 'sb.fieldUnsetfinal', 'uses' => 'ScoreboardController@fieldUnsetfinal']);

	Route::get('/systemsettings', ['as' => 'systemsettings.index', 'uses' => 'SystemSettingsController@index']);
	Route::get('/systemsettings/managestrategymap', ['as' => 'systemsettings.stratmap', 'uses' => 'SystemSettingsController@stratmap']);
	Route::post('/systemsettings/upload', ['as' => 'systemsettings.upload', 'uses' => 'SystemSettingsController@upload']);
	Route::get('/systemsettings/change/{id}', ['as' => 'systemsettings.change', 'uses' => 'SystemSettingsController@change']);
	Route::get('/systemsettings/aggregate', ['as' => 'systemsettings.aggregate', 'uses' => 'SystemSettingsController@aggregate']);
});

Route::group(['before' => 'role:013'], function()
{
	Route::get('/officesb', ['as' => 'sb.index', 'uses' => 'ScoreboardController@index']);
	Route::get('/officesb/field/createsb/{month}/{year}', ['as' => 'sb.fieldCreate', 'uses' => 'ScoreboardController@fieldCreate']);
	Route::get('/officesb/field/sbedit/{id}', ['as' => 'sb.fieldEdit', 'uses' => 'ScoreboardController@fieldEdit']);
	Route::post('/officesb/field/updatesb/{id}',['as' => 'sb.fieldUpdate', 'uses' => 'ScoreboardController@fieldUpdate']);
	Route::post('/officesb/field/{id}/remark',['as' => 'sb.fieldRemark', 'uses' => 'ScoreboardController@fieldRemark']);
	Route::get('/officesb/field/showsb/{id}', ['as' => 'sb.fieldShow', 'uses' => 'ScoreboardController@fieldShow']);
	Route::get('/officesb/field/forapproval/{id}',['as' => 'sb.fieldSetfinal', 'uses' => 'ScoreboardController@fieldApproval'] );
	Route::get('/officesb/field/setasfinal/{id}',['as' => 'sb.fieldSetfinal', 'uses' => 'ScoreboardController@fieldSetfinal'] );
	Route::post('/officesb/field/createsb/store', ['as' => 'sb.fieldStore', 'uses' => 'ScoreboardController@fieldStore']);
	Route::get('/officesb/field/delete/{id}', ['as' => 'sb.fieldDelete', 'uses' => 'ScoreboardController@fieldDelete']);
});


Route::group(['before' => 'role:024'], function()
{
	Route::get('/officesb/central/', ['as' => 'sb.central', 'uses' => 'ScoreboardController@central']);
	Route::get('/officesb/central/createsb/{month}/{year}', ['as' => 'sb.centralCreate', 'uses' => 'ScoreboardController@centralCreate']);
	Route::get('/officesb/central/showsb/{id}', ['as' => 'sb.centralShow', 'uses' => 'ScoreboardController@centralShow']);
	Route::post('/officesb/central/createsb/store', ['as' => 'sb.centralStore', 'uses' => 'ScoreboardController@centralStore']);
	Route::get('/officesb/central/sbedit/{id}', ['as' => 'sb.centralEdit', 'uses' => 'ScoreboardController@centralEdit']);
	Route::post('/officesb/central/updatesb/{id}',['as' => 'sb.centralUpdate', 'uses' => 'ScoreboardController@centralUpdate']);
	Route::post('/officesb/central/{id}/remark',['as' => 'sb.centralRemark', 'uses' => 'ScoreboardController@centralRemark']);
	Route::get('/officesb/central/delete/{id}', ['as' => 'sb.centralDelete', 'uses' => 'ScoreboardController@centralDelete']);
	Route::get('/officesb/central/forapproval/{id}', ['as' => 'sb.centralSetfinal', 'uses' => 'ScoreboardController@centralApproval'] );
	Route::get('/officesb/central/setasfinal/{id}', ['as' => 'sb.centralSetfinal', 'uses' => 'ScoreboardController@centralSetfinal'] );
});

Route::group(['before' => 'role:012345'], function()
{
	Route::get('/userprofile/', ['as' => 'users.profile', 'uses' => 'UsersController@profile']);
	Route::post('/updateprofile/{id}', ['as' => 'users.updateProfile', 'uses' => 'UsersController@updateProfile']);
	
	Route::get('/stratInit', ['as' => 'initiatives.index', 'uses' => 'InitiativesController@index']);
	Route::get('/initiative/{id}', ['as' => 'initiatives.show', 'uses' => 'InitiativesController@show']);
	Route::get('/initiative/{id}/edit', ['as' => 'initiatives.edit', 'uses' => 'InitiativesController@edit']);
	Route::post('/initiative/{id}/update', ['as' => 'initiatives.update', 'uses' => 'InitiativesController@update']);
	Route::post("/initiatives/{id}/remark", ['as' => 'initiative.remark', 'uses' => 'InitiativesController@remark']);

	Route::get('/opcr', ['as' => 'opcr.index', 'uses' => 'OpcrController@index']);
	Route::get('/opcr/create', ['as' => 'opcr.create', 'uses' => 'OpcrController@create']);
	Route::post('/opcr/store', ['as' => 'opcr.store', 'uses' => 'OpcrController@store']);
	Route::get("/opcr/{id}", ['as' => 'opcr.show', 'uses' => 'OpcrController@show']);
	Route::get('/opcr/{id}/forapproval', ['as' => 'opcr.mark', 'uses' => 'OpcrController@forapproval']);
	Route::get('/opcr/{id}/mark/{type}', ['as' => 'opcr.mark', 'uses' => 'OpcrController@MarkAs']);
	Route::get('/opcr/delete/{id}', ['as' => 'opcr.destroy', 'uses' => 'OpcrController@destroy']);
	Route::get("/opcr/{id}/edit", ['as' => 'opcr.edit', 'uses' => 'OpcrController@edit']);
	Route::post("/opcr/update/{id}", ['as' => 'opcr.update', 'uses' => 'OpcrController@update']);
	Route::get("/opcr/iDelete/{indicator_id}/{id}", ['as' => 'opcr.iDelete', 'uses' => 'OpcrController@iDelete']);
	Route::get("/opcr/iDelete1/{indicator_id}/{id}", ['as' => 'opcr.iDelete1', 'uses' => 'OpcrController@iDelete1']);
	Route::get("/opcr/iDelete2/{indicator_id}/{id}", ['as' => 'opcr.iDelete2', 'uses' => 'OpcrController@iDelete2']);
	Route::get("/opcr/iDelete3/{indicator_id}/{id}", ['as' => 'opcr.iDelete3', 'uses' => 'OpcrController@iDelete3']);
	Route::post("/opcr/iStore/{id}", ['as' => 'opcr.iStrore', 'uses' => 'OpcrController@iStore']);
	Route::post("/opcr/{id}/remark", ['as' => 'opcr.remark', 'uses' => 'OpcrController@remark']);
});
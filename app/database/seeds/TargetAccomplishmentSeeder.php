<?php
class TargetAccomplishmentSeeder extends Seeder {

    public function run()
    {
        TargetAccomplishment::truncate();
        
 		TargetAccomplishment::create(array(
            'year_id'=>1,
            'measure_id'=>1,
        ));
       TargetAccomplishment::create(array(
            'year_id'=>2,
            'measure_id'=>2,
        ));
       TargetAccomplishment::create(array(
            'year_id'=>3,
            'measure_id'=>3,
        ));
        TargetAccomplishment::create(array(
            'year_id'=>4,
            'measure_id'=>4,
        ));
    }
}
<?php
class ChallengesTableSeeder extends Seeder {

    public function run()
    {
        Challenge::truncate();
        
        Challenge::create([
            'issue'        => 'Si Superman ay sobrang kulit huhuuh.',
            'action_taken'    => 'Umiyak',
            'action_requested' =>'Bigyan ako ng tissue na pamunas huhuhu',
            'scorecard_id' => 1,
        ]);
    }
}
<?php
class UserTableSeeder extends Seeder {

    public function run()
    {
        // !!! All existing users are deleted !!!
        User::truncate();

        User::create(array(
            'username'        => 'admin',
            'firstName'  => 'OSM',
            'lastName'  => 'Administrator',
            'password'  => Hash::make('admin'),
            'email'     => 'osm@dswd.com'
        ));
    }
}
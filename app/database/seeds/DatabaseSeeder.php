<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UserTableSeeder');
		$this->call('ObjectivesTableSeeder');
		//$this->call('InitiativesTableSeeder');	
		$this->call('TargetAccomplishmentSeeder');	 
		$this->call('ScorecardYearsSeeder');	
		//$this->call('ChallengesTableSeeder');	
		$this->call('OfficeTableSeeder');
		$this->call('MeasuresTableSeeder');	 
		$this->call('PerspectiveTableSeeder');	 
		$this->call('ScorecardTableSeeder');
		// $this->call('PicturesTableSeeder');	
	}
}
<?php
class ScorecardYearsSeeder extends Seeder {

    public function run()
    {
 		ScorecardYears::truncate();
        
 		ScorecardYears::create(array(
            'year'=>2012,
            'scorecard_id'=>1,
        ));
        ScorecardYears::create(array(
            'year'=>2013,
            'scorecard_id'=>1,
        ));
        ScorecardYears::create(array(
            'year'=>2014,
            'scorecard_id'=>1,
        ));
        ScorecardYears::create(array(
            'year'=>2015,
            'scorecard_id'=>1,
        ));
    }
}
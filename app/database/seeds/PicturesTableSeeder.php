<?php
class PicturesTableSeeder extends Seeder {

    public function run()
    {
        Image::truncate();

        Image::create(
        [
            'name'        => 'StratMap.png',
            'size'        => '117515',
            'type'        => 'image/png',
            'url'         => 'http://localhost:8000/vendor/jfu/server/php/files/StratMap.png',
            'title'       => 'DSWD Strategy Map',
            'description' => 'Department of Social and Welfare and Development Strategey Map'
        ]);
    }
}

<?php
class OfficeTableSeeder extends Seeder {

    public function run()
    {
        Office::truncate();

        Office::create(
            ['office_type'=>'OSM',
            'office_name'=>'Office of Strategy Management',
            'office_abbreviation'=>'OSM']);
    }
}
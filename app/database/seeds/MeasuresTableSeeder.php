<?php
class MeasuresTableSeeder extends Seeder {

    /*
            $table->string('status');
            $table->integer('objective_id');
            $table->string('objective');
            $table->string('description');
            $table->string('frequency');
            $table->string('unitOfMeasure');
            $table->string('baseLine');
            $table->string('userSetTarget');
            $table->string('userAccountableTarget');
            $table->string('userTrackTarget');
            $table->string('availability');
            $table->string('reason');
            $table->string('acquiredReason');
            $table->string('formula');
    */
    public function run()
    {
        // !!! All existing users are deleted !!!
        Measure::truncate();
        /*Measure::create([
            //'objective_id'=>0,
            'status'=>'Draft',
            'objective'=>'Build a culture of innovation and convergence',
            'description' => '# of central and field offices with good practices of recognized by central office for replication',
            'reason' => 'Targetting good practices for replication completes the KM cycle and ensures that these practices are not simply shared or shelved but are actually used to contribute to process and organizational excellence.',
            'frequency'=>'Semestral',
            'unitOfMeasure'=>'Absolute Count',
            'formula'=>'Number of good practices that were recognized during the Scorecard Summit',
            'availability'=>'Currently available',
            'userSetTarget'=>'All HOBS',
            'userAccountableTarget'=>'All HOBS',
            'userTrackTarget' =>'OSM and OBS respective Strategy Officers/Focal Persons',
            'acquiredReason'=>'Each FO that is able to significantly surpass their targets may have eligible good practices
                        that will be reviewsed and validated via the Scorecard Summit'*/
        Measure::create([
            'objective_id'=>1,
            'status'    => 'Draft',
            'description' => 'No. of self-sufficient Pantawid Pamilya families',   

        ]);
        Measure::create([
            'objective_id'=>2,
            'status'    => 'Draft',
            'description' => 'No. of self-sufficient Pantawid Pamilya families',   
        ]);
         Measure::create([
            'objective_id'=>3,
            'status'    => 'Draft',
            'description' => 'No. of provinces with majority of their municipalities and cities implementing social welfare and development programs and services',   
        ]);
         //defg
         Measure::create([
            'objective_id'=>4,
            'status'    => 'Draft',
            'description' => 'No. of provinces with majority of their municipalities and cities implementing social welfare and development programs and services',   
            'userSetTarget' =>'All HOBS'//'PSB, SB, CBB, DRRROO, Regional Operations, Division',

        ]);
        Measure::create([
            'objective_id'=>5,
            'status'    => 'Draft',
            'description' => 'No. of of 4Ps cases manged by other Sp service providers',   
            'userSetTarget' =>'All HOBS'//'4Ps PMO, 4Ps RPMO, Convergence Team',
        ]);
         Measure::create([
            'objective_id'=>6,
            'status'    => 'Draft',
            'description' => 'Percentage of NHTS-PR identified poor families covered by at least SWD programs by NGAs/LGUs',   
            'userSetTarget' =>'All HOBS'//'NHTO Advocacy Unit Regional NHTO',
        ]);
         Measure::create([
            'objective_id'=>7,
            'status'    => 'Draft',
            'description' => 'No. of provinces with majority of their municipalities and cities integrating social protection policies and programs in their local/comprehensive development plans',   
            'userSetTarget' =>'All HOBS'//'PDPB, Planning & Monitoring, Division, Regional Policy and Plans Division',
        ]);
        
          //h,i
            Measure::create([
            'objective_id'=>8,
            'status'    => 'Draft',
            'description' => '# of critical central office units delivering their physical and financial targets as planned',   
            'userSetTarget' =>'All HOBS'//'PDPB, FMS',
        ]);
          Measure::create([
            'objective_id'=>9,
            'status'    => 'Draft',
            'description' => '100% Strategtic Initiatives Funded',   
            'userSetTarget' =>'All HOBS'//'TAU and OBSUs',
        ]);
          //j,k,l
         Measure::create([
            'objective_id'=>10,
            'status'    => 'Draft',
            'description' => 'No. of personnel in strategic units demonstrating required competencies',   
            'userSetTarget' =>'All HOBS'//'HRDB and HR unit in the FO',
        ]);
         
        Measure::create([
            'objective_id'=>11,
            'status'    => 'Draft',
            'description' => 'No. of central and field offcies with good practices recognized by central office for replication',   
            'userSetTarget' =>'All HOBS'//'OSM, All heads of offices, bureaus, and services (HOBS)',
        ]);
          Measure::create([
            'objective_id'=>12,
            'status'    => 'Draft',
            'description' => 'Percent of fully functional systsems integrated with other systems and being used',   
            'userSetTarget' =>'All HOBS'//'ICTMS and concerned OBS',
        ]);
        
    }
}
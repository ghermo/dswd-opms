<?php
class ScorecardTableSeeder extends Seeder {

    public function run()
    {
 		Scorecard::truncate();
        
 		Scorecard::create(array(
            'name'   => "DSWD's Enterprise Scorecard",
            'description'  => 'Sample scorecard',
            'flag' => '1',
            'archive' => '0'
        ));
    }
}
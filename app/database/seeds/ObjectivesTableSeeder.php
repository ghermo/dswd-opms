<?php
class ObjectivesTableSeeder extends Seeder {

    public function run()
    {
        // !!! All existing users are deleted !!!
        Objective::truncate();

        Objective::create([
            'objective'        => 'Raise the status of 2.3M Pantawid Pamilya families to self-sufficient by 2016',
            'perspective_id'    => 1,
            'measure_id'        => 1,
            'ordinal' =>1,
        ]);

        Objective::create([
            'objective'        => 'Increase the number of NHTS-PR identified poor families covered by at least two (2) SWD programs from 3.9M to 5.2M by 2016',
            'perspective_id'    => 1,
            'measure_id'        => 2,
            'ordinal' =>2,
        ]);

        Objective::create([
            'objective'        => 'Increase the number of provinces with majority of their municipalities/cities with fully functional Local Social Welfare and Development
            Office (LSWDOs) to 40 by 2016',
            'perspective_id'    => 1,
            'measure_id'        => 3,
            'ordinal' =>3,
        ]);

        //d,e,f,g
       Objective::create([
            'objective'        => 'Capacitate LGUs to fully implement social welfare and development programs and services',
            'perspective_id'    => 2,
            'measure_id'        => 4,
            'ordinal' =>4,
        ]);

        Objective::create([
            'objective'        => 'Forge partnership to strengthen case management of poor households',
            'perspective_id'    => 2,
            'measure_id'        => 5,
            'ordinal' =>5,
        ]);
        Objective::create([
            'objective'        => 'Promote the use of NHTS-PR to other SP service providers for external convergence',
            'perspective_id'    => 2,
            'measure_id'        => 6,
            'ordinal' =>6,
        ]);

        Objective::create([
            'objective'        => 'Mainstream social protection programs and initiatives at LGU level',
            'perspective_id'    => 2,
            'measure_id'        => 7,
            'ordinal' =>7,
        ]);
        //h,i
        Objective::create([
            'objective'        => 'Ensure values-based and strategy-based allocation and utilization of resources',
            'perspective_id'    => 3,
            'measure_id'        => 8,
            'ordinal' =>8,
        ]);

        Objective::create([
            'objective'        => 'Expand Private Sector Partnerships to support SWD programs',
            'perspective_id'    => 3,
            'measure_id'        => 9,
            'ordinal' =>9,
        ]);
        //j,k,l
        Objective::create([
            'objective'        => 'Ensure critical competencies are applied especially in key offices',
            'perspective_id'    => 4,
            'measure_id'        => 10,
            'ordinal' =>10,
        ]);

        Objective::create([
            'objective'        => 'Build a culture of innovation and convergence',
            'perspective_id'    => 4,
            'measure_id'        => 11,
            'ordinal' =>11,
        ]);
        Objective::create([
            'objective'        => 'Support decision making with an integrated ICT system',
            'perspective_id'    => 4,
            'measure_id'        => 12,
            'ordinal' =>12,
        ]);
    }
}
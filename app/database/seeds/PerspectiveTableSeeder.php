 <?php
class PerspectiveTableSeeder extends Seeder {

    public function run()
    {
        // !!! All existing users are deleted !!!
        Perspective::truncate();
        
 		Perspective::create(array(
            'perspective'   => 'Stakeholder Empowerent',
            'scorecard_id'  => '1',
            'flag' => '1'
        ));
       Perspective::create(array(
            'perspective'   => 'Process Excellence',
            'scorecard_id'  => '1',
            'flag' => '0'
        ));
        Perspective::create(array(
            'perspective'   => 'Resource Stewardship',
            'scorecard_id'  => '1',
            'flag' => '0'
        ));
        Perspective::create(array(
            'perspective'   => 'Organizational Excellence',
            'scorecard_id'  => '1',
            'flag' => '0'
        ));
    }
}
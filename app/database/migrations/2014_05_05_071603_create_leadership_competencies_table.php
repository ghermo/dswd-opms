<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadershipCompetenciesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('leadership_competencies', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('opcr_id');
			$table->string('indicator');
			$table->string('accomplishment');
			$table->integer('qn');
			$table->integer('q1');
			$table->integer('t');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('leadership_competencies');
	}

}

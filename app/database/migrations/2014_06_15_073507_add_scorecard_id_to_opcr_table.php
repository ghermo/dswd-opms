<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddScorecardIdToOpcrTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('opcr', function(Blueprint $table)
		{
			$table->integer('scorecard_id')->after('id')->default(1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('opcr', function(Blueprint $table)
		{
			$table->dropColumn('scorecard_id');
		});
	}

}

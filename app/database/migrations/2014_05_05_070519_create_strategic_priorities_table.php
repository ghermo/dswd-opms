<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStrategicPrioritiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('strategic_priorities', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('opcr_id');
			$table->tinyInteger('flag');
			$table->string('indicator');
			$table->string('accomplishment');
			$table->integer('qn');
			$table->integer('q1');
			$table->integer('t');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('strategic_priorities');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePicturesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pictures', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name',255);
			$table->integer('size');
			$table->string('type',255);
			$table->string('url',255);
			$table->string('title',255);
			$table->text('description');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pictures');

		$thumbDir = public_path().'/vendor/jfu/server/php/files/thumbnail';
		$imgDir = public_path().'/vendor/jfu/server/php/files';
		$files = File::files($imgDir);
		
		File::delete($files);
		File::cleanDirectory($thumbDir);
	}

}

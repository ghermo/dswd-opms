<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccomplishmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('accomplishments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('indicator_id');
			$table->integer('scoreboard_id');
			$table->tinyInteger('month');
			$table->integer('year');
			$table->integer('value');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('accomplishments');
	}

}

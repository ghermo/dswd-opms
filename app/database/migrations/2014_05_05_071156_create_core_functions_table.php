<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreFunctionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('core_functions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('opcr_id');
			$table->integer('objective_id');
			$table->string('indicator');
			$table->string('accomplishment');
			$table->string('description');
			$table->integer('qn');
			$table->integer('q1');
			$table->integer('t');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('core_functions');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScorecardTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('scorecard', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 45);
            $table->string('description', 45);
            $table->boolean('flag');
            $table->boolean('archive');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('scorecard');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTargetAccomplishmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('target_accomplishment', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('year_id');
			$table->integer('measure_id');
			$table->string('target')->default(0);
			$table->string('accomplishment')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('target_accomplishment');
	}

}

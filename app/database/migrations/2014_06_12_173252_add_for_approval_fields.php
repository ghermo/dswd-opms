<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForApprovalFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('opcr', function(Blueprint $table)
		{
			$table->tinyInteger('for_approval')->default(0);
		});

		Schema::table('scoreboards', function(Blueprint $table)
		{
			$table->tinyInteger('for_approval')->default(0);
		});

		Schema::table('scoreboards_holder', function(Blueprint $table)
		{
			$table->tinyInteger('for_approval')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('opcr', function(Blueprint $table)
		{
			$table->dropColumn('for_approval');
		});

		Schema::table('scoreboards', function(Blueprint $table)
		{
			$table->dropColumn('for_approval');
		});

		Schema::table('scoreboards_holder', function(Blueprint $table)
		{
			$table->dropColumn('for_approval');
		});
	}

}

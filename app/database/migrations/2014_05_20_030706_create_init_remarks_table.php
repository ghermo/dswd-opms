<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitRemarksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('init_remarks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('initiative_id');
			$table->tinyInteger('type');
			$table->text('remark');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('init_remarks');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitiativesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('initiatives', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('projectName');
			$table->string('projectDesc');
			$table->tinyInteger('status')->default(0);
			$table->integer('scorecard_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('initiatives');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficesInitiativesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offices_initiatives', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('office_id');
			$table->integer('initiative_id');
			$table->tinyInteger('flag'); // 0- co champion | 1 - team
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('offices_initiatives');
	}

}

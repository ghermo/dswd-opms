<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoreboardsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('scoreboards', function(Blueprint $table)
		{
			$table->increments('id');
			$table->date('date');
			$table->integer('year');
			$table->integer('scoreboards_holder_id');
			$table->integer('author_id');
			$table->integer('flag')->default(0);
			$table->integer('type');
			$table->integer('objective_id');
			$table->integer('baseline');
			$table->integer('universe');
			$table->integer('month')->unsigned();
			$table->string('goal');
			$table->text('goal_status');
			$table->string('statusDate');
			$table->text('narrative');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('scoreboards');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectMilestonesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project_milestones', function(Blueprint $table)
		{
			//create fields
			$table->increments('id');
			$table->integer('initiative_id');
			$table->string('start_date');
			$table->string('end_date');
			$table->string('milestones');
			$table->double('budget_amount');
			$table->string('budget_source');
			$table->integer('owner_id');
			$table->string('status1')->nullable();
			$table->string('status2')->nullable();
			$table->string('status3')->nullable();
			$table->string('status4')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('project_milestones');
	}

}

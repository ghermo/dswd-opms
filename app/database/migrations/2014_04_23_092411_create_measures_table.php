<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeasuresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('measures', function(Blueprint $table)
		{
			$table->increments('id')->nullable();
			$table->integer('objective_id')->nullable();
			$table->string('status')->nullable();
			//$table->string('owner');
			$table->string('description')->nullable();
            $table->string('frequency')->nullable();
            $table->string('unitOfMeasure')->nullable();
            $table->string('baseLine')->nullable();
            $table->string('userSetTarget')->nullable();
            $table->string('userAccountableTarget')->nullable();
            $table->string('userTrackTarget')->nullable();
            $table->string('infoAvailability')->nullable();
            $table->tinyInteger('availability')->nullable();
            $table->string('reason', 1000)->nullable();
            $table->string('acquiredReason')->nullable();
            $table->string('formula')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('measures');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCsbRemarksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('csb_remarks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('scoreboard_id');
			$table->tinyInteger('type');
			$table->text('remark');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('csb_remarks');
	}

}

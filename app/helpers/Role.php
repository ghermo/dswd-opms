<?php

class Role 
{
	public static function access($roles)
	{
		if (Auth::check() && (Auth::user()->role == 5 || Auth::user()->office))
		{
			foreach(str_split($roles) as $role)
			{
				if(Auth::user()->role == $role)
					return true;
			}
		}

		return false;
	}

	public static function accessScorecard()
	{
		return static::access('012345');
	}

	public static function accessAdministration()
	{
		return static::access('0');
	}
}
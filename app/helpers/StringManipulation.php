<?php

class StringManipulation
{
	public static function abbreviate($value)
	{
		$abbreviation = '';

		if ( ctype_alpha($value[0]) )
			$abbreviation .= $value[0];

		for ($i=1; $i < strlen($value); $i++) { 
			if($value[$i-1] == ' ')
				$abbreviation .= $value[$i];
		}

		return $abbreviation;
	}
}
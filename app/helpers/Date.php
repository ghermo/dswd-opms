<?php

	class Date
	{
		public static $months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September','October', 'November', 'December']; 

		public static function monthsBeforeToday()
		{
			$months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September','October', 'November', 'December']; 

			for($i = (int)date("m"); $i < 12; $i++)
				unset($months[$i]);
			
			return $months;
		}
	}
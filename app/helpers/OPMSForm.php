<?php

class OPMSForm
{
	public static function currentScorecard($value)
	{
		switch ($value) {
			case "scorecards/current":
				return true;
		}
		return false;
	}
	public static function scorecard($value)
	{
		switch($value)
		{
			case "scorecards":
			case "managesc":
				return true;
		}
		return false;
	}

	public static function perspective( $value)
	{
		switch($value)
		{
			case "manageperspective":
				return true;
		}
		return false;
	}
	
	public static function goals( $value)
	{
		switch($value)
		{
			case "managestratobj":
				return true;
		}
		return false;
	}

	public static function initiatives( $value)
	{
		switch($value)
		{
			case "stratInit":
				return true;
		}
		return false;
	}

	public static function measures( $value)
	{
		switch($value)
		{
			case "measurelist":
				return true;
		}
		return false;
	}

	public static function indicators( $value)
	{
		switch($value)
		{
			case "indicators":
				return true;
		}
		return false;
	}


	public static function agencysc( $value)
	{
		return static::currentScorecard($value) || static::scorecard($value) || static::perspective($value) || 
			static::goals($value) || static::initiatives($value) || static::measures($value) || static::indicators($value);
	}

	public static function accomplishments( $value)
	{
		if(strpos($value, "accomplishments") === FALSE)
			return false;
		return true;
	}

	public static function fieldsb( $value)
	{
		switch($value)
		{
			case "officesb":
				return true;
		}
		return false;
	}

	public static function centralsb( $value)
	{
		switch($value)
		{
			case "officesb/central":
				return true;
		}
		return false;
	}

	public static function officesb( $value)
	{
		return static::fieldsb($value) || static::centralsb($value);
	}

	public static function opcr( $value)
	{
		switch($value)
		{
			case "opcr":
				return true;
		}
		return false;
	}
	public static function visionbc( $value)
	{
		switch($value)
		{
			case "visionbc":
				return true;
		}
		return false;
	}

	public static function scorecards( $value)
	{
		if(static::agencysc($value) || static::officesb($value) || static::opcr($value) || static::visionbc($value))
			return true;
		else return false;
	}

	public static function stratMap( $value)
	{
		switch($value)
		{
			case "systemsettings/managestrategymap":
				return true;
		}
		return false;
	}
}
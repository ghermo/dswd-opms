<?php

class ScoreboardController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	
	public function index()
	{
		$officesc = [];
		if(Role::access('0'))
		{
			foreach (Scorecard::current()->goals() as $goal)
				foreach ($goal->scoreboard as $scoreboard)
					if( ! in_array($scoreboard->holder, $officesc))
						array_push($officesc, $scoreboard->holder);
		}

		else 
		{
			foreach (Scorecard::current()->goals() as $goal)
				foreach ($goal->scoreboard as $scoreboard)
					if($scoreboard->author_id == Auth::user()->office->id)
						if( ! in_array($scoreboard->holder, $officesc))
							array_push($officesc, $scoreboard->holder);
		}

		$months = Date::$months;
		return View::make('masterSC.officesbfield', compact('officesc','months'));
	}
	public function central()
	{
		if(Role::access('0'))
			$officesc = Scoreboard::where('type','1')->orderBy('created_at', 'desc')->paginate(50);
		else
			$officesc = Scoreboard::where('type','1')->where('author_id', Auth::user()->office_id)->orderBy('created_at', 'desc')->paginate(50);
		return View::make('masterSC.centralofficesb', compact('officesc'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function centralCreate($month, $year)
	{
		//$objective=Objective::where('id', '=', $id)->first();
		$offices=Office::get();
		$scorecard = Scorecard::current();
		$users=User::get();
		$office=[];
		$user=[]; 
		$scoreboards = Scoreboard::where('flag','1')->where('scoreboards_holder_id','0')->where('year',$year)->orderBy('month','DESC')->first();

		if(!count($scoreboards)){
			$scoreboard = null;
		}
		else{
			$scoreboard = $scoreboards->goal;
		}


		$i=0;
		foreach($offices as $item)
		{
			$office[$item->office_abbreviation]=$item->office_name;
			$i++;
		}
		

		$i=0;
		foreach($users as $item)
		{
			$user[$item->username]=$item->username;
			$i++;
		}
		//dd(count($scoreboards));
		//dd($scoreboards->first()->goal);
		//dd($scoreboard);

		$i=0;
		$officeAndUser=array_merge($office,$user);

		if(!count($officeAndUser))
		{
			$officeAndUser=['No office or user created.'];
		}

		$years = $scorecard->years()->lists('year','year');

		unset($years[$scorecard->years()->first()->year]);
		$months = Date::$months;
			
		return View::make('centralOfficeSB.createsb',compact('officeAndUser','scorecard','year','month','scoreboard'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function centralStore()
	{
		$input= Input::all();
		// $getArray = Input::get('challenge','actionTaken','actionRequested');

		//dd(array_filter($input['challenge']));

		$author=Auth::user()->office;
		$months = Date::$months;
		$scorecard=Scorecard::current();
		
		$rules = array(
			'goal'         =>'required',
			'date'         =>'required'
		);

		// dd($input);

		// $niceNames;

		// for($i = 0; isset($input['leadMeasure'.$i]); $i++)
		// {
		// 	$rules['leadMeasure'.$i]       ='required';
		// 	$niceNames['leadMeasure'.$i]   ='lead measure';
		// 	$rules['officeOrUser'.$i]      ='required';
		// 	$niceNames['officeOrUser'.$i]  ='responsible person/division';
		// 	$rules['status'.$i]            ='required';
		// 	$niceNames['status'.$i]        ='status';
		// }

		// foreach ($input['leadMeasure'] as $key => $value) {
		//     $rules['leadMeasure'.$key]      ='required';
		// 	$niceNames['leadMeasure'.$key]  ='lead measure';
		// 	$rules['officeOrUser'.$key]     ='required';
		// 	$niceNames['officeOrUser'.$key] ='responsible person/division';
		// 	$rules['status'.$key]           ='required';
		// 	$niceNames['status'.$key]       ='status';     
		// }

		//dd($niceNames);

		//dd($rules);
	    $validator = Validator::make($input, $rules);
	    if(isset($niceNames))
	    	$validator->setAttributeNames($niceNames); 
	   
	    if ($validator->fails())
	    {
	        return Redirect::back()->withErrors($validator)->withInput();
	    }
	    
		
		$scoreboard=Scoreboard::create([
			'goal'         => $input['goal'],
			'goal_status'  => $input['goalStatus'],
			'date'         => $input['date'],
			'year'         => $input['year'],
			'month'        => $input['month'],
			'type'         => $input['type'],
			'author_id'    => $author->id,
			'statusDate'   => $months[$input['statusMonth']]." ".$input['statusYear'],
			'narrative'    => $input['narrative']
		]);

		foreach (array_filter(array_map('trim', $input['leadMeasure'])) as $key => $value) {

			$officeOrUser = array_map('trim',$input['officeOrUser']);
			$status = array_map('trim',$input['status']);

			if($officeOrUser[$key] != NULL && $value != NULL){
				Assignments::create([
					'lead_measure'		 =>	$value, 
					'responsible_office' =>	$officeOrUser[$key],
					'status'        	 =>	$status[$key],
					'scoreboard_id'		 =>	$scoreboard->id
				]);
			}
		}


		foreach (array_filter(array_map('trim', $input['challenge'])) as $key => $value) { //loop through array, exclude empty arrays and remove whitespaces
			
			$actionTaken = array_map('trim',$input['actionTaken']);
			$actionRequested = array_map('trim',$input['actionRequested']);

			if($actionTaken[$key] != NULL && $value != NULL){
				Challenge::create([
					'issue'				=>	$value, 
					'action_taken'		=>	$actionTaken[$key],
					'action_requested'	=>	$actionRequested[$key],
					'scoreboard_id'		=>	$scoreboard->id
				]);
			}
		}


		

		// dd($input['challenge']);

		return Redirect::to('/officesb/central/')->with('success', 'Created Central Office Scoreboard');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function centralShow($id)
	{
		$scoreboard=Scoreboard::find($id);
		$remarks=$scoreboard->remarks;
		//dd($scoreboard);

		if( ! Role::access('0') && Auth::user()->office_id != $scoreboard->author_id)
			return Redirect::route('dashboard.index');
		
		return View::make('centralOfficeSB.showsb',compact('scoreboard', 'remarks'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function centralEdit($id)
	{
		$scoreboard= Scoreboard::find($id);
		$remarks=$scoreboard->remarks;
		
		if( ! Role::access('0') && Auth::user()->office_id != $scoreboard->author_id)
			return Redirect::route('dashboard.index');

		// show the edit form and pass the nerd
		return View::make('centralOfficeSB.editsb',compact('scoreboard','remarks'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function centralUpdate($id)
	{
		$input=Input::all();
		
		
		$rules = array('goal'=>'required');
		// $niceNames;
		// for($i = 0; isset($input['leadMeasure'.$i]); $i++)
		// {
		// 	$rules['leadMeasure'.$i] ='required';
		// 	$niceNames['leadMeasure'.$i]='lead measure';
		// 	$rules['officeOrUser'.$i]='required';
		// 	$niceNames['officeOrUser'.$i]='responsible person/division';
		// 	$rules['status'.$i]='required';
		// 	$niceNames['status'.$i]='status';
		// }
		//dd($rules);
	    $validator = Validator::make($input, $rules);
	    if(isset($niceNames))
	    	$validator->setAttributeNames($niceNames); 
	   
	    if ($validator->fails())
	    {
	        
	        return Redirect::back()->withErrors($validator)->withInput();
	    }
		//dd($input);
		
		$scoreboard = Scoreboard::find($id);
		$scoreboard->goal  = Input::get('goal');
		$scoreboard->goal_status = Input::get('goalStatus');
		$scoreboard->narrative  = Input::get('narrative');
		$scoreboard->save();
		
		$i=0;
		
		Challenge::where('scoreboard_id', $id)->delete();

		// for($i = 0; isset($input['challenge'.$i]); $i++)
		// 	{
		// 		Challenge::create([
		// 			'issue'=>$input['challenge'.$i], 
		// 			'action_taken'=>$input['actionTaken'.$i], 
		// 			'action_requested'=>$input['actionRequested'.$i],
		// 			'scoreboard_id'=>$scoreboard->id
		// 			]);
		// 		//var_dump($input['challenge'.$i]);
		// 	}

		foreach (array_filter(array_map('trim', $input['challenge'])) as $key => $value) { //loop through array, exclude empty arrays and remove whitespaces
			
			$actionTaken = array_map('trim',$input['actionTaken']);
			$actionRequested = array_map('trim',$input['actionRequested']);

			if($actionTaken[$key] != NULL && $value != NULL){
				Challenge::create([
					'issue'				=>	$value, 
					'action_taken'		=>	$actionTaken[$key],
					'action_requested'	=>	$actionRequested[$key],
					'scoreboard_id'		=>	$scoreboard->id
				]);
			}
		}

		Assignments::where('scoreboard_id', $id)->delete();

		// for($i = 0; isset($input['leadMeasure'.$i]); $i++)
		// {
		// 	Assignments::create([
		// 	'scoreboard_id'=>$scoreboard->id,
		// 	'lead_measure'=>$input['leadMeasure'.$i],
		// 	'responsible_office'=>$input['officeOrUser'.$i],
		// 	'status'=>$input['status'.$i],
		// 	]);
		// }

		foreach (array_filter(array_map('trim', $input['leadMeasure'])) as $key => $value) {

			$officeOrUser = array_map('trim',$input['officeOrUser']);
			$status = array_map('trim',$input['status']);

			if($officeOrUser[$key] != NULL && $value != NULL){
				Assignments::create([
					'lead_measure'		 =>	$value, 
					'responsible_office' =>	$officeOrUser[$key],
					'status'        	 =>	$status[$key],
					'scoreboard_id'		 =>	$scoreboard->id
				]);
			}
		}

		// if($input['remark'])
		// 	CsbRemark::create(['user_id' => Auth::user()->id, 'scoreboard_id' => $id, 'type' => $input['type'], 'remark' => $input['remark']]);
		
		
		return Redirect::to('/officesb/central/')->with('success', 'Updated '.$scoreboard->name())
			->with(["notification" => "edited " . $scoreboard->name() . " (Central Office).", "affected_users" => User::officers($scoreboard->author_id), "link" => URL::route('sb.centralShow', ['id' => $scoreboard->id])]);
	}
	public function centralUnsetfinal($id)
	{
		$officesc = Scoreboard::where('id', '=', $id)->first();
		$officesc->flag = 0;
		$officesc->save();
		return Redirect::to('/officesb/central/')->with('success', 'Successfully marked as draft')->with(["notification" => "marked " . $officesc->name() . " (Central Office) as draft.", "affected_users" => User::officers($officesc->author_id), "link" => URL::route('sb.centralShow', ['id' => $officesc->id])]);
	}
	public function centralSetfinal($id)
	{
		$officesc = Scoreboard::where('id', '=', $id)->first();
		$officesc->flag = 1;
		$officesc->save();
		return Redirect::to('/officesb/central/')->with('success', 'Successfully marked final')->with(["notification" => "marked " . $officesc->name() . " (Central Office) as final.", "affected_users" => User::officers($officesc->author_id), "link" => URL::route('sb.centralShow', ['id' => $officesc->id])]);
	}

	public function centralDelete($id)
	{
		Challenge::where('scoreboard_id', $id)->delete();
		$officesc = Scoreboard::find($id);
		$officesc->challenge()->delete();
		$officesc->assignments()->delete();
		$officesc->delete();
		return Redirect::to('/officesb/central/')->with('success', 'Deleted '.$officesc->name());

	}
	//field
	public function fieldunsetfinal($id)
	{
		$officesc=ScoreboardsHolder::find($id);
		
		$officesc->flag = 0;
		$officesc->save();
		
		foreach ($officesc->scoreboard as $scoreboard) {
			$scoreboard->flag = 0;
			$scoreboard->save();
		}
		return Redirect::to('officesb')->with('success', 'Successfully marked as draft')->with(["notification" => "marked " . $officesc->name() . " (Field Office) as draft.", "affected_users" => User::officers($officesc->author_id), "link" => URL::route('sb.fieldShow', ['id' => $officesc->id])]);
	}

	public function fieldApproval($id)
	{
		$scoreboard = ScoreboardsHolder::find($id);
		$scoreboard->for_approval = 1;
		$scoreboard->save();

		Notifications::notify(
			'requested for the approval of ' . $scoreboard->name() . " (Field Office).", 
			$scoreboard->notifiedUsers(), URL::route('sb.fieldShow', ['id' => $scoreboard->id]));

		return Redirect::back()->with('success', 'Successfully requested for approval!');
	}

	public function centralApproval($id)
	{
		$scoreboard = Scoreboard::find($id);
		$scoreboard->for_approval = 1;
		$scoreboard->save();

		Notifications::notify(
			'requested for the approval of ' . $scoreboard->name() . " (Central Office).", 
			$scoreboard->notifiedUsers(), URL::route('sb.centralShow', ['id' => $scoreboard->id]));
		
		return Redirect::back()->with('success', 'Successfully requested for approval!');
	}
	
	public function fieldsetfinal($id)
	{
		$officesc=ScoreboardsHolder::find($id);
		
		$officesc->flag = 1;
		$officesc->save();
		
		foreach ($officesc->scoreboard as $scoreboard) {
			$scoreboard->flag = 1;
			$scoreboard->save();
		}
		return Redirect::to('officesb')->with('success', 'Successfully marked final')->with(["notification" => "marked " . $officesc->name() . " (Field Office) as final.", "affected_users" => User::officers($officesc->author_id), "link" => URL::route('sb.fieldShow', ['id' => $officesc->id])]);
	}
	
	public function fieldDelete($id)
	{
		$offi=ScoreboardsHolder::find($id);

		foreach($offi->scoreboard()->get() as $item)
		{
			
			foreach($item->accomplishments($item->month,$item->year) as $i)
			{
				$i->delete();
			}//delete all its accomplishments
			$item->assignments()->delete();//delete all its assignments
			$item->delete();//delete the scoreboard itself
		}
		//$offi->remark()->delete();//delete the remarks
		$offi->delete();//delete the holder
		
		return Redirect::to('officesb')->with('success', 'Successfully deleted');

	}

	public function fieldCreate($month, $year)
	{
		$scorecard = Scorecard::current();
		$goal = $scorecard->getHighest()->objectives();

		if(count($goal->get()) == 0)
			return Redirect::back()->with('success','Could not create scoreboard. Strategic goals list is not yet complete.');

		$goals=[];

		$i=0;
		foreach($goal->get() as $item)
		{
			
			$goals[$i]=$item;
			$i++;

		}
		
		for($a=0;isset($goals[$a]);$a++)
		{
			if(!isset($goals[$a]->indicatorsBaseline[0])||!isset($goals[$a]->indicatorsUniverse[0])||!isset($goals[$a]->indicatorsMovement[0])||!isset($goals[$a]->indicatorsTarget[0]))
				return Redirect::back()->with('success','Could not create scoreboard. Indicators list is not yet complete.');
		}
		$years = $scorecard->years()->lists('year','year');
		unset($years[$scorecard->years()->first()->year]);
		$months = Date::$months;
		$office = Auth::user()->office;

		$officesc = [];
		foreach (Scorecard::current()->goals() as $goal)
		{
			foreach ($goal->scoreboard as $scoreboard)
				if($scoreboard->author->id == Auth::user()->office->id)
					if( ! in_array($scoreboard->holder, $officesc))
						$officesc[$scoreboard->id] = $scoreboard->holder;
		}
		
		$baseline = [];
		$universe = [];
		$target = [];

		foreach($officesc as $sh)
			if($sh->scoreboard[0]->year == $year && $sh->flag)
			{
				foreach($sh->scoreboard as $scoreboard)
					foreach($scoreboard->accomplishments as $accomplishment)
						if($accomplishment->indicator->type == 0)
							$baseline[$scoreboard->objective->id] = $accomplishment->value;
						else if($accomplishment->indicator->type == 1)
							$universe[$scoreboard->objective->id] = $accomplishment->value;
						else if($accomplishment->indicator->type == 3)
							$target[$scoreboard->objective->id] = $accomplishment->value;
				break;
			}

		return View::make('fieldOfficeSB.createsb2', compact('officeAndUser','scorecard','year', 'goals','month','office', 'baseline', 'universe', 'target'));
	}
	public function fieldShow($id)
	{
		$sc= ScoreboardsHolder::find($id);
		$scoreboard=$sc->scoreboard;
		// $sbYear=Scoreboard::find($id);


		if( ! Role::access('0') && Auth::user()->office_id != $sc->author_id)
			return Redirect::route('dashboard.index');

		$scorecard=Scorecard::current();
		$months = Date::$months;
		$goal = Scorecard::current()->getHighest()->objectives()->first();
		$years = $scorecard->years()->lists('year','year');
		$remarks=$sc->remarks;

		//dd(count($scoreboard));
		//dd($scoreboard->first()->year);

		$officesc = [];
		foreach (Scorecard::current()->goals() as $goal)
			foreach ($goal->scoreboard as $sb)
				if($sb->author->id == Auth::user()->office->id)
					if( ! in_array($sb->holder, $officesc))
						$officesc[$sb->id] = $sb->holder;

		$baseline = [];
		$universe = [];
		$target = [];

		foreach($officesc as $sh)
			if($sh->scoreboard[0]->year == $scoreboard[0]->year && $sh->flag)
			{
				foreach($sh->scoreboard as $sb)
					foreach($sb->accomplishments as $accomplishment)
						if($accomplishment->indicator->type == 0)
							$baseline[$sb->objective->id] = $accomplishment->value;
						else if($accomplishment->indicator->type == 1)
							$universe[$sb->objective->id] = $accomplishment->value;
						else if($accomplishment->indicator->type == 3)
							$target[$sb->objective->id] = $accomplishment->value;
				break;
			}

		return View::make('fieldOfficeSB.show', compact('remarks','sc','scoreboard','years','scorecard','goals','months', 'baseline', 'universe', 'target'));
	}

	public function fieldEdit($id)
	{
		$sc= ScoreboardsHolder::find($id);
		$sb=$sc->scoreboard;
		if( ! Role::access('0') && Auth::user()->office_id != $sc->author_id)
			return Redirect::route('dashboard.index');

		$i=0;
		$scoreboard=[];

		foreach($sc->scoreboard()->get() as $item)
		{
			$scoreboard[$i]=$item;
			$i++;
		}
		//dd($sb->first()->year);

		$scorecard=Scorecard::current();
		$months = Date::$months;
		$years = $scorecard->years()->lists('year','year');
		$remarks=$sc->remarks;
		
		$officesc = [];
		foreach (Scorecard::current()->goals() as $goal)
			foreach ($goal->scoreboard as $sb)
				if($sb->author->id == Auth::user()->office->id)
					if( ! in_array($sb->holder, $officesc))
						$officesc[$sb->id] = $sb->holder;

		$baseline = [];
		$universe = [];
		$target = [];

		foreach($officesc as $sh)
			if($sh->scoreboard[0]->year == $scoreboard[0]->year && $sh->flag)
			{
				foreach($sh->scoreboard as $sb)
					foreach($sb->accomplishments as $accomplishment)
						if($accomplishment->indicator->type == 0)
							$baseline[$sb->objective->id] = $accomplishment->value;
						else if($accomplishment->indicator->type == 1)
							$universe[$sb->objective->id] = $accomplishment->value;
						else if($accomplishment->indicator->type == 3)
							$target[$sb->objective->id] = $accomplishment->value;
				break;
			}

		return View::make('fieldOfficeSB.editsb',compact('sb','scoreboard','sc','years','months','remarks', 'baseline', 'universe', 'target'));
	}

	public function fieldUpdate($id)
	{
		$input=Input::all();
		//dd($input);
		$holder=ScoreboardsHolder::find($id);
		$months = Date::$months;
		$scorecard=Scorecard::current();
		$rules=[];
	   	
		$niceNames;
		for($goal=0;isset($input['goal'.$goal]);$goal++)
		{
			for($i = 0; isset($input['leadMeasure'.$goal.$i]); $i++)
			{
				$rules['leadMeasure'.$goal.$i] ='required';
				$niceNames['leadMeasure'.$goal.$i]='lead measure';
				$rules['officeOrUser'.$goal.$i]='required';
				$niceNames['officeOrUser'.$goal.$i]='responsible person/division';
				$rules['status'.$goal.$i]='required';
				$niceNames['status'.$goal.$i]='status';
			}
			for($i=0;isset($input['accomplishment'.$goal.$i]);$i++)
			{
				//$indicator=Indicators::find($input['indicator_id'.$goal.$i]);
				
				if($input['type'.$goal.$i]=="2")
				{
					$rules['accomplishment'.$goal.$i]='numeric|positive_num';
					$niceNames['accomplishment'.$goal.$i]='movement';
				}
				else if($input['type'.$goal.$i]=="1")
				{
					
					$rules['accomplishment'.$goal.$i]='numeric|required|positive_num';
					$niceNames['accomplishment'.$goal.$i]='universe';
				}
				else
				{
					$rules['accomplishment'.$goal.$i]='numeric|required|positive_num';
					$niceNames['accomplishment'.$goal.$i]='baseline';
				}
			}
		
		}
		//dd($rules);
	    $validator = Validator::make($input, $rules);
	    if(isset($niceNames))
	    	$validator->setAttributeNames($niceNames); 
	   	
	    if ($validator->fails())
	    {
	        //dd("weee");
	        return Redirect::to('/officesb/field/sbedit/'.$id)->withErrors($validator)->withInput();
	    }
	    $i2=0;
	   //dd($input);
	    $holderctr = 0;
	   foreach($holder->scoreboard()->get() as $sb)
	   { 
	   		if($holderctr == 0) $firstSB = $sb;
	   		$holderctr++;

	   		$scoreboard = Scoreboard::find($sb->id);
	   		$scoreboard->narrative  = Input::get('narrative'.$i2);
	   		$scoreboard->baseline= Input::get('baseline'.$i2);
	   		$scoreboard->month=Input::get('month'.$i2);
	   		$scoreboard->save();

	   		//dd($scoreboard);
	   		//var_dump($input);
	   		for($i=0;isset($input['accomplishment'.$i2.$i]);$i++)
			{
				$a=Accomplishments::find($input['id'.$i2.$i]);
				//dd($a);
					$a->month=$input['month'.$i2];
					$a->value=$input['accomplishment'.$i2.$i];
					$a->save();
					

			}

	   		Assignments::where('scoreboard_id', $scoreboard->id)->delete();
	   		for($i = 0; isset($input['leadMeasure'.$i2.$i]); $i++)
	   		{
	   			Assignments::create([
	   			'scoreboard_id'=>$scoreboard->id,
	   			'lead_measure'=>$input['leadMeasure'.$i2.$i],
	   			'responsible_office'=>$input['officeOrUser'.$i2.$i],
	   			'status'=>$input['status'.$i2.$i],
	   			]);
	   		}
	   		$i2++;
	   	}

		Challenge::where('scoreboard_id', $firstSB->id)->delete();
		for($i = 0; isset($input['issuetd1'.$i]); $i++)
		{
			Challenge::create([
				'issue'=>$input['issuetd1'.$i], 
				'action_taken'=>$input['issuetd2'.$i], 
				'action_requested'=>$input['issuetd3'.$i],
				'scoreboard_id'=>$firstSB->id
				]);
		}

	   	
		if($input['remark'])
			FsbRemark::create(['user_id' => Auth::user()->id, 'scoreboards_holder_id' => $id, 'type' => $input['type'], 'remark' => $input['remark']]);
		
		return Redirect::to('/officesb/')->with('success', 'Successfully updated scoreboard')
			->with(["notification" => "edited " . $holder->name() . " (Field Office).", "affected_users" => User::officers($holder->author_id), "link" => URL::route('sb.fieldShow', ['id' => $holder->id])]);
	   
	}

	public function fieldStore()
	{

		$input= Input::all();

		$months = Date::$months;
		$scorecard=Scorecard::current();
		$rules=[];
	   	
		$niceNames;
		for($goal=0;isset($input['goal'.$goal]);$goal++)
		{
			for($i = 0; isset($input['leadMeasure'.$goal.$i]); $i++)
			{
				$rules['leadMeasure'.$goal.$i] ='required';
				$niceNames['leadMeasure'.$goal.$i]='lead measure';
				$rules['officeOrUser'.$goal.$i]='required';
				$niceNames['officeOrUser'.$goal.$i]='responsible person/division';
				$rules['status'.$goal.$i]='required';
				$niceNames['status'.$goal.$i]='status';
			}
			for($i=1;isset($input['accomplishment'.$goal.$i]);$i++)
			{
				//$indicator=Indicators::find($input['indicator_id'.$goal.$i]);
				
				if($input['type'.$goal.$i]==2)
				{
					$rules['accomplishment'.$goal.$i]='numeric|positive_num';
					$niceNames['accomplishment'.$goal.$i]='movement';
				}
				else if($input['type'.$goal.$i]==1)
				{
					
					$rules['accomplishment'.$goal.$i]='numeric|required|positive_num';
					$niceNames['accomplishment'.$goal.$i]='universe';
				}
				else
				{
					$rules['accomplishment'.$goal.$i]='numeric|required|positive_num';
					$niceNames['accomplishment'.$goal.$i]='baseline';
				}
			}
		
		}
		//dd($input);
	    $validator = Validator::make($input, $rules);
	    if(isset($niceNames))
	    	$validator->setAttributeNames($niceNames); 
	   
	    if ($validator->fails())
	    {
	        return Redirect::back()->withErrors($validator)->withInput();
	    }
	   

		$holder=ScoreboardsHolder::create(['author_id' => Auth::user()->office->id]);

		for($goal=0;isset($input['goal'.$goal]);$goal++)
		{

			$s=Scoreboard::create([
				'objective_id'=>$input['goal'.$goal],
				'year'=>$input['year'],
				'date'=>$input['date'],
				'flag'=>0,
				'statusDate'=>$months[$input['statusMonth'.$goal]]." ".$input['statusYear'.$goal],
				'type'=>$input['type'],
				'baseline'=>$input['baseline'.$goal],
				'universe'=>$input['universe'.$goal],
				'month'=>$input['month'.$goal],
				'scoreboards_holder_id'=>$holder->id,
				'author_id'=>Auth::user()->office->id,
				'narrative'=>$input['narrative'.$goal]]);

			if ($goal == 0) $sc1 = $s;
			
			for($i = 0; isset($input['leadMeasure'.$goal.$i]); $i++)
			{
				Assignments::create([
				'scoreboard_id'=>$s->id,
				'lead_measure'=>$input['leadMeasure'.$goal.$i],
				'responsible_office'=>$input['officeOrUser'.$goal.$i],
				'status'=>$input['status'.$goal.$i],
				]);
			}
		
			for($i=1;isset($input['accomplishment'.$goal.$i]);$i++)
			{
				Accomplishments::create([
					'indicator_id'=>$input['indicator_id'.$goal.$i],
					'scoreboard_id'=>$s->id,
					'month'=>$input['month'.$goal],
					'year'=>$input['year'],
					'value'=>$input['accomplishment'.$goal.$i]
					]);

			}
		}

		foreach (array_filter(array_map('trim', $input['issuetd1'])) as $key => $value) {
			
			$issuetd2 = array_map('trim',$input['issuetd2']);
			$issuetd3 = array_map('trim',$input['issuetd3']);

			if($issuetd2[$key] != NULL && $value != NULL){
				Challenge::create([
					'issue'				=>	$value, 
					'action_taken'		=>	$issuetd2[$key],
					'action_requested'	=>	$issuetd3[$key],
					'scoreboard_id'		=>	$sc1->id
				]);
			}
		}

		return Redirect::to('officesb')->with('success', 'Successfully created');
	}

	public function fieldRemark($id)
	{
		$input = Input::all();
		$rules['remark'] ='required';
		$validator=Validator::make($input, $rules);
	    if ($validator->fails()) 
	    	return Redirect::back()->withErrors($validator)->withInput();
	    
		FsbRemark::create(['user_id' => Auth::user()->id, 'scoreboards_holder_id' => $id, 'type' => $input['type'], 'remark' => $input['remark']]);

	    $holder = ScoreboardsHolder::find($id);
	    if($input['type'])
	    {
    		Notifications::notify(
    			'gave a suggestion on ' . $holder->name() . " (Field Office).", 
    			$holder->notifiedUsers(), URL::route('sb.fieldShow', ['id' => $holder->id]));
	    }
	    else
	    {
    		Notifications::notify(
    			'commented on ' . $holder->name() . " (Field Office).", 
    			$holder->notifiedUsers(), URL::route('sb.fieldShow', ['id' => $holder->id]));
	    }

		return Redirect::back();
	}

	public function centralRemark($id)
	{
		$input = Input::all();
		$rules['remark'] ='required';
		$validator=Validator::make($input, $rules);
	    if ($validator->fails()) 
	    	return Redirect::back()->withErrors($validator)->withInput();

		CsbRemark::create(['user_id' => Auth::user()->id, 'scoreboard_id' => $id, 'type' => $input['type'], 'remark' => $input['remark']]);

	    $scoreboard = Scoreboard::find($id);
	    if($input['type'])
	    {
    		Notifications::notify(
    			'gave a suggestion on ' . $scoreboard->name() . " (Central Office).", 
    			$scoreboard->notifiedUsers(), URL::route('sb.centralShow', ['id' => $scoreboard->id]));
	    }
	    else
	    {
    		Notifications::notify(
    			'commented on ' . $scoreboard->name() . " (Central Office).", 
    			$scoreboard->notifiedUsers(), URL::route('sb.centralShow', ['id' => $scoreboard->id]));
	    }

		return Redirect::back();
	}
}

<?php

class OpcrController extends \BaseController {

	public $restful = true;
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Role::access('05'))
			$opcrlist = Scorecard::current()->opcr()->orderBy('created_at', 'desc')->paginate(50);
		else
			$opcrlist = Scorecard::current()->opcr()->where('office_id', Auth::user()->office_id)->orderBy('created_at', 'desc')->paginate(50);
		return View::make('opcr.index',compact('opcrlist'));
	}

	public function MarkAs($id, $type)
	{
		$editOpcr = Opcr::find($id);
		$editOpcr->flag = $type;
		$editOpcr->save();

		if ($type == 0)
			$type = " draft.";
		else if($type == 1)
			$type = " ready.";
		else
			$type = " final.";

		return Redirect::back()->with('success', "Successfully marked as".$type)
			->with(["notification" => "marked " . $editOpcr->name . " (OPCR) as ".$type, "affected_users" => User::officers($editOpcr->office_id), "link" => URL::route('opcr.show', ['id' => $editOpcr->id])]);
	}

	public function forapproval($id)
	{
		$opcr = Opcr::find($id);
		$opcr->for_approval = 1;
		$opcr->save();

		Notifications::notify(
			'requested for the approval of ' . $opcr->name . ".", 
			$opcr->notifiedUsers(), URL::route('opcr.show', ['id' => $opcr->id]));

		return Redirect::back()->with('success', 'Successfully requested for approval.');
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$objectives = [];
		foreach(Scorecard::current()->perspectives as $perspective)
			if( ! $perspective->flag)
				foreach($perspective->objectives as $objective)
					$objectives[$objective->id] = $objective;

		$offices = Office::lists('office_name', 'id');

		return View::make('opcr.create', compact('objectives', 'offices'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function iDelete($indicator_id,$id)
	{		
		$des=StrategicPriorities::find($indicator_id);
		$des->delete();
		return Redirect::to("/opcr/{$id}/edit/")->with('success', 'Deleted a Strategic Priority');	
	}
	
	public function iDelete1($indicator_id,$id)
	{		
		$des=CoreFunctions::find($indicator_id);
		$des->delete();
		return Redirect::to("/opcr/{$id}/edit/")->with('success', 'Deleted a Core Function');	
	}
	public function iDelete2($indicator_id,$id)
	{		
		$des=OtherFunctions::find($indicator_id);
		$des->delete();
		return Redirect::to("/opcr/{$id}/edit/")->with('success', 'Deleted a Function');	
	}
	public function iDelete3($indicator_id,$id)
	{		
		$des=LeadershipCompetencies::find($indicator_id);
		$des->delete();
		return Redirect::to("/opcr/{$id}/edit/")->with('success', 'Deleted a Leadership and Comepetencies');	
	}
	public function iStore($id)
	{
		$input=Input::all();

		if(isset($input['bg']))
		{
			StrategicPriorities::create([
				'opcr_id' => $id,
				'flag'	=> 0, // if that input is equal to BG, 0 else 1. 
				'indicator' => $input['indicator'],
				'accomplishment' => $input['accomplishment'],
				'qn' => isset($input['qn']) ? $input['qn'] : '',
				'q1' => isset($input['q1']) ? $input['q1'] : '',
				't' => isset($input['t']) ? $input['t'] : ''
				]);
			
			return Redirect::to("/opcr/{$id}/edit/")->with('success', 'Successfully added a new success indicator under Strategic Priorities');
		}
		else if(isset($input['lm']))
		{
			StrategicPriorities::create([
				'opcr_id' => $id,
				'flag'	=> 1, // if that input is equal to BG, 0 else 1. 
				'indicator' => $input['indicator'],
				'accomplishment' => $input['accomplishment'],
				'qn' => isset($input['qn']) ? $input['qn'] : '',
				'q1' => isset($input['q1']) ? $input['q1'] : '',
				't' => isset($input['t']) ? $input['t'] : ''
				]);
			
			return Redirect::to("/opcr/{$id}/edit/")->with('success', 'Successfully added a new success indicator under Strategic Priorities');
		}
		else if(isset($input['core']))
		{
			
			CoreFunctions::create([
				'opcr_id' => $id,
				'objective_id'	=> $input['KRA'],
				'indicator' => $input['indicator'],
				'accomplishment' => $input['accomplishment'],
				'qn' => isset($input['qn']) ? $input['qn'] : '',
				'q1' => isset($input['q1']) ? $input['q1'] : '',
				't' => isset($input['t']) ? $input['t'] : ''
				]);
			
			return Redirect::to("/opcr/{$id}/edit/")->with('success', 'Successfully added a new success indicator under Strategic Priorities');
		}
		else if(isset($input['other']))
		{
			
			OtherFunctions::create([
				'opcr_id' => $id,
				'indicator' => $input['indicator'],
				'accomplishment' => $input['accomplishment'],
				'qn' => isset($input['qn']) ? $input['qn'] : '',
				'q1' => isset($input['q1']) ? $input['q1'] : '',
				't' => isset($input['t']) ? $input['t'] : ''
				]);
			
			return Redirect::to("/opcr/{$id}/edit/")->with('success', 'Successfully added a new success indicator under Other KRAs');
		}
		else if(isset($input['leader']))
		{
			LeadershipCompetencies::create([
					'opcr_id' => $id,
					'indicator' => $input['indicator'],
					'accomplishment' => $input['accomplishment'],
					'qn' => isset($input['qn']) ? $input['qn'] : '',
					'q1' => isset($input['q1']) ? $input['q1'] : '',
					't' => isset($input['t']) ? $input['t'] : ''
					]);
			
			return Redirect::to("/opcr/{$id}/edit/")->with('success', 'Successfully added a new success indicator under Other Functions/Directives From The Secretary And Cluster Head');
		}
		else if(isset($input['bgedit'])||isset($input['lmedit']))
		{
			$ed=StrategicPriorities::find($input['indicator_id']);
			$ed->indicator=$input['indicator'];
			
			$ed->accomplishment = $input['accomplishment'];
			$ed->qn = $input['qn'];
			$ed->q1 = $input['q1'];
			$ed->t = $input['t'];
			$ed->save();
			return Redirect::to("/opcr/{$id}/edit/")->with('success', 'Successfully edited a success indicator under Strategic Priorities');
		}
		else if(isset($input['coreedit']))
		{
			
			$ed=CoreFunctions::find($input['indicator_id']);
			$ed->indicator=$input['indicator'];
			$ed->accomplishment = $input['accomplishment'];
			$ed->qn = $input['qn'];
			$ed->q1 = $input['q1'];
			$ed->t = $input['t'];
			$ed->save();
			return Redirect::to("/opcr/{$id}/edit/")->with('success', 'Successfully edited a success indicator under Strategic Priorities');
		}
		else if(isset($input['otheredit']))
		{
			
			$ed=OtherFunctions::find($input['indicator_id']);
			$ed->indicator=$input['indicator'];
			$ed->accomplishment = $input['accomplishment'];
			$ed->qn = $input['qn'];
			$ed->q1 = $input['q1'];
			$ed->t = $input['t'];
			$ed->save();
			
			return Redirect::to("/opcr/{$id}/edit/")->with('success', 'Successfully edited a success indicator under Other KRAs');
		}
		else if(isset($input['leaderedit']))
		{
			$ed=LeadershipCompetencies::find($input['indicator_id']);
			$ed->indicator = $input['indicator'];
			$ed->accomplishment = $input['accomplishment'];
			$ed->qn = $input['qn'];
			$ed->q1 = $input['q1'];
			$ed->t = $input['t'];
			$ed->save();	
			
			return Redirect::to("/opcr/{$id}/edit/")->with('success', 'Successfully added a success indicator under Other Functions/Directives From The Secretary And Cluster Head');
		}

	}
	
	public function store()
	{
		$input = Input::all();
		$rules = array(
			'name'=>'required',
			'period'=>'required');

	    $validator = Validator::make($input, $rules);
	   
	    if ($validator->fails())
	    {
	        
	        return Redirect::to('/opcr/create')->withErrors($validator)->withInput();
	        //return View::make('masterSC.createsb',compact('officeAndUser','scorecard'))->withErrors($validator);
	    }

		$opcr = Opcr::create([
			'scorecard_id' => Scorecard::current()->id,
			'name' => $input['name'],
			'office_id' => $input['office_id'],
			'period' => $input['period'],
			'flag' => 0
			]);
		
		if(isset($input['indicators1_flag']))
			for($i = 0; $i < count($input['indicators1_flag']); $i++)
				StrategicPriorities::create([
					'opcr_id' => $opcr->id,
					'flag'	=> $input['indicators1_flag'][$i], // if that input is equal to BG, 0 else 1. 
					'indicator' => $input['indicators1_indicator'][$i],
					'accomplishment' => $input['indicators1_actualAccomplishment'][$i]
				]);

		if(isset($input['indicators2_objective']))
			for($i = 0; $i < count($input['indicators2_objective']); $i++)
				CoreFunctions::create([
					'opcr_id' => $opcr->id,
					'objective_id'	=> $input['indicators2_objective'][$i],
					'indicator' => $input['indicators2_indicator'][$i],
					'accomplishment' => $input['indicators2_actualAccomplishment'][$i],
					'description' => $input['indicators2_description'][$i]
				]);

		if(isset($input['indicators3_indicator']))
			for($i = 0; $i < count($input['indicators3_indicator']); $i++)
				LeadershipCompetencies::create([
					'opcr_id' => $opcr->id,
					'indicator' => $input['indicators3_indicator'][$i],
					'accomplishment' => $input['indicators3_actualAccomplishment'][$i],
				]);

		if(isset($input['indicators4_indicator']))
			for($i = 0; $i < count($input['indicators4_indicator']); $i++)
				OtherFunctions::create([
					'opcr_id' => $opcr->id,
					'indicator' => $input['indicators4_indicator'][$i],
					'accomplishment' => $input['indicators4_actualAccomplishment'][$i],
				]);

		return Redirect::route('opcr.index')->with('success','Successfully created an OPCR');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

		$objectives = [];
		foreach(Scorecard::current()->perspectives as $perspective)
			if( ! $perspective->flag)
				foreach($perspective->objectives as $objective)
					$objectives[$objective->id] = $objective->objective;

		//$opcr = Opcr::find($id);//need year and month for accomplishment
		$opcr = Opcr::find($id);//need year and month for accomplishment
		

		$remarks = $opcr->remarks;

		if( ! Role::access('05') && Auth::user()->office_id != $opcr->office_id)
			return Redirect::route('dashboard.index');

		return View::make('opcr.show', compact('opcr', 'objectives', 'remarks'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$objectives = [];
		foreach(Scorecard::current()->perspectives as $perspective)
			if( ! $perspective->flag)
				foreach($perspective->objectives as $objective)
					$objectives[$objective->id] = $objective;

		$offices = Office::lists('office_name', 'id');
		$opcr=Opcr::find($id);
		$remarks = $opcr->remarks;

		if( ! Role::access('05') && Auth::user()->office_id != $opcr->office_id)
			return Redirect::route('dashboard.index');
		
		return View::make('opcr.edit', compact('objectives', 'offices','opcr', 'remarks'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input= Input::all();

		//dd($input);
		//dd($author);
		$rules = array('name'=>'required','period'=>'required');

	    $validator = Validator::make($input, $rules);
	   
	    if ($validator->fails())
	    {
	        
	        return Redirect::to('/opcr/'.$id.'/edit')->withErrors($validator)->withInput();
	        //return View::make('masterSC.createsb',compact('officeAndUser','scorecard'))->withErrors($validator);
	    }
	    $opcr = Opcr::find($id);
		$opcr->name  = Input::get('name');
		$opcr->period  = Input::get('period');
		//$opcr->office_id  = Input::get('office_id');
		
		$opcr->save();
		
	    return Redirect::to('/opcr')->with('success', 'Successfully edited a OPCR');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$editOpcr = Opcr::find($id);
		//dd($editOpcr);
		$editOpcr->delete();
		return Redirect::to('/opcr')
			->with('success', 'Successfully deleted.');
	}

	public function remark($id)
	{	
		$input = Input::all();
		$rules['remark'] ='required';
		$validator=Validator::make($input, $rules);
	    if ($validator->fails()) 
	    	return Redirect::back()->withErrors($validator)->withInput();
		OpcrRemark::create(['user_id' => Auth::user()->id, 'opcr_id' => $id, 'type' => $input['type'], 'remark' => $input['remark']]);

	    $opcr = Opcr::find($id);
	    if($input['type'])
	    {
    		Notifications::notify(
    			'gave a suggestion on ' . $opcr->name . ".", 
    			$opcr->notifiedUsers(), URL::route('opcr.show', ['id' => $opcr->id]));
	    }
	    else
	    {
    		Notifications::notify(
    			'commented on ' . $opcr->name . ".", 
    			$opcr->notifiedUsers(), URL::route('opcr.show', ['id' => $opcr->id]));
	    }

		return Redirect::back()->with('success', 'Added a remark');
	}
}
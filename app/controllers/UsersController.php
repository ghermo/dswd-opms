<?php

class UsersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$UserPage = User::orderBy('username', 'asc')->paginate(50);
		return View::make('administration.users', compact('UserPage'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$osm = [];
		foreach(Office::where('office_type', 'OSM')->where('office_flag',0)->get() as $office)
			$osm[$office->id] = $office->office_name;

		$fo = [0 => 'Select Field Office'];
		foreach(Office::where('office_type', 'Field Office')->where('office_flag',0)->get() as $office)
			$fo[$office->id] = $office->office_name;


		$co = [0 => 'Select Central Office'];
		foreach(Office::where('office_type', 'Central Office')->where('office_flag',0)->get() as $office)
			$co[$office->id] = $office->office_name;
		
		$roles = ['OSM Admin', 'Field Office User', 'Central Office User', 'Field Office Approving Authority', 'Central Office Approving Authority', 'OPCR Admin'];

		return View::make('administration.createuser', compact('roles', 'osm', 'fo', 'co'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();

		if(User::where('username', '=', $input['username'])->first() != null)
			return Redirect::back()->withErrors(['username' => 'Entered username is already in use. Please use a different one.'])->withInput(Input::all());

		$rules = [
		'username'  => 'alpha_dash|required',
		'first_name' =>'alpha_spaces|required',
		'last_name'	=> 'alpha_spaces|required',
		'email'		=> 'email|required',
		'password'  => 'min:6|required|same:cpassword',
		'cpassword' => 'required|same:password',
		];

		$messages = [
		'same'    => 'The passwords must match.',
		];

		$validation = Validator::make($input, $rules, $messages);

		if($validation->fails())
			return Redirect::back()->withErrors($validation)->withInput(Input::all());
		
		$role = $input['role'] > 2 ? $input['role'] - 2 : $input['role'];

		User::create([
				'username' => $input['username'],
				'firstName' => $input['first_name'],
				'lastName' => $input['last_name'],
				'email' => $input['email'],
				'password' => Hash::make($input['password']),
				'role' => $input['role'],
				'office_id' => $role == 3? 0 : $input['office'][$role]
			]);

		if(isset($input['create1']))
			return Redirect::to('/users')->with('success', 'User registered successfully');
		else
			return Redirect::to('/createuser')->with('success', 'User registered successfully');
	}

	
	public function show($id)
	{
		
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$editUser = User::find($id);

		foreach(Office::where('office_type', 'OSM')->where('office_flag',0)->get() as $office)
			$osm[$office->id] = $office->office_name;

		$fo = [0 => 'Select Field Office'];
		foreach(Office::where('office_type', 'Field Office')->whereOfficeFlag(0)->get() as $office)
			$fo[$office->id] = $office->office_name;

		$co = [0 => 'Select Central Office'];
		foreach(Office::where('office_type', 'Central Office')->whereOfficeFlag(0)->get() as $office)
			$co[$office->id] = $office->office_name;
		
		$roles = ['OSM Admin', 'Field Office User', 'Central Office User', 'Field Office Approving Authority', 'Central Office Approving Authority', 'OPCR Admin'];

		return View::make('administration.edituser', compact('editUser', 'roles', 'osm', 'fo', 'co'));
	}

	public function profile()
	{
		$editUser = Auth::user();
		$offices=Office::find(Auth::user()->office_id);
		$roles = ['OSM Admin', 'Field Officer User', 'Central Officer User'];
		return View::make('userprofile', compact('editUser','roles','offices'));
	}
	public function updateProfile($id)
	{
		$editUser = User::find($id);
		$input = Input::all();
		if($input['email']==$editUser->email)
		{
			$rules = [
				
				'password'  => $input['password'] ? 'min:6|same:cpassword' : '',
				'cpassword' => $input['cpassword'] ? 'same:password' : '',
			];

			$messages = [
				'same'    => 'The passwords must match.',
			];
		}
		else
		{
			$rules = [
				'email' =>'required|email',
				'password'  => $input['password'] ? 'min:6|same:cpassword' : '',
				'cpassword' => $input['cpassword'] ? 'same:password' : '',
			];

			$messages = [
				'same'    => 'The passwords must match.',
			];	
		}
		$validation = Validator::make($input, $rules, $messages);

		if($validation->fails())
			return Redirect::back()->withErrors($validation)->withInput($input);

		$editUser->email = $input['email'];
		if($input['password']) $editUser->password = Hash::make($input['password']);
		$editUser->save();    
		return Redirect::to('/')->with('success', 'Successfully updated profile');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$editUser = User::find($id);
		$input = Input::all();

		$rules = [
			'first_name' =>'alpha_spaces|required',
			'last_name'	=> 'alpha_spaces|required',
			'email'		=> 'email|required',
			'password'  => $input['password'] ? 'min:6|same:cpassword' : '',
			'cpassword' => $input['cpassword'] ? 'same:password' : '',
		];

		$messages = [
			'same'    => 'The passwords must match.',
		];

		$validation = Validator::make($input, $rules, $messages);

		if($validation->fails())
			return Redirect::back()->withErrors($validation)->withInput($input);

		$role = $input['role'] > 2 ? $input['role'] - 2 : $input['role'];
		
		$editUser->firstName = $input['first_name'];
		$editUser->lastName = $input['last_name'];
		$editUser->email = $input['email'];
		$editUser->role = $input['role'];
		if($input['password']) $editUser->password = Hash::make($input['password']);
		$editUser->office_id = $role == 3? 0 : $input['office'][$role];
		$editUser->disabled=input::get('user_disabled');
		$editUser->save();    
		return Redirect::to('/users')->with('success', 'Successfully updated user');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}

<?php

class ReportController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function generateSG($id)
	{

		$reportFile = storage_path() . '/reports/sgreport.jasper';
		$subreportUrl = storage_path() . '/reports/';

		if ($id == 1 || $id == 2 || $id == 3) {
			JasperPHP::process(
		        storage_path() . '/reports/sgreport.jasper', //Input file 
		        storage_path() . '/reports/Strategic_Goal_#' . $id . '_Report', //Output file without extension
		        array("pdf"), //Output format
		        array("SG" => $id, "SUBREPORT_DIR" => $subreportUrl), //Parameters array
		        Config::get('database.connections.mysql') //DB connection array
		    )->execute();

			$file = File::get( storage_path() . '/reports/Strategic_Goal_#' . $id . '_Report.pdf' );

		    return Response::make($file, 200, array('Content-type' => 'application/pdf'));	

		} else {
		    return Response::view('errors.404');
		}
		
		
	}
	
}


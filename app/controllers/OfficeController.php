<?php

class OfficeController extends \BaseController {

	protected $office;
	
	public function __construct(Office $office)
	{
		
		$this->office=$office;
	}
	public function index()
	{
		//$offices=$this->office->all();
		$offices=$this->office->orderBy('office_abbreviation','asc')->paginate(50);//->get();
		return View::make('administration.offices', compact('offices'));
	}
	
	public function create()
	{
		//$offices=$this->office->orderBy('office_abbreviation','asc')->get();
		return View::make('administration.createoffice');//, compact('offices'));
	}

	public function edit($id)
	{
		// get the nerd
		$office = $this->office->find($id);

		// show the edit form and pass the nerd
		return View::make('administration.editoffice',compact('office'));
	}
	public function update($id)
	{
		// validate
		// read more on validation at http://laravel.com/docs/validation
		$input=Input::all();
		//dd($input);
		if (!$this->office->fill($input)->isValidEdit()) 
		{
			//dd($this->office->errors);
				return Redirect::back()->withInput()->withErrors($this->office->errors);
			//return "Office: ".Input::get('office_name').$this->office->errors;

		} 
		else 
		{
			// store
			$office = $this->office->find($id);
			$office->office_name  = Input::get('office_name');
			$office->office_type  = Input::get('office_type');
			if(isset($input['office_flag']))
				$office->office_flag  = Input::get('office_flag');
			else
				$office->office_flag  = 0;
			$office->save();
			return Redirect::to('/offices')->with('success', 'Successfully updated office');
			//return "Gago ako";
		}
	}
	public function store()
	{
		
		$input=Input::all();
		

		if(isset($input['create1']))
		{

			if(!$this->office->fill($input)->isValid()) 
			{
				return Redirect::back()->withInput()->withErrors($this->office->errors);
			}
			else
			{
				$this->office->save();
		
				//return Redirect::route('\officesc');
				//$offices=$this->office->all();
				return Redirect::to('/offices')->with('success', 'Successfully created an office');
				//return "hahhahah";
			}
		}
		else if(isset($input['create2']))
		{

			if(!$this->office->fill($input)->isValid()) 
			{
				return Redirect::back()->withInput()->withErrors($this->office->errors);
			}
			else
			{
				$this->office->save();
		
				//return Redirect::route('\officesc');
				//$offices=$this->office->all();
				return Redirect::to('/offices/create')->with('success', 'Successfully created an office');
				//return "hahhahah";
			}
		}

	}
	
	

}

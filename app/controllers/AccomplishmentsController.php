<?php

class AccomplishmentsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$input = Input::all();

		if($input['quarter'] == 0)
			return Redirect::route('accomplishments.year', ['year' => $input['year']]);
		else
			return Redirect::route('accomplishments.quarter', ['year' => $input['year'], 'quarter' => $input['quarter']]);
	}
	public function year($year)
	{
		$goals = Scorecard::current()->getHighest()->objectives;
		$months = Date::$months;
		$years = [];

		foreach(Scorecard::current()->years()->orderBy('year')->get() as $accomplishment)
			$years[$accomplishment->year] = $accomplishment->year;

		return View::make('accomplishments', compact('goals','months', 'year', 'years'));
	}
	public function sample($year)
	{
		$goals = Scorecard::current()->getHighest()->objectives;
		$months = Date::$months;
		$years = [];

		foreach(Scorecard::current()->years()->orderBy('year')->get() as $accomplishment)
			$years[$accomplishment->year] = $accomplishment->year;

		return View::make('sample', compact('goals','months', 'year', 'years'));
	}

	public function quarter($year, $quarter)
	{
		$goals = Scorecard::current()->getHighest()->objectives;
		$months = Date::$months;
		$years = [];

		foreach(Scorecard::current()->years()->orderBy('year')->get() as $accomplishment)
			$years[$accomplishment->year] = $accomplishment->year;

		return View::make('accomplishments', compact('goals','months', 'year', 'years', 'quarter'));
	}
}

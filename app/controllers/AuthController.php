<?php

class AuthController extends BaseController {

    public function showLogin()
    {
        // Check if we already logged in
        if (Auth::check())
        {
            // Redirect to homepage
            return Redirect::to('')->with('success', 'You are already logged in');
        }

        // Show the login page
        return View::make('login');
    }
    public function postLogin()
    {
        // Get all the inputs
        // id is used for login, username is used for validation to return correct error-strings
        $userdata = array(
            'username' => Input::get('username'),
            'password' => Input::get('password')
        );

        // Declare the rules for the form validation.
        $rules = array(
            'username'  => 'Required',
            'password'  => 'Required'
        );


        // Validate the inputs.
        $validator = Validator::make($userdata, $rules);

        // Check if the form validates with success.
        if ($validator->passes())
        {
            // remove username, because it was just used for validation
            // unset($userdata['username']);
            // removing username will cause auth::attempt to accept anything as long as password is correct.

            
            if (Auth::attempt($userdata))
            {
                if(Auth::user()->disabled)
                {
                    Auth::logout();
                    return Redirect::to('login')->withErrors(array('disable' => 'Sorry your account has been disabled by the administrator.'))->withInput(Input::except('password'));
                }
                return Redirect::to('/');
            }
            else
            {
                // Redirect to the login page.
                return Redirect::to('login')->withErrors(array('password' => 'Invalid username/password'))->withInput(Input::except('password'));
            }
        }

        // Something went wrong.
        return Redirect::to('login')->withErrors($validator)->withInput(Input::except('password'));
    }
    public function getLogout()
    {
        Auth::logout();

        return Redirect::to('/');
    }
}
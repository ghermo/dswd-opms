<?php

class ObjectivesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$perspectives = Scorecard::current()->perspectives()->orderBy('flag', 'desc')->get();
		return View::make('masterSC.managestratobj', compact('perspectives'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();

		$rules = [
		'objective' => 'alpha_spaces|required',
		'measure'  => 'alpha_spaces|required'
		];

		$validation = Validator::make($input, $rules);
		if($validation->fails())
			return Redirect::back()->withErrors($validation)->withInput(Input::all());

		$measure = Measure::create(['description' => $input['measure']]);

		$objective = Objective::create(
			[
			'objective' => $input['objective'],
			'perspective_id' => $input['perspective'],
			'measure_id' => $measure->id
			]);

		$objective->ordinal = $objective->perspective()->first()->objectives()->count();
		$objective->save();

		$measure->objective_id = $objective->id;
		$measure->save();

		foreach(Scorecard::current()->years as $year)
		{
			TargetAccomplishment::create([
				'year_id'=>$year->id,
				'measure_id'=>$measure->id]);
		}

		return Redirect::to('/managestratobj')->with('success','Successfully created an objective')->withInput(Input::only(['perspective']));
	}

	public function move($id,$pid)
	{

		$objective = Objective::find($id);
		$input = Input::all();

		if(isset($input['move-up']))
		{
			$objective->ordinal--;
			$objective->save();
			$objective2 = Objective::where('perspective_id', '=', $pid)->where('ordinal','=',$objective->ordinal)->where('id','!=',$objective->id)->first();
			if(count($objective2))
			{
				
				

				
				$objective2->ordinal++;
				//echo "up";
				//dd($objective);
				//dd($objective2);
				//$objective->save();
				$objective2->save();
				//$objective=Objective::all();
				//dd($objective);
			}
		}
		elseif(!isset($input['move-up']))
		{
			
			$objective->ordinal++;
			$objective->save();
			$objective2 = Objective::where('perspective_id', '=', $pid)->where('ordinal','=',$objective->ordinal)->where('id','!=',$objective->id)->first();
			if(count($objective2))
			{
				$objective2->ordinal--;
				//echo "up";
				//dd($objective);
				//dd($objective2);
				//$objective->save();
				$objective2->save();
				
			}
		}
		
		return Redirect::to('managestratobj')->with('success','Successfully moved objective');
	
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();

		$objective = Objective::find($id);
	
		if(strlen($input['objective'])&&strlen( $input['measure']))
		{
			$objective->perspective_id = $input['perspective'];
			$objective->objective = $input['objective'];
			$objective->save();

			$measure = $objective->measure()->first();
			$measure->description = $input['measure'];
			$measure->save();

			return Redirect::to('managestratobj')->with('success','Successfully edited an objective');
		}
		return Redirect::to('managestratobj')->with('success',"Edit invalid. When editing an objective, please don't any field blank.");
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete($id)
	{
		Objective::find($id)->delete();

		return Redirect::to('managestratobj')->with('success','Successfully deleted an objective');
	}


}

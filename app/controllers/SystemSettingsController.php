<?php

class SystemSettingsController extends \BaseController {

	public function index()
	{
		
		return View::make('systemSettings.index');
	}
	public function stratmap()
	{
		
		return View::make('systemSettings.managestrategymap');
	}
	public function upload()
	{

		$input = Input::all();
		 // Get the image input
		   $file = Input::file('image');
		
		$rules = [
		    'image'     => 'required|image',
		];

		$messages = ['image'=>'You can only upload image files.'];

		$validate = Validator::make($input, $rules, $messages);
		if ($validate->passes()) 
		{

		   // Get the image input
		   $file = Input::file('image');

		   $destinationPath    = 'public/img'; // The destination were you store the image.
		   $filename           = $file->getClientOriginalName(); // Original file name that the end user used for it.
		   $mime_type          = $file->getMimeType(); // Gets this example image/png
		   $extension          = $file->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
		  
		   $upload_success     = $file->move($destinationPath, $filename); // Now we move the file to its new home.
		   Pictures::create(['image_url'=> $filename]);
		   // This is were you would store the image path in a table

		   // You don't need the Redirect to make the image upload work it's just here for example only
		   return Redirect::back()->with('success','Image uploaded.');
		} 
		else 
		{
		   return Redirect::back()->withErrors($validate)->withInput();
		}
	}
	public function change($id)
	{

		$images=Pictures::all();
		foreach($images as $img)
		{
			if($img->use==1)
			{
				$img->use=0;
				$img->save();
			}
		}
		$image=Pictures::find($id);
		$image->use=1;
		$image->save();
		
		return Redirect::to('/')->with('success', 'Strategic map changed')->with(["notification" => "changed the Strategic Map.", "affected_users" => User::all(), "link" => '/']);
		
	}
	public function aggregate()
	{
		
		
		$scorecard=Scorecard::current();
		$year = $scorecard->years()->lists('year','year');
		$years=[];
		$a=0;
		foreach($year as $y)
		{
			$years[$a]=$y;
			
			$a++;
		}
        foreach($scorecard->perspectives as $perspective)
        {
	        $objectives = Objective::where('perspective_id', '=', $perspective->id)->orderBy('ordinal')->get();
	        foreach($objectives as $objective)
	        {
		        if(count($objective->measure))
		        {
			        $id=0;
			        $measure = $objective->measure;

			        foreach($measure->targets2($scorecard->years)->get() as $target)
			        {
			        	$aggregated = 0; 
			        	$aggregation = 0;
				        foreach($objective->indicatorsMovement as $indicator)
				        {
				        	$totalValue = 0; 

						    if($indicator->affect)
						    {   
					        	if($perspective->flag)
					        	{
							        for($i = 0; $i < 12; $i++)
							        {
							            foreach($indicator->accomplishments($i, $years[$id]) as $accomplishment)
							            {
							                if($accomplishment->scoreboard()->get()[0]->holder()->get()[0]->flag==1)
							                    $totalValue += $accomplishment->value;
							        	}
							        }
						        }
						        else
						        {
						        	foreach ($indicator->accomplishments(1, $years[$id]) as $accomplishment) 
						        	{
						        		if($accomplishment->opcr->flag == 2)
						        			$totalValue += $accomplishment->accomplishment;
						        	}
						        }
						        
					            $aggregated += $totalValue * $indicator->affect; 
					            $aggregation++;
						    }
				        }

                        if($objective->aggregation_type == 1)
                        {
                            if($aggregation==0)
                            	$aggregation=1;
                            $target->accomplishment=$aggregated /$aggregation;
                        }
                        else
                            $target->accomplishment=$aggregated;

                        $target->save(); 
                        
				         

				        
				        $id++;
				    }
		    	}
	    	}
        }
		return Redirect::to('/scorecards/current')->with('success', 'Aggregated current scorecard')->with(["notification" => "aggregated current scorecard.", "affected_users" => User::all(), "link" => '/']);
	}
}

<?php

class PerspectivesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//return Perspective::all() . Scorecard::current();
		$perspective = Scorecard::current()->perspectives()->orderBy('flag', 'desc')->get();
		return View::make('masterSC.manageperspective', compact('perspective'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		
		$rules = ['name' => 'required'];
		$validation = Validator::make(Input::all(), $rules);
		$validation->setAttributeNames(['name' => 'perspective']);
		if($validation->fails())
			return Redirect::back()->withErrors($validation);

		$flag = Scorecard::current()->getHighest() == null;

		Perspective::create([
			'perspective' => $input['name'],
			'scorecard_id' => Scorecard::current()->id,
			'flag' => $flag
			]);

		return Redirect::to('/manageperspective')->with('success','Added perspective');
	}

	public function setHighest($id)
	{

		$highest = Scorecard::current()->getHighest();
		
		if($highest != null)
		{
			$highest->flag = 0;
			$highest->save();
		}

		$perspective = Perspective::where('id', '=', $id)->first();
		$perspective->flag = 1;
		$perspective->save();
		return Redirect::to('/manageperspective')->with('success','Set selected perspective as highest');
	
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = ['perspective' => 'required'];
		$validation = Validator::make(Input::all(), $rules);

		if($validation->fails())
			return Redirect::back();
		
		$perspective = Perspective::where('id', '=', $id)->first();
		$perspective->perspective = Input::get('perspective');
		$perspective->save();
		
		return Redirect::to('/manageperspective')->with('success','Modified selected perspective');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete($id)
	{
		Perspective::where('id', '=', $id)->first()->delete();

		$scorecard = Scorecard::current();
		if($scorecard->getHighest() == null && count($scorecard->perspectives))
			$this->setHighest($scorecard->perspectives[0]->id);
		return Redirect::to('/manageperspective')->with('success','Deleted selected perspective');
	}


}

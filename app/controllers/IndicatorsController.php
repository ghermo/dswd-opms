<?php

class IndicatorsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$goals = Scorecard::current()->getHighest()->objectives;
		$goalsList = Scorecard::current()->getHighest()->objectives->lists('objective', 'id');
		return View::make("masterSC.indicators.index", compact('indicators', 'goalsList', 'goals'));
	}

	public function index2($id)
	{
		$objectives = [];
		$objectivesList = [];

		foreach (Scorecard::current()->perspectives as $perspective)
			if( ! $perspective->flag)
			{
				foreach ($perspective->objectives as $objective)
					array_push($objectives, $objective);

				foreach ($perspective->objectives as $objective)
					$objectivesList[$objective->id] = $objective->objective;
			}
		
		return View::make("masterSC.indicators.index2", compact('indicators', 'objectivesList', 'objectives', 'id'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$rules['goal_id']='required';
		
		$rules['indicator']='required';
		$validation = Validator::make(Input::all(), $rules);
		$niceNames['goal_id']='goal';

		if(isset($niceNames))
	    	$validation->setAttributeNames($niceNames); 
		
		if($validation->fails())
			return Redirect::route('indicators.index')->withErrors($validation)->withInput();
		
		$goal=Objective::find($input['goal_id']);

		if(isset($input['type']))
		{
			// if(isset($goal->indicatorsBaseline[0])&&$input['type']==0)
			// 	return Redirect::back()->with('success', "You can only have one baseline for each strategic goal.")->withInput();
			
			// if(isset($goal->indicatorsUniverse[0])&&$input['type']==1)
			// 	return Redirect::back()->with('success', "You can only have one universe for each strategic goal.")->withInput();
			
			// if(isset($goal->indicatorsTarget[0])&&$input['type']==3)
			// 	return Redirect::back()->with('success', "You can only have one target for each strategic goal.")->withInput();
			
			$indicator = Indicators::create(Input::all());
			
			if($input['type'] == 2)
			{
				$indicator->ordinal = count($goal->indicatorsMovement);
				$indicator->save();
			}
		}
		else 
		{
			$indicator = Indicators::create(Input::all());
			$indicator->type = 2;
			$indicator->ordinal = count($goal->indicatorsMovement);
			$indicator->save();
		}
		if(isset($input['affect']))
		{
			$indicator->affect = 1;
			$indicator->save();
		}

		return Redirect::back()->with(['success'=> "Successfully created {$indicator->indicator}.", 'id' => $goal->id]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input=Input::all();
		//dd($input);
		$indicator = Indicators::find($id);
		$goal=Objective::find($input['goal']);

		// if(isset($goal->indicatorsBaseline[0])&&$input['type']==0&&$goal->indicatorsBaseline[0]->id!=$indicator->id)
		// 	return Redirect::back()->with('success', "You can only have one baseline for each strategic goal.")->withInput();
		
		// if(isset($goal->indicatorsUniverse[0])&&$input['type']==1&&$goal->indicatorsUniverse[0]->id!=$indicator->id)
		// 	return Redirect::route('indicators.index')->with('success', "You can only have one universe for each strategic goal.");
		
		// if(isset($goal->indicatorsTarget[0])&&$input['type']==3&&$goal->indicatorsTarget[0]->id!=$indicator->id)
		// 	return Redirect::back()->with('success', "You can only have one target for each strategic goal.")->withInput();
		
		if(strlen($input['indicator']))
		{
			$indicator->update(Input::all());
			return Redirect::back()->with('success', "Successfully updated indicator.");
		}
		return Redirect::back()->with(['success' => "Edit invalid. When editing an indicator, please fill up the indicator field.", 'id' => $goal->id]);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$indicator = Indicators::find($id);

		if(count($indicator->accomplishments))
			return Redirect::route("indicators.index")
				->with('success', "You cannot delete an indicator while it is being used by a field office scoreboard.");

		$indicator->delete();

		return Redirect::back()->with(['success'=> "Successfully deleted indicator.", 'id' => $indicator->goal->id]);
	}

	public function aggregation($id)
	{
		$input = Input::all();
		$goal = Objective::find($id);
		//dd($input);
		$goal->aggregation_type = $input['aggregation_type'];
		$goal->save();

		return Redirect::back()->with(['success' => 'Successfully updated aggregation type.', 'id' => $goal->id]);
	}

	public function moveUp($gid, $id)
	{
		$indicator = Indicators::find($id);

		$indicator->ordinal--;
		$indicator->save();

		if($indicator2 = Indicators::whereGoalId($gid)->whereOrdinal($indicator->ordinal)->where('id', '!=', $id)->first())
		{
			$indicator2->ordinal++;
			$indicator2->save();
		}

		return Redirect::back()->with(['success'=>'Successfully moved performance indicator.', 'id' => $indicator->goal->id]);
	}

	public function moveDown($gid, $id)
	{
		$indicator = Indicators::find($id);

		$indicator->ordinal++;
		$indicator->save();

		if($indicator2 = Indicators::whereGoalId($gid)->whereOrdinal($indicator->ordinal)->where('id', '!=', $id)->first())
		{
			$indicator2->ordinal--;
			$indicator2->save();
		}

		return Redirect::back()->with(['success'=>'Successfully moved performance indicator.', 'id' => $indicator->goal->id]);
	}
}
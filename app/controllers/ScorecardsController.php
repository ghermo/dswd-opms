<?php

class ScorecardsController extends \BaseController {

	public function current()
	{
		$scorecard = Scorecard::current();
		if( ! $scorecard) return Redirect::route('dashboard.index')->with('success', 'Information not available');

		$months=Date::$months;
		
		$year = $scorecard->years()->lists('year','year');
		$years=[];
		$i=0;
		foreach($year as $y)
		{
			$years[$i]=$y;
			
			$i++;
		}

		return View::make('masterSC.index', compact('scorecard','years'));
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$scorecards=Scorecard::all();
		return View::make('masterSC.managesc',compact('scorecards'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('masterSC.createsc');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();

		$scorecards = Scorecard::all();

		foreach($scorecards as $scorecard)
		{
			$scorecard->flag = 0;
			$scorecard->save();
		}

		$scorecard = Scorecard::create([
			'name' => $input['name'],
			'description' => $input['description']
			]);
		$scorecard->flag = 1;
		$scorecard->save();

		$years=[];

		for($year=date("Y")-1;$year<date("Y")+17;$year++)
		{
			if(isset($input["year-".$year]))
			{
				array_push($years,ScorecardYears::create([
					'scorecard_id'=>$scorecard->id,
					'year'=>$year]));
			}
		}
		$i = 1;

		while(isset($input['p'.$i])) 
		{
			if($input['p'.$i] != '')
			{
				$perspective = Perspective::create([
					'perspective' => $input['p'.$i],
					'scorecard_id' => Scorecard::current()->id,
					'flag' => $input['highest-perspective'] == $i
					]);

				$j = 'A';

				while(isset($input['o'.$j.'1']))
				{
					if($input['o'.$j.'1'] == $i)
					{
						$measure = Measure::create(['description' => $input['o'.$j.'3']]);
						$objective = Objective::create([
							'objective' => $input['o'.$j.'2'],
							'perspective_id' => $perspective->id,
							'measure_id' => $measure->id
							]);

						$objective->ordinal = $objective->perspective()->first()->objectives()->count();
						$objective->save();

						$measure->objective_id = $objective->id;
						$measure->save();

						foreach($years as $year)
						{
							TargetAccomplishment::create([
								'year_id'=>$year->id,
								'measure_id'=>$measure->id]);
						}
					}
					$j++;
				}
			}
			$i++;
		}

		return Redirect::route('scorecards.index')->with('success','Scorecard created')->with(["notification" => "changed the current scorecard.", "affected_users" => User::all(), "link" => URL::route('scorecards.current')]);
	}

	public function setcurrent($id)
	{
		$current=Scorecard::where('flag', '=', 1)->first();
		if($current!=null)
		{
			$current->flag=0;
			$current->save();
		}
		
		
		$scorecard=Scorecard::where('id', '=', $id)->first();
		$scorecard->flag = 1;
		$scorecard->save();
		return Redirect::route('scorecards.index')->with('success', 'Scorecard set as current')->with(["notification" => "changed the current scorecard.", "affected_users" => User::all(), "link" => URL::route('scorecards.current')]);
	}

	public function setarchive($id)
	{	
		$scorecard=Scorecard::find($id);

		$scorecard->archive= 1;
		$scorecard->save();

		return Redirect::route('scorecards.index')->with('success', 'Scorecard sent to archives')->with(["notification" => "archived a scorecard.", "affected_users" => User::admins(), "link" => URL::route('scorecards.archives')]);
	}

	public function unsetarchive($id)
	{
		$scorecard=Scorecard::find($id);

		$scorecard->archive= 0;
		$scorecard->save();

		return Redirect::route('scorecards.index')->with('success', 'Scorecard restored')->with(["notification" => "restored a scorecard.", "affected_users" => User::admins(), "link" => URL::route('scorecards.index')]);
	}

	public function archives()
	{
		$scorecards=Scorecard::all();
		return View::make('masterSC.archives',compact('scorecards'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$scorecard = Scorecard::find($id);
		$years = $scorecard->years()->orderBy('year');
		$targetYears = $years->lists('year');
		$baseline = $years->first()->year;
		return View::make('masterSC.scorecards.edit', compact('scorecard', 'baseline', 'targetYears'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();
		
		//dd($input);
		$rules = array('name'=>'required','years'=>'required');
		$validator = Validator::make($input, $rules);
	   
	    if ($validator->fails())
	    {
	        //dd("fail");
	        //dd($input);
	        return Redirect::to("/scorecards/{$id}/edit")->withErrors($validator)->withInput();
	        //return View::make('masterSC.createsb',compact('officeAndUser','scorecard'))->withErrors($validator);
	    }
	    $scorecard = Scorecard::find($id);
		$scorecard->update($input);
		$currentYears = $scorecard->years()->lists('year');

		foreach($input['years'] as $year)
		{
			if( ! in_array($year, $currentYears))
			{
				ScorecardYears::create(['scorecard_id'=>$scorecard->id, 'year'=>$year]);
			}
		}

		foreach($scorecard->years()->get() as $year)
		{
			if( ! in_array($year->year, $input['years']))
			{
				$year->delete();
			}
		}

		if($scorecard == Scorecard::current())
			return Redirect::route('scorecards.index')->with('success','Updated scorecard')->with(["notification" => "updated the current scorecard.", "affected_users" => User::all(), "link" => URL::route('scorecards.current')]);
		else
			return Redirect::route('scorecards.index')->with('success','Updated scorecard')->with(["notification" => "updated a scorecard.", "affected_users" => User::admins(), "link" => URL::route('scorecards.index')]);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}

<?php

class InitiativesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Role::access('0'))
			if(Scorecard::current())
				$initiatives=Scorecard::current()->initiatives;
			else $initiatives = [];
		else
			$initiatives=Auth::user()->initiatives();

		return View::make('masterSC.stratInit', compact('initiatives'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$query = DB::select('SELECT m.id, m.description FROM scorecard as s, perspectives as p, objectives as o, measures as m WHERE m.objective_id = o.id AND o.perspective_id = p.id AND p.scorecard_id = s.id AND s.flag = 1; ');
		
		$measures = [];

		foreach($query as $measure)
			$measures[$measure->id] = $measure->description;


		$offices = Office::where('office_flag','0')->lists('office_abbreviation', 'id');
		//dd($offices);
		$users = User::select(DB::raw('id, concat (firstName," ", lastName," (",username,")") as name'))->orderBy('name')->lists('name', 'id');

		return View::make('masterSC.createInitiative', compact('measures', 'offices', 'users'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		// dd($input);
		$rules = [
			'projName'  => 'required',
			'projDesc' =>'required',
			'measures'=>'required|',
			'coChampions'=>'required'
		];
		
		$niceNames = array(
		    'projName' => 'name of project',
		    'projDesc' => 'project description',
		    'measures'=>  'measures affected',
		    'coChampions'=>	'project co-champion'
		);

		$validation = Validator::make(Input::all(), $rules);
		$validation->setAttributeNames($niceNames); 

		$oldMeasures = [];
		$oldCoChampions = [];
		$oldTeams = [];

		if(isset($input['measures']))
			foreach($input['measures'] as $id)
				array_push($oldMeasures, Measure::find($id));

		if(isset($input['coChampions']))
			foreach($input['coChampions'] as $id)
				array_push($oldCoChampions, Office::find($id));

		if(isset($input['teams']))
			foreach($input['teams'] as $id)
				array_push($oldTeams, Office::find($id));

		if($validation->fails())
			return Redirect::back()->withErrors($validation)->withInput(Input::all())
				->with(compact('oldMeasures', 'oldCoChampions', 'oldTeams'));

		$initiative = Initiatives::create(['projectName' => $input['projName'], 'projectDesc' => $input['projDesc'], 'scorecard_id' => Scorecard::current()->id]);

		foreach ($oldMeasures as $measure)
			$initiative->measures()->attach($measure);

		foreach ($oldCoChampions as $office)
			$initiative->offices()->attach($office, ['flag' => '0']);

		foreach ($oldTeams as $office)
			$initiative->offices()->attach($office, ['flag' => '1']);

		for($i = 0; isset($input['PM'.$i.'a']); $i++)
		{
			if($input['PM'.$i.'a'] && $input['PM'.$i.'b'] && $input['PM'.$i.'c'] && $input['PM'.$i.'d'] && $input['PM'.$i.'e'] && $input['PM'.$i.'f'])
				ProjectMilestones::create([
					'initiative_id' => $initiative->id,
					'start_date' 	=> $input['PM'.$i.'a'],
					'end_date' 		=> $input['PM'.$i.'b'],
					'milestones' 	=> $input['PM'.$i.'c'],
					'budget_amount' => $input['PM'.$i.'d'],
					'budget_source' => $input['PM'.$i.'e'],
					'owner_id' 		=> $input['PM'.$i.'f'],
					'status1' 		=> $input['PM'.$i.'g'],
				]);
		}

		if(isset($input['create1']))
		{	
			return Redirect::to('/stratInit')->with('success','Successfully created an intiative');
		}
		else if(isset($input['create2']))
		{
			return Redirect::to('/initiative/create')->with('success','Successfully created an intiative');
		}
	}
	private function access($initiative1)
	{
		foreach (Auth::user()->initiatives() as $initiative2)
			if($initiative1->id == $initiative2->id)
				return true;

		return false;
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$initiative = Initiatives::find($id);
		$offices = Office::all();

		//dd($offices->first()->office_name);
		$remarks = $initiative->remarks;

		if( ! Role::access('0') && ! $this->access($initiative, Auth::user()->initiatives()))
			return Redirect::route('dashboard.index');

		return View::make('masterSC.initiative', compact('initiative', 'remarks', 'offices'));
	}

	public function contains($measures, $measure1)
	{
		foreach ($measures as $measure2) 
		{
			if($measure1->id == $measure2->id)
				return true;
		}

		return false;
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$initiative = Initiatives::find($id);
		$remarks = $initiative->remarks;

		if( ! Role::access('0') && ! $this->access($initiative, Auth::user()->initiatives()))
			return Redirect::route('dashboard.index');

		$query = DB::select('SELECT m.id, m.description FROM scorecard as s, perspectives as p, objectives as o, measures as m WHERE m.objective_id = o.id AND o.perspective_id = p.id AND p.scorecard_id = s.id AND s.flag = 1; ');
		
		$measures = [];

		foreach($query as $measure)
		{
			if( ! $this->contains($initiative->measures, $measure))
				$measures[$measure->id] = $measure->description;
		}

		$offices = [];

		foreach(Office::where('office_flag',0)->get() as $office)
		{
			if( ! $this->contains($initiative->offices, $office))
				$offices[$office->id] = $office->office_abbreviation;
		}
		//dd($offices);
		$users = User::select(DB::raw('id, concat (firstName," ", lastName," (",username,")") as name'))->orderBy('name')->lists('name', 'id');

		return View::make('masterSC.editInitiative', compact('initiative', 'measures', 'offices', 'users', 'remarks'));
	}

	public function setFinal($id)
	{	
		$initiatives=Initiatives::where('id', '=', $id)->first();
		$initiatives->status= 1;
		$initiatives->save();
		return Redirect::to('/stratInit')->with('success','Successfully set an initiative as final');
	}

	public function setDraft($id)
	{	
		$initiatives=Initiatives::where('id', '=', $id)->first();
		$initiatives->status= 0;
		$initiatives->save();
		return Redirect::to('/stratInit')->with('success','Successfully set an initiative as draft');
	}

	public function update($id)
	{
		$input = Input::all();

		$initiative = Initiatives::find($id);

		if(Role::access(0))
		{
			$projectMilestonesId = $initiative->projectMilestones()->lists('id');

			if(isset($input['PM_ids']))
			{
				foreach($projectMilestonesId as $id)
					if( ! in_array($id, $input['PM_ids']))
						ProjectMilestones::find($id)->delete();

				foreach ($input['PM_ids'] as $i => $id)
				{
					$milestone = ProjectMilestones::find($id);

					$milestone->update([
						'start_date' 		=> $input['start_periods'][$i],
						'end_date' 			=> $input['end_periods'][$i],
						'milestones' 		=> $input['milestones'][$i],
						'budget_amount' 	=> $input['amounts'][$i],
						'budget_source' 	=> $input['sources'][$i],
						'owner_id' 			=> $input['onwers'][$i],
						'status1' 			=> $input['statuses1'][$i],
						'status2' 			=> $input['statuses2'][$i],
						'status3' 			=> $input['statuses3'][$i],
						'status4' 			=> $input['statuses4'][$i],
						]);
				}
			}

			$oldMeasures = [];
			$oldCoChampions = [];
			$oldTeams = [];
			
			if(isset($input['measures']))
				foreach($input['measures'] as $id)
					array_push($oldMeasures, $id);

			if(isset($input['coChampions']))
				foreach($input['coChampions'] as $id)
					array_push($oldCoChampions, $id);

			if(isset($input['teams']))
				foreach($input['teams'] as $id)
					array_push($oldTeams, $id);

			foreach ($initiative->measures()->lists('id') as $measure)
				if ( ! in_array($measure, $oldMeasures))
					$initiative->measures()->detach($measure);

			foreach ($initiative->offices()->lists('office_id') as $office)
				if ( ! in_array($office, $oldCoChampions))
					$initiative->offices()->detach($office);

			foreach ($initiative->offices()->lists('office_id') as $office)
				if ( ! in_array($office, $oldTeams))
					$initiative->offices()->detach($office);

			foreach ($oldMeasures as $measure)
				if( ! $initiative->measures->contains($measure))
					$initiative->measures()->attach($measure);

			foreach ($oldCoChampions as $office)
				if( ! $initiative->offices->contains($office))
					$initiative->offices()->attach($office, ['flag' => '0']);

			foreach ($oldTeams as $office)
				if( ! $initiative->offices->contains($office))
					$initiative->offices()->attach($office, ['flag' => '1']);

			for($i = 0; isset($input['PM'.$i.'a']); $i++)
			{
				if($input['PM'.$i.'a'] && $input['PM'.$i.'b'] && $input['PM'.$i.'c'] && $input['PM'.$i.'d'] && $input['PM'.$i.'e'] && $input['PM'.$i.'f'])
					ProjectMilestones::create([
						'initiative_id' => $initiative->id,
						'start_date' 	=> $input['PM'.$i.'a'],
						'end_date' 		=> $input['PM'.$i.'b'],
						'milestones' 	=> $input['PM'.$i.'c'],
						'budget_amount' => $input['PM'.$i.'d'],
						'budget_source' => $input['PM'.$i.'e'],
						'owner_id' 		=> $input['PM'.$i.'f'],
						'status1' 		=> $input['PM'.$i.'g'],
					]);
			}

			$rules = [
				'projName'  => 'required',
				'projDesc' =>'required',
				'measures'=>'required|',
				'coChampions'=>'required'
			];
			
			$niceNames = array(
			    'projName' => 'name of project',
			    'projDesc' => 'project description',
			    'measures'=>  'measures affected',
			    'coChampions'=>	'project co-champion'
			);

			$validation = Validator::make(Input::all(), $rules);
			$validation->setAttributeNames($niceNames); 

			if($validation->fails())
				return Redirect::back()->withErrors($validation)->withInput(Input::all());

			$initiative->update(['projectName' => $input['projName'], 'projectDesc' => $input['projDesc']]);

		}
		else if(isset($input['PM_ids'])) foreach ($input['PM_ids'] as $i => $id)
		{
			$milestone = ProjectMilestones::find($id);

			if($milestone->currentStatus() <= 4)
				$milestone->update(['status'.$milestone->currentStatus() => $input['statuses' . $milestone->currentStatus()][$i]  . "\n" . $milestone->status]);
		}

		if(trim($input['remark']))
		{
			$remark = InitRemark::create(['user_id' => Auth::user()->id, 'initiative_id' => $initiative->id, 'type' => $input['type'], 'remark' => $input['remark']]);
		}

		return Redirect::route('initiatives.show', ['id'=>$initiative->id])->with('success','Successfully updated');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete($id)
	{
		Initiatives::where('id', '=', $id)->first()->delete();
		return Redirect::to('/stratInit')->with('success','Successfully deleted an initiative');
	}

	public function remark($id)
	{
		$input = Input::all();

		$rules['remark'] ='required';
		$validator=Validator::make($input, $rules);
	    if ($validator->fails())
	    	return Redirect::back()->withErrors($validator)->withInput();

		InitRemark::create(['user_id' => Auth::user()->id, 'initiative_id' => $id, 'type' => $input['type'], 'remark' => $input['remark']]);

	    $initiative = Initiatives::find($id);
	    if($input['type'])
	    {
    		Notifications::notify(
    			'gave a suggestion on ' . $initiative->projectName . ".", 
    			$initiative->notifiedUsers(), URL::route('initiatives.show', ['id' => $initiative->id]));
	    }
	    else
	    {
    		Notifications::notify(
    			'commented on ' . $initiative->projectName . ".",  
    			$initiative->notifiedUsers(), URL::route('initiatives.show', ['id' => $initiative->id]));
	    }

		return Redirect::back();
	}
}

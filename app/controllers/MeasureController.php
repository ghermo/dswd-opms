<?php

class MeasureController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() 
	{
		//$perspectives = Scorecard::current()->perspectives()->objectives()->get();
		//$measures=Measure::where('objective_id', '=', $ObjectiveId)->get();
		$perspectives=Scorecard::current()->perspectives()->orderBy('flag', 'desc')->get();
		//dd($perspectives);
		return View::make('masterSC.measurelist',compact('perspectives'));
	}



	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$objective=Objective::where('id', '=', $id)->first();
		
		$offices=Office::get();
		$users=User::get();
		$office=[];
		$user=[];
		$i=0;
		foreach($offices as $item)
		{
			$office[$i]=$item->office_name;
			$i++;
		}
		$i=0;
		foreach($users as $item)
		{
			//$user[$i]=$item->username;
			$i++;
		}
		$i=0;
		$officeAndUser=array_merge($office,$user);
		if(!count($officeAndUser))
		{
			$officeAndUser=['No office or user created.'];
		}
		
		
		return View::make('masterSC.createsb',compact('officeAndUser','scorecard'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$objective=Objective::where('id', '=', $id)->first();
		$scorecard=Scorecard::current();

		return View::make('masterSC.measure.show', compact('id','objective','scorecard'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$objective=Objective::where('id', '=', $id)->first();
		$oftenUpdate = ['Monthly' => 'Monthly', 'Quarterly' => 'Quarterly', 'Semi-annually' => 'Semi-annually', 'Annually' => 'Annually' ];
		$unitMeasure = ['Absolute count' => 'Absolute count', 'Percentage' => 'Percentage'];
		$scorecard=Scorecard::current();
		//dd($objective);
		return View::make('masterSC.measure.edit', compact('objective', 'oftenUpdate', 'unitMeasure','scorecard'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$editMeasure = Measure::find($id);
	
	    $editMeasure->reason = Input::get('reason');
		$editMeasure->frequency = Input::get('oftenUpdate');
		$editMeasure->unitOfMeasure= Input::get('unitMeasure');
		$editMeasure->formula= Input::get('measureCalculated');
		$editMeasure->availability=Input::get('availability');
		$editMeasure->infoAvailability=Input::get('infoAvailability');
		$editMeasure->userSetTarget=Input::get('responsible');
		$editMeasure->userAccountableTarget=Input::get('accountable');
		$editMeasure->userTrackTarget=Input::get('responsibleTracking');
		$editMeasure->acquiredReason=Input::get('howAcquired');
		$editMeasure->baseline=Input::get('baseline');
	   /* $rules = [
	        'formula'  => 'alpha_spaces|required',
	        
	    ];*/
	   // $input=Input::all();
	    //$validation = Validator::make($input, $rules, $messages);

	    //if($validation->fails())
	    	//return Redirect::back()->withErrors($validation)->withInput(Input::all());
		//$editMeasure->
		/*['status','objective','description',
	           'frequency','unitOfMeasure','baseLine','userSetTarget',
	            'userAccountableTarget','userTrackTarget','availability',
	            'reason','acquiredReason','formula'];*/
		$editMeasure->save(); 
		$input=Input::all();
		$years = Scorecard::current()->years;
		$targets = $editMeasure->targets2($years)->get();
		//$accom=$input['accomplishments'];
		//var_dump($accom);
		var_dump($input);
		$rules=[];
		$niceNames=[];
		$accomplishments=[];
		for($i=0;$i<count($targets);$i++)
		{
			$rules['accomplishments'.$i]="numeric|positive_num";
			$niceNames['accomplishments'.$i]="accomplishment";
			$accomplishments[$i]=$input['accomplishments'.$i];
		}
		//var_dump($rules);
		$validator = Validator::make($input, $rules);
		//dd($validator->fails());
	    if(isset($niceNames))
	    	$validator->setAttributeNames($niceNames); 
	   //dd($validator);
	    if ($validator->fails())
	    {
	        return Redirect::route('measure.edit',['id'=>$id])->withErrors($validator)->withInput();
	    }

		foreach ($accomplishments as $key => $accomplishment) 
		{
			$targets[$key]->target = $accomplishment;
			$targets[$key]->save();
		}

		return Redirect::route('measure.show', ['id' => $id])->with('success', 'Successfully updated');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Objective::find($id)->delete();

		return Redirect::to('measurelist')->with('success', 'Successfully deleted');
	}	
	public function setFinal($id)
	{
		$measures=Measure::where('id', '=', $id)->first();
		$measures->status= "Final";
		$measures->save();
		return Redirect::to('/measurelist');
	}
	public function setDraft($id)
	{
		$measures=Measure::where('id', '=', $id)->first();
		$measures->status= "Draft";
		$measures->save();
		return Redirect::to('/measurelist');
	}


}

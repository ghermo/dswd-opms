<div class="navbar navbar-inverse navbar-fixed-top sub-nav" role="navigation">
    <div class="container sub-nav-menu-1">
        <div class="row scorecard-menu">
            <div class="col-md-12">
                <ul class="nav nav-pills" style="color: #fff;">
                    @if(Role::access('0'))
                        <li class="{{ OPMSForm::stratMap(Request::path())? 'active':'' }}"> 
                            
                            {{ link_to(URL::route('systemsettings.stratmap'), 'Manage Strategy') }} 
                        </li>
                    @endif
             
                </ul>
            </div>
        </div> 
    </div>
</div>
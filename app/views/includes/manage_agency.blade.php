@if(Role::access('0'))
	<div class="navbar navbar-inverse navbar-fixed-top sub-nav2" role="navigation">
	    <div class="container sub-nav-menu-2">
	        <div class="row">
				<div class="col-md-12">
					<strong>Manage:</strong>
					{{ link_to(URL::route('scorecards.index'), 'Scorecards', ['class' => OPMSForm::scorecard(Request::path()) ? 'active-link' : '']) }} |
					{{ link_to('/manageperspective', 'Perspectives', ['class' => OPMSForm::perspective(Request::path()) ? 'active-link' : '']) }} |
					{{ link_to('/managestratobj', 'Strategic Goals/Objectives', ['class' => OPMSForm::goals(Request::path()) ? 'active-link' : '']) }} | 
				    {{ link_to('/stratInit', 'Initiative Profiles', ['class' => OPMSForm::initiatives(Request::path()) ? 'active-link' : '']) }} | 
				    {{ link_to('/measurelist', 'Measure Profiles', ['class' => OPMSForm::measures(Request::path()) ? 'active-link' : '']) }} | 
				    {{ link_to(URL::route("indicators.index"), 'Performance Indicators', ['class' => OPMSForm::indicators(Request::path()) ? 'active-link' : '']) }}
				</div>
			</div>	
	    </div>
	</div>
@elseif(Auth::check() && count(Auth::user()->projectMilestones))
	<div class="navbar navbar-inverse navbar-fixed-top sub-nav2" role="navigation">
	    <div class="container sub-nav-menu-2">
	        <div class="row">
				<div class="col-md-12">
					<strong>Manage:</strong>
				    {{ link_to('/stratInit', 'Initiative Profiles', ['class' => OPMSForm::initiatives(Request::path()) ? 'active-link' : '']) }}
				</div>
			</div>	
	    </div>
	</div>
@endif
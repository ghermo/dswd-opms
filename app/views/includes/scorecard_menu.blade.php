<div class="navbar navbar-inverse navbar-fixed-top sub-nav" role="navigation">
    <div class="container sub-nav-menu-1">
        <div class="row scorecard-menu">
            <div class="col-md-12">
                <ul class="nav nav-pills" style="color: #fff;">
                    @if(Role::access('01234'))
                        <li class="{{ OPMSForm::agencysc(Request::path())? 'active':'' }}"> 
                            {{ link_to(URL::route('scorecards.current'), 'Agency Scorecard') }}
                        </li>
                    @endif
                    @if(Role::access('01234'))
                        <li class="{{ OPMSForm::accomplishments(Request::path())? 'active active-link':'' }}">  
                            {{ link_to(URL::route('accomplishments.year', ['year' => date("Y")]), 'Strategic Goals Accomplishments') }}
                        </li>
                    @endif
                    @if(Role::access('013'))
                        <li class="{{ OPMSForm::officesb(Request::path())? 'active active-link':'' }}"> 
                            {{ link_to('/officesb', 'Office Scoreboard') }}
                        </li>
                    @endif
                    @if(Role::access('24'))
                        <li class="{{ OPMSForm::officesb(Request::path())? 'active active-link':'' }}"> 
                            {{ link_to('/officesb/central', 'Office Scoreboard') }}
                        </li>
                    @endif
                    @if(Role::access('01234'))
                    <li class="{{ OPMSForm::opcr(Request::path())? 'active active-link':'' }}">
                        {{ link_to('/opcr', 'Office Performance Commitment and Review') }}
                    </li>
                    @endif
                    @if(Role::access('01234'))
                        <li class="{{ OPMSForm::visionbc(Request::path())? 'active active-link':'' }}"> 
                            {{ link_to('/visionbc', 'Vision Basecamps') }}
                        </li>
                    @endif
                </ul>
            </div>
        </div> 
    </div>
</div>

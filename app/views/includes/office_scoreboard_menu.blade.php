@if(Role::access('0'))
	<div class="navbar navbar-inverse navbar-fixed-top sub-nav2" role="navigation">
	    <div class="container sub-nav-menu-2">
	        <div class="row sbmenu">
				<div class="col-md-12">
					<strong>Manage:</strong>
					{{ link_to('/officesb', 'Field Office Scoreboards', ['class' => OPMSForm::fieldsb(Request::path()) ? 'active-link' : '']) }} |
					{{ link_to('/officesb/central/', 'Central Office Scoreboards', ['class' => OPMSForm::centralsb(Request::path()) ? 'active-link' : '']) }}		
				</div>
			</div>	
	    </div>
	</div>
@endif
@extends('layouts.default')
@section('content')


{{ Form::open(['url' => "/officesb/central/updatesb/".$scoreboard->id, 'name' => 'myForm', 'class' => 'form-horizontal col-md-12', 'role' => 'form']) }}
<h2 class="pull-left">{{ HTML::decode (link_to( "/officesb/central", '<i class="fa fa-chevron-left fw"></i>', array('class'=>'link-black','title'=>'Central Office Scoreboard List') )) }} Edit Central Office Scoreboard
</h2>
<hr class="clear" />
<div class="well">
  <strong>Office:</strong> {{$scoreboard->author->office_name}} <br />
  <strong>Date:</strong> {{$scoreboard->date}} <br />
</div>

<div class="row office-scoreboard">
  <div class="col-md-12">
    <h4>I.Office Scoreboard for {{$scoreboard->year}}</h4>
    <div class="panel panel-primary">
      <div class="panel-heading">
        <strong>Breakthrough Goal:  *</strong>
      </div>

      <div class="panel-body">{{ Form::text('goal', $scoreboard->goal, ['class'=>'form-control','clearable','maxlength'=>'255']) }}
      {{ $errors->first('goal', '<span class=errormsg>*:message</span>') }}<br /><br />
        {{ Form::label('goalStatus', 'Status:')}}
        {{ Form::textarea('goalStatus', $scoreboard->goal_status, ['class'=>'form-control animated','clearable','rows'=>'2','maxlength'=>'255']) }}

      </div>
      
      <table class="table table-bordered" id="assignments">
        <thead>
          <tr class="active">
            <th><center>Lead Measures *</center></th>
            <th><center>Responsible Person/Division*</center></th>
            <th><center>Status (as to Targets and Timeline)*</center></th>
            <th><center>Add/Remove</center></th>
          </tr>
        </thead>
        <tbody>
          <script type = "text/javascript">
            var counter=0;
            function add(){
              counter++;
            }

            function sub(){
              counter--;
            }
          </script>
            
          <?php $i=0;?>
          
          @if(count(Input::old()))
           @foreach (Input::old('leadMeasure') as $key => $n)
             <tr class="<?php echo $key?>">
                <td style="vertical-align:top;">
                  <textarea class='form-control animated leadMeasure' name='leadMeasure[]' clearable rows='2' maxlength='1000'>{{ $n }}</textarea>
                  {{ $errors->first('leadMeasure[]', '<span class=errormsg>*:message</span>') }}
                </td>
                <td style="vertical-align:top;">
                  <textarea class='form-control animated officeOrUser' name='officeOrUser[]' clearable rows='2' maxlength='1000'>{{ Input::old('officeOrUser')[$key] }}</textarea>
                  {{ $errors->first('officeOrUser[]', '<span class=errormsg>*:message</span>') }}
                </td>
                <td style="vertical-align:top;">
                  <textarea class='form-control animated status' name='status[]' clearable rows='2' maxlength='1000'>{{ Input::old('status')[$key] }}</textarea>
                  {{ $errors->first('status[]', '<span class=errormsg>*:message</span>') }}
                </td>
                <td style="vertical-align:top;">
                  {{ HTML::decode(link_to('#', '<i class="fa fa-plus fw"></i>', array('class' => 'btn btn-default add'))) }}
                  {{ HTML::decode(link_to('#', '<i class="fa fa-minus-circle fw"></i>', array('class' => 'btn btn-default remove'))) }}
                </td>
                <!-- {{ HTML::script('js/jquery.min.js') }} -->
                <script type = "text/javascript">
                  add();
                </script>
             </tr>
            @endforeach

          @elseif(count($scoreboard->assignments()->get()))
            @foreach($scoreboard->assignments()->get() as $item)
              <tr>
                <td style="vertical-align:top;">
                  <textarea class='form-control animated leadMeasure' name='leadMeasure[]' clearable rows='2' maxlength='1000'>{{ $item->lead_measure }}</textarea>
                  {{ $errors->first('leadMeasure'.$i, '<span class=errormsg>*:message</span>') }}
                </td>
                <td style="vertical-align:top;">
                  <textarea class='form-control animated officeOrUser' name='officeOrUser[]' clearable rows='2' maxlength='1000'>{{ $item->responsible_office }}</textarea>
                  {{ $errors->first('officeOrUser'.$i, '<span class=errormsg>*:message</span>') }}
                </td>
                <td style="vertical-align:top;">
                  <textarea class='form-control animated status' name='status[]' clearable rows='2' maxlength='1000'>{{ $item->status }}</textarea>
                  {{ $errors->first('status'.$i, '<span class=errormsg>*:message</span>') }}
                </td>
                <td style="vertical-align:top;">
                  {{ HTML::decode(link_to('#', '<i class="fa fa-plus fw"></i>', array('class' => 'btn btn-default add'))) }}
                  @if($i>0)
                    {{ HTML::decode(link_to('#', '<i class="fa fa-minus-circle fw"></i>', array('class' => 'btn btn-default remove'))) }}
                  @endif
                </td>
                  
                <script type = "text/javascript">
                  add();
                </script>    
              </tr>
            <?php $i++;?>
          @endforeach
   
          @else
               <tr>
                  <td style="vertical-align:top;">
                    <textarea class='form-control animated leadMeasure' name='leadMeasure[]' clearable rows='2' maxlength='1000'></textarea>
                    {{ $errors->first('leadMeasure'.$i, '<span class=errormsg>*:message</span>') }}
                  </td>
                  <td style="vertical-align:top;">
                    <textarea class='form-control animated officeOrUser' name='officeOrUser[]' clearable rows='2' maxlength='1000'></textarea>
                    {{ $errors->first('officeOrUser'.$i, '<span class=errormsg>*:message</span>') }}
                  </td>
                  <td style="vertical-align:top;">
                    <textarea class='form-control animated status' name='status[]' clearable rows='2' maxlength='1000'></textarea>
                    {{ $errors->first('status'.$i, '<span class=errormsg>*:message</span>') }}
                  </td>
                  <td style="vertical-align:top;">
                    {{ HTML::decode(link_to('#', '<i class="fa fa-plus fw"></i>', array('class' => 'btn btn-default add'))) }}
                   
                    {{ HTML::decode(link_to('#', '<i class="fa fa-minus-circle fw"></i>', array('class' => 'btn btn-default remove'))) }}
                  </td>
                  {{ HTML::script('js/jquery.min.js') }}
                  <script type = "text/javascript">
                    add();
                  </script>
               </tr>
          @endif
        </table>
      </div>
  </div>
 
  <div class="col-md-12">
    <h4>II. Narrative</h4>
    <div class="panel panel-primary">
      <div class="panel-heading"><strong>Status as of : {{ $scoreboard->statusDate }} </strong></div>
      <div class="panel-body">
        <strong>Key Accomplishments:</strong> Activities Completed, Coordination done with Partner OBS and Issues Resolved(<em>if any</em>)
      </div>
      <table class="table">
        <tr>
          <td colspan="4">{{ Form::textarea('narrative', $scoreboard->narrative, ['class'=>'form-control summernote','clearable']) }}</td>
        </tr>
      </table>
    </div>
  </div>
  
  <div class="col-md-12">
    <h4>III. Issues and Challenges (if any)</h4>
      <div class="panel panel-primary">
        <div class="panel-heading"><strong>Issues and Challenges </strong></div>
        <div class="panel-body"><i>Please report major issues in this format, if applicable</i></div>
          <table class="table table-bordered" id="myTable">
            <tr class="active">
              <td style="vertical-align:top;"><strong><i>Identify issues and challenges encountered:</i></strong> Specify details under each element, if any</td>
              <td style="vertical-align:top;"><strong><i>Action being taken by team:</i></strong> Summary of the Office or team’s plan to resolve it, including key resources involved, and indicative target by when the identified issue shall have been addressed or resolved </td>
              <td style="vertical-align:top;"><strong><i>Action requested from Management or other OBS (if any):</i></strong> Please indicate desired assistance/intervention from Management or other OBS to facilitate the achievement of the Office Breakthrough. </td>
              <td style="vertical-align:top;"><strong><i>Add/Remove</i></strong> </td>
           </tr>
          
            <?php $i=0;?>
            <!--{{ HTML::script('js/jquery.min.js') }}-->
                 <script type = "text/javascript">
                  var counterChallenge=0;
                  function addChallenge()
                  {
                    counterChallenge++;
                    
                  }
                  function subChallenge()
                  {
                    counterChallenge--;
                    
                  }
                </script>
            @if(count(Input::old()))

               @foreach (Input::old('challenge') as $key => $n)
                 <tr>
                    <td style="vertical-align:top;"><textarea class='form-control animated challenge' name='challenge[]' clearable rows='2' maxlength='255'>{{ $n }}</textarea>
                    </td>
                    <td style="vertical-align:top;"><textarea class='form-control animated actionTaken' name='actionTaken[]' clearable rows='2' maxlength='255'>{{ Input::old('actionTaken')[$key] }}</textarea>
                    </td>
                    <td style="vertical-align:top;"><textarea class='form-control animated actionRequested' name='actionRequested[]' clearable rows='2' maxlength='255'>{{ Input::old('actionRequested')[$key] }}</textarea>
                    </td>
                    <td style="vertical-align:top;">
                      {{ HTML::decode(link_to('#', '<i class="fa fa-plus fw"></i>', array('class' => 'btn btn-default add1'))) }}
                     
                      {{ HTML::decode(link_to('#', '<i class="fa fa-minus-circle fw"></i>', array('class' => 'btn btn-default remove1'))) }}
                    </td>
                    {{ HTML::script('js/jquery.min.js') }}
                    <script type = "text/javascript">
                      addChallenge();
                    </script>
                 </tr>
                @endforeach

            @elseif(count($scoreboard->challenge()->get()))
              @foreach($scoreboard->challenge()->get() as $item)
              <tr>
                <td class="width-20" style="vertical-align:top;">
                  <textarea class='form-control animated challenge' name='challenge[]' clearable rows='2' maxlength='255'>{{ $item->issue }}</textarea>
                </td>
                <td style="vertical-align:top;">
                  <textarea class='form-control animated actionTaken' name='actionTaken[]' clearable rows='2' maxlength='255'>{{ $item->action_taken }}</textarea>
                </td>
                <td style="vertical-align:top;">
                  <textarea class='form-control animated actionRequested' name='actionRequested[]' clearable rows='2' maxlength='255'>{{ $item->action_requested }}</textarea>
                </td>
                <td style="vertical-align:top;">
                  {{ HTML::decode(link_to('#', '<i class="fa fa-plus fw"></i>', array('class' => 'btn btn-default add1'))) }}
                  @if($i>0)
                  {{ HTML::decode(link_to('#', '<i class="fa fa-minus-circle fw"></i>', array('class' => 'btn btn-default remove1'))) }}
                  @endif
                  <?php $i++;?>
                </td>
                <!--{{ HTML::script('js/jquery.min.js') }}-->
                <script type = "text/javascript">
                  addChallenge();
                </script>
              </tr>
              @endforeach
            @else
             <tr>
                <td class="width-20" style="vertical-align:top;">{{ Form::textarea('challenge[]', ' ', ['class'=>'form-control challenge','clearable','rows'=>'2','maxlength'=>'255']) }}</td>
                <td style="vertical-align:top;">{{ Form::textarea('actionTaken[]', null, ['class'=>'form-control actionTaken','clearable','rows'=>'2','maxlength'=>'255']) }}</td>
                <td style="vertical-align:top;">{{ Form::textarea('actionRequested[]', null, ['class'=>'form-control actionRequested','clearable','rows'=>'2','maxlength'=>'255']) }}</td>
                <td style="vertical-align:top;">
                  {{ HTML::decode(link_to('#', '<i class="fa fa-plus fw"></i>', array('class' => 'btn btn-default add1'))) }}
                  <script type = "text/javascript">
                  addChallenge();
                </script>
                  
                </td>
             </tr>
             @endif
          <!--</div>-->
          </table>
  </div>
</div>
</div>
<div class="panel panel-default remarks">
  <div class="panel-heading">Remarks
    <div class="pull-right">
      {{Form::label('filter', 'Filter:') }} 
      {{ Form::select('filter', ['Comments', 'Suggestions', 'All'], 2, ['class' => 'baseline filter'])}}
    </div>
  </div>

  <div class="panel-body">
    <div class="list-group">
      @if(count($remarks))
        <?php $show_more = true; ?>
        <div class="show-more">
          @foreach($remarks as $key => $remark)
            @if($show_more && ((count($remarks)-3) <= $key))
              </div>
              <?php $show_more = false; ?>
            @endif
            <div class="list-group-item remark{{$remark->type}}">
              <strong> {{ $remark->user->fullName() }} </strong>
              {{ $remark->remark }} <br/>
              <i class="livetimestamp"> 
                {{ $remark->type? "suggested" : "commented"}} {{ $remark->created_at->diffForHumans() }} 
              </i>
            </div>
          @endforeach
        @if(count($remarks) > 3)
          <div class="list-group-item">
            <center>
              {{ Form::button('See All', ['class' => 'btn btn-default remarks-collapse'])}}
            </center>
          </div>
        @endif
      @endif
    </div>
  </div>
</div>

{{ Form::submit('Save', ['name' => 'edit', 'class' => 'btn btn-primary', 'id' => 'create1']) }}
{{Form::Close()}}
@stop
@section('cssjs')
  {{ HTML::script('js/remarks.js')}}
<script type = "text/javascript">
  $(document).ready(function() {

    $('.add').closest('tr').not('tr:last-child').children().find('.add').hide();
    $('.add1').closest('tr').not('tr:last-child').children().find('.add1').hide();

    $('.summernote').summernote({
      height: "150px",
      toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
      ]
    });
  
     function addIssue(event)  {
      var issues = counter;
      var id = issues++;
      var $parent = $(this).closest("tr");
      // if($.trim($parent.find(".leadMeasure").val()) && $.trim($parent.find(".officeOrUser").val()) && $.trim($parent.find(".status").val())){
      var newIssue = $("<tr><td style='vertical-align:top;'><textarea class='form-control leadMeasure animated' rows='2' name='leadMeasure[]' clearable maxlength='1000'></textarea></td><td style='vertical-align:top;'><textarea class='form-control officeOrUser animated' rows='2' name='officeOrUser[]' clearable maxlength='1000'></textarea></td><td style='vertical-align:top;'><textarea name='status[]' class='form-control status animated' rows='2' clearable maxlength='1000'></textarea></td><td style='vertical-align:top;'><a href='#' class='btn btn-default add'><i class='fa fa-plus fw'></i></a> <a href='#' class='btn btn-default remove'><i class='fa fa-minus-circle fw'></i></a></td></tr>");
      add();
      newIssue.find(".add").click(addIssue);
      newIssue.find(".remove").click(removeIssue);
      
      $('#assignments').append(newIssue);
      $(this).hide();
      event.preventDefault();
    // } else {
    //   event.preventDefault();
    // }
    }

    function removeIssue(event) {      
      if($(this).closest('tr').is('tr:last-child')){
        $(this).closest('tr').prev().children('td:last').children('a:first').show(); 
      }

      $(this).closest('tr').remove();
       sub();
      event.preventDefault();
    }

    $(".add").click(addIssue);
    $(".remove").click(removeIssue);

    function addChallenges(event)  {
      var issues = counterChallenge;
      var id = issues++;
      var $parent = $(this).closest("tr");
      
      // if($.trim($parent.find(".challenge").val()) && $.trim($parent.find(".actionTaken").val()) && $.trim($parent.find(".actionRequested").val())){
      var newChallenge = $(
        "<tr><td class='width-20'><textarea class='form-control challenge animated' name='challenge[]' clearable rows='2' maxlength='255'></textarea></td><td style='vertical-align:top;'><textarea class='form-control actionTaken animated' name='actionTaken[]' clearable rows='2' maxlength='255'></textarea></td><td style='vertical-align:top;'><textarea class='form-control actionRequested animated' name='actionRequested[]' clearable rows='2' maxlength='255'></textarea></td><td style='vertical-align:top;'><a href='#' class='btn btn-default add1'><i class='fa fa-plus fw'></i></a> <a href='#' class='btn btn-default remove1'><i class='fa fa-minus-circle fw'></i></a></td></tr>");
      addChallenge();
      newChallenge.find(".add1").click(addChallenges);
      newChallenge.find(".remove1").click(removeChallenges);
      
      $('#myTable').append(newChallenge);
      $(this).hide();
      event.preventDefault();
    // } else {
    //   event.preventDefault();
    // }
    }

    function removeChallenges(event) {      
      if($(this).closest('tr').is('tr:last-child')){
        $(this).closest('tr').prev().children('td:last').children('a:first').show(); 
      }
      $(this).closest('tr').remove();
      subChallenge();
      event.preventDefault();
    }

    $(".add1").click(addChallenges);
    $(".remove1").click(removeChallenges);
  });
</script>
@stop
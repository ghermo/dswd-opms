@extends('layouts.default')
@section('content')

<h2 class="pull-left">{{ HTML::decode (link_to( "/officesb/central", '<i class="fa fa-chevron-left fw"></i>', array('class'=>'link-black hidden-print','title'=>'Central Office Scoreboard List') )) }} Central Office Scoreboard
</h2>
<div class="btn-group pull-right btn-option hidden-print">
    <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown">
    <i class="fa fa-bars fw"></i> Options <span class="caret"></span>
    </button>
    <ul class="dropdown-menu pull-right btn-option-dropdown" role="menu">
      <li>{{ HTML::decode(link_to( URL::route('sb.centralEdit', ['id' => $scoreboard->id]), '<i class="fa fa-pencil fw"></i> Edit'))}}</li>
      @if($scoreboard->flag && Role::access('0'))
        <li>{{ HTML::decode(link_to( URL::route('sb.centralUnsetfinal', ['id' => $scoreboard->id]), '<i class="fa fa-file-text fw"></i> Mark As Draft' ))}}</li>
      @elseif(Role::access('4') && ! $scoreboard->flag)
        <li>{{ HTML::decode(link_to( URL::route('sb.centralSetfinal', ['id' => $scoreboard->id]), '<i class="fa fa-check fw"></i> Mark As Final', array('onclick' => "return confirm('Are you sure you want to mark as final?')") ))}}</li>
      @endif
    <li class="divider"></li>
    <li><a href="javascript:window.print()"><i class="fa fa-file fw"></i> Export as PDF</a></li>
    </ul>
</div>
<hr class="clear" />
<div class="well">
  <strong>Office:</strong> {{$scoreboard->author->office_name}} <br />
  <strong>Date:</strong> {{$scoreboard->date}}
</div>

<div class="row office-scoreboard">
  <div class="col-md-12">
    <h4>I.Office Scoreboard for {{$scoreboard->year}}</h4>
    <div class="panel panel-primary">
      <div class="panel-heading"><strong>Breakthrough Goal: </strong></div>
      <div class="panel-body">{{ $scoreboard->goal }}<br /><br />
        <strong>Status:</strong><br />
        @if($scoreboard->goal_status) 
          {{ nl2br($scoreboard->goal_status) }}
        @else <em>N/A</em>
        @endif
      </div>
      <table class="table table-bordered">
        <thead>
          <tr class="active">
            <td ><strong><center>Lead Measures</center> </strong></td>
            <td ><strong><center>Responsible Person/Division</center> </strong></td>
            <td colspan="1" ><strong><center>Status (as to Targets and Timeline)</center></strong></td>
          </tr>
        </thead>
        <tbody>
          @foreach($scoreboard->assignments()->get() as $item)
            <tr>
              <td style="vertical-align:top;">{{ nl2br($item->lead_measure) }}</td>
              <td style="vertical-align:top;">{{ nl2br($item->responsible_office) }}</td>
              <td style="vertical-align:top;">{{ nl2br($item->status) }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <div class="col-md-12">
    <h4>II. Narrative</h4>
    <div class="panel panel-primary">
      <div class="panel-heading">
        <strong>Status as of {{$scoreboard->statusDate}}:</strong>
      </div>
      <div class="panel-body"><strong>Key Accomplishments:</strong> Activities Completed, Coordination done with Partner OBS and Issues Resolved (<em>if any</em>)
      </div>
      <table class="table">
        <tbody>
          <tr>
            @if($scoreboard->narrative) 
              <td colspan="3">{{ $scoreboard->narrative }}</td>
            @else 
              <td colspan="3"><em>N/A</em></td>
            @endif
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  
  <div class="col-md-12">
    <h4>III. Issues and Challenges (if any)</h4>
    <div class="panel panel-primary">
      <div class="panel-heading"><strong>Issues and Challenges </strong></div>
      <div class="panel-body">
        <i>Please report major issues in this format, if applicable</i>
      </div>
      <table class="table table-bordered" id="myTable">
        <thead>
          <tr class="active">
            <td style="vertical-align:top;"><strong>#</strong></td>
            <td cstyle="vertical-align:top;">
              <strong><i>Identify issues and challenges encountered:</i> </strong>Specify details under each element, if any
            </td>
            <td style="vertical-align:top;">
              <strong><i>Action being taken by team:</i> </strong>Summary of the Office or team’s plan to resolve it, including key resources involved, and indicative target by when the identified issue shall have been addressed or resolved 
            </td>
            <td style="vertical-align:top;">
              <strong><i>Action requested from Management or other OBS (if any):</i> </strong>Please indicate desired assistance/intervention from Management or other OBSto facilitate the achievement of the Office Breakthrough. 
            </td>
          </tr>
        </thead>
        <tbody>
        <?php $i=1;?>
        @foreach($scoreboard->challenge()->get() as $item)
          <?php $i++;?>
        @endforeach
        
        @if($i>1)
          <?php $i=1;?>
          @foreach($scoreboard->challenge()->get() as $item)
          <tr>
            <td width="1%" style="vertical-align:top;">{{$i++;}}</td>
            <td class="width-20" style="vertical-align:top;">{{ nl2br($item->issue) }}</td>
            <td style="vertical-align:top;">{{ nl2br($item->action_taken) }}</td>
            <td style="vertical-align:top;">{{ nl2br($item->action_requested) }}</td>
          </tr>
          @endforeach
        @else
          <tr>
            <td colspan="4"><center><b>There are no challenges/issues. </b></center></td>
          </tr>
        @endif
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="panel panel-default remarks">
  <div class="panel-heading">Remarks
    <div class="pull-right hidden-print">
      {{ Form::label('filter', 'Filter:') }} 
      {{ Form::select('filter', ['Comments', 'Suggestions', 'All'], 2, ['class' => 'baseline filter'])}}
    </div>
  </div>
  <div class="panel-body">
    <div class="list-group">
      <div class=" list-group-item">
        <div class="row hidden-print">
          {{ Form::open(['url' => URL::route('sb.centralRemark', ['id' => $scoreboard->id])]) }}
            <div class="col-md-2">
              {{ Form::radio('type', 0, 1)}} Comment <br/>
              {{ Form::radio('type', 1, 0)}} Suggestion
            </div>
            <div class="col-lg-10">
              <div class="input-group">
                {{ Form::text('remark', null, ['class' => 'form-control', 'rows' => 3])}}
                <span class="input-group-btn">
                  {{ Form::submit('Add Remark', ['class' => 'btn btn-primary'])}}
                </span>
              </div><!-- /input-group -->
              {{ $errors->first('remark', '<span class=errormsg>*:message</span>') }}
            </div><!-- /.col-lg-10 -->
          {{ Form::close() }}
        </div>
      </div>
      @if(count($remarks))
        <?php $show_more = true; ?>
        <div class="show-more">
          @foreach($remarks as $key => $remark)
            @if($show_more && ((count($remarks)-3) <= $key))
              </div>
              <?php $show_more = false; ?>
            @endif
            <div class="list-group-item remark{{$remark->type}}">
              <strong> {{ $remark->user->fullName() }} </strong>
              {{ $remark->remark }} <br/>
              <i class="livetimestamp"> 
                {{ $remark->type? "suggested" : "commented"}} {{ $remark->created_at->diffForHumans() }} 
              </i>
            </div>
          @endforeach
        @if(count($remarks) > 3)
        <div class="list-group-item">
          <center>
            {{ Form::button('See All', ['class' => 'btn btn-default remarks-collapse'])}}
          </center>
        </div>
        @endif
      @endif
    </div>
  </div>
</div>

@stop

@section('cssjs')
  {{ HTML::script('js/remarks.js')}}
@stop
@extends('layouts.default')
@section('content')
{{ Form::open(['url' => "/officesb/central/createsb/store", 'name' => 'myForm', 'class' => 'sbForm form-horizontal col-md-12', 'role' => 'form']) }}

<h2 class="pull-left">{{ HTML::decode (link_to( "/officesb/central", '<i class="fa fa-chevron-left fw"></i>', array('class'=>'link-black','title'=>'Central Office Scoreboard List') )) }} Scoreboard Status Report - Central Office
</h2>
<hr class="clear" />
<div class="well">
@if ( Auth::check() ) 
    <strong>Office:</strong>  {{Auth::user()->office->office_name}}<br />
@endif

{{Form::hidden('type',1)}}
<strong>Date:</strong> {{date("Y/m/d")}}{{Form::hidden('date',date("Y/m/d"))}}
</div>

<div class="row office-scoreboard">
  
  <div class="col-md-12">
      <h4>I.Office Scoreboard for {{ Date::$months[$month]}} {{$year}}</h4>
      {{Form::hidden('month', $month) }}
      {{Form::hidden('year', $year) }}
      <div class="panel panel-primary">
        <div class="panel-heading">
          <strong>Breakthrough Goal: *</strong>
        </div>
        <div class="panel-body">
          {{ Form::text('goal', $scoreboard, ['class'=>'form-control','clearable','placeholder'=>'Enter breakthrough goal','maxlength'=>'255']) }}
          {{ $errors->first('goal', '<span class=errormsg>*:message</span>') }}<br /><br />
          {{ Form::label('goalStatus', 'Status:')}}
          {{ Form::textarea('goalStatus', null, ['class'=>'form-control animated','clearable','rows'=>'2','maxlength'=>'255']) }}
        </div>
          <table class="table table-borderless no-top-border " id="assignments">
            <thead>
              <tr class="active">
                <th><center>Lead Measures *</center></th>
                <th><center>Responsible Person/Division*</center></th>
                <th colspan><center>Status (as to Targets and Timeline)*</center></th>
                <th><center>Add Remove<center></th>
              </tr>
            </thead>
            <tbody>
              <script type = "text/javascript">
                var counter=0;

                function add(){
                  counter++;
                }

                function sub(){
                  counter--;
                }
              </script>

             @if(count(Input::old()))
                @foreach (Input::old('leadMeasure') as $key => $n)
                 <tr class="<?php echo $key?>">
                    <td style="vertical-align:top;">
                      <textarea class='form-control animated leadMeasure' name='leadMeasure[]' clearable rows='2' maxlength='1000'>{{ $n }}</textarea>
                      {{ $errors->first('leadMeasure[]', '<span class=errormsg>*:message</span>') }}
                    </td>
                    <td style="vertical-align:top;">
                      <textarea class='form-control animated officeOrUser' name='officeOrUser[]' clearable rows='2' maxlength='1000'>{{ Input::old('officeOrUser')[$key] }}</textarea>
                      {{ $errors->first('officeOrUser[]', '<span class=errormsg>*:message</span>') }}
                    </td>
                    <td style="vertical-align:top;">
                      <textarea class='form-control animated status' name='status[]' clearable rows='2' maxlength='1000'>{{ Input::old('status')[$key] }}</textarea>
                      {{ $errors->first('status[]', '<span class=errormsg>*:message</span>') }}
                    </td>
                    <td style="vertical-align:top;">
                      {{ HTML::decode(link_to('#', '<i class="fa fa-plus fw"></i>', array('class' => 'btn btn-default add'))) }}
                      {{ HTML::decode(link_to('#', '<i class="fa fa-minus-circle fw"></i>', array('class' => 'btn btn-default remove'))) }}
                    </td>
                    <!-- {{ HTML::script('js/jquery.min.js') }} -->
                    <script type = "text/javascript">
                      add();
                    </script>
                 </tr>
                @endforeach
              @else
              <tr class="0">
                <td style="vertical-align:top;">
                  <textarea class='form-control animated leadMeasure' name='leadMeasure[]' clearable rows='2' maxlength='1000'></textarea>
                  {{ $errors->first('leadMeasure[]', '<span class=errormsg>*:message</span>') }}
                </td>
                <td style="vertical-align:top;" >
                  <textarea class='form-control animated officeOrUser' name='officeOrUser[]' clearable rows='2' maxlength='1000'></textarea>
                  {{ $errors->first('officeOrUser[]', '<span class=errormsg>*:message</span>') }}
                </td>
                <td style="vertical-align:top;" >
                  <textarea class='form-control animated status' name='status[]' clearable rows='2' maxlength='1000'></textarea>
                  {{ $errors->first('status[]', '<span class=errormsg>*:message</span>') }}
                  </td>
                <td style="vertical-align:top;">
                  {{ HTML::decode(link_to('#', '<i class="fa fa-plus fw"></i>', array('class' => 'btn btn-default add'))) }}
                </td>

                <!-- {{ HTML::script('js/jquery.min.js') }} -->
                <script type = "text/javascript">
                  add();
                </script>
              </tr> 
              @endif
              </tbody>
          </table>
      </div>
  </div>
 
  <div class="col-md-12">
    <h4>II. Narrative</h4>
    <div class="panel panel-primary">
      <div class="panel-heading">
        <strong>Status as of : 
          {{ Form::hidden('statusYear', $year)}}
          {{ Form::hidden('statusMonth', $month) }}
          {{ Date::$months[$month] }}
          {{ $year }}
        </strong>
      </div>
      <div class="panel-body">
        <strong>Key Accomplishments:</strong> Activities Completed, Coordination done with Partner OBS and Issues Resolved (<em>if any</em>)
      </div>
        <table class="table table-borderless no-top-border no-inner-border">
          <tbody>
            <tr>
              <td colspan="4">{{ Form::textarea('narrative', null, ['class'=>'form-control summernote','clearable','rows'=>'2']) }}</td>
            </tr>
          </tbody>
        </table>
    </div>
  </div>

<div class="col-md-12">
    <h4>III. Issues and Challenges (if any)</h4>
      <div class="panel panel-primary">
        <div class="panel-heading">
          <strong>Issues and Challenges </strong>
        </div>
        <div class="panel-body">
          <i>Please report major issues in this format, if applicable</i>
        </div>
          <table class="table table-borderless no-top-border " id="myTable">
            <tr class="active">
              <td style="vertical-align: top;"><strong><i>Identify issues and challenges encountered:</i></strong>Specify details under each element, if any</td>
              <td class="width-30" style="vertical-align: top;"><strong><i>Action being taken by team:</i></strong>Summary of the Office or team’s plan to resolve it, including key resources involved, and indicative target by when the identified issue shall have been addressed or resolved </td>
              <td class="width-30" style="vertical-align: top;"><strong><i>Action requested from Management or other OBS (if any):</i></strong>Please indicate desired assistance/intervention from Management or other OBSto facilitate the achievement of the Office Breakthrough. </td>
              <td class="width-10" style="vertical-align: top;"><strong><i>Add/Remove</i></strong> </td>
           </tr>
            <script type = "text/javascript">
                  var counterChallenge=0;
                  function addChallenge()
                  {
                    counterChallenge++;
                    
                  }
                  function subChallenge()
                  {
                    counterChallenge--;
                    
                  }
                </script>
            @if(count(Input::old()))
                @foreach (Input::old('challenge') as $key => $n)
                 <tr>
                    <td style="vertical-align:top;"><textarea class='form-control animated challenge' name='challenge[]' clearable rows='2' maxlength='255'>{{ $n }}</textarea>
                    </td>
                    <td style="vertical-align:top;"><textarea class='form-control animated actionTaken' name='actionTaken[]' clearable rows='2' maxlength='255'>{{ Input::old('actionTaken')[$key] }}</textarea>
                    </td>
                    <td style="vertical-align:top;"><textarea class='form-control animated actionRequested' name='actionRequested[]' clearable rows='2' maxlength='255'>{{ Input::old('actionRequested')[$key] }}</textarea>
                    </td>
                    <td style="vertical-align:top;">
                      {{ HTML::decode(link_to('#', '<i class="fa fa-plus fw"></i>', array('class' => 'btn btn-default add1'))) }}
                     
                      {{ HTML::decode(link_to('#', '<i class="fa fa-minus-circle fw"></i>', array('class' => 'btn btn-default remove1'))) }}
                    </td>
                    {{ HTML::script('js/jquery.min.js') }}
                    <script type = "text/javascript">
                      addChallenge();
                    </script>
                 </tr>
                @endforeach
            @else
            <tr>
                <td style="vertical-align: top;">
                  <textarea class='form-control animated challenge' name='challenge[]' clearable rows='2' maxlength='255'></textarea>
                </td>
                <td style="vertical-align: top;">
                  <textarea class='form-control animated actionTaken' name='actionTaken[]' clearable rows='2' maxlength='255'></textarea>
                </td>
                <td style="vertical-align: top;">
                  <textarea class='form-control animated actionRequested' name='actionRequested[]' clearable rows='2' maxlength='255'></textarea>
                </td>
                <td style="vertical-align: top;" >
                  {{ HTML::decode(link_to('#', '<i class="fa fa-plus fw"></i>', array('class' => 'btn btn-default add1'))) }}
                 
                {{ HTML::script('js/jquery.min.js') }}
                <script type = "text/javascript">
                  addChallenge();
                </script>
                 
                </td>
             </tr>
            @endif
          </div>
          </table>
  </div>
</div>
</div>  

{{ Form::submit('Create', ['name' => 'create', 'class' => 'btn btn-primary', 'id' => 'create1']) }}
{{Form::Close()}}
@stop
@section('cssjs')

<script type = "text/javascript">
  $(document).ready(function() {

    $('.add').closest('tr').not('tr:last-child').children().find('.add').hide();
    $('.add1').closest('tr').not('tr:last-child').children().find('.add1').hide();
    $('#assignments > tbody').children(':nth-child(2)').children('td').last().find('.remove').hide();
    $('#myTable > tbody').children(':nth-child(3)').children('td').last().find('.remove1').hide();

    $('form').submit(function(e){
      // e.preventDefault();
      $(this).find('input[type=submit]').attr('disabled', 'disabled');
      $(this).submit();
    });

    function addIssue(event)  {
      var issues = counter;
      var previous= issues-1;
      //alert($(this).closest("tr").find(".leadMeasure"+previous).val());
      // if($.trim($(this).closest("tr").find(".leadMeasure").val())&&$.trim($(this).closest("tr").find(".officeOrUser").val())&&$.trim($(this).closest("tr").find(".status").val()))
      // {
        var id = issues++;
        var newIssue = $("<tr><td style='vertical-align: top;'><textarea class='form-control animated leadMeasure' name='leadMeasure[]' clearable maxlength='1000'></textarea></td><td style='vertical-align: top;'><textarea class='form-control animated officeOrUser' name='officeOrUser[]' clearable maxlength='1000'></textarea></td><td style='vertical-align: top;'><textarea name='status[]' class='form-control animated status' rows='2' clearable maxlength='1000'></textarea></td><td><a href='#' class='btn btn-default add'><i class='fa fa-plus fw'></i></a> <a href='#' class='btn btn-default remove' id='removeIssue"+id+"'><i class='fa fa-minus-circle fw'></i></a></td></tr>");
        add();
        newIssue.find(".add").click(addIssue);
        newIssue.find(".remove").click(removeIssue);
        
        $('#assignments').append(newIssue);
        $(this).hide();
        //event.preventDefault();
      // }
      event.preventDefault();
    }

    function removeIssue(event) {      
      if($(this).closest('tr').is('tr:last-child')){
        $(this).closest('tr').prev().children('td:last').children('a:first').show(); 
      }
      $(this).closest('tr').remove();
       sub();
      event.preventDefault();
    }

    $(".add").click(addIssue);
    $(".remove").click(removeIssue);

    function addChallenges(event)  {

      var issues = counterChallenge;
      var id = issues++;
      var previous = issues--;
      var $parent = $(this).closest("tr");
      
      // if($.trim($parent.find(".challenge").val()) && $.trim($parent.find(".actionTaken").val()) && $.trim($parent.find(".actionRequested").val())) {

      var newChallenge = $("<tr><td style='vertical-align:top;'><textarea type='text' class='form-control animated challenge' id='challenge"+id+"' name='challenge[]' clearable maxlength='255'></textarea></td><td style='vertical-align:top;'><textarea type='text' class='form-control animated actionTaken' id='actionTaken"+id+"' name='actionTaken[]' clearable maxlength='255'></textarea></td><td style='vertical-align:top;'><textarea  type='text' class='form-control animated actionRequested' id='actionRequested"+id+"' name='actionRequested[]' clearable maxlength='255'></textarea></td><td style='vertical-align:top;'><a href='#' class='btn btn-default add1'><i class='fa fa-plus fw'></i></a> <a href='#' class='btn btn-default remove1' id='removeChallenge"+id+"'><i class='fa fa-minus-circle fw'></i></a></td></tr>");
      addChallenge();
      newChallenge.find(".add1").click(addChallenges);
      newChallenge.find(".remove1").click(removeChallenges);
      
      $('#myTable').append(newChallenge);
      event.preventDefault();
      $(this).hide();
    // }
    // else {
    //   event.preventDefault();
    // }
    }


    function removeChallenges(event) { 
      if($(this).closest('tr').is('tr:last-child')){
        $(this).closest('tr').prev().children('td:last').children('a:first').show(); 
      }
      $(this).closest('tr').remove();
       subChallenge();
      event.preventDefault();
    }

    $(".add1").click(addChallenges);
    $(".remove1").click(removeChallenges);

    // $( document ).on( "click", "a.add1", addChallenges);
    // $( document ).on( "click", "a.remove1", removeChallenges);

  });
</script>

@stop


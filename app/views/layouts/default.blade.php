<!DOCTYPE html>
<html lang="en" ng-app="dswdApp">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>DSWD - Online Performance Management System</title>
  {{ HTML::style('css/bootstrap.css') }}
  {{ HTML::style('css/grg.css') }}
  {{ HTML::style('css/bootstrap-theme.min.css') }}
  {{ HTML::style('css/datepicker.css') }}
  {{ HTML::style('font-awesome-4.0.3/css/font-awesome.min.css') }}
  {{ HTML::style('css/custom-fonts/font.css') }}
  {{ HTML::style('css/summernote.css') }}
  {{ HTML::style('vendor/tooltipster-master/css/tooltipster.css') }}
  @yield('css')
  {{ HTML::style('css/main.css') }}
  <link rel="shortcut icon" href="{{ asset('opms.ico') }}">
</head>
<body>
  <!-- Main Navigation -->
	<div class="navbar navbar-inverse navbar-fixed-top main-nav" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/"><img class="opms-logo" width="30px" src= "{{ asset('opms.ico') }}"> OPMS</a>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <!-- <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="/">Dashboard</a></li> -->
          @if(Role::accessScorecard())
              <ul class="nav navbar-nav">
                @if(Role::access('0'))
                  <li class="dropdown {{ OPMSForm::agencysc(Request::path())? 'active':'' }}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Agency Scorecard <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      <li>{{ link_to(URL::route('scorecards.current'), 'Current Agency Scorecard', ['class' => Scorecard::current() ? '':'inactive-subnav']) }}</li>
                      <li class="divider"></li>
                      <li class="dropdown-header">MANAGE SCORECARD</li>
                      <li>{{ link_to(URL::route('scorecards.index'), 'Scorecard List') }}</li>
                      <li>{{ link_to('/manageperspective', 'Perspectives', ['class' => Scorecard::current() ? '':'inactive-subnav']) }}</li>
                      <li>{{ link_to('/managestratobj', 'Strategic Goals/Objectives', ['class' => Scorecard::current() ? '':'inactive-subnav']) }}</li>
                      <li>{{ link_to('/stratInit', 'Initiative Profiles', ['class' => Scorecard::current() ? '':'inactive-subnav']) }}</li>
                      <li>{{ link_to('/measurelist', 'Measure Profiles', ['class' => Scorecard::current() ? '':'inactive-subnav']) }}</li>
                      <li>{{ link_to(URL::route("indicators.index"), 'SG Performance Indicators', ['class' => Scorecard::current() ? '':'inactive-subnav']) }}</li>
                      <li>{{ link_to(URL::route("indicators.index2", ['id' => 0]), 'SO Performance Indicators', ['class' => Scorecard::current() ? '':'inactive-subnav']) }}</li>
                    </ul>
                  </li>
                  @elseif(count(Auth::user()->projectMilestones))
                    <li class="dropdown {{ OPMSForm::agencysc(Request::path())? 'active':'' }}">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Agency Scorecard <b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        <li>{{ link_to(URL::route('scorecards.current'), 'Current Agency Scorecard', ['class' => Scorecard::current() ? '':'inactive-subnav']) }}</li>
                        <li>{{ link_to('/stratInit', 'Initiative Profiles', ['class' => Scorecard::current() ? '':'inactive-subnav']) }}</li>
                    </ul>
                    </li>
                  @else
                    <li class="{{Request::is('scorecards/current') ? 'active' : ''}} {{Scorecard::current()? '' : 'inactive-nav'}}">
                      {{ link_to(URL::route('scorecards.current'), 'Agency Scorecard') }}
                    </li>
                  @endif
                  <li class="{{ OPMSForm::accomplishments(Request::path())? 'active':'' }} {{Scorecard::current()? '' : 'inactive-nav'}}">{{ link_to(URL::route('accomplishments.year', ['year' => date("Y")]), 'SG Accomplishments') }}</li>
                  @if(Role::access('0'))
                    <li class="dropdown {{ OPMSForm::officesb(Request::path())? 'active':'' }} {{Scorecard::current()? '' : 'inactive-nav'}}">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Office Scoreboards <b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        <li>{{ link_to('/officesb', 'Field Office Scoreboards') }}</li>
                        <li>{{ link_to('/officesb/central/', 'Central Office Scoreboards') }}</li>

                      </ul>
                    </li>
                  @elseif(Role::access('13'))
                    <li class="{{ OPMSForm::officesb(Request::path())? 'active':'' }} {{Scorecard::current()? '' : 'inactive-nav'}}">
                      {{ link_to('/officesb', 'Office Scoreboards') }}</li>
                    </li>
                  @elseif(Role::access('24'))
                    <li class="{{ OPMSForm::officesb(Request::path())? 'active':'' }} {{Scorecard::current()? '' : 'inactive-nav'}}">
                      {{ link_to('/officesb/central/', 'Office Scoreboards') }}
                    </li>
                  @endif
                  <li class="{{ OPMSForm::opcr(Request::path())? 'active':'' }} {{Scorecard::current()? '' : 'inactive-nav'}}">{{ link_to('/opcr', 'OPCR') }}</li>
                  @if(Role::access('012345'))
                  <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports <b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        <li>{{ link_to('/report/sg/1', 'Strategic Goal 1') }}</li>
                        <li>{{ link_to('/report/sg/2', 'Strategic Goal 2') }}</li>
                        <li>{{ link_to('/report/sg/3', 'Strategic Goal 3') }}</li>


                      </ul>
                    </li>
                  @endif
              </ul>

          @else
            <li class="{{Request::is('scorecards/current') ? 'active' : ''}} {{Scorecard::current()? '' : 'inactive-nav'}}">
              {{ link_to(URL::route('scorecards.current'), 'Agency Scorecard') }}
            </li>
            <li class="{{Request::is('accomplishments') ? 'active' : ''}} {{Scorecard::current()? '' : 'inactive-nav'}}">
              {{ link_to(URL::route('accomplishments.year', ['year' => date("Y")]), 'Strategic Goals Accomplishments') }}
            </li>
          @endif
        </ul>
        <div class="nav pull-right">
          <ul class="nav navbar-nav">
            @if ( Auth::guest() ) 
            <li>{{ HTML::decode(HTML::link('login', '<i class="fa fa-sign-in"></i> Login')) }}</li>
            @else
            <!-- <li>{{ HTML::decode(HTML::link('logout', '<i class="fa fa-sign-out"></i> Logout')) }}</li> -->

            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  <i class="fa fa-user fa-fw"></i> {{Auth::user()->firstName." ".Auth::user()->lastName}} <i class="fa fa-caret-down"></i>
              </a>
              <ul class="dropdown-menu dropdown-user">
                  <!-- @if(Auth::user()->role==0)
                    <li><a href="/systemsettings"><i class="fa fa-cog"></i> System Settings</a>
                    </li>
                  @endif
                  <li><a href="/userprofile/"><i class="fa fa-user fa-fw"></i> User Profile</a>
                  </li>
                  <li class="divider"></li>
                  <li>{{ HTML::decode(HTML::link('logout', '<i class="fa fa-sign-out"></i> Logout')) }}
                  </li> -->
                  <li>
                    <a href="/userprofile/"><i class="fa fa-user fa-fw"></i> User Profile</a>
                  </li>
                  @if(Role::access('0'))
                  <li> 
                      
                      {{ HTML::decode(link_to(URL::route('systemsettings.stratmap'), '<i class="fa fa-cog fa-fw"></i> Manage Homepage')) }} 
                  </li>
                  @endif
                  @if(Role::accessAdministration())
                  <li class="divider"></li>
                  <li class="dropdown-header">ADMINISTRATION</li>
                  <li>{{ HTML::decode(link_to('/offices', '<i class="fa fa-cogs fa-fw"></i> Manage Offices')) }}</li>
                  <li>{{ HTML::decode(link_to('/users', '<i class="fa fa-users fa-fw"></i> Manage Users')) }}</li>
                  @endif
                  <li class="divider"></li>
                  <li>{{ HTML::decode(HTML::link('logout', '<i class="fa fa-sign-out fa-fw"></i> Logout')) }}</li>
              </ul>
              <!-- /.dropdown-user -->
            </li>
            @endif
            
          </ul>
        </div>
      </div><!--/.navbar-collapse -->
    </div>
  </div>
    @yield('banner')
  <!-- Main Content -->
  <div class="main-content">
  <div class="container">
    @if(Role::access('34') && $office = Auth::user()->office)
      @if($count = $office->scoreboardsForApproval())
        <a href="{{ Role::access('3') ? '/officesb' : '/officesb/central' }}">
          <div class="span2 well bg-info">You have {{ $count }} Office Scoreboards ready for approval</div>
        </a>
      @endif
      @if($count = $office->opcrForApproval())
        <a href="/opcr">
          <div class="span2 well bg-info">You have {{$count}} OPCs ready for approval</div>
        </a>
      @endif
    @endif
    @yield('content')
  </div>
  </div>

  <div class="container container2 hidden-print">
    <!-- Example row of columns -->
    <div class="row">
      <div class="col-sm-6 col-md-4">
        <div class="thumbnail ft-box">
          <!-- <img data-src="holder.js/300x200" alt="..."> -->
          <div class="caption">
            <h3>VISION 2030</h3>
            <p>We envision a society where the poor, vulnerable and disadvantaged are empowered for an improved quality of life. Towards this end, DSWD will be the world's standard for the delivery of coordinated social services and social protection for poverty reduction by 2030.</p>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="thumbnail ft-box">
          <!-- <img data-src="holder.js/300x200" alt="..."> -->
          <div class="caption">
            <h3>MISSION</h3>
            <p>To develop, implement and coordinate social protection and poverty reduction solutions for and with the poor, vulnerable and disadvantaged.
            </p>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="thumbnail ft-box">
          <!-- <img data-src="holder.js/300x200" alt="..."> -->
          <div class="caption">
            <h3>VALUES</h3>
            <ul>
              <li>Respect for Human Dignity</li>
              <li>Integrity</li>
              <li>Service Excellence</li>
            </ul>
          </div>
        </div>
      </div>

    </div>

    <hr class="ft-divider">

    <footer>
      <div>
      <span class="pull-left">
      <p>Developed by <a href="http://solutionsresource.com/" target="_BLANK">Solutions Resource INC.</a> All rights reserved. &copy; {{ date('Y') }}</p>
      </span>

      <span class="pull-right">Powered by <img class="laravel-logo" src="{{ asset('img/laravel-four-icon.png') }}"> <a href="http://laravel.com/" target="_BLANK">LARAVEL</a></span>
      </div>
    </footer>
  </div> <!-- /container -->

<!-- Notifications -->
  <?php
    $action = Session::get('notification');
    $affected_users = Session::get('affected_users');
    $link = Session::get('link');

    if($action && $affected_users)
      Notifications::notify($action, $affected_users, $link);
  ?>

  <!-- Success-Messages -->
    @if ($message = Session::get('success'))
    <div class="alert notif">
      <!-- <button type="button" class="close" data-dismiss="alert">&times;</button> -->
      <span class="message">{{{ $message }}}</span>
      </div>
    @endif
    {{ Form::hidden('dirty_page', 0, ['id' => 'dirty_page']) }}

  <!-- section for JS and CSS -->
  {{ HTML::script('js/jquery.min.js') }}
  {{ HTML::script('js/jquery.easing.1.3.js') }}
  {{ HTML::script('js/bootstrap.min.js') }}
  {{ HTML::script('js/bootstrap-datepicker.js') }}
  {{ HTML::script('js/angular.min.js') }}
  {{ HTML::script('js/config.js') }}
  {{ HTML::script('js/summernote.min.js') }}
  {{ HTML::script('js/wurfl.js') }}
  {{ HTML::script('vendor/tooltipster-master/js/jquery.tooltipster.min.js') }}
  {{ HTML::script('vendor/autosize-master/jquery.autosize.min.js') }}
  {{ HTML::script('js/main.js') }}

  @yield('cssjs')

  <style>
    .box_rotate {
     -moz-transform: rotate(-90deg);  /* FF3.5+ */
     -o-transform: rotate(-90deg);  /* Opera 10.5 */
     -webkit-transform: rotate(-90deg);  /* Saf3.1+, Chrome */
     filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
     -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
   }
 </style>

</body>
</html>
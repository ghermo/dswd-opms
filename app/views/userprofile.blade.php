@extends('layouts.default')
@section('content')
	<h2>User Profile: {{ $editUser->firstName }} {{ $editUser->lastName }} </h2>
	{{ Form::open(['url' => "/updateprofile/$editUser->id", 'name' => 'create1', 'class' => 'form-horizontal col-md-6', 'role' => 'form']) }}
		<div class = "form-group">
			{{ Form::label('username', 'Username:'); }}
			{{ Form::text('username', $editUser->username, ['class' => 'form-control', 'placeholder' => 'Enter Username', 'disabled' => 'disabled']) }}
		</div>
		<div class = "form-group">
			{{ Form::label('firstName', 'First Name:'); }}
			{{ Form::text('first_name', $editUser->firstName, ['class' => 'form-control', 'placeholder' => 'Enter First Name', 'disabled']) }}
		</div>
		<div class = "form-group">
			{{ Form::label('lastName', 'Last Name:'); }}
			{{ Form::text('last_name', $editUser->lastName, ['class' => 'form-control', 'placeholder' => 'Enter Last Name','disabled']) }}
		</div>
		<div class = "form-group">
			{{ Form::label('email', 'Email Address:'); }}{{ $errors->first('email', '<span class=errormsg>*:message</span>') }}<span class="descriptionMsg">(You may edit email.)</span>
			
			{{ Form::text('email', $editUser->email, ['class' => 'form-control', 'placeholder' => 'Enter Email Address']) }}

			
			
		</div>
		<hr/>
		<div class = "form-group">
			{{ Form::label('password', 'Password:'); }} <span class="descriptionMsg">(Leave empty to retain the password.)</span>
			{{ $errors->first('password', '<span class=errormsg>*:message</span>') }}
			{{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Enter Password']) }}
		</div>
		<div class = "form-group">
			{{ Form::label('cpassword', 'Confirm Password:'); }}
			{{ $errors->first('cpassword', '<span class=errormsg>*:message</span>') }}
			{{ Form::password('cpassword', ['class' => 'form-control', 'placeholder' => 'Confirm Password']) }}
		</div>
		<div class = "form-group">
				{{ Form::label('role', 'Role:'); }}
				{{ Form::select('role', $roles, $editUser->role, ['id' => 'roles', 'class' => 'form-control roles','disabled']) }}
		</div>
		<div  class="form-group">
			{{ Form::label('offices', 'Office:'); }}
			{{ Form::text('offices', $offices->office_name, ['id' => 'roles', 'class' => 'form-control roles','disabled']) }}
		</div>
		
		<div class = "form-inline form-group">
			{{ Form::submit('Save Changes', ['name' => 'create1', 'class' => 'btn btn-primary', 'id' => 'create1']) }}
			{{ link_to( '/', 'Cancel', array('class'=>'btn btn-default') ) }}
		</div>
		
	{{ Form::close() }}
</div>
</div>
@stop
@extends('layouts.default')
@section('content')
{{ Form::open(['url' => "/officesb/field/updatesb/".$sc->id, 'name' => 'myForm', 'class' => 'form-horizontal col-md-12', 'role' => 'form']) }}	
<h2 class="pull-left">{{ HTML::decode (link_to( "/officesb", '<i class="fa fa-chevron-left fw"></i>', array('class'=>'link-black','title'=>'Field Office Scoreboard List') )) }} Edit Field Office Scoreboard
</h2>
<hr class="clear" />
<div class="well">
	<strong>Office:</strong> {{$sc->author->office_name}} <br />
	<strong>Date:</strong> {{$scoreboard[0]->date}} <br />
	<strong>Strategic Initiatives done by FO for this SG:</strong>
</div>
{{Form::hidden('type',0)}}
<h4>Office Scoreboard for {{ $sb->first()->year }}</h4>
<?php $number=1;?>
@for($i=0;isset($scoreboard[$i]);$i++)
	
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
			        {{ Form::label('goal1', 'Strategic Goal #'.($i+1).': ') }}
		            <strong>{{Form::hidden('goal'.$i,$scoreboard[$i]->objective->objective)}}</strong>
			    </div>

		        <div class="panel-body">{{$scoreboard[$i]->objective->objective}}</div>
		          	<table class="table table-borderless" id="assignments">
			  			<tr>
			              <td colspan="2">
			                {{ Form::label('baseline1', 'Baseline for: ') }}
			                <b>{{ Form::select('baseline'.$i, $years, $scoreboard[$i]->baseline, ['class' => 'baseline'])}}  </b>
			             
			              </td>
			              <td colspan="2">
			                {{ Form::label('month1', 'Movements for the month of: ') }} 
			                <b>{{ Form::select('month'.$i, $months, $scoreboard[$i]->month, ['class' => 'baseline'])}} </b>
			              </td>
			            </tr>
			            <?php $id=1;?>
			           
			          <?php $printLabel1=[];
			          $printValue1=[];//universe and accomplishment
			          $printLabel2=[];
			          $printValue2=[]; ?>
			          <?php $indi=0?>

			          	@foreach($scoreboard[$i]->accomplishments as $item)
	            			@if($item->indicator->type==0 || $item->indicator->type==1 || $item->indicator->type==3)
		            			<?php array_push($printLabel1,$item->indicator->indicator) ?>
		            			<?php 
		            				$value = $item->value;

		            				switch($item->indicator->type)
		            				{
		            					case 0:
		            						if(isset($baseline[$scoreboard[$i]->objective->id]))
			            						$value = $baseline[$scoreboard[$i]->objective->id];
			            					break;
		            					case 1:
		            						if(isset($universe[$scoreboard[$i]->objective->id]))
			            						$value = $universe[$scoreboard[$i]->objective->id];
			            					break;
		            					case 2:
		            						if(isset($target[$scoreboard[$i]->objective->id]))
			            						$value = $target[$scoreboard[$i]->objective->id];
			            					break;
		            				}
		            				array_push( $printValue1, $value) 
		            			?>
	            			@elseif($item->indicator->type==2)
		            			<?php array_push($printLabel2, $item->indicator->indicator) ?>
		            			<?php array_push( $printValue2, $item->value) ?>
	            			@endif
                            {{ Form::hidden('type'.$i.$indi, $item->indicator->type) }}
							{{ Form::hidden('id'.$i.$indi, $item->id) }}
                            <?php $indi++;?>
			            @endforeach
			            
			           <?php $indi=0?>
			          	@for($i2 = 0; $i2 < max([count($printLabel1), count($printLabel2)]); $i2++)
			          		<tr>

			          			<td colspan="2">
				          			@if(isset($printLabel1[$i2]))
				          				{{Form::label('accomplishment'.$i.$indi, $printLabel1[$i2], ['class' => 'label-normal']).": "}}
				          				{{ $errors->first('accomplishment'.$i.$indi, '<span class=errormsg>*:message</span>') }}
				          				{{Form::input((isset($baseline[$scoreboard[$i]->objective->id]) && ! Role::access('0')) ? 'hidden' : 'text' , 'accomplishment'.$i.$indi,$printValue1[$i2],  ['class'=>'form-control'])}}
				          				@if(isset($baseline[$scoreboard[$i]->objective->id]) && ! Role::access('0'))
				          					{{ $printValue1[$i2] }}
				          				@endif
					          		@else
					          			&nbsp;	
				          			@endif
				          			<?php $indi++?>
			          			</td>
			          			<td colspan="2">
				          			@if(isset($printLabel2[$i2]))
				          				{{Form::label('',$printLabel2[$i2], ['class' => 'label-normal']).": "}}
					          			{{ $errors->first('accomplishment'.$i.$indi, '<span class=errormsg>*:message</span>') }}
					          			{{Form::text('accomplishment'.$i.$indi,$printValue2[$i2],  ['class'=>'form-control'])}}
					          		@else
					          			&nbsp;
					          			
				          			@endif
				          			<?php $indi++?>
			          			</td>
			          		</tr>
			          	@endfor

			            <tr class="active">
              <td>
                <strong><center>Lead Measures *</center> </strong>
                
              </td>
              <td>
                <strong><center>Responsible Person/Division*</center> </strong>
               
              </td>
              <td colspan>
                <strong><center>Status (as to Targets and Timeline)*</center></strong>
                
              </td>
              <td>
                <strong>Add/Remove</strong>
              </td>
            </tr>
            <script type = "text/javascript">
              var counter=0;
              function add()
              {
                counter++;
                
              }
              function sub()
              {
                counter--;
                
              }
            </script>
            <?php $goal=0;?>
            @if(count(Input::old()))

             @for($goal = 0; count(Input::old('leadMeasure'.$i.$goal)); $goal++)
                 <tr class="assign" id="<?php echo $i ?>">
                    <td style='vertical-align:top;'>
                      {{ $errors->first('leadMeasure'.$i.$goal, '<span class=errormsg>*:message</span>') }}
                      {{ Form::textarea('leadMeasure'.$i.$goal,null  , ['class'=>'form-control leadMeasure','clearable','rows'=>'2','maxlength'=>'1000']) }}
                    </td>
                    <td style='vertical-align:top;'>
                      {{ $errors->first('officeOrUser'.$i.$goal, '<span class=errormsg>*:message</span>') }}
                      {{ Form::textarea('officeOrUser'.$i.$goal, null, ['class'=>'form-control officeOrUser','clearable','rows'=>'2','maxlength'=>'1000']) }}
                    </td>
                    <td style='vertical-align:top;'>
                      {{ $errors->first('status'.$i.$goal, '<span class=errormsg>*:message</span>') }}
                      {{ Form::textarea('status'.$i.$goal, null, ['class'=>'form-control status','clearable','rows'=>'2','maxlength'=>'1000']) }}
                    </td>
                    <td>
                      {{ HTML::decode(link_to('#', '<i class="fa fa-plus fw"></i>', array('class' => 'btn btn-default add'))) }}
                     	@if($goal>0)
	                      {{ HTML::decode(link_to('#', '<i class="fa fa-minus-circle fw"></i>', array('class' => 'btn btn-default remove'))) }}
	                      @endif
                    </td>
                    {{ HTML::script('js/jquery.min.js') }}
                    <script type = "text/javascript">
                      add();
                    </script>
                 </tr>
              @endfor

            @elseif(count($scoreboard[$i]->assignments()->get()))
               @foreach($scoreboard[$i]->assignments()->get() as $item)
               <tr class="assign" id="<?php echo $i ?>">
                  <td style='vertical-align:top;'>
                    {{ $errors->first('leadMeasure'.$i.$goal, '<span class=errormsg>*:message</span>') }}
                    {{ Form::textarea('leadMeasure'.$i.$goal, $item->lead_measure , ['class'=>'form-control leadMeasure','clearable','rows'=>'2','maxlength'=>'1000']) }}
                  </td>
                  <td style='vertical-align:top;'>
                    {{ $errors->first('officeOrUser'.$i.$goal, '<span class=errormsg>*:message</span>') }}
                    {{ Form::textarea('officeOrUser'.$i.$goal, $item->responsible_office, ['class'=>'form-control officeOrUser','clearable','rows'=>'2','maxlength'=>'1000']) }}
                  </td>
                  <td style='vertical-align:top;'>
                    {{ $errors->first('status'.$i.$goal, '<span class=errormsg>*:message</span>') }}
                    {{ Form::textarea('status'.$i.$goal, $item->status, ['class'=>'form-control status','clearable','rows'=>'2','maxlength'=>'1000']) }}
                  </td>
                  <td>
                    {{ HTML::decode(link_to('#', '<i class="fa fa-plus fw"></i>', array('class' => 'btn btn-default add'))) }}
                   @if($goal>0)
                    {{ HTML::decode(link_to('#', '<i class="fa fa-minus-circle fw"></i>', array('class' => 'btn btn-default remove'))) }}
                    @endif
                  </td>
                  {{ HTML::script('js/jquery.min.js') }}
                  <script type = "text/javascript">
                    add();
                  </script>    
               </tr>
               <?php $goal++;?>
              @endforeach

             
             @else
               <tr class="assign" id="<?php echo $i ?>">
                  <td style='vertical-align:top;'>
                    {{ $errors->first('leadMeasure'.$i.$goal, '<span class=errormsg>*:message</span>') }}
                    <br>{{ Form::textarea('leadMeasure'.$i.$goal,null  , ['class'=>'form-control leadMeasure','clearable','rows'=>'2','maxlength'=>'1000']) }}
                  </td>
                  <td style='vertical-align:top;'>
                    {{ $errors->first('officeOrUser'.$i.$goal, '<span class=errormsg>*:message</span>') }}
                    <br>{{ Form::textarea('officeOrUser'.$i.$goal, null, ['class'=>'form-control officeOrUser','clearable','rows'=>'2','maxlength'=>'1000']) }}
                  </td>
                  <td style='vertical-align:top;'>
                    {{ $errors->first('status'.$i.$goal, '<span class=errormsg>*:message</span>') }}
                    <br>{{ Form::textarea('status'.$i.$goal, null, ['class'=>'form-control status','clearable','rows'=>'2','maxlength'=>'1000']) }}
                  </td>
                  <td>
                    {{ HTML::decode(link_to('#', '<i class="fa fa-plus fw"></i>', array('class' => 'btn btn-default add'))) }}
                   
                    {{ HTML::decode(link_to('#', '<i class="fa fa-minus-circle fw"></i>', array('class' => 'btn btn-default remove'))) }}
                  </td>
                  {{ HTML::script('js/jquery.min.js') }}
                  <script type = "text/javascript">
                    add();
                  </script>

               </tr>
             @endif
		          	</table>
			</div>

			<h4>Narrative</h4>
			<div class="panel panel-primary">
				<div class="panel-heading">
			        {{ Form::label('statusDate1', 'Status as of: ') }}
			        <strong>{{ $scoreboard[$i]->statusDate }}</strong>
			    </div>

		        <div class="panel-body">{{ Form::label('narrative1', 'Key Accomplishments: Activities Completed, Coordination done with Partner OBS and Issues Resolved (if any) ') }}
		        </div>
	          	<table class="table table-borderless">
		  			<tr>
		              <td>
		                 {{ Form::textarea('narrative'.$i, $scoreboard[$i]->narrative, ['class'=>'form-control summernote','rows'=>'2']) }} 
		              </td>
		            </tr>
	          	</table>
			</div>		
		</div>
	</div>
	
@endfor

<div class="panel panel-primary">
    <div class="panel-heading"><strong>Issues and Challenges (if any)</strong></div>
    <div class="panel-body"><em>Please report major issues in this format, if applicable</em></div>
    <table class="table table-borderless no-top-border" id="issuestable">
        <thead>
          <tr class="active">
            <td style='vertical-align:top;'>
                <strong><i>Identify issues and challenges encountered:</i> </strong>Specify details under each element, if any
            </td>
            <td style='vertical-align:top;'>
                <strong><i>Action being taken by team:</i> </strong>Summary of the Office or team’s plan to resolve it, including key resources involved, and indicative target by when the identified issue shall have been addressed or resolved 
            </td>
            <td style='vertical-align:top;'>
                <strong><i>Action requested from Management or other OBS (if any):</i> </strong>Please indicate desired assistance/intervention from Management or other OBSto facilitate the achievement of the Office Breakthrough. 
            </td>
            <td style='vertical-align:top;'>
                <strong>Add/Remove</strong>
            </td>
          </tr>
        </thead>
        <tbody>
        	<?php $i = 0; ?>
        	<script type = "text/javascript">
			              var counterIssue=0;
			              
			              function addcIssue()
			              {
			                counterIssue++;
			                
			              }
			              function subcIssue()
			              {
			                counterIssue--;
			                
			              }
			              
			            </script>
			             @if(count(Input::old()))
			                @for($i = 0; count(Input::old('issuetd1'.$i)); $i++)
			                 <tr class="assign" >
			                    <td >
				                {{ $errors->first('issuetd1'.$i, '<span class=errormsg>*:message</span>') }}
				                {{ Form::textarea('issuetd1'.$i,null  , ['class'=>'form-control td1','clearable','rows'=>'2','maxlength'=>'255']) }}
				              </td>
				              <td >
				                 {{ $errors->first('issuetd2'.$i, '<span class=errormsg>*:message</span>') }}
				                {{ Form::textarea('issuetd2'.$i, null, ['class'=>'form-control td2','clearable','rows'=>'2','maxlength'=>'255']) }}
				              </td>
				              <td >
				                {{ $errors->first('issuetd3'.$i, '<span class=errormsg>*:message</span>') }}
				                {{ Form::textarea('issuetd3'.$i, null, ['class'=>'form-control td3','clearable','rows'=>'2','maxlength'=>'255']) }}
				              </td>
			                    <td>
			                      {{ HTML::decode(link_to('#', '<i class="fa fa-plus fw"></i>', array('class' => 'btn btn-default add2'))) }}
			                     
			                      {{ HTML::decode(link_to('#', '<i class="fa fa-minus-circle fw"></i>', array('class' => 'btn btn-default remove2'))) }}
			                    </td>

			                    <script type = "text/javascript">
			                      addcIssue();
			                    </script>
			                </tr>
			                @endfor
			              
			              @elseif(count($scoreboard[0]->challenge()->get()))
             @foreach($scoreboard[0]->challenge()->get() as $item)
             <tr>
                <td class="width-20" style="vertical-align:top;">{{ Form::textarea('issuetd1'.$i, $item->issue, ['class'=>'form-control td1','clearable','rows'=>'2','maxlength'=>'255']) }}</td>
                <td style="vertical-align:top;">{{ Form::textarea('issuetd2'.$i, $item->action_taken, ['class'=>'form-control td2','clearable','rows'=>'2','maxlength'=>'255']) }}</td>
                <td style="vertical-align:top;">{{ Form::textarea('issuetd3'.$i, $item->action_requested, ['class'=>'form-control td3','clearable','rows'=>'2','maxlength'=>'255']) }}</td>
                <td style="vertical-align:top;">
                  {{ HTML::decode(link_to('#', '<i class="fa fa-plus fw"></i>', array('class' => 'btn btn-default add2'))) }}
                  @if($i>0)
                  {{ HTML::decode(link_to('#', '<i class="fa fa-minus-circle fw"></i>', array('class' => 'btn btn-default remove2'))) }}
                  @endif
                  <?php $i++;?>
                </td>
                <!--{{ HTML::script('js/jquery.min.js') }}-->
                <script type = "text/javascript">
                  addcIssue();
                </script>
             </tr>
             @endforeach

             
             @else
             <tr>
                <td class="width-20" style="vertical-align:top;">{{ Form::textarea('issuetd1'.$i, ' ', ['class'=>'form-control td1','clearable','rows'=>'2','maxlength'=>'255']) }}</td>
                <td style="vertical-align:top;">{{ Form::textarea('issuetd2'.$i, null, ['class'=>'form-control td2','clearable','rows'=>'2','maxlength'=>'255']) }}</td>
                <td style="vertical-align:top;">{{ Form::textarea('issuetd3'.$i, null, ['class'=>'form-control td3','clearable','rows'=>'2','maxlength'=>'255']) }}</td>
                <td style="vertical-align:top;">
                  {{ HTML::decode(link_to('#', '<i class="fa fa-plus fw"></i>', array('class' => 'btn btn-default add2'))) }}
                  <script type = "text/javascript">
                  addcIssue();
                </script>
                  
                </td>
             </tr>
             @endif
			              
        </tbody>
	</table>
</div>

<div class="panel panel-default remarks">
	<div class="panel-heading">
		Remarks
		<div class="pull-right">
			{{Form::label('filter', 'Filter:') }} 
			{{ Form::select('filter', ['Comments', 'Suggestions', 'All'], 2, ['class' => 'baseline filter'])}}
		</div>
	</div>
	<div class="panel-body">
		<div class="list-group">
			@if(count($remarks))
				<?php $show_more = true; ?>
				<div class="show-more">
					@foreach($remarks as $key => $remark)
						@if($show_more && ((count($remarks)-3) <= $key))
							</div>
							<?php $show_more = false; ?>
						@endif
						<div class="list-group-item remark{{$remark->type}}">
							<strong> {{ $remark->user->fullName() }} </strong>
							{{ $remark->remark }} <br/>
							<i class="livetimestamp"> 
								{{ $remark->type? "suggested" : "commented"}} {{ $remark->created_at->diffForHumans() }} 
							</i>
						</div>
					@endforeach
				@if(count($remarks) > 3)
					<div class="list-group-item">
						<center>
							{{ Form::button('See All', ['class' => 'btn btn-default remarks-collapse'])}}
						</center>
					</div>
				@endif
			@endif
		</div>
	</div>
</div>

{{ Form::submit('Save', ['name' => 'edit', 'class' => 'btn btn-primary', 'id' => 'create1']) }}
{{Form::Close()}}

@stop
@section('cssjs')

<script type = "text/javascript">
  $(document).ready(function() {
  
  	$('.add').closest('tr').not('tr:last-child').children().find('.add').hide();
    $('.add2').closest('tr').not('tr:last-child').children().find('.add2').hide();

    function addIssue(event)  {
      var issues = counter;
      var id = issues++;
      var goalNum=$(this).closest("#assignments").find('tr:last').attr('id');
      //alert(goalNum);
      var $parent = $(this).closest("tr");
      if($.trim($parent.find(".leadMeasure").val()) && $.trim($parent.find(".officeOrUser").val()) && $.trim($parent.find(".status").val())){
      var newIssue = $("<tr class='assign' id='"+goalNum+"''><td style='vertical-align:top;'><textarea class='form-control animated leadMeasure' name='leadMeasure"+goalNum+id+"' clearable rows'2' maxlength='1000'></textarea></td><td style='vertical-align:top;'><textarea class='form-control animated officeOrUser' name='officeOrUser"+goalNum+id+"' clearable rows'2' maxlength='1000'></textarea></td><td style='vertical-align:top;'><textarea class='form-control animated status' name='status"+goalNum+id+"' clearable rows'2' maxlength='1000'></textarea></td><td><a href='#' class='btn btn-default add'><i class='fa fa-plus fw'></i></a> <a href='#' class='btn btn-default remove'><i class='fa fa-minus-circle fw'></i></a></td></tr>");
      add();
      newIssue.find(".add").click(addIssue);
      newIssue.find(".remove").click(removeIssue);
      var form = $(this).closest("#assignments");

      form.append(newIssue);
      $(this).hide();
      event.preventDefault();
  	}
  	else {
  		event.preventDefault();
  	}
    }

    function removeIssue(event) { 
      if($(this).closest('tr').is('tr:last-child')){
        $(this).closest('tr').prev().children('td:last').children('a:first').show(); 
      }
      $(this).closest('tr').remove();
    	 sub();
      	// var form = $(this).closest("#assignments");
      	// form.find('tr:last').remove();
      
      event.preventDefault();
    }

    $(".add").click(addIssue);
    $(".remove").click(removeIssue);

function addIssue2(event)  {
      var issues = counterIssue;
      var previous= issues-1;
      var $parent = $(this).closest("tr");
      
      if($.trim($parent.find(".td1").val()) && $.trim($parent.find(".td2").val()) && $.trim($parent.find(".td3").val()))
      {
        var id = issues++;
        var num = id+1
        var newIssue = $("<tr><td style='vertical-align:top;'><textarea class='form-control animated td1' name='issuetd1"+id+"' clearable maxlength='255'></textarea></td><td style='vertical-align:top;'><textarea class='form-control animated td2' name='issuetd2"+id+"' clearable maxlength='255'></textarea></td><td style='vertical-align:top;'><textarea name='issuetd3"+id+"' class='form-control animated td3' rows='2' clearable maxlength='255'></textarea></td><td><a href='#' class='btn btn-default add2'><i class='fa fa-plus fw'></i></a> <a href='#' class='btn btn-default remove2'><i class='fa fa-minus-circle fw'></i></a></td></tr>");
        addcIssue();

        
        $('#issuestable tbody').append(newIssue);
        $(this).hide();
        //event.preventDefault();
      }
      event.preventDefault();
    }

    function removeIssue2(event) {      
      if($(this).closest('tr').is('tr:last-child')){
        $(this).closest('tr').prev().children('td:last').children('a:first').show(); 
      }
      $(this).closest('tr').remove();
       subcIssue();
      event.preventDefault();
    }

    
    $( document ).on( "click", "a.add2", addIssue2);
    $( document ).on( "click", "a.remove2", removeIssue2);

  });
</script>
@stop
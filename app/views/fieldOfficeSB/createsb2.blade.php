@extends('layouts.default')
@section('content')
{{ Form::open(['url' => "/officesb/field/createsb/store", 'name' => 'myForm', 'class' => 'sbForm form-horizontal col-md-12', 'role' => 'form']) }}
<h2 class="pull-left">{{ HTML::decode (link_to( "/officesb", '<i class="fa fa-chevron-left fw"></i>', array('class'=>'link-black','title'=>'Field Office Scoreboard List') )) }} Scoreboard Status Report - Field Office
</h2>
<hr class="clear" />
<div class="well">
<strong>Office:</strong>
	{{$office->office_name}}
	{{Form::hidden('author',$office->office_name)}}
	{{Form::hidden('office_id',$office->id)}}<br />
<strong>Date:</strong> {{date("Y/m/d")}}{{Form::hidden('date',date("Y/m/d"))}}<br />
<strong>Strategic Initiatives done by FO for this SG:</strong>
</div>

{{Form::hidden('type',0)}}
<h4>Office Scoreboard for {{$year}}{{ Form::hidden('year', $year) }}</h4>
@for($goal=0;isset($goals[$goal]->objective);$goal++)
	<div class="row">
		<div class="col-md-12">
			
			<div class="panel panel-primary">
					<div class="panel-heading">
				        {{ Form::label('goal'.$goal, 'Strategic Goal # ') }}<strong>{{($goal+1).": "}}</strong>
				    </div>
		        <div class="panel-body">
	          		 {{ $goals[$goal]->objective }}
	                 <!-- {{ Form::text('', $goals[$goal]->objective, ['class'=>'form-control','disabled']) }} -->
	                 {{ Form::hidden('goal'.$goal, $goals[$goal]->id) }} 
	            </div>
		          	<table class="table table-borderless" id="assignments">
			  			<tr>
			              <td colspan="2">
			                {{ Form::hidden('baseline'.$goal, $year) }}
			                {{ Form::hidden('baseline'.$goal, $year) }}
			                {{ Form::hidden('universe'.$goal, $year) }}
			                
			                 
			              </td>
			              <td colspan="2">
			                {{ Form::label('month'.$goal, 'Movements for the month of: ') }} 
			                {{ Form::hidden('month'.$goal, $month) }}
			                {{ Date::$months[$month] }}
			              </td>
			            </tr>
			            <?php $id=1;?>
			           	
			            @for($i = 0; $i < max([count($goals[$goal]->indicatorsMovement), 3]); $i++)
			            	<tr>
			            		<td colspan="2">
			            			@if($i == 0)
			            				<strong>Universe:</strong><br />
			            				{{ Form::label('universe', $goals[$goal]->indicatorsUniverse[0]->indicator.':', ['class'=>'label-normal']) }}
			            				{{ Form::hidden('type'.$goal.$id, $goals[$goal]->indicatorsUniverse[0]->type) }}
			            				{{ Form::hidden('indicator_id'.$goal.$id, $goals[$goal]->indicatorsUniverse[0]->id) }}
			            				{{ $errors->first('accomplishment'.$goal.$id, '<span class=errormsg>*:message</span>') }}
			            				{{ Form::input(isset($universe[$goals[$goal]->id]) ? 'hidden' : 'text', 'accomplishment'.$goal.$id++, isset($universe[$goals[$goal]->id]) ? $universe[$goals[$goal]->id] : null, ['class'=>'form-control']) }}
			            				@if(isset($universe[$goals[$goal]->id]))
			            					{{$universe[$goals[$goal]->id]}}

			            				@endif
			            			@elseif($i == 1)
			            				<strong>Target:</strong><br />
			            				{{ Form::label('target'.$goal, $goals[$goal]->indicatorsTarget[0]->indicator.':', ['class'=>'label-normal']) }}
			            				{{ Form::hidden('indicator_id'.$goal.$id, $goals[$goal]->indicatorsTarget[0]->id) }}
			            				{{ Form::hidden('type'.$goal.$id, $goals[$goal]->indicatorsTarget[0]->type) }}
			            				{{ $errors->first('accomplishment'.$goal.$id, '<span class=errormsg>*:message</span>') }}
			            				{{ Form::input(isset($target[$goals[$goal]->id]) ? 'hidden' : 'text', 'accomplishment'.$goal.$id++, isset($target[$goals[$goal]->id]) ? $target[$goals[$goal]->id] : null, ['class'=>'form-control']) }}
			            				@if(isset($target[$goals[$goal]->id]))
			            					{{$target[$goals[$goal]->id]}}
			            				@endif
			            			@elseif($i == 2)
			            				<strong>Baseline:</strong><br />
			            				{{ Form::label('baseline'.$goal, $goals[$goal]->indicatorsBaseline[0]->indicator.':', ['class'=>'label-normal']) }}
			            				{{ Form::hidden('indicator_id'.$goal.$id, $goals[$goal]->indicatorsBaseline[0]->id) }}
			            				{{ Form::hidden('type'.$goal.$id, $goals[$goal]->indicatorsBaseline[0]->type) }}
			            				{{ $errors->first('accomplishment'.$goal.$id, '<span class=errormsg>*:message</span>') }}
			            				{{ Form::input(isset($baseline[$goals[$goal]->id]) ? 'hidden' : 'text', 'accomplishment'.$goal.$id++, isset($baseline[$goals[$goal]->id]) ? $baseline[$goals[$goal]->id] : null, ['class'=>'form-control']) }}
			            				@if(isset($baseline[$goals[$goal]->id]))
			            					{{$baseline[$goals[$goal]->id]}}
			            				@endif
			            			
			            			@else
			            				&nbsp;
			            			@endif
			            		</td>
			            		<td colspan="2">
			            			@if(isset($goals[$goal]->indicatorsMovement[$i]))
			            				{{ Form::label('accomplishment'.$goal, $goals[$goal]->indicatorsMovement[$i]->indicator.':', ['class'=>'label-normal']) }}
			            				
			              				{{ Form::hidden('type'.$goal.$id, $goals[$goal]->indicatorsMovement[$i]->type) }}
			            				{{ Form::hidden('indicator_id'.$goal.$id, $goals[$goal]->indicatorsMovement[$i]->id) }}
			            				{{ $errors->first('accomplishment'.$goal.$id, '<span class=errormsg>*:message</span>') }}
			            				{{ Form::text('accomplishment'.$goal.$id++, null, ['class'=>'form-control']) }} 
			         					
			            			@endif
			            		</td>
			            	</tr>
			            @endfor
			            <tr class="active">
			              <td>
			                <strong><center>Lead Measures *</center> </strong>
			                
			              </td>
			              <td>
			                <strong><center>Responsible Person/Division*</center> </strong>
			               
			              </td>
			              <td colspan>
			                <strong><center>Status (as to Targets and Timeline)*</center></strong>
			                
			              </td>
			              <td>
			                <strong>Add/Remove</strong>
			              </td>
			            </tr>
			           <script type = "text/javascript">
			              var counter=0;
			              
			              function add()
			              {
			                counter++;
			                
			              }
			              function sub()
			              {
			                counter--;
			                
			              }
			              
			            </script>

			            <tr class="assign" id="<?php echo $goal ?>">
			              <td  style='vertical-align: top;'>
			                {{ $errors->first('leadMeasure'.$goal.'0', '<span class=errormsg>*:message</span>') }}
			                {{ Form::textarea('leadMeasure'.$goal.'0',null  , ['class'=>'form-control animated leadMeasure','clearable','rows'=>'2','maxlength'=>'1000']) }}
			              </td>
			              <td  style='vertical-align: top;'>
			                 {{ $errors->first('officeOrUser'.$goal.'0', '<span class=errormsg>*:message</span>') }}
			                {{ Form::textarea('officeOrUser'.$goal.'0', null, ['class'=>'form-control animated officeOrUser','clearable','rows'=>'2','maxlength'=>'1000']) }}
			              </td>
			              <td  style='vertical-align: top;'>
			                {{ $errors->first('status'.$goal.'0', '<span class=errormsg>*:message</span>') }}
			                {{ Form::textarea('status'.$goal.'0', null, ['class'=>'form-control animated status','clearable','rows'=>'2','maxlength'=>'1000']) }}</td>
			              <td>
			                {{ HTML::decode(link_to('#', '<i class="fa fa-plus fw"></i>', array('class' => 'btn btn-default add','title'=>'Add Row Below'))) }}
			              </td>

			              {{ HTML::script('js/jquery.min.js') }}
			              <script type = "text/javascript">
			                add();

			              </script>
			            </tr>
			             @if(count(Input::old()))
			                @for($i = 1; count(Input::old('leadMeasure'.$goal.$i)); $i++)
			                 <tr class="assign" id="<?php echo $goal ?>" >
			                    <td  style='vertical-align: top;'>
			                      {{ $errors->first('leadMeasure'.$goal.$i, '<span class=errormsg>*:message</span>') }}
			                      {{ Form::textarea('leadMeasure'.$goal.$i,null  , ['class'=>'form-control animated leadMeasure','clearable','rows'=>'2','maxlength'=>'1000']) }}
			                    </td>
			                    <td  style='vertical-align: top;'>
			                      {{ $errors->first('officeOrUser'.$goal.$i, '<span class=errormsg>*:message</span>') }}
			                      {{ Form::textarea('officeOrUser'.$goal.$i, null, ['class'=>'form-control animated officeOrUser','clearable','rows'=>'2','maxlength'=>'1000']) }}
			                    </td>
			                    <td  style='vertical-align: top;'>
			                      {{ $errors->first('status'.$goal.$i, '<span class=errormsg>*:message</span>') }}
			                      {{ Form::textarea('status'.$goal.$i, null, ['class'=>'form-control animated status','clearable','rows'=>'2','maxlength'=>'1000']) }}
			                    </td>
			                    <td>
			                      {{ HTML::decode(link_to('#', '<i class="fa fa-plus fw"></i>', array('class' => 'btn btn-default add','title'=>'Add Row Below'))) }}
			                     
			                      {{ HTML::decode(link_to('#', '<i class="fa fa-minus-circle fw"></i>', array('class' => 'btn btn-default remove','title'=>'Remove This Row'))) }}
			                    </td>
			                    {{ HTML::script('js/jquery.min.js') }}
			                    <script type = "text/javascript">
			                      add();
			                    </script>
			                 </tr>
			                  
			                 
			                @endfor
			              @endif
			         
		          	</table>
			</div>

			<h4>Narrative</h4>
			<div class="panel panel-primary">
				<div class="panel-heading">
			        {{ Form::label('statusDate'.$goal, 'Status as of: ') }}
	                {{ Form::hidden('statusMonth'.$goal, $month)}} <strong>{{ Date::$months[$month] }}</strong>
	                {{ Form::hidden('statusYear'.$goal, $year)}} <strong>{{ $year }}</strong>
			    </div>
			    <div class="panel-body">
	                {{ Form::label('narrative'.$goal, 'Key Accomplishments: Activities Completed, Coordination done with Partner OBS and Issues Resolved (if any) ') }}

	                {{ Form::textarea('narrative'.$goal, '', ['class'=>'form-control summernote','rows'=>'2']) }}
		        </div>   
			</div>	
			

		</div>
	</div>
	
@endfor

<div class="panel panel-primary">
    <div class="panel-heading"><strong>Issues and Challenges (if any)</strong></div>
    <div class="panel-body"><em>Please report major issues in this format, if applicable</em></div>
	<table class="table table-borderless" id="issuestable">
		<tr class="active">
			              <td style="vertical-align: top;">
			                <strong><i>Identify issues and challenges encountered</i></strong> Specify details under each element, if any
			              </td>
			              <td style="vertical-align: top;">
			                <strong><i>Action being taken by team</i></strong> Summary of the Office or team’s plan to resolve it, including key resources involved, and indicative target by when the identified issue shall have been addressed or resolved
			              </td>
			              <td style="vertical-align: top;">
			                <strong><i>Action requested from Management or other OBS (if any)</i></strong> Please indicate desired assistance/intervention from Management or other OBSto facilitate the achievement of the Office Breakthrough.
			              </td>
			              <td style="vertical-align: top;">
			                <strong>Add/Remove</strong>
			              </td>
			            </tr>
			           <script type = "text/javascript">
			              var counter=0;
			              
			              function add()
			              {
			                counter++;
			                
			              }
			              function sub()
			              {
			                counter--;
			                
			              }
			              
			            </script>

			             @if(count(Input::old()))
			                @foreach (Input::old('issuetd1') as $key => $n)
			                 <tr class="assign" id="<?php echo $goal ?>" >
			                  <td style="vertical-align:top;">
				              	<textarea class='form-control animated challenge' name='issuetd1[]' clearable rows='2' maxlength='255'>{{ $n }}
				              	</textarea>
                    		  </td>
                    		  <td style="vertical-align:top;">
				              	<textarea class='form-control animated challenge' name='issuetd2[]' clearable rows='2' maxlength='255'>{{ Input::old('issuetd2')[$key] }}
				              	</textarea>
                    		  </td>
                    		  <td style="vertical-align:top;">
				              	<textarea class='form-control animated challenge' name='issuetd3[]' clearable rows='2' maxlength='255'>{{ Input::old('issuetd3')[$key] }}
				              	</textarea>
                    		  </td>
			                    <td>
			                      {{ HTML::decode(link_to('#', '<i class="fa fa-plus fw"></i>', array('class' => 'btn btn-default add2','title'=>'Add Row Below'))) }}
			                     
			                      {{ HTML::decode(link_to('#', '<i class="fa fa-minus-circle fw"></i>', array('class' => 'btn btn-default remove2','title'=>'Remove This Row'))) }}
			                    </td>

			                    <script type = "text/javascript">
			                      add();
			                    </script>
			                 </tr>
			                @endforeach
			              @else
			              <tr class="assign" id="<?php echo $goal ?>">
			              <td  style='vertical-align: top;'>
			                <textarea class='form-control animated td1' name='issuetd1[]' clearable maxlength='255'></textarea>
			              </td>
			              <td  style='vertical-align: top;'>
			                <textarea class='form-control animated td2' name='issuetd2[]' clearable maxlength='255'></textarea>
			              </td>
			              <td  style='vertical-align: top;'>
			                <textarea class='form-control animated td3' name='issuetd3[]' clearable maxlength='255'></textarea>
			              </td>
			              <td>
			                {{ HTML::decode(link_to('#', '<i class="fa fa-plus fw"></i>', array('class' => 'btn btn-default add2','title'=>'Add Row Below'))) }}
			              </td>

			              <script type = "text/javascript">
			                add();

			              </script>
			            </tr>
			              @endif
		</table>
</div>
		
{{Form::submit('Create', ['class' => 'btn btn-primary'])}}


{{Form::close()}}
@stop
@section('cssjs')

<script type = "text/javascript">
  $(document).ready(function() {

  	$('.add').closest('tr').not('tr:last-child').children().find('.add').hide();
    $('.add2').closest('tr').not('tr:last-child').children().find('.add2').hide();
    $('#issuestable > tbody').children(':nth-child(3)').children('td').last().find('.remove2').hide()
  
  	$('form').submit(function(e){
      // e.preventDefault();
      $(this).find('input[type=submit]').attr('disabled', 'disabled');
      $(this).submit();
    });

    function addIssue(event)  {
      var issues = counter;
      var id = issues++;
      var goalNum=$(this).closest("#assignments").find('tr:last').attr('id');
      var $parent = $(this).closest("tr");
      
      // if($.trim($parent.find(".leadMeasure").val()) && $.trim($parent.find(".officeOrUser").val()) && $.trim($parent.find(".status").val())) {
      var newIssue = $("<tr class='assign' id='"+goalNum+"''><td style='vertical-align: top;'><textarea class='form-control animated leadMeasure' name='leadMeasure"+goalNum+id+"' clearable rows='2' maxlength='1000'></textarea></td><td style='vertical-align: top;'><textarea class='form-control animated officeOrUser' name='officeOrUser"+goalNum+id+"' clearable rows='2' maxlength='1000'></textarea></td><td style='vertical-align: top;'><textarea class='form-control animated status' name='status"+goalNum+id+"' clearable rows='2' maxlength='1000'></textarea></td><td><a href='#' class='btn btn-default add' title='Add Row Below'><i class='fa fa-plus fw'></i></a> <a href='#' class='btn btn-default remove' title='Remove This Row'><i class='fa fa-minus-circle fw'></i></a></td></tr>");
      add();
      newIssue.find(".add").click(addIssue);
      newIssue.find(".remove").click(removeIssue);
      var form = $(this).closest("#assignments");

      form.append(newIssue);
      $(this).hide();
      // }
      event.preventDefault();
    }

    function removeIssue(event) { 
    	if($(this).closest('tr').is('tr:last-child')){
        $(this).closest('tr').prev().children('td:last').children('a:first').show(); 
      }
      $(this).closest('tr').remove();
    	sub();
      	// var form = $(this).closest("#assignments");
      	// form.find('tr:last').remove();
      
      event.preventDefault();
    }

    $(".add").click(addIssue);
    $(".remove").click(removeIssue);

function addIssue2(event)  {
      var issues = counter;
      var previous= issues-1;
      var $parent = $(this).closest("tr");
      //alert($(this).closest("tr").find(".leadMeasure"+previous).val());
      // if($.trim($parent.find(".td1").val()) && $.trim($parent.find(".td2").val()) && $.trim($parent.find(".td3").val()))
      // {
        var id = issues++;
        var num = id+1;

        var newIssue = $("<tr id='issue"+num+"'><td style='vertical-align: top;'><textarea class='form-control animated td1' name='issuetd1[]' clearable maxlength='255'></textarea></td><td style='vertical-align: top;'><textarea class='form-control animated td2' name='issuetd2[]' clearable maxlength='255'></textarea></td><td style='vertical-align: top;'><textarea name='issuetd3[]' class='form-control animated td3' rows='2' clearable maxlength='255'></textarea></td><td><a href='#' class='btn btn-default add2' title='Add Row Below'><i class='fa fa-plus fw'></i></a> <a href='#' id='remove"+num+"' class='btn btn-default remove2' title='Remove This Row'><i class='fa fa-minus-circle fw'></i></a></td></tr>");
        add();
        
        $('#issuestable').append(newIssue);
        $(this).hide();
        //event.preventDefault();
      // }
      event.preventDefault();
    }

    function removeIssue2(event) {      
      if($(this).closest('tr').is('tr:last-child')){
        $(this).closest('tr').prev().children('td:last').children('a:first').show(); 
      }
      $(this).closest('tr').remove();
      sub();
      event.preventDefault();
    }

    
    $( document ).on( "click", "a.add2", addIssue2);
    $( document ).on( "click", "a.remove2", removeIssue2);


  });
</script>

@stop
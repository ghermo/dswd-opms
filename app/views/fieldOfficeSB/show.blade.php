@extends('layouts.default')
@section('content')

<h2 class="pull-left">{{ HTML::decode (link_to( "/officesb", '<i class="fa fa-chevron-left fw"></i>', array('class'=>'link-black hidden-print','title'=>'Field Office Scoreboard List') )) }} Field Office Scoreboard
</h2>
<div class="btn-group pull-right btn-option hidden-print">
    <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown">
    <i class="fa fa-bars fw"></i> Options <span class="caret"></span>
    </button>
    <ul class="dropdown-menu pull-right btn-option-dropdown" role="menu">
	    @if(Role::access('0'))
		<li>{{ HTML::decode (link_to( URL::route('sb.fieldEdit', ['id' => $sc->id]), '<i class="fa fa-edit fw"></i> Edit' )) }}</li>
			@if($sc->flag)
				<li>{{ HTML::decode (link_to( URL::route('sb.fieldUnsetfinal', ['id' => $sc->id]), '<i class="fa fa-file-text fw"></i> Mark As Draft' ))}}</li>
			@else
				<li>{{ HTML::decode (link_to( URL::route('sb.fieldSetfinal', ['id' => $sc->id]), '<i class="fa fa-check fw"></i> Mark As Final', array('onclick' => "return confirm('Are you sure you want to mark as final?')") ))}}</li>
			@endif
		@elseif( Role::access('3') && ! $sc->flag)
			<li>{{ HTML::decode (link_to( URL::route('sb.fieldEdit', ['id' => $sc->id]), '<i class="fa fa-edit fw"></i> Edit' ))}}</li>
			<li>{{ HTML::decode (link_to( URL::route('sb.fieldSetfinal', ['id' => $sc->id]), '<i class="fa fa-check fw"></i> Mark As Final', array('onclick' => "return confirm('Are you sure you want to mark as final?')") ))}}</li>
		@endif
		<li class="divider"></li>
		<li><a href="javascript:window.print()"><i class="fa fa-file fw"></i> Export as PDF</a></li>
    </ul>
</div>
<hr class="clear" />
<div class="well">
	<strong>Office:</strong> {{$scoreboard[0]->author->office_name}} <br />
	<strong>Date:</strong> {{$scoreboard[0]->date}} <br />
	<strong>Strategic Initiatives done by FO for this SG:</strong>
</div>
{{Form::hidden('type',0)}}
<h4>Office Scoreboard for {{ $scoreboard->first()->year }}</h4>
<?php $number=1;?>
@for($i=0;isset($scoreboard[$i]);$i++)

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
					<div class="panel-heading">
				        {{ Form::label('goal1', 'Strategic Goal #'.$number++) }}
				    </div>

		        <div class="panel-body">{{$scoreboard[$i]->objective->objective}}</div>
		          	<table class="table table-borderless no-top-border" >
			  			<tr>
			              <td colspan="2">
			                {{ Form::label('baseline1', 'Baseline for: ') }}
			                <b>{{ Form::select('', $years, $scoreboard[$i]->baseline, ['class' => 'baseline','disabled'])}}  </b>
			              </td>
			              <td>
			                {{ Form::label('month1', 'Movements for the month of: ') }} 
			                <b>{{ Form::select('', $months, $scoreboard[$i]->month, ['class' => 'baseline','disabled'])}} </b>
			              </td>
			            </tr>
			            <?php $id=1;?>
			           
			          <?php $printLabel1=[];
			          $printValue1=[];//universe and accomplishment
			          $printLabel2=[];
			          $printValue2=[]; ?>

			          	@foreach($scoreboard[$i]->accomplishments as $item)
	            			@if($item->indicator->type==0 || $item->indicator->type==1 || $item->indicator->type==3)
		            			<?php array_push($printLabel1, $item->indicator->indicator) ?>
		            			<?php array_push( $printValue1, $item->value) ?>
	            			@elseif($item->indicator->type==2)
		            			<?php array_push($printLabel2, $item->indicator->indicator) ?>
		            			<?php array_push( $printValue2, $item->value) ?>
	            			@endif		            	
			            @endforeach

			          	@for($i2 = 0; $i2 < max([3, count($printLabel2)]); $i2++)
			          		<tr>
			          			<td colspan="2">
				          			@if(isset($printLabel1[$i2]))
					          				{{Form::label('',$printLabel1[$i2]).": "}}
					          				{{$printValue1[$i2]}}
					          		@else
					          			&nbsp;	
				          			@endif
			          			</td>
			          			<td>
				          			@if(isset($printLabel2[$i2]))
					          			
				          				{{Form::label('',$printLabel2[$i2]).": "}}
					          			{{$printValue2[$i2]}}
					          		@else
					          			&nbsp;
					          			
				          			@endif
			          			</td>
			          		</tr>
			          	@endfor

			            <tr class="active">
			            	<th>Lead Measures</th>
			            	<th>Responsible Person/Division</th>
			            	<th>Status (as to Targets and Timeline)</th>
			            </tr>
			            
			            @foreach($scoreboard[$i]->assignments()->get() as $item)
			               <tr>
			                  <td >
			                    {{ nl2br($item->lead_measure) }}
			                  </td>
			                  <td >
			                    {{ nl2br($item->responsible_office) }}
			                  </td>
			                  <td >
			                    {{ nl2br($item->status) }}
			                  </td>
			                    
			               </tr>
			               
			              @endforeach
		          	</table>
			</div>

			<h4>Narrative</h4>
			<div class="panel panel-primary">
				<div class="panel-heading">
			        {{ Form::label('statusDate1', 'Status as of: ') }}
			        <strong>{{ $scoreboard[$i]->statusDate }}</strong>
			    </div>

		        <div class="panel-body">
		        {{ Form::label('narrative1', 'Key Accomplishments: Activities Completed, Coordination done with Partner OBS and Issues Resolved (if any) ') }}
		        </div>
		          	<table class="table table-borderless no-top-border" >
			  			<tr>
			              <td>
			                 {{ nl2br($scoreboard[$i]->narrative) }} 
			              </td>
			            </tr>
		          	</table>
			</div>		
		</div>
	</div>
	
@endfor
			<div class="panel panel-primary">
				<div class="panel-heading">
			        <strong>Issues and Challenges (if any)</strong>
			    </div>

		        <div class="panel-body"><em>Please report major issues in this format, if applicable</em></div>
      <table class="table table-borderless" id="myTable">
        <thead>
          <tr class="active">
          <td>#</td>
            <td>
              <strong><i>Identify issues and challenges encountered:</i> </strong>Specify details under each element, if any
            </td>
            <td>
              <strong><i>Action being taken by team:</i> </strong>Summary of the Office or team’s plan to resolve it, including key resources involved, and indicative target by when the identified issue shall have been addressed or resolved 
            </td>
            <td>
              <strong><i>Action requested from Management or other OBS (if any):</i> </strong>Please indicate desired assistance/intervention from Management or other OBSto facilitate the achievement of the Office Breakthrough. 
            </td>
          </tr>
        </thead>
        <tbody>
        <?php $i=1;?>
        @foreach($scoreboard[0]->challenge()->get() as $item)
          <?php $i++;?>
        @endforeach
        
        @if($i>1)
          <?php $i=1;?>
          @foreach($scoreboard[0]->challenge()->get() as $item)
          <tr>
            <td width="1%" style="vertical-align:top;">{{$i++;}}</td>
            <td class="width-20" style="vertical-align:top;">{{ nl2br($item->issue) }}</td>
            <td style="vertical-align:top;">{{ nl2br($item->action_taken) }}</td>
            <td style="vertical-align:top;">{{ nl2br($item->action_requested) }}</td>
          </tr>
          @endforeach
        @else
          <tr>
            <td colspan="3"><center><b>There are no challenges/issues. </b></center></td>
          </tr>
        @endif
        </tbody>
      </table>
      </div>


<div class="panel panel-default remarks">
	<div class="panel-heading">
		Remarks
		<div class="pull-right hidden-print">
			{{Form::label('filter', 'Filter:') }} 
			{{ Form::select('filter', ['Comments', 'Suggestions', 'All'], 2, ['class' => 'baseline filter'])}}
		</div>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class=" list-group-item hidden-print">
				<div class="row ">
					{{Form::open(['url' => URL::route('sb.fieldRemark', ['id' => $sc->id])])}}
						<div class="col-md-2">
							{{ Form::radio('type', 0, 1)}} Comment <br/>
							{{ Form::radio('type', 1, 0)}} Suggestion
						</div>
						<div class="col-lg-10">
			                <div class="input-group">
			                    {{ Form::text('remark', null, ['class' => 'form-control', 'rows' => 3])}}
				                <span class="input-group-btn">
				                    {{ Form::submit('Add Remark', ['class' => 'btn btn-primary'])}}
				                </span>
			                </div><!-- /input-group -->
			                {{ $errors->first('remark', '<span class=errormsg>*:message</span>') }}
			            </div><!-- /.col-lg-10 -->
					{{ Form::close() }}
				</div>
			</div>
			@if(count($remarks))
				<?php $show_more = true; ?>
				<div class="show-more hidden-print">
					@foreach($remarks as $key => $remark)
						@if($show_more && ((count($remarks)-3) <= $key))
							</div>
							<?php $show_more = false; ?>
						@endif
						<div class="list-group-item remark{{$remark->type}}">
							<strong> {{ $remark->user->fullName() }} </strong>
							{{ $remark->remark }} <br/>
							<i class="livetimestamp"> 
								{{ $remark->type? "suggested" : "commented"}} {{ $remark->created_at->diffForHumans() }} 
							</i>
						</div>
					@endforeach
				@if(count($remarks) > 3)
					<div class="list-group-item hidden-print">
						<center>
							{{ Form::button('See All', ['class' => 'btn btn-default remarks-collapse'])}}
						</center>
					</div>
				@endif
			@endif
		</div>
	</div>
</div>

@stop

@section('cssjs')
	{{ HTML::script('js/remarks.js')}}
@stop
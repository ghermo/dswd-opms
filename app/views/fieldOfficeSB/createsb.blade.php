@extends('layouts.default')
@section('content')

@include('includes.scorecard_menu')
@include('includes.office_scoreboard_menu')
<h2>Manage Central Office Scoreboards</h2>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header box-header-default">
		        I. Office Scoreboard for {{$year}}
		    </div>

	        <div class="box-body">
	          	<table class="table table-borderless no-top-border" >
		            <tr>
		              <td colspan="4">
		                {{ Form::label('stratGoal', 'Strategic Goal #1 ') }}
		                {{ Form::text('stratGoal', '', ['class'=>'form-control']) }} 
		              </td>
		            </tr>
		  			<tr>
		              <td colspan="2">
		                {{ Form::label('baseline', 'Baseline for: ') }}
		                {{ Form::text('baseline', '', ['class'=>'form-control']) }} 
		              </td>
		              <td>
		                {{ Form::label('movements', 'Movements for the month of: ') }}
		                {{ Form::text('movements', '', ['class'=>'form-control']) }} 
		              </td>
		            </tr>
		            <tr>
		              <td colspan="2">
		                {{ Form::label('selfSuffiency', 'Total No. of HHs in Self-Sufficiency Level: ') }}
		                {{ Form::text('selfSuffiency', '', ['class'=>'form-control']) }} 
		              </td>
		              <td>
		                {{ Form::label('stossl', 'No. of HHs moved from Subsistence to Self-Sufficiency: ') }}
		                {{ Form::text('stossl', '', ['class'=>'form-control']) }} 
		              </td>
		            </tr>
		            <tr>
		              <td colspan="2">
		                {{ Form::label('subsistence', 'Total No. of HHs in Subsistence Level: ') }}
		                {{ Form::text('subsistence', '', ['class'=>'form-control']) }} 
		              </td>
		              <td>
		                {{ Form::label('stos', 'No. of HHs moved from Survival to Subsistence: ') }}
		                {{ Form::text('stos', '', ['class'=>'form-control']) }} 
		              </td>
		            </tr>
		            <tr>
		              <td colspan="2">
		                {{ Form::label('survival', 'Total No. of HHs in Survival Level: ') }}
		                {{ Form::text('survival', '', ['class'=>'form-control']) }} 
		              </td>
		              <td>
		                {{ Form::label('movement', 'Movement from score to score of families with improved well being: ') }}
		                {{ Form::text('movement', '', ['class'=>'form-control']) }} 
		              </td>
		            </tr>
		            <tr>
		            	<td colspan="2">
		            		{{ Form::label('hhs', 'No. of HHs in Sets 1-7: ') }}
		                	{{ Form::text('hhs', '', ['class'=>'form-control']) }} 
		            	</td>
		            </tr>
		            <tr>
		            	<th>Lead Measures</th>
		            	<th>Responsible Person/Division</th>
		            	<th>Status (as to Targets and Timeline)</th>
		            </tr>
		            <tr>
		              	<td>
		                	{{ Form::text('lead_measure1', '', ['class'=>'form-control','placeholder'=>'Lead Measure 1']) }} 
		              	</td>
		              	<td>
		              		{{ Form::select('responsible_office1', array('' => 'Select person/division', 'Person 1' => 'Person 1', 'Person 2' => 'Person 2'), 'S',['id' => 'selectedTeam', 'class' => 'form-control']) }}
		              	</td>
		              	<td>
		                	{{ Form::text('status', '', ['class'=>'form-control']) }} 
		              	</td>
		            </tr>
		            <tr>
		              	<td>
		                	{{ Form::text('lead_measure2', '', ['class'=>'form-control','placeholder'=>'Lead Measure 2']) }} 
		              	</td>
		              	<td>
		              		{{ Form::select('responsible_office2', array('' => 'Select person/division', 'Person 1' => 'Person 1', 'Person 2' => 'Person 2'), 'S',['id' => 'selectedTeam', 'class' => 'form-control']) }}
		              	</td>
		              	<td>
		                	{{ Form::text('status', '', ['class'=>'form-control']) }} 
		              	</td>
		            </tr>
	          	</table>
	        </div>
		</div>

		<div class="box">
			<div class="box-header box-header-default">
		        II. Narrative
		    </div>

	        <div class="box-body">
	          	<table class="table table-borderless no-top-border" >
		            <tr>
		              <td>
		                {{ Form::label('statusDate', 'Status as of: ') }}
		                {{ Form::text('statusDate', '', ['class'=>'form-control']) }} 
		              </td>
		            </tr>
		  			<tr>
		              <td>
		                {{ Form::label('keyAccomplishments', 'Key Accomplishments: Activities Completed, Coordination done with Partner OBS and Issues Resolved (if any) ') }}
		                {{ Form::text('keyAccomplishments', '', ['class'=>'form-control']) }} 
		              </td>
		            </tr>
	          	</table>
	        </div>
		</div>

		<div class="box">
	        <div class="box-body">
	          	<table class="table table-borderless no-top-border" >
		            <tr>
		              <td colspan="3">
		                {{ Form::label('stratGoal2', 'Strategic Goal 2: ') }}
		                {{ Form::text('stratGoal2', '', ['class'=>'form-control']) }} 
		              </td>
		            </tr>
		  			<tr>
		              <td colspan="2">
		                {{ Form::label('poorHHs', 'Total No. of poor HHs covered by at least 2 SWD: ') }}
		                {{ Form::text('poorHHs', '', ['class'=>'form-control']) }} 
		              </td>
		              <td rowspan="4">
		                <!-- {{ Form::label('movements', 'Movements for the month of: ') }}
		                {{ Form::text('movements', '', ['class'=>'form-control']) }}  -->
		                <table class="table table-bordered">
		                	<tr>
		                		<th colspan="3">Number of HHs:</th>
		                	</tr>
		                	<tr>
		                		<th></th>
		                		<th>Pantawid</th>
		                		<th>Non-Pantawid</th>
		                	</tr>
		                	<tr>
		                		<th>w/ 2 SWD Services</th>
		                		<td>{{ Form::text('pantawid2', '', ['class'=>'form-control']) }}</td>
		                		<td>{{ Form::text('nonPantawid2', '', ['class'=>'form-control']) }}</td>
		                	</tr>
		                	<tr>
		                		<th>w/ 3 SWD Services</th>
		                		<td>{{ Form::text('pantawid3', '', ['class'=>'form-control']) }}</td>
		                		<td>{{ Form::text('nonPantawid3', '', ['class'=>'form-control']) }}</td>
		                	</tr>
		                	<tr>
		                		<th>w/ 4 SWD Services</th>
		                		<td>{{ Form::text('pantawid4', '', ['class'=>'form-control']) }}</td>
		                		<td>{{ Form::text('nonPantawid4', '', ['class'=>'form-control']) }}</td>
		                	</tr>
		                	<tr>
		                		<th>w/ 5 or more SWD Services</th>
		                		<td>{{ Form::text('pantawid5', '', ['class'=>'form-control']) }}</td>
		                		<td>{{ Form::text('nonPantawid5', '', ['class'=>'form-control']) }}</td>
		                	</tr>
		                	<tr>
		                		<th>Total</th>
		                		<td>...</td>
		                		<td>...</td>
		                	</tr>
		                </table>
		              </td>
		            </tr>
		            <tr>
		              <td colspan="2">
		                {{ Form::label('pantawidHHs', 'No. of Pantawid HHs covered by at least 2 SWD: ') }}
		                {{ Form::text('pantawidHHs', '', ['class'=>'form-control']) }} 
		              </td>
		            </tr>
		            <tr>
		              <td colspan="2">
		                {{ Form::label('nonPantawidHHs', 'No. of non-Pantawid HHs covered by at least 2 SWD: ') }}
		                {{ Form::text('nonPantawidHHs', '', ['class'=>'form-control']) }} 
		              </td>
		            <tr>
		              <td colspan="2">
		                {{ Form::label('identifiedPoorHHs', 'No. of identified poor HHs: ') }}
		                {{ Form::text('identifiedPoorHHs', '', ['class'=>'form-control']) }} 
		              </td>
		            </tr>
		            <tr>
		            	<td colspan="2">
		            		{{ Form::label('hhs', 'No. of HHs in Sets 1-7: ') }}
		                	{{ Form::text('hhs', '', ['class'=>'form-control']) }} 
		            	</td>
		            	<td>
		            		{{ Form::label('namesOfSWDservices', 'Names of SWD services/programs currently being provided to these HHs:') }}
		            		{{ Form::text('namesOfSWDservices', '', ['class'=>'form-control']) }} 
		            	</td>
		            </tr>
		            <tr>
		            	<th>Lead Measures</th>
		            	<th>Responsible Person/Division</th>
		            	<th>Status (as to Targets and Timeline</th>
		            </tr>
		            <tr>
		              	<td>
		                	{{ Form::text('leadMeasure', '', ['class'=>'form-control','placeholder'=>'Lead Measure 1']) }} 
		              	</td>
		              	<td>
		                	{{ Form::select('size', array('' => 'Select person/division', 'Person 1' => 'Person 1', 'Person 2' => 'Person 2'), 'S',['id' => 'selectedTeam', 'class' => 'form-control']) }}
		              	</td>
		              	<td>
		                	{{ Form::text('status', '', ['class'=>'form-control']) }} 
		              	</td>
		            </tr>
	          	</table>
	        </div>
		</div>

		<div class="box">
			<div class="box-header box-header-default">
		        III. Narrative
		    </div>

	        <div class="box-body">
	          	<table class="table table-borderless no-top-border" >
		            <tr>
		              <td>
		                {{ Form::label('statusDate', 'Status as of: ') }}
		                {{ Form::text('statusDate', '', ['class'=>'form-control']) }} 
		              </td>
		            </tr>
		  			<tr>
		              <td>
		                {{ Form::label('keyAccomplishments', 'Key Accomplishments: Activities Completed, Coordination done with Partner OBS and Issues Resolved (if any) ') }}
		                {{ Form::text('keyAccomplishments', '', ['class'=>'form-control']) }} 
		              </td>
		            </tr>
	          	</table>
	        </div>
		</div>

		<div class="box">
	        <div class="box-body">
	          	<table class="table table-borderless no-top-border" >
		            <tr>
		              <td colspan="4">
		                {{ Form::label('stratGoal2', 'Strategic Goal 3: ') }}
		                {{ Form::text('stratGoal2', '', ['class'=>'form-control']) }} 
		              </td>
		            </tr>
		  			<tr>
		              <td colspan="2">
		                {{ Form::label('poorHHs', 'Baseline for: ') }}
		                {{ Form::text('poorHHs', '', ['class'=>'form-control']) }} 
		              </td>
		              <td rowspan="3">
		                <!-- {{ Form::label('movements', 'Movements for the month of: ') }}
		                {{ Form::text('movements', '', ['class'=>'form-control']) }}  -->
		                <table class="table table-bordered">
		                	<tr>
		                		<td></td>
		                		<th>Number of LSWDOs assesed</th>
		                	</tr>
		                	<tr>
		                		<th>Provinces</th>
		                		<td>{{ Form::text('lswdoProvinces', '', ['class'=>'form-control']) }}</td>
		                	</tr>
		                	<tr>
		                		<th>Cities</th>
		                		<td>{{ Form::text('lswdoCities', '', ['class'=>'form-control']) }}</td>
		                	</tr>
		                	<tr>
		                		<th>Municipalities</th>
		                		<td>{{ Form::text('lswdoMunicipalities', '', ['class'=>'form-control']) }}</td>
		                	</tr>
		                	<tr>
		                		<th>Total Number</th>
		                		<td>{{ Form::text('total', '', ['class'=>'form-control']) }}</td>
		                	</tr>
		                </table>
		              </td>
		            </tr>
		            <tr>
		              <td colspan="2">
		                {{ Form::label('lswdoAssessed', 'No. of LSWDO assessed: ') }}
		                {{ Form::text('lswdoAssessed', '', ['class'=>'form-control']) }} 
		              </td>
		            </tr>
		            <tr>
		              <td colspan="2">
		                <strong>Universe for 2014</strong><br />
		                {{ Form::label('lswdoIdentified', 'No. of target LSWDOs identified: ') }}
		                {{ Form::text('lswdoIdentified', '', ['class'=>'form-control']) }} 
		              </td>
		            <tr>
		            	<th>Lead Measures</th>
		            	<th>Responsible Person/Division</th>
		            	<th>Status (as to Targets and Timeline</th>
		            </tr>
		            <tr>
		              	<td>
		                	{{ Form::text('leadMeasure', '', ['class'=>'form-control','placeholder'=>'Lead Measure 1']) }} 
		              	</td>
		              	<td>
		                	{{ Form::select('size', array('' => 'Select person/division', 'Person 1' => 'Person 1', 'Person 2' => 'Person 2'), 'S',['id' => 'selectedTeam', 'class' => 'form-control']) }} 
		              	</td>
		              	<td>
		                	{{ Form::text('status', '', ['class'=>'form-control']) }} 
		              	</td>
		            </tr>
	          	</table>
	        </div>
		</div>

		<div class="box">
			<div class="box-header box-header-default">
		        IV. Narrative
		    </div>

	        <div class="box-body">
	          	<table class="table table-borderless no-top-border" >
		            <tr>
		              <td>
		                {{ Form::label('statusDate', 'Status as of: ') }}
		                {{ Form::text('statusDate', '', ['class'=>'form-control']) }} 
		              </td>
		            </tr>
		  			<tr>
		              <td>
		                {{ Form::label('keyAccomplishments', 'Key Accomplishments: Activities Completed, Coordination done with Partner OBS and Issues Resolved (if any) ') }}
		                {{ Form::text('keyAccomplishments', '', ['class'=>'form-control']) }} 
		              </td>
		            </tr>
	          	</table>
	        </div>
		</div>

		<div class="box">
	        <div class="box-body">
	          	<table class="table table-borderless no-top-border" >
		            <tr>
		              <td colspan="3">
		                <strong>Issues and Challenges</strong> (<em>please report major issues in this format, if applicable</em>) 
		              </td>
		            </tr>
		  			<tr>
		              <td>
						<strong>Identify issues and challenges encountered:</strong> (<em>Specify details under each element, if any.</em>)
		              </td>
		              <td>
						<strong>Action being taken by team:</strong> (<em>Summary of the Office or team’s plan to resolve it, including key resources involved, and indicative target by when the identified issue shall have been addressed or resolved.</em>)
		              </td>
		              <td>
						<strong>Action requested from Management or other OBS (if any):</strong> (<em>Please indicate desired assistance/intervention from Management or other OBSto facilitate the achievement of the Office Breakthrough.</em>)
		              </td>
		            </tr>
		            <tr>
		            	<td>{{ Form::text('issueAndChallenges1', '', ['class'=>'form-control']) }}</td>
		            	<td>{{ Form::text('issueAndChallenges2', '', ['class'=>'form-control']) }}</td>
		            	<td>{{ Form::text('issueAndChallenges3', '', ['class'=>'form-control']) }}</td>
		            </tr>
	          	</table>
	        </div>
		</div>
	</div>
</div>
{{Form::Close()}}
@stop
@extends('layouts.default')
@section('content')

<h2>Manage Users</h2>
<hr />
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<i class="fa fa-table fw"></i> Users
				<div class="pull-right">
					{{ HTML::decode(link_to('/createuser', '<i class="fa fa-plus fw"></i> Create User', null)) }}
				</div>

			</div>
			<div class="panel-body remove-padding">
				<table class="table table-striped table-hover table-select">
					<thead>
						<tr>
							<th width="15%">Username</th>
							<th width="15%">First Name</th>
							<th width="15%">Last Name</th>
							<th width="15%">Role</th>
							<th width="25%">Office</th>
							<th width="15%">Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($UserPage as $user)
							<tr>
								@if($user->disabled==1)
									<td class="strike">{{ $user->username }}</td>
									<td class="strike">{{ $user->firstName }}</td>
									<td class="strike">{{ $user->lastName }}</td>
									<td class="strike">{{ $user->role() }}</td>
									<td class="strike">
										@if($user->office)
											{{ $user->office->office_name }}
										@endif
									</td>
									<td>
										<div class="btn-group actions">
										{{ HTML::decode(link_to("/edit/$user->id", '<i class="fa fa-pencil fw"></i>', array('class' => 'btn btn-default action-btn hide-button-style', 'title' => 'Edit'))) }}
										</div>
									</td>
								@else
									<td >{{ $user->username }}</td>
									<td>{{ $user->firstName }}</td>
									<td>{{ $user->lastName }}</td>
									<td>{{ $user->role() }}</td>
									<td>
										@if($user->office)
											{{ $user->office->office_name }}
										@endif
									</td>
									<td>
										<div class="btn-group actions">
										{{ HTML::decode(link_to("/edit/$user->id", '<i class="fa fa-pencil fw"></i>', array('class' => 'btn btn-default action-btn hide-button-style', 'title' => 'Edit'))) }}
										</div>
									</td>
								@endif
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="panel-footer">
				<span class="pull-right"><?php echo $UserPage->links(); ?></span>
			</div>
		</div>
	</div>
</div>
@stop
@extends('layouts.default')
@section('content')
	<h2>Edit Office</h2>
	<hr />

	<div class="row">
		<div class="col-md-6">	
			{{ Form::model($office, array('route' => array('createoffice.update', $office->id), 'method' => 'PUT')) }}

			<div class="form-group">
				{{ Form::label('office_abbreviation', 'Office Abbreviation:') }}
				{{ Form::text('office_abbreviation', $office->office_abbreviation, array('class' => 'form-control','disabled')) }}
			</div>

			<div class="form-group">
				{{ Form::label('office_name', 'Office Name:') }}{{ $errors->first('office_name', '<span class=errormsg>*:message</span>') }}
				{{ Form::text('office_name', null, array('class' => 'form-control')) }}
			</div>

			@if($office->users()->count())
				<div class="form-group">
					{{ Form::label('office_type', 'Office Type: ') }}
					{{ $errors->first('office_type', '<span class=errormsg>*:message</span>') }}
					<b class="errormsg"> uneditable due to the office having at least one member </b>
					<br />
				
						{{ Form::radio('office_type', 'Central Office', 1,['disabled']) }} Central Office<br>
				
						{{ Form::radio('office_type', 'Field Office', 0,['disabled']) }} Field Office
						{{Form::hidden('office_type',$office->office_type)}}
					
				</div>
				<div class="form-group">
					{{ Form::label('office_flag', 'Option: ') }}
					{{ $errors->first('office_flag') }}
					<b class="errormsg"> uneditable due to the office having at least one member </b>
					<br />

					<input type="checkbox" name="office_flag" id="myCheck"> Disable<br>

					
				</div>
			@else
				<div class="form-group">
					{{ Form::label('office_type', 'Office Type: ') }}
					{{ $errors->first('office_type', '<span class=errormsg>*:message</span>') }}
					<br />
					{{ Form::radio('office_type', 'Central Office') }} Central Office<br>
					{{ Form::radio('office_type', 'Field Office') }} Field Office
				</div>
				<div class="form-group">
					{{ Form::label('office_flag', 'Option: ') }}
					<br />
					{{ Form::checkbox('office_flag', '1') }} Disable<br>
					<input type="checkbox" class="hidden" name="office_flag" id="myCheck">
					{{ $errors->first('office_flag') }}
				</div>
			@endif

			
	

	{{ Form::submit('Save Changes', array('class' => 'btn btn-primary')) }}
	{{ link_to( '/offices', 'Cancel', array('class'=>'btn btn-default') ) }}
	{{ Form::close() }}
	
		</div>
	</div>
@stop
@section('cssjs')

<script type = "text/javascript">
  $(document).ready(function() {
  		document.getElementById("myCheck").disabled=true;
		
  });
</script>

@stop


@extends('layouts.default')
@section('content')
	<h2>Edit User: {{ $editUser->firstName }} {{ $editUser->lastName }} </h2>
	<hr />
	{{ Form::open(['url' => "/edituser/$editUser->id", 'name' => 'create1', 'class' => 'form-horizontal col-md-6', 'role' => 'form']) }}
		<div class = "form-group">
			{{ Form::label('username', 'Username:'); }}
			{{ $errors->first('username', '<span class=errormsg>*:message</span>') }}
			{{ Form::text('username', $editUser->username, ['class' => 'form-control', 'placeholder' => 'Enter Username', 'disabled' => 'disabled']) }}
		</div>
		<div class = "form-group">
			{{ Form::label('firstName', 'First Name:'); }}
			{{ $errors->first('first_name', '<span class=errormsg>*:message</span>') }}
			{{ Form::text('first_name', $editUser->firstName, ['class' => 'form-control', 'placeholder' => 'Enter First Name']) }}
		</div>
		<div class = "form-group">
			{{ Form::label('lastName', 'Last Name:'); }}
			{{ $errors->first('last_name', '<span class=errormsg>*:message</span>') }}
			{{ Form::text('last_name', $editUser->lastName, ['class' => 'form-control', 'placeholder' => 'Enter Last Name']) }}
		</div>
		<div class = "form-group">
			{{ Form::label('email', 'Email Address:'); }}
			{{ $errors->first('email', '<span class=errormsg>*:message</span>') }}
			{{ Form::text('email', $editUser->email, ['class' => 'form-control', 'placeholder' => 'Enter Email Address']) }}
		</div>
		<hr/>
		<div class = "form-group">
			{{ Form::label('password', 'Password:'); }} <span class="descriptionMsg">(Leave empty to retain the password.)</span>
			{{ $errors->first('password', '<span class=errormsg>*:message</span>') }}
			{{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Enter Password']) }}
		</div>
		<div class = "form-group">
			{{ Form::label('cpassword', 'Confirm Password:'); }}
			{{ $errors->first('cpassword', '<span class=errormsg>*:message</span>') }}
			{{ Form::password('cpassword', ['class' => 'form-control', 'placeholder' => 'Confirm Password']) }}
		</div>
		<div class = "form-group">
				{{ Form::label('role', 'Role: *'); }}
				{{ $errors->first('role', '<span class=errormsg>*:message</span>') }}
				{{ Form::select('role', $roles, $editUser->role, ['id' => 'roles', 'class' => 'form-control roles']) }}
		</div>
		<div id="osm" class="form-group">
			{{ Form::label('offices', 'Office:'); }}
			{{ $errors->first('office', '<span class=errormsg>*:message</span>') }}
			{{ Form::select('office[]', $osm, $editUser->office_id, ['class' => 'form-control offices']) }}
		</div>
		<div id="fo" class="form-group">
			{{ Form::label('offices', 'Office:'); }}
			{{ $errors->first('office', '<span class=errormsg>*:message</span>') }}
			{{ Form::select('office[]', $fo, $editUser->office_id, ['class' => 'form-control offices']) }}
		</div>
		<div id="co" class="form-group">
			{{ Form::label('offices', 'Office:'); }}
			{{ $errors->first('office', '<span class=errormsg>*:message</span>') }}
			{{ Form::select('office[]', $co, $editUser->office_id, ['class' => 'form-control offices']) }}
		</div>
		<div class="form-group">
			{{ Form::label('user_disabled', 'Disable: ') }}
			<br />
			@if($editUser->disabled)
				{{ Form::checkbox('user_disabled', 0) }} Enable<br>
			@else
				{{ Form::checkbox('user_disabled', 1) }} Disable<br>
			@endif	
		</div>
		<div class = "form-inline form-group">
			{{ Form::submit('Save Changes', ['name' => 'create1', 'class' => 'btn btn-primary', 'id' => 'create1']) }}
			{{ link_to( '/users', 'Cancel', array('class'=>'btn btn-default') ) }}
		</div>
		
	{{ Form::close() }}
</div>
</div>
@stop

@section('cssjs')
<script type="text/javascript">
    $(document).ready(function() {
    	switch($('#roles').val())
    	{
    		case '0': 
    			$('#osm').show();
    			$('#fo').hide();
    			$('#co').hide();
			break;
    		case '1': 
    		case '3':
    			$('#osm').hide();
    			$('#fo').show();
    			$('#co').hide();
			break;
    		case '2': 
    		case '4':
    			$('#osm').hide();
    			$('#fo').hide();
    			$('#co').show();
			break;
			case '5':
    			$('#osm').hide();
    			$('#fo').hide();
    			$('#co').hide();
    		break;
    	}
        $('#roles').change(function(event) {
        	switch($(this).val())
        	{
        		case '0': 
        			$('#osm').show();
        			$('#fo').hide();
        			$('#co').hide();
    			break;
        		case '1': 
        		case '3':
        			$('#osm').hide();
        			$('#fo').show();
        			$('#co').hide();
    			break;
        		case '2': 
        		case '4':
        			$('#osm').hide();
        			$('#fo').hide();
        			$('#co').show();
    			break;
				case '5':
	    			$('#osm').hide();
	    			$('#fo').hide();
	    			$('#co').hide();
	    		break;
        	}
        });
    });
</script>
@stop
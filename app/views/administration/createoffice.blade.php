@extends('layouts.default')
@section('content')
	<h2>Create New Office</h2>
	<hr />
	{{ Form::open(['route'=>'createoffice.store'],'POST') }}
		<div class="row">
			<div class="col-md-6">	
			<div class="form-group">
				{{ Form::label('office_abbreviation', 'Office Abbreviation: * ') }}{{ $errors->first('office_abbreviation', '<span class=errormsg>:message</span>') }}
				<br>
				{{ Form::text('office_abbreviation','',['class'=>'form-control'])}}
			</div>

			<div class="form-group">
				{{ Form::label('office_name', 'Office Name: * ') }}{{ $errors->first('office_name', '<span class=errormsg>:message</span>') }}
				<br>
				{{ Form::text('office_name','',['class'=>'form-control'])}}
			</div>

			

			<div class="form-group">
				{{ Form::label('office_type', 'Office Type: * ') }}{{ $errors->first('office_type', '<span class=errormsg>:message</span>') }}
				<br>
				{{ Form::radio('office_type', 'Central Office', ['selected'=>'selected']) }} Central Office<br>
				{{ Form::radio('office_type', 'Field Office') }} Field Office
			</div>
			
			<button type="submit" name="create1" class="btn btn-primary" value="create">Create Office</button>
			<button type="submit" name="create2"class="btn btn-default" value="another">Create and Create Another</button>
			{{ link_to( '/offices', 'Cancel', array('class'=>'btn btn-default') ) }}

			</div>
			</div>	
    		
    			
	{{ Form::close() }}	

@stop
@extends('layouts.default')
@section('content')
	<h2>Create User Account</h2>
	<hr />
	{{ Form::open(['url' => '/createuser', 'name' => 'create1', 'class' => 'form-horizontal col-md-6', 'role' => 'form']) }}
		<div class = "form-group">
			{{ Form::label('username', 'Username: *'); }}
			{{ $errors->first('username', '<span class=errormsg>*:message</span>') }}
			{{ Form::text('username', null, ['class' => 'form-control']) }}
		</div>
		<div class = "form-group">
		{{ Form::label('firstName', 'First Name: *'); }}
		{{ $errors->first('first_name', '<span class=errormsg>*:message</span>') }}
		{{ Form::text('first_name', null, ['class' => 'form-control']) }}
		</div>
		<div class = "form-group">
		{{ Form::label('lastName', 'Last Name: *'); }}
		{{ $errors->first('last_name', '<span class=errormsg>*:message</span>') }}
		{{ Form::text('last_name', null, ['class' => 'form-control']) }}
		</div>
		<div class = "form-group">
		{{ Form::label('email', 'Email Address: *'); }}
		{{ $errors->first('email', '<span class=errormsg>*:message</span>') }}
		{{ Form::email('email', null, ['class' => 'form-control']) }}
		</div>
		<div class = "form-group">
		{{ Form::label('password', 'Password: *'); }}
		{{ $errors->first('password', '<span class=errormsg>*:message</span>') }}
		{{ Form::password('password', ['class' => 'form-control']) }}
		</div>
		<div class = "form-group">
			{{ Form::label('cpassword', 'Confirm Password: *'); }}
			{{ $errors->first('cpassword', '<span class=errormsg>*:message</span>') }}
			{{ Form::password('cpassword', ['class' => 'form-control']) }}
		</div>
		<div class = "form-group">
				{{ Form::label('role', 'Role: *'); }}
				{{ $errors->first('role', '<span class=errormsg>*:message</span>') }}
				{{ Form::select('role', $roles, null, ['id' => 'roles', 'class' => 'form-control roles']) }}
		</div>
		<div id="osm" class="form-group">
			{{ Form::label('offices', 'Office:'); }}
			{{ $errors->first('office', '<span class=errormsg>*:message</span>') }}
			{{ Form::select('office[]', $osm, null, ['class' => 'form-control offices']) }}
		</div>
		<div id="fo" class="form-group">
			{{ Form::label('offices', 'Office:'); }}
			{{ $errors->first('office', '<span class=errormsg>*:message</span>') }}
			{{ Form::select('office[]', $fo, null, ['class' => 'form-control offices']) }}
		</div>
		<div id="co" class="form-group">
			{{ Form::label('offices', 'Office:'); }}
			{{ $errors->first('office', '<span class=errormsg>*:message</span>') }}
			{{ Form::select('office[]', $co, null, ['class' => 'form-control offices']) }}
		</div>
		<div class = "form-inline form-group">
			{{ Form::submit('Create User', ['name' => 'create1', 'class' => 'btn btn-primary', 'id' => 'create1']) }}
			{{ Form::submit('Create and Create Another', ['name' => 'create2', 'class' => 'btn btn-default', 'id' => 'create2']) }}
			{{ link_to( '/users', 'Cancel', array('class'=>'btn btn-default') ) }}
		</div>
	{{ Form::close() }}
	{{ Form::close() }}
@stop

@section('cssjs')
<script type="text/javascript">
    $(document).ready(function() {
    	switch($('#roles').val())
    	{
    		case '0': 
    			$('#osm').show();
    			$('#fo').hide();
    			$('#co').hide();
			break;
    		case '1': 
    		case '3':
    			$('#osm').hide();
    			$('#fo').show();
    			$('#co').hide();
			break;
    		case '2': 
    		case '4':
    			$('#osm').hide();
    			$('#fo').hide();
    			$('#co').show();
			break;
			case '5':
    			$('#osm').hide();
    			$('#fo').hide();
    			$('#co').hide();
    		break;
    	}
        $('#roles').change(function(event) {
        	switch($(this).val())
        	{
        		case '0': 
        			$('#osm').show();
        			$('#fo').hide();
        			$('#co').hide();
    			break;
        		case '1': 
        		case '3':
        			$('#osm').hide();
        			$('#fo').show();
        			$('#co').hide();
    			break;
        		case '2': 
        		case '4':
        			$('#osm').hide();
        			$('#fo').hide();
        			$('#co').show();
    			break;
				case '5':
	    			$('#osm').hide();
	    			$('#fo').hide();
	    			$('#co').hide();
	    		break;
        	}
        });
    });
</script>
@stop
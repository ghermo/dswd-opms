@extends('layouts.default')
@section('content')
<h2>Manage Offices</h2>
<hr />
<div class="row" id="testcase">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<i class="fa fa-table fw"></i> Offices
				<div class="pull-right">
					{{ HTML::decode(link_to_route('createoffice.create', '<i class="fa fa-plus fw"></i> Create Office', null,null)) }}
				</div>
			</div>
			<div class="panel-body remove-padding">
				<table class="table table-striped table-hover table-select">
					<thead>
						<tr>
							<th class="width-20">Abbreviation</th>
							<th class="width-40">Office Name</th>
							<th class="width-20">Office Type</th>
							<th class="width-20">Action</th>
						</tr>
					</thead>
					
					<tbody>
						@if(count($offices))
							@foreach($offices as $office)
								@if($office->office_flag==1)
									<tr>
										<td class="strike">{{$office->office_abbreviation}}</td>
										<td class="strike">{{$office->office_name}}</td>
										<td class="strike">{{$office->office_type}}</td>
										<td>
											<div class="btn-group actions">
											    {{ HTML::decode(link_to("createoffice.edit", '<i class="fa fa-pencil fw"></i>', array('class' => 'btn btn-default action-btn hide-button-style', 'title' => 'Edit'))) }}
											</div>
										</td>

									</tr>
								@else
									<tr>
										<td>{{$office->office_abbreviation}}</td>
										<td>{{$office->office_name}}</td>
										<td>{{$office->office_type == "OSM" ? "Central Office" : $office->office_type}}</td>
										<td>
											<div class="btn-group actions">
											    {{ HTML::decode(link_to_route('createoffice.edit', '<i class="fa fa-pencil fw"></i>', $office->id, array('class' => 'btn btn-default action-btn hide-button-style', 'title' => 'Edit'))) }}
											</div>
										</td>
									</tr>
								@endif
							@endforeach
						@else
							<tr>
								<td colspan="4">No office</td>
							</tr>
						@endif
					</tbody>
				</table>
			</div>
			<div class="panel-footer">
				<span class="pull-right"><?php echo $offices->links(); ?></span>
			</div>
		</div>
	</div>
</div>
@stop
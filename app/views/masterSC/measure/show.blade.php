@extends('layouts.default')
@section('content')
<h2 class="pull-left">{{ HTML::decode (link_to( "/measurelist", '<i class="fa fa-chevron-left fw"></i>', array('class'=>'link-black hidden-print','title'=>'Measure Profile List') )) }} Measure Profile
</h2>
<div class="pull-right btn-option hidden-print">
	<a href="javascript:window.print()"><button class="btn btn-default"><i class="fa fa-print fw"></i> Export as PDF</button></a>
</div>
<hr class="clear" />

<div class="row measure-profile">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-body remove-padding">
				<table class="table table-borderless no-top-border no-inner-border measure-table">
					<tbody>
						<tr>
							<td class="width-30"><strong>What is the Objective?</strong></td>
							<td class="width-70">{{$objective->objective}}</td>
						</tr>
						<tr>
							<td class="width-30"><strong>What is the Measure?</strong></td>
							<td class="width-70">{{$objective->measure()->first()->description}}</td>
						</tr>
						<tr>
							<td class="width-30"><strong>What is the reason behind choosing this measure?</strong></td>
							<td class="width-70"> {{ nl2br($objective->measure()->first()->reason) }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="row  measure-profile">
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-body remove-padding">
				<table class="table table-borderless no-top-border no-inner-border">
					<tbody>
						<tr>
							<td class="width-70%"><strong>How often is the measure updated/calculated?</strong></td>
							<td>{{$objective->measure()->first()->frequency}}</td>
						</tr>
						<tr>
							<td class="width-70%"><strong>What is the unit of measure?</strong></td>
							<td>{{$objective->measure()->first()->unitOfMeasure}}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-body remove-padding">
				<table class="table table-borderless no-top-border no-inner-border">
					<tbody>
						<tr>
							<td class="width-50"><strong>How is the measure calculated? Clarify the terms in the formula</strong></td>
							<td>{{ nl2br($objective->measure()->first()->formula) }}</td>
						</tr>
						<tr>
							<td class="width-50"><strong>When will this information be available?</strong></td>
							<td>{{$objective->measure()->first()->infoAvailability}}</td>
						</tr>
						<tr>
							<td class="width-50"><strong>Is information about the measure available?</strong></td>
							<td>
								{{ Form::radio('availability', 0, $objective->measure->availability==0, ['disabled']) }} Currently available<br/>
								{{ Form::radio('availability', 1, $objective->measure->availability==1, ['disabled']) }} With minor changes<br/>
								{{ Form::radio('availability', 2, $objective->measure->availability==2, ['disabled']) }} Still to be formulated

							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-body remove-padding">
				<table class="table table-borderless no-top-border no-inner-border">
					<tbody>
						<tr>
							<td class="width-50"><strong>Who is responsible for setting targets?</strong></td>
							<td>{{$objective->measure()->first()->userSetTarget}}</td>
						</tr>
						<tr>
							<td class="width-50"><strong>Who is accountable for targets?</strong></td>
							<td>{{$objective->measure()->first()->userAccountableTarget}}</td>
						</tr>
						<tr>
							<td class="width-50"><strong>Who is responsible for tracking and reporting targets?</strong></td>
							<td>{{$objective->measure()->first()->userTrackTarget}}</td>
						</tr>
						<tr>
							<td class="width-50"><strong>Where/How is it acquired? (Specify document or person)</strong></td>
							<td>{{ nl2br($objective->measure()->first()->acquiredReason) }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>	
	</div>
</div>
<div class="row measure-profile">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-body remove-padding">
				<table class="table table-borderless no-top-border table-center-all">
					<tbody>
						<tr>
							<td colspan="{{count($scorecard->targetYear)}}"> <strong>TARGETS</strong> </td>
						</tr>
						<tr>
							<?php $first = true; ?>
							@foreach($scorecard->targetYear()->get() as $year)
								<td><strong>{{$year->year}} {{$first ? '(Baseline)' : ''}}</strong></td>
								<?php $first = false; ?>
							@endforeach
						</tr>
						<tr>
							<?php $measure = $objective->measure ?>

						 	@foreach($measure->targets2($scorecard->years)->get() as $target)
	                            <td> {{$target->target}} </td>
	                        @endforeach
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>





@stop

<!--
<div class="row">
	<div class="col-lg-12">
		<div class="pull-right tools">
			{{ link_to( '/mastersc', '&larr; Back', array('class'=>'btn btn-default') ) }}	
			{{ link_to( "/measure/$id/edit", 'Edit', array('class'=>'btn btn-default') ) }}	
			{{ link_to( "/measure/$id/final", 'Mark as Final', array('class'=>'btn btn-default') ) }}
			{{ Form::button('Print', ['class'=>'btn btn-default'])}}
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<table class="table table-bordered table-fixed table-center-headers table-responsive">
			<tr>
				<td colspan="2">What is the Objective?</td>
				<td colspan="2"><b> Build a culture of innovation and convergence </b></td>
				<td colspan="2" rowspan="2">How often is the measure Semestral updated/calculated</td>
				<td colspan="3" rowspan="2"> <b> Semestral </b> </td>
			</tr>
			<tr>
				<td colspan="2">What is the measure</td>
				<td colspan="2"><b># of central and field offices with good practices of recognized by central office for replication<b></td>
			</tr>
			<tr>
				<td colspan="2"> What is the reason behind choosing this measure? </td>
				<td colspan="2"> Targetting good practices for replication completes the KM cycle and ensures that these practices are 
				not simply shared or shelved but are actually used to contribute to process and organizational excellence </td>
				<td colspan="2"> What is the unit of measure? </td>
				<td colspan="3"> <b> Absolute count </b> </td>
			</tr>
			<tr>
				<td colspan="3" rowspan="1"> 
					How id the measure calculated? Clarify the terms in the formula 
					<br> <br> Number of good practices that were recognized during the Scorecard Summit
				</td>
				<td colspan="1" rowspan="2"> When will this information be available? </td>
				<td colspan="2" rowspan="1"> 
					Where/ How is it acquired? 
					<br> (Specify document or person)
				</td>
				<td colspan="3" rowspan="1"> Each FO that is able to significantly surpass their targets may have eligible good practices
				that will be reviewsed and validated via the Scorecard Summit </td>
			</tr>
			
			<tr>
				<td colspan="3" rowspan="3"> Is the information about the measure available? 
					<br> [✓] Currently available
					<br> [ ] With minor changes
					<br> [ ] Still to be formulated
				</td>
				<th colspan="1" rowspan="2"> Baseline </th>
				<th colspan="4"> TARGETS </th>
			</tr>
			<tr>
				<td rowspan="2"> EO 2013 </td>
				<th> 2013 </th>
				<th> 2014 </th>
				<th> 2015 </th>
				<th> 2016 </th>
			</tr>
			<tr>
				<td rowspan="5"> <center> 1 <br> (DSWD-CAR's <br> Grievance <br> Response System) </center> </td>
				<td rowspan="5"> <center> 5 </center></td>
				<td rowspan="5"> <center> 15 </center></td>
				<td rowspan="5"> <center> 20 </center></td>
				<td rowspan="5"> <center> 25 </center></td>			
			</tr>
			<tr>
				<td colspan="2"> Who is responsible for setting targets? </td>
				<td colspan="2"> ALL HOBS </td>
			</tr>
			<tr>
				<td colspan="2"> Who is accountable targets? </td>
				<td colspan="2"> ALL HOBS </td>
			</tr>
			<tr>
				<td colspan="2"> Who is responsible for tracking and reporting targets? </td>
				<td colspan="2"> OSM and OBS respective Strategy Officers/Focal Persons </td>
			</tr>
		</table>
	</div>
</div>
-->
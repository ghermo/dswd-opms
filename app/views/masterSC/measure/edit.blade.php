@extends('layouts.default')
@section('content')
{{ Form::open(['url' => "/measure/save/".$objective->measure()->first()->id, 'name' => 'myForm', 'class' => 'form-horizontal col-md-12', 'role' => 'form']) }}

<h2 class="pull-left">{{ HTML::decode (link_to( "/measurelist", '<i class="fa fa-chevron-left fw"></i>', array('class'=>'link-black hidden-print','title'=>'Measure Profile List') )) }} Edit Measure Profile
</h2>
<div class="pull-right btn-option hidden-print">
	{{ Form::reset('Reset', ['class' => 'btn btn-default']) }}
</div>
<hr class="clear" />

<div class="row measure-profile">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-body remove-padding">
				<table class="table table-borderless no-top-border no-inner-border">
					<tbody>
						<tr>
							<td><strong>What is the Objective?</strong></td>
							<td><input type="text" class="form-control" name="objective" value="<?php echo $objective->objective?>" disabled></td>
						</tr>
						<tr>
							<td><strong>What is the Measure?</strong></td>
							<td>{{ Form::text('measure', $objective->measure->description, ['class' => 'form-control', 'disabled']) }}</td>
						</tr>
						<tr>
							<td><strong>What is the reason behind choosing this measure?</strong></td>
							<td>{{ Form::textarea('reason', $objective->measure->reason, ['class'=>'form-control','clearable']) }} </td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="row  measure-profile">
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-body remove-padding">
				<table class="table table-borderless no-top-border no-inner-border">
					<tbody>
						<tr>
							<td><strong>How often is the measure updated/calculated?</strong></td>
							<td>{{ Form::select('oftenUpdate', $oftenUpdate, $objective->measure->frequency, ['class' => 'form-control']) }}</td>
						</tr>
						<tr>
							<td><strong>What is the unit of measure?</strong></td>
							<td>{{ Form::select('unitMeasure', $unitMeasure, $objective->measure->unitOfMeasure, ['class' => 'form-control']) }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-body remove-padding">
				<table class="table table-borderless no-top-border no-inner-border">
					<tr>
						<td><strong>How is the measure calculated? Clarify the terms in the formula</strong></td>
						<td>{{ Form::textarea('measureCalculated', $objective->measure->formula, ['class'=>'form-control',  'clearable']) }}</td>
					</tr>
					<tr>
						<td><strong>When will this information be available?</strong></td>
						<td>{{ Form::text('infoAvailability', $objective->measure->infoAvailability, ['class' => 'form-control','clearable']) }}</td>
					</tr>
					<tr>
						<td><strong>Is information about the measure available?</strong></td>
						<td>
							{{ Form::radio('availability', 0, $objective->measure->availability==0) }} Currently available<br/>
							{{ Form::radio('availability', 1, $objective->measure->availability==1) }} With minor changes<br/>
							{{ Form::radio('availability', 2, $objective->measure->availability==2) }} Still to be formulated
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-body remove-padding">
				<table class="table table-borderless no-top-border no-inner-border">
					<tbody>
						<tr>
							<td><strong>Who is responsible for setting targets?</strong></td>
							<td>{{ Form::text('responsible', $objective->measure->userSetTarget, ['class'=>'form-control', 'clearable']) }}</td>
						</tr>
						<tr>
							<td><strong>Who is accountable for targets?</strong></td>
							<td>{{ Form::text('accountable', $objective->measure->userAccountableTarget, ['class'=>'form-control','clearable']) }}</td>
						</tr>
						<tr>
							<td><strong>Who is responsible for tracking and reporting targets?</strong></td>
							<td>{{ Form::text('responsibleTracking', $objective->measure->userTrackTarget, ['class'=>'form-control', 'clearable']) }}</td>
						</tr>
						<tr>
							<td><strong>Where/How is it acquired? (Specify document or person)</strong></td>
							<td>{{ Form::textarea('howAcquired', $objective->measure->acquiredReason, ['class'=>'form-control',  'clearable']) }} </td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>	
	</div>
</div>
<div class="row measure-profile">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-body remove-padding">
				<table class="table table-borderless no-top-border table-center-all">
					<tbody>
						<tr>
							<td colspan="{{count($scorecard->targetYear)}}"> <strong>TARGETS</strong> </td>
						</tr>
						<tr>
							<?php $first = true; ?>
							@foreach($scorecard->targetYear()->get() as $year)
								<td><strong>{{$year->year}} {{$first ? '(Baseline)' : ''}}</strong></td>
								<?php $first = false; ?>
							@endforeach
						</tr>
						<tr>
							<?php $measure = $objective->measure ?>
							<?php $id=0;?>
						 	@foreach($measure->targets2($scorecard->years)->get() as $target)
	                            <td>
	                            	{{Form::text('accomplishments'.$id, $target->target, ['class' => 'form-control center-text'])}}
	                            	@if($errors->first('accomplishments'.$id, '<span class=errormsg>*:message</span>'))
	                            		{{ $errors->first('accomplishments'.$id, '<span class=errormsg>*:message</span>') }}
	                            	@else 
	                            		<br>
	                            	@endif
	                         		<?php $id++;?>
	                            </td>
	                        @endforeach
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	</div>
	{{ Form::submit('Save', ['name' => 'save', 'class' => 'btn btn-primary', 'id' => 'create1']) }}
	{{ link_to( '/measurelist', 'Cancel', array('class'=>'btn btn-default') ) }}
{{Form::Close()}}
@stop

@section('cssjs')
{{ HTML::script('js/jquery.min.js')}}
<script type="text/javascript">
	$(document).ready(function() {
		$('#clear-form').click(function() {
			$(".clearable").val('');
		});
	});
</script>

@stop
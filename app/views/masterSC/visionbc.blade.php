@extends('layouts.default')
@section('nav')
	@include('includes.scorecard_menu')
@stop
@section('content')

<h2>Vision Basecamps</h2>
<hr />
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<i class="fa fa-table fw"></i> Vision Basecamps
				<div class="pull-right">
				    {{ HTML::decode(link_to_route('opcr.create', '<i class="fa fa-pencil fw"></i> Create New', null)) }}
				</div>
			</div>
			<div class="panel-body remove-padding">
				<table class="table table-striped">
					<tbody>
						<tr>
							<td>No Data Available</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop
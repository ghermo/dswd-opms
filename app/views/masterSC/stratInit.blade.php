@extends('layouts.default')
@section('nav')
    @include('includes.scorecard_menu')
@stop
@section('nav2')
    @include('includes.manage_agency')
@stop
@section('content')

		
<h2>Initiative Profiles Lists</h2>
<hr />
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<i class="fa fa-table fw"></i> Initiative Profiles
				<div class="pull-right">
				    {{ HTML::decode(link_to_route('initiatives.create', '<i class="fa fa-plus fw"></i> Create New', null)) }}
				</div>
			</div>
			<div class="panel-body remove-padding">
				<table class="table table-striped table-hover table-select">
					<thead><tr>
						<th class="width-10">#</th>
						<th class="width-40">Initiative Name</th>
						<th class="width-20"> Status (Draft/Final)</th>
						<th class="width-30">Action</th>
					</tr></thead>
					<php $i=1; ?>
					@if(count($initiatives))
						@foreach($initiatives as $initiative)
							@if($initiative->status=="Final")
								<tr class="info">
							@else
								<tr>
							@endif
								<td>{{$initiative->id}}</td>
								<td>{{link_to( "/initiative/$initiative->id/", $initiative->projectName)}}</td>
								<td>{{$initiative->status?'<span class="label label-success">Final</span>':'<span class="label label-default">Draft</span>'}}</td>
								<td>
									@if(Role::access('0'))
										@if($initiative->status==1)
										<div class="btn-group actions">
											{{ HTML::decode (link_to( "/initiatives/unsetfinal/$initiative->id", '<i class="fa fa-file-text"></i>', array('class'=>'btn btn-default action-btn hide-button-style','title'=>'Mark as Draft') )) }}
										</div>
										@else
										<div class="btn-group actions">
											{{ HTML::decode (link_to( "/initiative/$initiative->id/edit", '<i class="fa fa-pencil"></i>', array('class'=>'btn btn-default action-btn hide-button-style','title'=>'Edit') )) }}
											{{ HTML::decode (link_to( "#", '<i class="fa fa-check"></i>', array('class'=>'btn btn-success action-btn hide-button-style','title'=>'Mark as Final','data-id'=>$initiative->id) )) }}
											{{ HTML::decode (link_to( "/initiatives/delete/$initiative->id", '<i class="fa fa-times"></i>', array('class'=>'btn btn-danger action-btn hide-button-style', 'title'=>'Delete', 'onclick' => "return confirm('Are you sure?');") )) }}
										</div>
										@endif
									@else
										@if($initiative->status==0)
										<div class="btn-group actions">
											{{ HTML::decode (link_to( "/initiative/$initiative->id/edit", '<i class="fa fa-pencil"></i>', array('class'=>'btn btn-default action-btn hide-button-style','title'=>'Edit') )) }}
										</div>
										@endif
									@endif
								</td>
							</tr>
							
						@endforeach
					@else
						<tr>
							<td colspan="4"> <center> <i> No existing initiatives </i> </center> </td>
						</tr>
					@endif
				</table>
			</div>
		</div>
	</div>
</div>
@stop
@section('cssjs')
  {{ HTML::script('js/remarks.js')}}
<script type = "text/javascript">
  $(document).ready(function() {
  
    $(".btn-success").click(function(){
    	var id=$(this).closest("a").data("id");
    	//alert(id);
    	var con=confirm("Are you sure to mark this as final?");
    	if(con==true)
    		$(this).closest("a").attr("href","/initiatives/setfinal/"+id);
  	});
  	/*$(".btn-danger").click(function(){
    	var id=$(this).closest("a").data("id");
    	//alert(id);
    	var con=confirm("Are you sure to delete this?");
    	if(con==true)
    		$(this).closest("a").attr("href","/officesb/central/delete/"+id);
    		//alert("delete!");
    		//
  	});*/
  });
</script>
@stop
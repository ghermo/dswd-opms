@extends('layouts.default')
@section('nav')
    @include('includes.scorecard_menu')
@stop
@section('nav2')
    @include('includes.manage_agency')
@stop
@section('content')

<h2>Manage Perspective</h2>
<hr />

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<i class="fa fa-table fw"></i> Perspectives
			</div>
			<div class="panel-body remove-padding">

				{{ Form::open(['url' => '/manageperspective', 'class' => 'form-inline', 'role' => 'form']) }}
					<div class="pull-right table-tools">
						{{ $errors->first('name', '<span class=errormsg>:message* </span>') }}
						{{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter Perspective Name']) }}
						{{ Form::submit('Add', ['class' => 'btn btn-primary']) }}
					</div>
				{{ Form::close() }}

				<table class="table table-striped table-hover table-select">
					<tr>
						<th class="width-10" style="text-align: center;"> No. </th>
						<th class="width-50"> Perspective </th>
						<th class="width-40"> Actions </th>
					</tr>
					<?php $i = 1; ?>

					@if(count($perspective))
						@foreach($perspective as $item)
							<tr class="perspective {{$item->flag?'info':''}}">
								<td style="text-align: center;">
									{{ $i++ }}
								</td>
								<td>
									<span class = "current-text mode1">
										{{$item->perspective}}
									</span>
									{{ Form::open(['url' => "perspective/$item->id/edit", 'class' => 'form-inline', 'role' => 'form']) }}
										<input type = "text" name = "perspective" class = "edit-text form-control mode2"/>
									{{ Form::close() }}
								</td>
								<td>
									@if(! $item->flag)
									<div class="btn-group actions">
										{{HTML::decode (Form::button('<i class="fa fa-pencil"></i>', ['class' => 'btn btn-default action-btn hide-button-style allow-edit mode1', 'title' => 'Edit']))}}
										{{HTML::decode (link_to( "/perspective/sethighest/$item->id", '<i class="fa fa-check"></i>', ['class'=>'btn btn-success action-btn hide-button-style mode1','title'=>'Set as Highest'] )) }}
										{{HTML::decode (link_to("perspective/delete/$item->id", '<i class="fa fa-times"></i>', ['class'=>'btn btn-danger action-btn hide-button-style mode1', 'onclick' => "return confirm('Are you sure?');",'title'=>'Delete']))}}
									</div>
									{{Form::button('Save', ['class' => 'btn btn-default btn-xs save-edit mode2'])}}
									{{Form::button('Cancel', ['class' => 'btn btn-default btn-xs cancel-edit mode2'])}}
									
									@else
									<div class="btn-group actions">
									{{HTML::decode (Form::button('<i class="fa fa-pencil"></i>', ['class' => 'btn btn-default action-btn hide-button-style allow-edit mode1', 'title' => 'Edit']))}}
									{{HTML::decode (link_to("perspective/delete/$item->id", '<i class="fa fa-times"></i>', ['class'=>'btn btn-danger action-btn hide-button-style mode1', 'onclick' => "return confirm('Are you sure?');",'title'=>'Delete']))}}
									</div>
									{{Form::button('Save', ['class' => 'btn btn-default btn-xs save-edit mode2'])}}
									{{Form::button('Cancel', ['class' => 'btn btn-default btn-xs cancel-edit mode2'])}}

									@endif
								</td>
							</tr>
						@endforeach
			          @else
			            <tr>
			              <td colspan="3"> <center> <i> No existing perspectives </i> </center> </td>
			            </tr>
			          @endif
				</table>
			</div>
		</div>
		<div class="pull-left">
			<h5>Legend:</h5> <i class="fa fa-square fw" style="color: #c4e3f3;"></i> = Highest Perspective
		</div>
	</div>
</div>
@stop

@section('cssjs')
<script type = "text/javascript">
	$(document).ready(function() {
		$(".mode2").hide();
		$(".allow-edit").on("click", function() {
			var current = $(this).closest("tr").find(".current-text");
			var textfield = $(this).closest("tr").find(".edit-text");
			var text = current.text().trim();
			current.hide();
			textfield.val(text);
			textfield.attr({"placeholder": text, "value": text}).show().focus();
			$(this).closest("tr").find(".mode1").hide();
			$(this).closest("tr").find(".mode2").show();
		});

		$(".save-edit").on("click", function(event) {
			var current = $(this).closest("tr").find(".current-text");
			var textfield = $(this).closest("tr").find(".edit-text");
			var text = textfield.val();
			textfield.hide();
			current.text(text);
			current.show();
			textfield.parent().submit();
			$(this).closest("tr").find(".mode1").show();
			$(this).closest("tr").find(".mode2").hide();
		});
		
		$(".cancel-edit").on("click", function() {
			$(this).closest("tr").find(".mode2").hide();
			$(this).closest("tr").find(".mode1").show();
		});
	});
</script>
@stop
@extends('layouts.default')
@section('nav')
    @include('includes.scorecard_menu')
@stop
@section('nav2')
    @include('includes.manage_agency')
@stop
@section('content')

<h2>Measure Profiles Lists</h2>
<hr />
  
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<i class="fa fa-table fw"></i> Measures
			</div>
			<div class="panel-body remove-padding">
				<table class="table table-striped table-hover table-select">
          <thead>
              <tr>
                <th width="5%">#</th>
                <th width="75%">Measure Name</th>
                <th width="35%">Action</th>
              </tr>
          </thead>
          <tbody>
            <?php $i=1; ?>
            
            @foreach($perspectives as $perspective)
              <?php $objectives = Objective::where('perspective_id', '=', $perspective->id)->orderBy('ordinal')->get(); ?>
          
              @foreach($objectives as $objective)
              <tr >
                <td>{{ $i++ }}</td>
                <td>{{ link_to( "/measure/".$objective->id, $objective->measure()->first()->description)}}</td>
                <td>
                  <div class="btn-group actions">
                    {{ HTML::decode (link_to( "measure/$objective->id/edit", '<i class="fa fa-pencil"></i>', array('class'=>'btn btn-default action-btn hide-button-style form in-line','title'=>'Edit') )) }}
                  </div>
                </td>
              </tr>
              @endforeach
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@stop
@extends('layouts.default')
@section('nav')
    @include('includes.scorecard_menu')
@stop
@section('nav2')
    @include('includes.manage_agency')
@stop
@section('content')

<?php
	$id = Session::get('id', $id);
?>
<h2>Manage Performace Indicators for Strategic Objectives</h2>
<hr />

{{ Form::open(['url' => URL::route('indicators.store'), 'role' => 'form']) }}
<div class="row form-group">
	<div class="col-md-5">
		{{ Form::select('goal_id', $objectivesList, $id, ['class' => 'form-control'])}}
		{{ $errors->first('goal', '<span class=errormsg>*:message</span>') }}
	</div>
	<div class="col-md-2">
		{{ Form::text('indicator', null, ['class' => 'form-control', 'placeholder' => 'Enter Indicator Name']) }}
		{{ $errors->first('indicator', '<span class=errormsg>*:message</span>') }}
	</div>
	<div class="col-md-2">
		{{ Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Description'])}}
		{{ $errors->first('description', '<span class=errormsg>*:message</span>') }}
	</div>
	<div class="col-md-2">
		<span class="measure">
			{{ Form::checkbox('affect', '', 1) }} <small> Aggregate to Scorecard </small>
		</span>
	</div>
	<div class="col-md-1">
		{{ Form::submit('Add', ['class' => 'btn btn-primary pull-right']) }}
	</div>
</div>
{{ Form::close() }}
@if (count($objectivesList)==0) 
	<div class="row">
		<center><h3>{{"There are no objectives"}}</h3></center>
	</div>
@endif


<div class="panel-group" id="accordion">
	<?php $first = true ?>
	@foreach($objectives as $objective)
		<div class="panel panel-primary indicators">
			<div class="panel-heading">
				<div class="form-group col-sm-3 pull-right indicator-formula">
				    <div class="input-group">
				      	<span class="input-group-addon">Formula</span>
				        {{Form::open(['url' => URL::route('indicators.aggregation', ['id' => $objective->id])])}}
							{{Form::hidden('id', $objective->id)}}
							{{Form::select('aggregation_type', ['Summation', 'Average'], $objective->aggregation_type, ['class' => 'form-control aggregation'])}}
						{{Form::close()}}
				    </div>
				</div>
				<div class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#objective{{$objective->id}}" class="objective collapsed">
						{{ $objective->objective }}
					</a>
				</div>
			</div>
			<div id="objective{{$objective->id}}" class="panel-collapse collapse {{(($first && ! $id) || $objective->id == $id) ? 'in' : ''}}">
				<div class="panel-body remove-padding">
					<table class="table table-striped table-hover table-select">
						<tr>
							<th class="width-30"> Indicator </th>
							<th class="width-40"> Description </th>
							<th class="width-10" style="text-align: center;"> Aggregate to Scorecard </th>
							<th class="width-20" style="text-align: center;"> Actions </th>
						</tr>

						@if(count($objective->indicators))
 				            <?php $indicatorsLength = count($objective->indicators);?>
              				<?php $first2 = true; ?>
		              		<?php $counter=0;?>

							@foreach($objective->indicators as $indicator)
								<tr class="indicator">
									<td>
										<span class = "mode1">
											{{$indicator->indicator}}
											{{$indicator->ordinal}}
										</span>
										{{ Form::text('indicator', $indicator->indicator, ['class' => 'edit-indicator form-control mode2', 'data-original' => $indicator->indicator]) }}
									</td>
									<td>
										<span class = "mode1">
											{{ $indicator->description }}
										</span>
										{{ Form::text('description', $indicator->description, ['class' => 'edit-description form-control mode2', 'data-original' => $indicator->description]) }}
									</td>
									<td>
										<center>
											{{ Form::checkbox('affect', 1, $indicator->affect, ['class' => 'edit-affect', 'disabled', 'data-initial' => $indicator->affect,'data-inittype' => $indicator->description]) }}
										</center>
									</td>
									<td>
										<div class="btn-group actions">
										{{HTML::decode (Form::button('<i class="fa fa-pencil"></i>', ['class' => 'btn btn-default action-btn hide-button-style allow-edit mode1', 'title' => 'Edit', 'data-placement' => 'bottom', 'data-toggle' => 'tooltip']))}}              
										{{HTML::decode (link_to(URL::route('indicators.destroy', ['id' => $indicator->id]), '<i class="fa fa-times"></i>', ['class'=>'btn btn-danger mode1 action-btn hide-button-style', 'onclick' => "return confirm('Are you sure?');",'title'=>'Delete']))}}

										@if( ! $first2)
											{{ HTML::decode (link_to(URL::route('indicators.moveUp', ['id' => $indicator->id, 'gid' => $objective->id]), '<i class="fa fa-chevron-up fw"></i>', ['name' => 'move-up', 'class'=>'btn btn-default action-btn hide-button-style','title'=>'Move Up','type'=>'submit']))}}
										@endif
										@if($counter != $indicatorsLength-1)
											{{ HTML::decode ((link_to(URL::route('indicators.moveDown', ['id' => $indicator->id, 'gid' => $objective->id]), '<i class="fa fa-chevron-down fw"></i>', ['name' => 'move-down', 'class'=>'btn btn-default action-btn hide-button-style mode1','title'=>'Move Down','type'=>'submit'])))}}
										
										@endif
										</div>
										{{Form::open(['url' => URL::route('indicators.update', ['id' => $indicator->id])]) }}
											{{Form::button('Save', ['class' => 'btn btn-default btn-xs save-edit mode2'])}}
											{{Form::button('Cancel', ['class' => 'btn btn-default btn-xs cancel-edit mode2'])}}
											{{Form::hidden('goal', $objective->id)}}
											{{Form::hidden('description', null, ['class' => 'description'])}}
											{{Form::hidden('indicator', null, ['class' => 'indicator'])}}
											{{Form::hidden('affect', 0, ['class' => 'affect'])}}
										{{ Form::close() }}
									</td>
								</tr>

								<?php $first2 = false; ?>
		                      	<?php $counter++; ?>
							@endforeach
				          @else
				            <tr>
				              <td colspan="4"> <center> <i> No existing indicator </i> </center> </td>
				            </tr>
				          @endif
					</table>
				</div>
			</div>
		</div>
		<?php $first = false ?>
	@endforeach
</div>

<div class="pull-right">
	{{ Form::button('Show All', ['id' => 'showAll', 'class' => 'btn btn-default'])}}
	{{ Form::button('Hide All', ['id' => 'hideAll', 'class' => 'btn btn-default'])}}
</div>
@stop

@section('cssjs')
<script type = "text/javascript">
	$(document).ready(function() {
		$(".mode2").hide();

		$(".edit-affect").each(function(index, el) {
			$(this).prop('checked', $(this).data("initial"));
		});

		$(".indicator-description").change(function() {
			if($(this).val() == 2)
			{
				$(".measure").find("input").removeAttr('disabled');
				$(".measure").find("input").attr('checked', true);
			}
			else
			{
				$(".measure").find("input").attr('disabled', 'disabled');
				$(".measure").find("input").attr('checked', false);
			}
		});

		$(".edit-description").change(function() {
			if($(this).val() == 2)
			{
				$(this).closest("tr").find('.edit-affect').removeClass('hidden').show();
			}
			else
			{
				$(this).closest("tr").find('.edit-affect').hide();
			}
		});


		$(".allow-edit").on("click", function() {

			$(".mode2").hide();
			$(".mode1").show();
			var indicator = $(this).closest(".indicator");
			$(this).closest("tr").find('.edit-affect').removeAttr('disabled');
			$(this).closest("tr").find(".mode1").hide();
			$(this).closest("tr").find(".mode2").show();
			indicator.find('.edit-description').val(indicator.find(".edit-affect").data("inittype"));
			indicator.find('.edit-indicator').val(indicator.find('.edit-indicator').data('original'));
		});

		$(".save-edit").on("click", function(event) {
			var indicator = $(this).closest(".indicator");

			var description 		= indicator.find('.edit-description').val();
			var name 		= indicator.find('.edit-indicator').val();
			var affect 		= indicator.find('.edit-affect').val();

			var form = $(this).closest('form');
			form.find('.description').val(description);
			form.find('.indicator').val(name);
			if(indicator.find('.edit-affect').is(':checked'))
				form.find('.affect').val(1);
			
			form.submit();
		});
		
		$(".cancel-edit").on("click", function() {
			var indicator = $(this).closest(".indicator");

			$('.edit-affect').attr('disabled', 'disabled');

			
			indicator.find(".mode2").hide();
			indicator.find(".mode1").show();
			
			if(indicator.find(".edit-affect").data("inittype") == 2)
			{
				$(this).closest("tr").find('.edit-affect').show();
			}
			else
			{
				$(this).closest("tr").find('.edit-affect').hide();
			}
			
			
			indicator.find('.edit-affect').prop('checked', indicator.find(".edit-affect").data("initial"));
			
		});

		$(".aggregation").change(function(event) {
			$(this).closest('form').submit();
		});

		$("#showAll").click(function(event) {
			$(".objective").removeClass('collapsed');
			$(".panel-collapse").addClass('in');
		});

		$("#hideAll").click(function(event) {
			$(".objective").removeClass('collapsed');
			$(".panel-collapse").removeClass('in');
		});
	});
</script>
@stop
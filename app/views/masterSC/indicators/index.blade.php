@extends('layouts.default')
@section('nav')
    @include('includes.scorecard_menu')
@stop
@section('nav2')
    @include('includes.manage_agency')
@stop
@section('content')

<?php
	$id = Session::get('id', 0);
?>

<h2>Manage Performace Indicators for Strategic Goals</h2>
<hr />


{{ Form::open(['url' => URL::route('indicators.store'), 'role' => 'form']) }}
<div class="row form-group">
	<div class="col-md-5">
		{{ Form::select('goal_id', $goalsList, $id, ['class' => 'form-control'])}}
		{{ $errors->first('goal', '<span class=errormsg>*:message</span>') }}
	</div>
	<div class="col-md-2">
		{{ Form::select('type', ['Baseline', 'Universe', 'Movement', 'Target'], null, ['class' => 'form-control indicator-type'])}}
	</div>
	<div class="col-md-2">
		{{ Form::text('indicator', null, ['class' => 'form-control', 'placeholder' => 'Enter Indicator Name']) }}
		{{ $errors->first('indicator', '<span class=errormsg>*:message</span>') }}
	</div>
	<div class="col-md-2">
		<span class="measure">
			{{ Form::checkbox('affect', '', 0) }} <small> Aggregate to Scorecard </small>
		</span>
	</div>
	<div class="col-md-1">
		{{ Form::submit('Add', ['class' => 'btn btn-primary pull-right']) }}
	</div>
</div>
{{ Form::close() }}
@if (count($goalsList)==0) 

	<div class="row">
		<center><h3>{{"There are no goals set to the highest perspective"}}</h3></center>
	</div>
@endif


@foreach($goals as $goal)
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary indicators">
				<div class="panel-heading">
					<div class="col-md-9 indicator-sg">
						{{$goal->objective}}
					</div>	
					<div class="form-group col-sm-3 pull-right indicator-formula">
					    <div class="input-group">
					      	<span class="input-group-addon">Formula</span>
					        {{Form::open(['url' => URL::route('indicators.aggregation', ['id' => $goal->id])])}}
							{{Form::hidden('id', $goal->id)}}
							{{Form::select('aggregation_type', ['Summation', 'Average'], $goal->aggregation_type, ['class' => 'form-control aggregation'])}}
						{{Form::close()}}
					    </div>
					</div>
				</div>
				<table class="table table-striped table-hover table-select">
						<thead>
							<tr>
							<th class="width-20"> Type </th>
							<th class="width-50"> Indicator </th>
							<th class="width-10" style="text-align: center;"> Aggregate to Scorecard </th>
							<th class="width-20" style="text-align: center;"> Actions </th>
						</tr>
						</thead>
						<tbody>
							@if(count($goal->indicators))
 				            <?php $indicatorsLength = count($goal->indicators);?>
              				<?php $first = true; ?>
		              		<?php $counter=0;?>

							@foreach($goal->indicators as $indicator)
								<tr class="indicator">
									<td>
										<span class = "mode1">
											@if($indicator->type==0)
												Baseline
											@elseif($indicator->type==1)
												Universe
											@elseif($indicator->type==2)
												Movement
											@elseif($indicator->type==3)
												Target
											@else
												Error
											@endif
										</span>
										{{ Form::select('type', ['Baseline', 'Universe', 'Movement', 'Target'], $indicator->type, ['class' => 'edit-type form-control mode2']) }}
									</td>
									<td>
										<span class = "mode1">
											{{$indicator->indicator}}
										</span>
										{{ Form::text('indicator', $indicator->indicator, ['class' => 'edit-indicator form-control mode2', 'data-original' => $indicator->indicator]) }}
									</td>
									<td>
										<center>
										@if($indicator->type==2)
											{{ Form::checkbox('affect', 1, $indicator->affect, ['class' => 'edit-affect', 'disabled', 'data-initial' => $indicator->affect,'data-inittype' => $indicator->type]) }}
										@else
											{{ Form::checkbox('affect', 1, $indicator->affect, ['class' => 'edit-affect hidden', 'disabled', 'data-initial' => $indicator->affect,'data-inittype' => $indicator->type]) }}
										@endif
										</center>
									</td>
									<td class="buttons">
										<div class="btn-group actions">
										{{HTML::decode (Form::button('<i class="fa fa-pencil"></i>', ['class' => 'btn btn-default action-btn hide-button-style allow-edit mode1', 'title' => 'Edit', 'data-placement' => 'bottom', 'data-toggle' => 'tooltip']))}}              
										{{HTML::decode (link_to(URL::route('indicators.destroy', ['id' => $indicator->id]), '<i class="fa fa-times"></i>', ['class'=>'btn btn-danger mode1 action-btn hide-button-style', 'onclick' => "return confirm('Are you sure?');",'title'=>'Delete']))}}

										@if( ! $first)
											{{ HTML::decode (link_to(URL::route('indicators.moveUp', ['id' => $indicator->id, 'gid' => $goal->id]), '<i class="fa fa-chevron-up"></i>', ['name' => 'move-up', 'class'=>'btn btn-default action-btn hide-button-style mode1','title'=>'Move Up','type'=>'submit']))}}

										@endif
										@if($indicator->type == 2 && $counter != $indicatorsLength-1)
											{{ HTML::decode ((link_to(URL::route('indicators.moveDown', ['id' => $indicator->id, 'gid' => $goal->id]), '<i class="fa fa-chevron-down"></i>', ['name' => 'move-down', 'class'=>'btn btn-default action-btn hide-button-style mode1','title'=>'Move Down','type'=>'submit'])))}}
										
										@endif
										</div>
										
										{{Form::open(['url' => URL::route('indicators.update', ['id' => $indicator->id])]) }}
											{{Form::button('Save', ['class' => 'btn btn-default btn-xs save-edit mode2'])}}
											{{Form::button('Cancel', ['class' => 'btn btn-default btn-xs cancel-edit mode2'])}}
											{{Form::hidden('goal', $goal->id)}}
											{{Form::hidden('type', null, ['class' => 'type'])}}
											{{Form::hidden('indicator', null, ['class' => 'indicator'])}}
											{{Form::hidden('affect', 0, ['class' => 'affect'])}}
										{{ Form::close() }}
									</td>
								</tr>

								<?php 
									if($indicator->type == 2)
										$first = false; 
								?>
		                      	<?php $counter++; ?>
							@endforeach
				          @else
				            <tr>
				              <td colspan="4"> <center> <i> No existing indicator </i> </center> </td>
				            </tr>
				          @endif
						</tbody>
					</table>
			</div>
		</div>
	</div>
@endforeach
@stop

@section('cssjs')
<script type = "text/javascript">
	$(document).ready(function() {
		$(".mode2").hide();

		$(".measure").find("input").removeAttr('checked');
		$(".measure").find("input").attr('disabled', 'disabled');

		$(".edit-affect").each(function(index, el) {
			$(this).prop('checked', $(this).data("initial"));
		});

		$(".indicator-type").change(function() {
			if($(this).val() == 2)
			{
				$(".measure").find("input").removeAttr('disabled');
				$(".measure").find("input").attr('checked', true);
			}
			else
			{
				$(".measure").find("input").attr('disabled', 'disabled');
				$(".measure").find("input").attr('checked', false);
			}
		});

		$(".edit-type").change(function() {
			if($(this).val() == 2)
			{
				$(this).closest("tr").find('.edit-affect').removeClass('hidden').show();
			}
			else
			{
				$(this).closest("tr").find('.edit-affect').hide();
			}
		});


		$(".allow-edit").on("click", function() {

			$(".mode2").hide();
			$(".mode1").show();
			var indicator = $(this).closest(".indicator");
			$(this).closest("tr").find('.edit-affect').removeAttr('disabled');
			$(this).closest("tr").find(".mode1").hide();
			$(this).closest("tr").find(".mode2").show();
			indicator.find('.edit-type').val(indicator.find(".edit-affect").data("inittype"));
			indicator.find('.edit-indicator').val(indicator.find('.edit-indicator').data('original'));
		});

		$(".save-edit").on("click", function(event) {
			var indicator = $(this).closest(".indicator");

			var type 		= indicator.find('.edit-type').val();
			var name 		= indicator.find('.edit-indicator').val();
			var affect 		= indicator.find('.edit-affect').val();

			var form = $(this).closest('form');
			form.find('.type').val(type);
			form.find('.indicator').val(name);
			if(indicator.find('.edit-affect').is(':checked'))
				form.find('.affect').val(1);
			
			form.submit();
		});
		
		$(".cancel-edit").on("click", function() {
			var indicator = $(this).closest(".indicator");

			$('.edit-affect').attr('disabled', 'disabled');

			
			indicator.find(".mode2").hide();
			indicator.find(".mode1").show();
			
			if(indicator.find(".edit-affect").data("inittype") == 2)
			{
				$(this).closest("tr").find('.edit-affect').show();
			}
			else
			{
				$(this).closest("tr").find('.edit-affect').hide();
			}
			
			
			indicator.find('.edit-affect').prop('checked', indicator.find(".edit-affect").data("initial"));
			
		});

		$(".aggregation").change(function(event) {
			$(this).closest('form').submit();
		});
	});
</script>
@stop
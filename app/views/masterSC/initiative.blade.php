@extends('layouts.default')
@section('content')
<h2 class="pull-left">{{ HTML::decode (link_to( "/stratInit", '<i class="fa fa-chevron-left fw"></i>', array('class'=>'link-black hidden-print','title'=>'Initiative Profiles List') )) }} Initiative Profile
</h2>
<div class="btn-group pull-right btn-option hidden-print">
    <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown">
    <i class="fa fa-bars fw"></i> Options <span class="caret"></span>
    </button>
    <ul class="dropdown-menu pull-right btn-option-dropdown" role="menu">
	    @if(Role::access('0'))
			@if($initiative->status==0)
				<li>{{ HTML::decode(link_to("/initiative/{$initiative->id}/edit", '<i class="fa fa-pencil fw"></i> Edit')) }}</li>
				<li>{{ HTML::decode(link_to( "/initiatives/setfinal/{$initiative->id}", '<i class="fa fa-check fw"></i> Mark as Final')) }}</li>

			@else
				<li>{{ HTML::decode(link_to( "/initiatives/unsetfinal/{$initiative->id}", '<i class="fa fa-file-text fw"></i> Mark as Draft')) }}</li>
			@endif
		@elseif($initiative->status==0)
			<li>{{ HTML::decode(link_to("/initiative/{$initiative->id}/edit", '<i class="fa fa-pencil fw"></i> Edit', array('class' => 'btn btn-default'))) }}</li>
		@endif
		<li class="divider"></li>
		<li><a href="javascript:window.print()"><i class="fa fa-file fw"></i> Export as PDF</a></li>
    </ul>
</div>
<hr class="clear" />

<div class="row measure-profile">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-body remove-padding">
				<table class="table table-borderless no-inner-border no-top-border">
					<tr>
						<th colspan="2"><strong>Name of Project</strong></th>
						<td>{{ $initiative->projectName }}</td>
					</tr>
					<tr>
						<th colspan="2"><strong>Project Description</strong></th>
						<td>{{ $initiative->projectDesc }}</td>
					</tr>
					<tr>
						<th colspan="2"><strong>Measures Affected</strong></th>
						<td>{{ $initiative->measuresList() }}</td>
					</tr>
					<tr>
						<th colspan="2"><strong>Project Co-Champion</strong></th>
						<td> {{ $initiative->coChampionsList() }} </td>
					</tr>
					<tr>
						<th colspan="2"><strong>Project Team</strong></th>
						<td> {{ $initiative->teamsList() }} </td>
				</table>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-body remove-padding">
				<table class="table table-borderless no-top-border">
					<tr>
						<th colspan="8" class="center-text"><strong>INITIATIVES SCORING</strong></th>
					</tr>
					<tr>
						<th colspan="3"><strong>Project Milestones</strong></th>
						<th colspan="2"><strong>Budget</strong></th>
						<th colspan="3"><strong>Status</strong></th>
					</tr>
					<tr>
						<th colspan="2"><strong>Period</strong></th>
						<th rowspan="2"><strong>Milestones/Activities</strong></th>
						<th rowspan="2"><strong>Amount</strong></th>
						<th rowspan="2"><strong>Source</strong></th>
						<th rowspan="2"><strong>Owner</strong></th>
						<th rowspan="2"><strong>Status</strong></th>
					</tr>
					<tr>
						<th><strong>Start</strong></th>
						<th><strong>End</strong></th>
					</tr>
					@foreach($initiative->projectMilestones as $PM)
						<tr>
							<td>{{$PM->start_date}}</td>
							<td>{{$PM->end_date}}</td>
							<td>{{$PM->milestones}}</td>
							<td>{{ 'Php ' . number_format($PM->budget_amount, 2)}}</td>
							<td>{{$PM->budget_source}}</td>
							@if($offices->find($PM->owner->office_id))
								<td>{{ $offices->find($PM->owner->office_id)->office_abbreviation }}</td>
							@else
								<td>No Office</td>
							@endif
							<td>
								@if($PM->status4)
									<div class="status">
										<div class="head"> 4th quarter: </div>
										<div class="box body">{{nl2br($PM->status4) }} </div>
									</div>
								@endif
								@if($PM->status3)
									<div class="status">
										<div class="head"> 3rd quarter: </div>
										<div class="box body">{{nl2br($PM->status3) }} </div>
									</div>
								@endif
								@if($PM->status2)
									<div class="status">
										<div class="head"> 2nd quarter: </div>
										<div class="box body">{{nl2br($PM->status2) }} </div>
									</div>
								@endif
								@if($PM->status1)
									<div class="status">
										<div class="head"> 1st quarter: </div>
										<div class="box body">{{nl2br($PM->status1) }} </div>
									</div>
								@endif
							</td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>
	</div>
</div>


<div class="panel panel-default remarks">
	<div class="panel-heading box-header-default">
		Remarks
		<div class="pull-right hidden-print">
			{{Form::label('filter', 'Filter:') }} 
			{{ Form::select('filter', ['Comments', 'Suggestions', 'All'], 2, ['class' => 'baseline filter'])}}
		</div>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class=" list-group-item hidden-print">
				<div class="row">
					{{ Form::open(['url' => URL::route('initiative.remark', ['id' => $initiative->id])])}}
						<div class="col-md-2">
							{{ Form::radio('type', 0, 1)}} Comment <br/>
							{{ Form::radio('type', 1, 0)}} Suggestion
						</div>
						<div class="col-lg-10">
			                <div class="input-group">
			                  {{ Form::text('remark', null, ['class' => 'form-control', 'rows' => 3])}}
			                  <span class="input-group-btn">
			                  {{ Form::submit('Add Remark', ['class' => 'btn btn-primary'])}}
			                  </span>
			                </div><!-- /input-group -->
			                {{ $errors->first('remark', '<span class=errormsg>*:message</span>') }}
			            </div><!-- /.col-lg-10 -->
				</div>
			</div>
			@if(count($remarks))
				<?php $show_more = true; ?>
				<div class="show-more">
					@foreach($remarks as $key => $remark)
						@if($show_more && ((count($remarks)-3) <= $key))
							</div>
							<?php $show_more = false; ?>
						@endif
						<div class="list-group-item remark{{$remark->type}}">
							<strong> {{ $remark->user->fullName() }} </strong>
							{{ $remark->remark }} <br/>
							<i class="livetimestamp"> 
								{{ $remark->type? "suggested" : "commented"}} {{ $remark->created_at->diffForHumans() }} 
							</i>
						</div>
					@endforeach
				@if(count($remarks) > 3)
					<div class="list-group-item">
						<center>
							{{ Form::button('See All', ['class' => 'btn btn-default remarks-collapse'])}}
						</center>
					</div>
				@endif
			@endif
		</div>
	</div>
</div>

@stop

@section('cssjs')

	{{ HTML::script('js/remarks.js')}}
	<script type="text/javascript">
	$(document).ready(function() {
		$(".status").find(".head").click(function(event) {
			$(this).closest('.status').find(".body").slideToggle('slow')
		});		
	});
	</script>
@stop
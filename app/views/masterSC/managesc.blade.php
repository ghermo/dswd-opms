@extends('layouts.default')
@section('nav')
    @include('includes.scorecard_menu')
@stop
@section('nav2')
    @include('includes.manage_agency')
@stop
@section('content')

<h2>Manage Scorecards</h2>
<hr />

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<i class="fa fa-table fw"></i> Scorecards
				<div class="pull-right">
				    {{ HTML::decode(link_to_route('scorecards.create', '<i class="fa fa-plus fw"></i> Create New', null)) }}
				</div>
			</div>
			<div class="panel-body remove-padding">
				<table class="table table-striped table-hover table-select">
					<thead>
						<tr>
							<th class="width-50">Scorecard</th>
							<th class="width-30">Date Created</th>
							<th class="width-20">Action</th>
						</tr>
					</thead>

					<tbody>
						<?php $counter=0;?>
						@foreach($scorecards as $scorecard)
							@if($scorecard->archive==0)
								<?php $counter++;?>
							@endif
						@endforeach

						@if($counter>0)
							@foreach($scorecards as $scorecard)
								@if($scorecard->archive==0)
									<div id="entry">
										@if($scorecard->flag==1)
										<tr class="info">
										@else
										<tr>
										@endif
											<td>{{$scorecard->name}}</td>
											<td><small>{{$scorecard->created_at}}</small></td>
											<td>
												<div class="btn-group actions">
												{{ HTML::decode (link_to( URL::route('scorecards.edit', ['id' => $scorecard->id]), '<i class="fa fa-pencil fw"></i>', ['class'=>'btn btn-default action-btn hide-button-style', 'title' => 'Edit'] )) }}
												@if(! $scorecard->flag)

													{{ HTML::decode (link_to( "/scorecard/setcurrent/$scorecard->id", '<i class="fa fa-check fw"></i>', array('class'=>'btn btn-success action-btn hide-button-style', 'title' => 'Set as Current') )) }}
													{{ HTML::decode (link_to( "scorecard/setarchive/$scorecard->id", '<i class="fa fa-archive fw"></i>', array('class'=>'btn btn-info action-btn hide-button-style', 'title' => 'Archive','onclick' => 'return confirm("Are you sure to archive this?")') )) }}
												@endif
												</div>
											</td>
										</tr>
									</div>
								@endif
							@endforeach
						@else
							<td colspan="3"><b><center>No scorecards available. Check the archives.</center></b></td>
						@endif
					</tbody>
				</table>
			</div>				
		</div>
	</div>
</div>
<div class="pull-left">
	<h5>Legend:</h5> <i class="fa fa-square fw" style="color: #c4e3f3;"></i> = Current Scorecard
</div>
<div class="pull-right">
	{{ HTML::decode(link_to('scorecard/archives', '<i class="fa fa-archive fw"></i> Archives', array('class' => 'btn btn-info'))) }}
</div>
@stop

@section('cssjs')
@stop
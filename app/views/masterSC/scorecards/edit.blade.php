@extends('layouts.default')
@section('content')

<h2>Edit Scorecard</h2>
<hr />
{{Form::model($scorecard, ['role' => 'form'])}}
<div class="box">
  <div class="box-body">
  <br/> <br/>
    <div class="container">
      <div class="col-md-6">
        <div class="form-group">

          {{ Form::label('name', 'Scorecard Name*'); }} <span> <font color="red">{{$errors->first('name')}}</font></span>
         
          {{ Form::text('name', null, ['id' => 'input1', 'class' => 'form-control', 'placeholder' => 'Enter scorecard name']) }}
        </div>
        <div class="form-group">
          {{ Form::label('description', 'Description'); }}
          <span id='error2' class='errormsg'>Invalid description.</span>
          {{ Form::textarea('description', null, ['id' => 'input2', 'class' => 'form-control', 'placeholder' => 'Enter description', 'rows' => 4]) }}
        </div>
      </div>
      <div class="col-md-6">
        <?php $years = [] ?>
        @for($year = $baseline-1; $year < $baseline+17; $year++)
          <?php array_push($years, $year); ?>
        @endfor
        <table id="years" class='table table-responsive table-center-headers'>
          <td colspan="6">
            <center><b>Select years to be used to measure targets* </b><span><font color="red">{{$errors->first('years')}}</font></span></center>
        <br/>
      
          </td>
          @for($i = 0; $i < count($years); $i++)
            @if($i % 6 == 0)
              <tr>
            @endif

            <td>
              <?php 
                 $input=Input::old();
               
                
               ?>
              @if(isset($input['years']))
                <?php  $years1=$input['years'];?>

               @if($years[$i]==$years1[0])
                 {{Form::button($years[$i], ['class' => "btn btn-default btn-xs year btn-primary btn-danger"])}}
               @elseif(in_array($years[$i],$years1))
                 {{Form::button($years[$i], ['class' => "btn btn-default btn-xs year btn-primary"])}}
               @else
                 {{Form::button($years[$i], ['class' => "btn btn-default btn-xs year"])}}
               @endif 
             @elseif(count(Input::old()))
                {{Form::button($years[$i], ['class' => "btn btn-default btn-xs year"])}}
             @else
               @if($baseline == $years[$i])
                 {{Form::button($years[$i], ['class' => "btn btn-default btn-xs year btn-primary btn-danger"])}}
               @elseif(in_array($years[$i], $targetYears))
                 {{Form::button($years[$i], ['class' => "btn btn-default btn-xs year btn-primary"])}}
               @else
                 {{Form::button($years[$i], ['class' => "btn btn-default btn-xs year"])}}
               @endif 
             @endif
            </td>

            @if($i % 6 == 5)
              </tr>
            @endif
          @endfor
          <tr>
            <td colspan="6">
              <b> Legend: </b> <br/>
              <i class="fa fa-circle fa-1 red"></i> Baseline <br/>
              <i class="fa fa-circle fa-1 blue"></i> Target Year
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
{{Form::button('Save', ['class'=>'btn btn-primary btn-save'])}}
{{ link_to('/scorecards', 'Cancel', array('class'=>'btn btn-default') ) }} 

{{ Form::close()}}
@stop

@section('cssjs')
<script type="text/javascript">
  $(document).ready(function() {
    $('.errormsg').hide();

    $('.year').click(function() {
      $(this).toggleClass('btn-primary');
      
      $("#years").find(".btn-danger").removeClass("btn-danger")
      var baseline = $("#years").find(".btn-primary").first();
      baseline.addClass("btn-danger");
    });

    $('.btn-save').click(function() {
      var form = $(this).closest('form');

      $('#years').find(".btn-primary").each( function() {
        form.append($("<input name='years[]' value='"+$(this).text()+"' type='hidden'>"));
      });

      form.submit();
    });
  });
</script>
@stop
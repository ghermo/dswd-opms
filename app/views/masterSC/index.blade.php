@extends('layouts.default')
@section('nav')
    @include('includes.scorecard_menu')
@stop
@section('nav2')
    @include('includes.manage_agency')
@stop
@section('content')
<h2 class="hidden-print">Scorecard</h2><h2 class="visible-print">DSWD Scorecard</h2>
<hr />
@if($scorecard != null)
    <div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary print">
			<div class="panel-heading">
				<i class="fa fa-table fw"></i> DSWD SCORECARD
			</div>
			<div class="panel-body remove-padding">
            <table style="text-align:center;" class="table table-bordered table-center-all font-12">
                <thead>
                    <tr>
                        <th rowspan="3" style="text-align:center;"> <span title="Target">P</span> </th>
                        <th rowspan="3" style="text-align:center;"> &nbsp; </th>
                        <th rowspan="3" style="text-align:center;"> Objectives/Goals </th>
                        <th rowspan="3" style="text-align:center;"> # </th>
                        <th rowspan="3" style="text-align:center;"> Measure </th>
                        <th rowspan="3" style="text-align:center;"> Owner</th>
                        <th colspan="2" style="text-align:center;"> <span title="Baseline">BL</span></th>
                        <th colspan="{{($scorecard->years()->count()-1)*2}}" style="text-align:center;">Targets</th>
                    </tr>
                    <tr>
                        <?php $first = true; ?>
                        @foreach($scorecard->years as $year)
                            <th colspan="2" rowspan="{{$first?'2':'1'}}"> {{$year->year}} </th>
                            <?php $first = false; ?>
                        @endforeach
                    </tr>
                    <tr>
                        @if(count($scorecard->years)-1)
                            @for($i = 0; $i < count($scorecard->years)-1; $i++)
                                <th> <span title="Target">T</span> </th>
                                <th> <span title="Accomplishment">A</span> </th>
                            @endfor
                        @else
                            <td> &nbsp; </td>
                        @endif
                    </tr>
                </thead>
                <style>
                    .perspective
                    {
                        margin-top: 40%;
                       
                        filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
                        -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
                        -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
                        -ms-transform: rotate(-90.0deg);  /* IE9+ */
                        -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
                        -webkit-transform: rotate(-90.0deg);  /* Safari 3.1+, Chrome */
                        transform: rotate(-90.0deg);  /* Standard */

                    }
                </style>
                <?php $letter = 'A'; $number = 1; ?>
                <?php $highest = true; ?>

                @foreach($scorecard->perspectives as $perspective)

                    <?php $first = true; ?>
                  
                    <?php $objectives = Objective::where('perspective_id', '=', $perspective->id)->orderBy('ordinal')->get(); ?>
                    @foreach($objectives as $objective)

                        <tr>
                            @if($first)
                                <td rowspan="{{$perspective->objectives()->count()}}">
                                    <span title="{{$perspective->perspective}}">
                                    {{ StringManipulation::abbreviate($perspective->perspective) }}
                                    </span>
                                </td>
                            @endif
                            <td>{{$letter++}}</td>
                            <td class="{{$highest ? 'info active' : ''}}">{{$objective->objective}}</td>
                            <td>{{$number++}}</td>
                            @if(count($objective->measure))
                                <td>
                                    <?php $measure = $objective->measure ?>
                                    <span class="hidden-print">{{ link_to(URL::route('measure.show', ['id' => $measure->id]), $measure->description) }}</span>
                                    <span class="visible-print">{{ $measure->description }}</span>
                                </td>
                                <td>{{$measure->owners()}}</td>
                                <?php $baseline = true; ?>
                                <?php $id=0;?>
                                @foreach($measure->targets2($scorecard->years)->get() as $target)
                                    @if($baseline)
                                        <td colspan="2">{{$target->target? number_format($target->target, 0, '.', ',') : '&nbsp;'}}</td>
                                    @else
                                        <td>{{$target->target? number_format($target->target, 0, '.', ',') : '&nbsp;'}}</td>
                                        <td>
                                                {{$target->accomplishment? number_format($target->accomplishment, 0, '.', ','): '&nbsp;'}}
                                        </td>
                                        
                                    @endif
                                    <?php $baseline = false; ?>
                                    <?php $id++;?>
                                @endforeach
                            @else
                                 <td> No measure found ({{Measure::find($objective->measure_id)}}) </td>
                            
                                <td colspan="5">&nbsp;</td>
                            @endif
                        </tr>
                        <?php $first = false; ?>
                    @endforeach
                   <?php $highest = false; ?>
                @endforeach
            </table>
            </div>
        </div>
    </div>
    </div>
@else
    <h3> No existing scorecards </h3>
@endif

    <div class="row">
        <div class="col-md-12">
            <h5>Legend:</h5>
            <p>P = Perspective</p>
            <p>BL = Baseline</p>
            <p>T = Target</p>
            <p>A = Accomplishment</p>
        </div>
    </div>
@stop
@extends('layouts.default')
@section('content')


	<?php
		if($oldMeasures = Session::get('oldMeasures'))
			foreach ($oldMeasures as $measure) 
				unset($measures[$measure->id]);

		if($oldCoChampions = Session::get('oldCoChampions'))
			foreach ($oldCoChampions as $office) 
				unset($offices[$office->id]);

		if($oldTeams = Session::get('oldTeams'))
			foreach ($oldTeams as $office) 
				unset($offices[$office->id]);
	?>

	<h2>Create New Initiative</h2>
	<hr />
	{{Form::open(['url' => URL::route("initiatives.store"), 'id' => 'form'])}}
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					{{ Form::label('projName', 'Name of Project: * ') }}
					{{ $errors->first('projName', '<span class=errormsg>:message</span>') }}
					{{ Form::text('projName','',['class'=>'form-control'])}}
				</div>
				<div class="form-group">
					{{ Form::label('projDesc', 'Project Description: * ') }}
					{{ $errors->first('projDesc', '<span class=errormsg>:message</span>') }}
					{{ Form::text('projDesc','',['class'=>'form-control'])}}
				</div>
				<div class="form-group">

					{{ Form::label('measuresAffected', 'Measures Affected: * ') }} 
					{{ $errors->first('measures', '<span class=errormsg>:message</span>') }}
					
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="form-group margin-bottom-0">

								<div class="input-group">
									{{Form::select('measuresAffected', $measures, null, ['id' => 'selectedMeasure', 'class' => 'form-control']); }}
									<span class="input-group-btn">
								  		<button id="addMeasure" class="btn btn-default" type="button"><i class="fa fa-plus fa-fw"></i> Add to List</button>
									</span>
								</div>

							</div>
						</div>
						<div class="box-body">
							<table id="measures" class="table">
								@if($oldMeasures)
									@foreach($oldMeasures as $measure)
										<tr> 
											<td> <input type="hidden" name="measures[]" value="{{$measure->id}}"> &nbsp; </td>
											<td width="75%"> {{$measure->description}} </td> 
											<td width="25%">
												<a class="removeItem" data-id="{{$measure->id}}" title="Delete">
													<i class="fa fa-times" style="color:red;"></i>
												</a>
											</td>

										</tr>
									@endforeach
								@else
									<tr class="empty"><td><center><i>No measure(s) selected.</i></center></td></tr>
								@endif
							</table>
						</div>
					</div>

					<div class="form-group">
						{{ Form::label('coChampions', 'Project Co-Champion: * ') }}
						{{ $errors->first('coChampions', '<span class=errormsg>:message</span>') }}
						
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="form-group margin-bottom-0">
									<div class="input-group">
										{{Form::select('coChampion', $offices, null, ['id' => 'selectedCoChampion', 'class' => 'form-control']); }}
										<span class="input-group-btn">
									  	<button id="addCoChampion" class="btn btn-default" type="button"><i class="fa fa-plus fa-fw"></i> Add to List</button>
										</span>
									</div>
								</div>
							</div>
							<div class="box-body">
								<table id="coChampions" class="table">
									@if($oldCoChampions)
										@foreach($oldCoChampions as $office)
											<tr> 
												<td width="0%"> <input type="hidden" name="coChampions[]" value="{{$office->id}}"> &nbsp; </td>
												<td width="75%"> {{$office->office_abbreviation}} </td> 
												<td width="25%">
													<a class="removeItem" data-id="{{$office->id}}" title="Delete">
														<i class="fa fa-times" style="color:red;"></i>
													</a>
												</td>
											</tr>
										@endforeach
									@else
										<tr class="empty"><td><center><i>No office(s) selected.</i></center></td></tr>
									@endif
								</table>
							</div>
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('teams', 'Project Team: ') }}
						{{ $errors->first('teams', '<span class=errormsg>:message</span>') }}
						
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="form-group margin-bottom-0">
									<div class="input-group">
										{{ Form::select('projTeam', $offices, null, ['id' => 'selectedTeam', 'class' => 'form-control']); }}
										<span class="input-group-btn">
									  	<button id="addTeam" class="btn btn-default" type="button"><i class="fa fa-plus fa-fw"></i> Add to List</button>
										</span>
									</div>
								</div>
							</div>
							<div class="box-body">
								<table id="teams" class="table">
									@if($oldTeams)
										@foreach($oldTeams as $office)
											<tr> 
												<td width="0%"> <input type="hidden" name="teams[]" value="{{$office->id}}"> &nbsp; </td>
												<td width="75%"> {{$office->office_abbreviation}} </td> 
												<td width="25%">
													<a class="removeItem" data-id="{{$office->id}}" title="Delete">
														<i class="fa fa-times" style="color:red;"></i>
													</a>
												</td>
											</tr>
										@endforeach
									@else
										<tr class="empty"><td><center><i>No office(s) selected.</i></center></td></tr>
									@endif
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading" style="text-align: center;">
					INITIATIVES SCORING
				</div>
				<div class="box-body">
					<table id="project-milestones" class="table table-center-headers table-borderless no-top-border table-striped">
						<thead>
							<tr>
								<th width="50%" colspan="3"><strong>Project Milestones</strong></th>
								<th width="25%" colspan="2"><strong>Budget</strong></th>
								<th width="25%" colspan="3"><strong>Status</strong></th>
							</tr>
							<tr>
								<th colspan="2"><strong>Period</strong></th>
								<th rowspan="2" style="vertical-align:middle;"><strong>Milestones/Activities</strong></th>
								<th rowspan="2" style="vertical-align:middle;"><strong>Amount</strong></th>
								<th rowspan="2" style="vertical-align:middle;"><strong>Source</strong></th>
								<th rowspan="2" style="vertical-align:middle;"><strong>Owner</strong></th>
								<th rowspan="2"><strong>Status</strong></th>
							</tr>
							<tr>
								<th style="vertical-align:middle;"><strong>Start</strong></th>
							<th style="vertical-align:middle;"><strong>End</strong></th>
							</tr>
							<tr>
								<td>{{Form::text('start_period', null, ['id' => 'input-start', 'class' => 'form-control', 'placeholder' => 'Start Date'])}}</td>
								<td>{{Form::text('end_period', null, ['id' => 'input-end', 'class' => 'form-control', 'placeholder' => 'End Date'])}}</td>
								<td>{{Form::text('milestone', null, ['id' => 'input-milestone', 'class' => 'form-control', 'placeholder' => 'Milestone/Activity'])}}</td>
								<td>{{Form::text('amount', null, ['id' => 'input-amount', 'class' => 'form-control input-numbers', 'placeholder' => 'Amount'])}}</td>
								<td>{{Form::text('source', null, ['id' => 'input-source', 'class' => 'form-control', 'placeholder' => 'Source'])}}</td>
								<td>{{Form::select('owner', $users, null, ['id' => 'input-owner', 'class' => 'form-control'])}}</td>
								<td>{{Form::textarea('status', null, ['id' => 'input-status', 'rows' => 3, 'class' => 'form-control', 'placeholder' => 'Status'])}}</td>
								<td>{{Form::button('<i class="fa fa-pencil fw"></i>', ['id' => 'addPM', 'class' => 'btn btn-default btn-sm pull-right','title'=>'Add Entry'])}}</td>
							</tr>
							@if(count(Input::old()))
			                  @for($i=0; count(Input::old('PM'.$i.'a')); $i++)
			                    <tr>
									<td>{{Form::text('PM'.$i.'a', null, ['id' => 'input-start', 'class' => 'form-control', 'placeholder' => 'Start Date'])}}</td>
									<td>{{Form::text('PM'.$i.'b', null, ['id' => 'input-end', 'class' => 'form-control', 'placeholder' => 'End Date'])}}</td>
									<td>{{Form::text('PM'.$i.'c', null, ['id' => 'input-milestone', 'class' => 'form-control', 'placeholder' => 'Milestone/Activity'])}}</td>
									<td><div class="input-group"><span class="input-group-addon">Php</span>
										{{Form::text('PM'.$i.'d', null, ['id' => 'input-amount', 'class' => 'form-control input-numbers', 'placeholder' => 'Amount'])}}
										</div>
									</td>
									<td>{{Form::text('PM'.$i.'e', null, ['id' => 'input-source', 'class' => 'form-control', 'placeholder' => 'Source'])}}</td>
									<td>{{Form::select('PM'.$i.'f', $users, null, ['id' => 'input-owner', 'class' => 'form-control'])}}</td>
									<td> 

									<div class="status"> <div class="head"> 4th quarter: </div> 
										{{Form::textarea('PM'.$i.'j', null, ['id' => 'input-start', 'class' => 'form-control', 'placeholder' => 'Start Date'])}} 
									</div> 

									<div class="status"> <div class="head"> 3rd quarter: </div> 
										{{Form::textarea('PM'.$i.'i', null, ['id' => 'input-start', 'class' => 'form-control', 'placeholder' => 'Start Date'])}} 
									</div> 

									<div class="status"> <div class="head"> 2nd quarter: </div> 
										{{Form::textarea('PM'.$i.'h', null, ['id' => 'input-start', 'class' => 'form-control', 'placeholder' => 'Start Date'])}} 
									</div> 

									<div class="status"> <div class="head"> 1st quarter: </div> 
										{{Form::textarea('PM'.$i.'g', null, ['id' => 'input-start', 'class' => 'form-control', 'placeholder' => 'Start Date'])}}
									</div>

									<td><button class="btn btn-danger delete-pm" type="button"><i class="fa fa-trash-o"></i></button></td>

								</tr>
			                  @endfor
			                @endif
						</thead>
					</table>
				</div>
			</div>
		<div class="pull-left">
			<button type="submit" name="create1" class="btn btn-primary" value="create">Create</button>
			<button type="submit" name="create2"class="btn btn-default" value="another">Create and Create Another</button>
			{{ link_to( URL::route("initiatives.index"), 'Cancel', array('class'=>'btn btn-default') ) }}
		</div>	
		{{Form::close()}}
@stop

@section('cssjs')
	{{HTML::script('js/createinit.js')}}
@stop
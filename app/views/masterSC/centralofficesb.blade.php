@extends('layouts.default')
@section('nav')
	@include('includes.scorecard_menu')
@stop
@section('nav2')
	@include('includes.office_scoreboard_menu')
@stop
@section('content')

<h2>Manage Office Scoreboards - Central Office</h2>
<hr />
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<i class="fa fa-table fw"></i> Central Office Scoreboard
			</div>
			<div class="panel-body">
				<div class="panel-group" id="accordion">
					@foreach(Scorecard::current()->years as $year)
						<div class="panel panel-default">
							<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#year{{$year->year}}">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#year{{$year->year}}" class="{{$year->year == date('Y') ? '' : 'collapsed'}}">
										{{$year->year}}
									</a>
								</h4>
							</div>
							<div id="year{{$year->year}}" class="panel-collapse collapse {{$year->year == date('Y') ? 'in' : ''}}">
								<div class="panel-body remove-padding">
									<table class="table table-striped table-hover table-select">
										<thead>
											<tr>
												<th class="width-10">Month </th>
												<th class="width-50">Scoreboard</th>
												<th class="width-10">Date Created</th>
												<th class="width-10">Status</th>
												<th class="width-20">Actions</th>
											</tr>
										</thead>
										<tbody>
											@for($i = 0; $i < 12; $i++)
												<?php $month = Date::$months[$i] ?>
												<?php $found = false; ?>
												@foreach($officesc as $ofsc)
													@if($ofsc->type == 1 && $ofsc->month == $i && $ofsc->year == $year->year)
														<tr>
															<td style="min-height: 57px; height: 57px;"> {{$month}} </td>
															<td>{{ link_to('/officesb/central/showsb/'.$ofsc->id, $ofsc->name()) }}</td>
															<td> {{ $ofsc->created_at }} </td>
															<td>
																@if($ofsc->flag)
																	<span class="label label-success">Final</span>
																@elseif($ofsc->for_approval)
																	<span class="label label-warning">Approval</span>
																@else
																	<span class="label label-default">Draft</span>
																@endif
															</td>
															<td>
																<div class="btn-group actions">
																@if(Role::access('0'))
																	@if ($ofsc->flag == 0)
																	
																	{{ HTML::decode (link_to( "/officesb/central/sbedit/".$ofsc->id, '<i class="fa fa-pencil fw"></i>', array('class'=>'btn btn-default inline action-btn hide-button-style','title'=>'Edit') )) }}
																	{{ HTML::decode (link_to( "/officesb/central/setasfinal/{$ofsc->id}", '<i class="fa fa-check fw"></i>', array('class'=>'btn btn-success inline action-btn hide-button-style','data-id'=>$ofsc->id,'title'=>'Mark as Final', 'onclick' => "return confirm('Are you sure you want to mark this as final?')") )) }}
																	{{ HTML::decode (link_to( "/officesb/central/delete/{$ofsc->id}", '<i class="fa fa-times fw"></i>', array('class'=>'btn btn-danger inline action-btn hide-button-style','title'=>'Delete','data-id'=>$ofsc->id, 'onclick' => "return confirm('Are you sure you want to delete this?')") )) }}
																	@elseif( ! $ofsc->for_approval)
																	{{ HTML::decode (link_to( "/officesb/central/sbedit/".$ofsc->id, '<i class="fa fa-pencil fw"></i>', array('class'=>'btn btn-default inline action-btn hide-button-style','title'=>'Edit') )) }}
																	{{ HTML::decode (link_to( "/officesb/central/unsetasfinal/{$ofsc->id}", '<i class="fa fa-file-text fw"></i>', array('class'=>'btn btn-default inline action-btn hide-button-style','title'=>'Mark as Draft') )) }}
																	@endif
																@else
																	@if ($ofsc->flag == 0)
																	{{ HTML::decode (link_to( "/officesb/central/sbedit/".$ofsc->id, '<i class="fa fa-pencil fw"></i>', array('class'=>'btn btn-default inline action-btn hide-button-style','title'=>'Edit') )) }}
																	@if(Role::access('4'))
																	{{ HTML::decode (link_to( "/officesb/central/setasfinal/{$ofsc->id}", '<i class="fa fa-check fw"></i>', array('class'=>'btn btn-success inline action-btn hide-button-style','title'=>'Mark as Final', 'onclick' => "return confirm('Are you sure you want to mark this as final?')") )) }}
																	@elseif( ! $ofsc->for_approval)
																	{{ HTML::decode (link_to( "/officesb/central/forapproval/{$ofsc->id}", '<i class="fa fa-thumbs-up fw"></i>', array('class'=>'btn btn-warning inline action-btn hide-button-style','title'=>'Submit for Approval', 'onclick' => "return confirm('Are you sure you want to request for the approval of this?')") )) }}
																	@endif
																	{{ HTML::decode (link_to( "/officesb/central/delete/{$ofsc->id}", '<i class="fa fa-times fw"></i>', array('class'=>'btn btn-danger inline action-btn hide-button-style','title'=>'Delete','data-id'=>$ofsc->id, 'onclick' => "return confirm('Are you sure you want to delete this?')") )) }}
																	
																	@endif
																@endif
																</div>
															</td>
														</tr>
														<?php $found = true ?>
													@endif
												@endforeach
												@if( ! $found || Role::access('0'))
													<tr>
														<td>{{$month}} </td>
														<td colspan="3"> &nbsp; </td>
														<td style="min-height: 57px; height: 57px;">
															<div class="btn-group actions">
														    {{ HTML::decode(link_to_route('sb.centralCreate', '<i class="fa fa-plus fw"></i>', ['month' => $i, 'year' => $year->year], ['class' => 'btn btn-primary action-btn hide-button-style', 'title'=>"Create"])) }}
														    </div>
														</td>
													</tr>
												@endif
											@endfor
										</tbody>
									</table>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
					
@stop
@section('cssjs')
  {{ HTML::script('js/remarks.js')}}
@stop
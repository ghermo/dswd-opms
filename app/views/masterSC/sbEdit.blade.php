@extends('layouts.default')
@section('content')
{{ link_to( '/officesc/sc/1', '&larr; Back', array('class'=>'btn btn-default') ) }}	
{{ link_to( '/officesc/sc/1', 'Save', array('class'=>'btn btn-default') ) }}	
<br/><br/>
<div class="panel panel-default">
  <div class="panel-heading">Scoreboard Status Report Template - Central Offices</div>
  <div class="panel-body">
    <dl class="dl-horizontal">
	  <dt>Office:</dt>
	  <dd><input type="text" class="form-control" /></dd>
	  <dt>Date:</dt>
	  <dd><input type="text" class="form-control datepicker"/></dd>
	  <dt>Strategic Initatives done by CO for the Breathrough Goals:</dt>
	  <dd><textarea class="form-control" rows="3"></textarea></dd>
	</dl>
	<h4>I. Office Scoreboard for {{date("Y")}}</h4>
	<div class="highlight">
	<h5>1.1. Breakthrough Goal #1</h5>
	<table class="table">
		<thead>
			<tr>
				<th>Breakthrough Goal</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><input type="text" class="form-control" /></td>
				<td><input type="text" class="form-control" /></td>
			</tr>
		</tbody>
	</table>
	<div ng-controller="TodoCtrl">
      <span><%remaining()%> of <%todos.length%> goals</span>
      [ <a href="" ng-click="archive()">remove checked</a> ]
      <table class="table table-responsive">
        <thead>
            <tr>
              <th>#</th>
              <th>Lead Masure</th>
              <th>Responsible Person / Division</th>
              <th>Status <br/><small><em>(as to targets and Timeline)</em></small></th>
              <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <tr ng-repeat="todo in todos">
                <td><input type="checkbox" ng-model="todo.done"></td>
                <td><span class="done-<%todo.done%>"><%todo.text%></span></td>
                <td><span class="done-<%todo.done%>"><%todo.person%></span></td>
                <td><span class="done-<%todo.done%>"><%todo.status%></span></td>
                <td><a href="#">Edit</a></td>

            </tr>
        </tbody>
      </table>
      <form ng-submit="addTodo()" class="form-inline" role="form">
        
        <div class="form-group">
    <label class="sr-only" for="exampleInputEmail2">Lead Measure</label>
    <input type="text" ng-model="todoText"  class="form-control" 
               placeholder="Enter Lead Measure">
  </div>
  <div class="form-group">
    <label class="sr-only" for="exampleInputPassword2">Person/Division</label>
    <input type="text" ng-model="personText"  class="form-control"
               placeholder="Enter Person/Division" />
  </div>
  <div class="form-group">
    <label class="sr-only" for="exampleInputPassword2">Status</label>
    <input type="text" ng-model="statusText"  class="form-control"
               placeholder="Enter Status" />
  </div>
  <button class="btn btn-primary" type="submit" value="add">Add</button>
      </form>
    </div>
	<h5>1.2. Narrative for Goal #1</h5>
	<table class="table">
		<thead>
			<tr>
				<th>Status as of (April,2014)</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Key Accomplishments: Activities completed, Coordination done with Partner OBS and Issues Resolved (if any)</td>
			</tr>
						<tr>
				<td><textarea class="form-control" rows="5"></textarea></td>
			</tr>
		</tbody>
	</table>
	<!-- Indicates caution should be taken with this action -->
<button type="button" class="btn btn-warning">Add New Goal</button>

<!-- Indicates a dangerous or potentially negative action -->
<button type="button" class="btn btn-danger">Delete Goal</button>
</div>
	<h4>III. Issues and Challenges</h4>
		<div ng-controller="TodoCtrl">
      <span><%remaining()%> of <%todos.length%> goals</span>
      [ <a href="" ng-click="archive()">remove checked</a> ]
      <table class="table table-responsive">
        <thead>
            <tr>
              <th>#</th>
              <th>Identifiy issues and challenges encountered<small>Specifiy details under each element, if any</small></th>
				<th>Action being taken by team <small>Summary of the office or team's plan to resolve it, including key resources involved, and indicatie target by when the identified issues shall have been addressed or resolved</small></th>
				<th>Action requested from Management or other OBS (if any) <small>Please indicate desired assistance/intervention from Management or other OBS to facilitate the achievement of the office breakthrough</small></th>
              <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <tr ng-repeat="todo in todos">
                <td><input type="checkbox" ng-model="todo.done"></td>
                <td><span class="done-<%todo.done%>"><%todo.text%></span></td>
                <td><span class="done-<%todo.done%>"><%todo.person%></span></td>
                <td><span class="done-<%todo.done%>"><%todo.status%></span></td>
                <td><a href="#">Edit</a></td>

            </tr>
        </tbody>
      </table>
      <form ng-submit="addTodo()" class="form-inline" role="form">
        
        <div class="form-group">
    <label class="sr-only" for="exampleInputEmail2">Lead Measure</label>
    <input type="text" ng-model="todoText"  class="form-control" 
               placeholder="Enter Lead Measure">
  </div>
  <div class="form-group">
    <label class="sr-only" for="exampleInputPassword2">Person/Division</label>
    <input type="text" ng-model="personText"  class="form-control"
               placeholder="Enter Person/Division" />
  </div>
  <div class="form-group">
    <label class="sr-only" for="exampleInputPassword2">Status</label>
    <input type="text" ng-model="statusText"  class="form-control"
               placeholder="Enter Status" />
  </div>
  <button class="btn btn-primary" type="submit" value="add">Add</button>
      </form>
    </div>
  </div>
</div>
@stop
@section('cssjs')
{{ HTML::style('css/todo.css') }}
{{ HTML::script('js/todo.js') }}
<script>
(function($){
  $(document).ready(function() {

    $('#accordion').collapse({
      toggle: false
    })

    $('.datepicker').datepicker()

  
  });
})(jQuery);
</script>
@stop
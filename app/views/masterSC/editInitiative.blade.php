@extends('layouts.default')
@section('content')

<script type = "text/javascript">
	var counter=0;
	var counter1=0;
	var counter2=0;

	function add()
	{
		counter++;
	}
	function add1()
	{
		counter1++;
	}
	function add2()
	{
		counter2++;
	}
</script>

<h2 class="pull-left">{{ HTML::decode (link_to( "/stratInit", '<i class="fa fa-chevron-left fw"></i>', array('class'=>'link-black hidden-print','title'=>'Initiative Profiles List') )) }} Edit Initiative Profile
</h2>
<div class="btn-group pull-right btn-option hidden-print">
    <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown">
    <i class="fa fa-bars fw"></i> Options <span class="caret"></span>
    </button>
    <ul class="dropdown-menu pull-right btn-option-dropdown" role="menu">
	    @if(Role::access('0'))
			<li>{{ HTML::decode(link_to( "initiatives/setfinal/{$initiative->id}", '<i class="fa fa-check fw"></i> Mark as Final') ) }}
			</li>
		@endif
		<li class="divider"></li>
		<li><a href="javascript:window.print()"><i class="fa fa-file fw"></i> Export as PDF</a></li>
    </ul>
</div>
<hr class="clear" />
{{ Form::open(['url' => URL::route('initiatives.update', ['id' => $initiative->id]), 'id' => 'form'])}}
	<div class="row measure-profile">
		<div class="col-md-6">
			<div class="form-group">
				{{ Form::label('projName', 'Name of Project: * ') }}
				{{ $errors->first('projName', '<span class=errormsg>:message</span>') }}
				@if(Role::access(0)) 
					{{ Form::text('projName', $initiative->projectName, ['class'=>'form-control']) }} 
				@else
					{{ Form::text('projName', $initiative->projectName, ['class'=>'form-control', 'disabled']) }} 
				@endif
			</div>
			<div class="form-group">
				{{ Form::label('projDesc', 'Project Description: * ') }}
				{{ $errors->first('projDesc', '<span class=errormsg>:message</span>') }}
				@if(Role::access(0)) 
					{{ Form::text('projDesc', $initiative->projectDesc, ['class'=>'form-control']) }}
				@else
					{{ Form::text('projDesc', $initiative->projectDesc, ['class'=>'form-control', 'disabled']) }}
				@endif
			</div>
			<div class="form-group">

				{{ Form::label('measuresAffected', 'Measures Affected: * ') }} 
				{{ $errors->first('measures', '<span class=errormsg>:message</span>') }}

				<div class="panel panel-default">
					@if(Role::access(0)) 
						<div class="panel-heading">
							<div class="form-group margin-bottom-0">
								<div class="input-group">
									{{Form::select('measuresAffected', $measures, null, ['id' => 'selectedMeasure', 'class' => 'form-control']); }}
									<span class="input-group-btn">
								  		<button id="addMeasure" class="btn btn-default" type="button"><i class="fa fa-plus fa-fw"></i> Add to List</button>
									</span>
								</div>
							</div>
						</div>
					@endif
					<div class="box-body">
						<table id="measures" class="table">
							@foreach ($initiative->measures as $measure)
								<tr data-text="{{$measure->description}}" data-type="measure"> 
									<td> <input type="hidden" name="measures[]" value="{{$measure->id}}"> &nbsp; </td>
									<td width="75%"> {{$measure->description}} </td> 
									<td width="25%">
										@if(Role::access(0))
											<a class="btn removeItem" data-id="{{$measure->id}}" title="Remove">
												<i class="fa fa-times" style="color:red;"></i>
											</a>
										@endif
									</td>
								</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div>
			<div class="form-group">
				{{ Form::label('coChampions', 'Project Co-Champion: * ') }}
				{{ $errors->first('coChampions', '<span class=errormsg>:message</span>') }}
				
				<div class="panel panel-default">
					@if(Role::access(0)) 
						<div class="panel-heading">
							<div class="form-group margin-bottom-0">
								<div class="input-group">
									{{Form::select('coChampion', $offices, null, ['id' => 'selectedCoChampion', 'class' => 'form-control']); }}
									<span class="input-group-btn">
								  	<button id="addCoChampion" class="btn btn-default" type="button"><i class="fa fa-plus fa-fw"></i> Add to List</button>
									</span>
								</div>
							</div>
						</div>
					@endif
					<div class="box-body">
						<table id="coChampions" class="table">
						
							@foreach($initiative->coChampions as $office)
								<tr data-text="{{$office->office_abbreviation}}" data-type="office"> 
									<td width="0%"> <input type="hidden" name="coChampions[]" value="{{$office->id}}"> &nbsp; </td>
									<td width="75%"> {{$office->office_abbreviation}} </td> 
									<td width="25%">
										@if(Role::access(0))
											<a class="btn removeItem" data-id="{{$office->id}}" title="Remove">
												<i class="fa fa-times" style="color:red;"></i>
											</a>
										@endif
									</td>
								</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div>
			<div class="form-group">
				{{ Form::label('teams', 'Project Team: ') }}
				{{ $errors->first('teams', '<span class=errormsg>:message</span>') }}
				
				<div class="panel panel-default">
					@if(Role::access(0)) 
						<div class="panel-heading">
							<div class="form-group margin-bottom-0">
								<div class="input-group">
									{{ Form::select('projTeam', $offices, null, ['id' => 'selectedTeam', 'class' => 'form-control']); }}
									<span class="input-group-btn">
								  	<button id="addTeam" class="btn btn-default" type="button"><i class="fa fa-plus fa-fw"></i> Add to List</button>
									</span>
								</div>
							</div>
						</div>
					@endif
					<div class="box-body">
						<table id="teams" class="table">
							@foreach($initiative->teams as $office)
								<tr data-text="{{$office->office_abbreviation}}" data-type="office">
									<td width="0%"> <input type="hidden" name="teams[]" value="{{$office->id}}"> &nbsp; </td>
									<td width="75%"> {{$office->office_abbreviation}} </td> 
									<td width="25%">
										@if(Role::access(0))
											<a class="btn removeItem" data-id="{{$office->id}}" title="Remove">
												<i class="fa fa-times" style="color:red;"></i>
											</a>
										@endif
									</td>
								</tr>
							@endforeach
						</table>
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="panel panel-primary">
				<div class="panel-heading" style="text-align: center;">
					INITIATIVES SCORING
				</div>
				<div class="box-body">
					<table id="project-milestones" class="table table-center-headers table-borderless no-top-border table-striped">
						<tr>
							<th width="50%" colspan="3"><strong>Project Milestones</strong></th>
							<th width="25%" colspan="2"><strong>Budget</strong></th>
							<th width="25%" colspan="3"><strong>Status</strong></th>
						</tr>
						<tr>
							<th colspan="2"><strong>Period</strong></th>
							<th rowspan="2" style="vertical-align:middle;"><strong>Milestones/Activities</strong></th>
							<th rowspan="2" style="vertical-align:middle;"><strong>Amount</strong></th>
							<th rowspan="2" style="vertical-align:middle;"><strong>Source</strong></th>
							<th rowspan="2" style="vertical-align:middle;"><strong>Owner</strong></th>
							<th rowspan="2"><strong>Status</strong></th>
						</tr>
						<tr>
							<th style="vertical-align:middle;"><strong>Start</strong></th>
							<th style="vertical-align:middle;"><strong>End</strong></th>
						</tr>						
						@if(Role::access(0)) 
							<tr>
								<td>{{Form::text('start_period', null, ['id' => 'input-start', 'class' => 'form-control', 'placeholder' => 'Start Date'])}}</td>
								<td>{{Form::text('end_period', null, ['id' => 'input-end', 'class' => 'form-control', 'placeholder' => 'End Date'])}}</td>
								<td>{{Form::text('milestone', null, ['id' => 'input-milestone', 'class' => 'form-control', 'placeholder' => 'Milestone/Activity'])}}</td>
								<td>{{Form::text('amount', null, ['id' => 'input-amount', 'class' => 'form-control input-numbers', 'placeholder' => 'Amount'])}}</td>
								<td>{{Form::text('source', null, ['id' => 'input-source', 'class' => 'form-control', 'placeholder' => 'Source'])}}</td>
								<td>{{Form::select('owner', $users, null, ['id' => 'input-owner', 'class' => 'form-control'])}}</td>
								<td>{{Form::textarea('status', null, ['id' => 'input-status', 'rows' => 3, 'class' => 'form-control', 'placeholder' => 'Status'])}}</td>
								<td>{{Form::button('<i class="fa fa-pencil fw"></i>', ['id' => 'addPM', 'class' => 'btn btn-default btn-sm pull-right'])}}</td>
							</tr>
						@endif
						@foreach($initiative->projectMilestones as $PM)
							@if(Role::access('0'))
								<tr>
									<td>
										{{ Form::hidden('PM_ids[]', $PM->id, ['class' => 'PM-id']) }}
										{{ Form::text('start_periods[]',  $PM->start_date, ['class' => 'form-control', 'placeholder' => $PM->start_date])}}
									</td>
									<td>{{ Form::text('end_periods[]',  $PM->end_date, ['class' => 'form-control', 'placeholder' => $PM->end_date])}}</td>
									<td>{{ Form::text('milestones[]',  $PM->milestones, ['class' => 'form-control', 'placeholder' => $PM->milestones])}}</td>
									<td>
										<div class="input-group">
											<span class="input-group-addon">Php</span>
											{{ Form::text('amounts[]', number_format($PM->budget_amount, 2, '.' ,''), ['class' => 'form-control input-numbers', 'placeholder' => $PM->budget_amount])}}
										</div>
									</td>
									<td>{{ Form::text('sources[]', $PM->budget_source,  ['class' => 'form-control', 'placeholder' => $PM->budget_source])}}</td>
									<td>{{ Form::select('onwers[]', $users, $PM->owner->id, ['class' => 'form-control']) }}</td>
									<td>
										<div class="status">
											<div class="head"> 4th quarter: </div>
											{{ Form::textarea('statuses4[]', $PM->status4, ['class' => 'form-control body', 'rows' => 3, 'data-current' => ($PM->currentStatus() == 4?1:0), 'placeholder' => $PM->status4])}}
										</div>
										<div class="status">
											<div class="head"> 3rd quarter: </div>
											{{ Form::textarea('statuses3[]', $PM->status3, ['class' => 'form-control body', 'rows' => 3, 'placeholder' => $PM->status3, 'data-current' => ($PM->currentStatus() == 3?1:0)])}}
										</div>
										<div class="status">
											<div class="head"> 2nd quarter: </div>
											{{ Form::textarea('statuses2[]', $PM->status2, ['class' => 'form-control body', 'rows' => 3, 'placeholder' => $PM->status2, 'data-current' => ($PM->currentStatus() == 2?1:0)])}}
										</div>
										<div class="status">
											<div class="head"> 1st quarter: </div>
											{{ Form::textarea('statuses1[]', $PM->status1, ['class' => 'form-control body', 'rows' => 3, 'placeholder' => $PM->status1, 'data-current' => ($PM->currentStatus() == 1?1:0)])}}
										</div>
									</td>
									<td>
										{{Form::button('<i class="fa fa-trash-o"></i>', ['class' => 'btn btn-danger delete-pm'])}}
									</td>
								</tr>
							@else
								<tr>
									<td>{{$PM->start_date}}</td>
									<td>{{$PM->end_date}}</td>
									<td>{{$PM->milestones}}</td>
									<td>{{ 'Php ' . number_format($PM->budget_amount, 2)}}</td>
									<td>{{$PM->budget_source}}</td>
									<td>{{$PM->owner->fullName()}}</td>
									<td>
										@if(Auth::user()->id == $PM->owner_id)
											{{ Form::hidden('PM_ids[]', $PM->id) }}

											{{ Form::textarea('statuses4[]', null, ['class' => "form-control ".($PM->currentStatus() != 4? 'hide':''), 'rows' => 3, 'placeholder' => '4th quarter'])}}
											{{ Form::textarea('statuses3[]', null, ['class' => "form-control ".($PM->currentStatus() != 3? 'hide':''), 'rows' => 3, 'placeholder' => '3rd quarter'])}}
											{{ Form::textarea('statuses2[]', null, ['class' => "form-control ".($PM->currentStatus() != 2? 'hide':''), 'rows' => 3, 'placeholder' => '2nd quarter'])}}
											{{ Form::textarea('statuses1[]', null, ['class' => "form-control ".($PM->currentStatus() != 1? 'hide':''), 'rows' => 3, 'placeholder' => '1st quarter'])}}
										@endif

										@if($PM->status4)
											<div class="status">
												<div class="head"> 4th quarter: </div>
												<div class="box body">{{nl2br($PM->status4) }} </div>
											</div>
										@endif
										@if($PM->status3)
											<div class="status">
												<div class="head"> 3rd quarter: </div>
												<div class="box body">{{nl2br($PM->status3) }} </div>
											</div>
										@endif
										@if($PM->status2)

											<div class="status">
												<div class="head"> 2nd quarter: </div>
												<div class="box body">{{nl2br($PM->status2) }} </div>
											</div>
										@endif
										@if($PM->status1)
											<div class="status">
												<div class="head"> 1st quarter: </div>
												<div class="box body">{{nl2br($PM->status1) }} </div>
											</div>
										@endif
									</td>
								</tr>
							@endif
						@endforeach
					</table>
				</div>
			</div>

		<div class="panel panel-default remarks">
			<div class="panel-heading box-header-default">
				Remarks
				<div class="pull-right">
					{{Form::label('filter', 'Filter:') }} 
					{{ Form::select('filter', ['Comments', 'Suggestions', 'All'], 2, ['class' => 'baseline filter'])}}
				</div>
			</div>
			<div class="panel-body">
				<div class="list-group">
					<div class=" list-group-item">
						<div class="row">
							{{Form::open(['url' => URL::route('initiative.remark', ['id' => $initiative->id])])}}
								<div class="col-md-2">
									{{ Form::radio('type', 0, 1)}} Comment <br/>
									{{ Form::radio('type', 1, 0)}} Suggestion
								</div>
								<div class="col-lg-10">
					                <div class="input-group">
					                    {{ Form::text('remark', null, ['class' => 'form-control', 'rows' => 3])}}
					                    <span class="input-group-btn">
					                        {{ Form::submit('Add Remark', ['class' => 'btn btn-primary'])}}
					                    </span>
					                </div><!-- /input-group -->
					              {{ $errors->first('remark', '<span class=errormsg>*:message</span>') }}
					            </div><!-- /.col-lg-10 -->
							{{ Form::close() }}
						</div>
					</div>
					@if(count($remarks))
						<?php $show_more = true; ?>
						<div class="show-more">
							@foreach($remarks as $key => $remark)
								@if($show_more && ((count($remarks)-3) <= $key))
									</div>
									<?php $show_more = false; ?>
								@endif
								<div class="list-group-item remark{{$remark->type}}">
									<strong> {{ $remark->user->fullName() }} </strong>
									{{ $remark->remark }} <br/>
									<i class="livetimestamp"> 
										{{ $remark->type? "suggested" : "commented"}} {{ $remark->created_at->diffForHumans() }} 
									</i>
								</div>
							@endforeach
						@if(count($remarks) > 3)
							<div class="list-group-item">
								<center>
									{{ Form::button('See All', ['class' => 'btn btn-default remarks-collapse'])}}
								</center>
							</div>
						@endif
					@endif
				</div>
				{{ Form::submit('Save', array('class'=>'btn btn-primary')) }}
			</div>
		
		</div>
{{ Form::close()}}
	</div>
@stop

@section('cssjs')
	{{ HTML::script('js/remarks.js')}}
	{{ HTML::script('js/createinit.js')}}
	<script type="text/javascript">
	$(document).ready(function() {
		$(".status").find(".head").click(function(event) {
			$(this).closest('.status').find(".body").slideToggle('slow');
		});

		$(".status").find(".body[data-current='0']").slideUp('slow');
	});
	</script>
@stop
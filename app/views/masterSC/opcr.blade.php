@extends('layouts.default')
@section('content')
<ul class="nav nav-pills">
        <li>{{ link_to('/mastersc  ', 'Enterprise Scorecard') }}</li>
        <li>{{ link_to('/officesc', 'Office Scoreboard') }}</li>
        <li>{{ link_to('/stratInit', 'Strategic Initiatives') }}</li>
        <li class="active">{{ link_to('/opcr', 'Office Performance Commitment and Review') }}</li>
        <li>{{ link_to('/visionbc', 'Vision Basecamps') }}</li>
</ul>
<br/>
<h2>Manage OPCR </h2><br/>
		{{ link_to( '/createsb', 'Create New', array('class'=>'btn btn-primary pull-right') ) }}
		<div class="pull-left col-md-3 search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Enter Scoreboard Name">
              <span class="input-group-btn">
                  <button class="btn btn-default" type="button">Search</button>
              </span>
          </div>
        </div>
<br/><br/>
		<table class="table table-bordered table-responsive">
			<thead>
			<tr>
				<th>OPCR name</th>
				<th>Date Created</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach($opcrlist as $list)
			<tr>
				<td>{{ link_to('opcr/view/{id}', $list->name) }}</td>
				<td> {{ $list->created_at }} </td>
				<td>
					@if ($list->flag == 0)
						Draft
					@else
						Final
					@endif
				</td>
				<td>
					@if ($list->flag== 0)
					{{ link_to( "/opcr/{$list->id}/mark/{1}", 'Edit', array('class'=>'btn btn-default btn-xs inline') ) }}
					{{ link_to("/opcr/{$list->id}/mark/1", 'Mark As Final', array('class'=>'btn btn-default btn-xs inline') ) }}
					
					{{ link_to( "#", 'Archive', ['class'=>'btn btn-default btn-xs','onclick' => 'return confirm("Are you sure to archive this?")'] ) }}
					@else
					{{ link_to( "/opcr/{$list->id}/mark/0", 'Mark as Draft', array('class'=>'btn btn-default btn-xs inline') ) }}
					@endif
				</td>
			</tr>
			@endforeach
			
		</tbody>
		</table>
		<div class="container">
			<span class="pull-right">
				<?php echo $opcrlist->links(); ?>
			</span>
		</div>
@stop
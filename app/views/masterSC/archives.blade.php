@extends('layouts.default')
@section('nav')
    @include('includes.scorecard_menu')
@stop
@section('nav2')
    @include('includes.manage_agency')
@stop
@section('content')

<h2 class="pull-left">{{ HTML::decode(link_to(URL::route('scorecards.index'), '<i class="fa fa-chevron-left fw"></i>', array('class' => 'link-black hidden-print', 'title' => 'Scorecard List'))) }} Archived Scorecards
</h2>
<hr class="clear" />

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<i class="fa fa-archive fw"></i> Archives
			</div>
			<div class="panel-body remove-padding">
				<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th class="width-50">Scorecard</th>
								<th class="width-30">Date Created</th>
								<th class="width-20">Action</th>
							</tr>
						</thead>
						<tbody>
						<?php $counter=0;?>
						@foreach($scorecards as $scorecard)
								@if($scorecard->archive==1)
									<?php $counter++;?>
								@endif
						@endforeach

						
						@if($counter>0)
							@foreach($scorecards as $scorecard)
								@if($scorecard->archive==1)
									<div id="entry">
										@if($scorecard->flag==1)
										<tr class="info">
										@else
										<tr>
										@endif
											<td>{{$scorecard->name}}</td>
											<td><small>{{$scorecard->created_at}}</small></td>
											<td>
												<div class="btn-group btn-group actions">
												{{ HTML::decode (link_to( "scorecard/unsetarchive/$scorecard->id", '<i class="fa fa-undo"></i>', ['class'=>'btn btn-default action-btn hide-button-style','title'=>'Restore'])) }}
												</div>
											</td>
										</tr>
									</div>
								@endif
							@endforeach
						@else
							<tr>
								<td colspan="3"> <center> <i> No existing archived scorecards </i> </center> </td>
							</tr>
						@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@stop
@extends('layouts.default')
@section('nav')
    @include('includes.scorecard_menu')
@stop
@section('nav2')
    @include('includes.manage_agency')
@stop
@section('content')

<h2>Manage Strategic Objectives/Goals</h2>
<hr />
<!--
<div class="pull-left col-md-4 search">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search Objective">
          <span class="input-group-btn">
              <button class="btn btn-default" type="button">Search</button>
          </span>
      </div>
</div>
-->


<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<i class="fa fa-table fw"></i> Strategic Objectives/Goals
			</div>
			<div class="panel-body remove-padding">

				<div class="pull-right table-tools hidden-print">
  
				  {{ Form::open(['url' => '/objectives/', 'class' => 'form-inline', 'role' => 'form form-inline']) }}
              <div class="form-group">
		  		     {{ Form::select("perspective", $perspectives->lists('perspective','id'), null, ['class' => 'form-control'])}}
               @if($errors->first())
                <br/>&nbsp;
              @endif
              </div>
				      <div class="form-group">
  				     {{ Form::textarea('objective', null, ['class' => 'form-control input-large','rows'=>1,'placeholder' => 'Enter Goal/Objective']) }}<br/>
               {{ $errors->first('objective', '<span class=errormsg>*:message</span>') }}
              </div>
             <div class="form-group">
              {{ Form::text('measure', null, ['class' => 'form-control', 'placeholder' => 'Enter Measure']) }} <br/>
				      {{ $errors->first('measure', '<span class=errormsg>*:message</span>') }}
            </div>
				    {{ Form::submit('Add', ['class' => 'btn btn-primary']) }}
				  {{ Form::close() }}
				 </div>
        </div>

				<table class="table table-striped strat-obj-goal table-hover table-select">
          <thead>
            <tr>
              <th> No. </th>
              <th> Perspective </th>
              <th> Goals </th>
              <th> Measurement </th>
              <th class="width-20 hidden-print"> Actions </th>
            </tr>
          </thead>
          <tbody>
          <?php $i = 'A'; ?>
          <!-- find highest and place it at the very first -->
          @if(count($perspectives))
            @foreach($perspectives as $perspective)
              <?php $objectives = Objective::where('perspective_id', '=', $perspective->id)->orderBy('ordinal')->get(); ?>
              <?php $objectivesLength = count($objectives);?>
              <?php $first = true; ?>
              <?php $counter=0;?>
              @foreach($objectives as $objective)
                <tr class="perspective {{$perspective->flag?'info':''}}">
                  <td> 
                   
                      {{ $i++ }}
                      
                  </td>
                  <td>
                    <span class = "current-perspective mode1">
                      {{$perspective->perspective}}
                    </span>
                      {{Form::select("perspective", $perspectives->lists('perspective','id'), $perspective->id, ['class' => 'edit-perspective form-control mode2'])}}
                  </td>
                  <td>
                    <span class = "current-text mode1">
                     {{ $objective->objective }}
                    </span>
                    <input type = "text" name = "objective" class = "edit-text form-control mode2" />
                  </td>
                  <td>
                    <span class = "current-measure mode1">
                      {{$objective->measure()->first()->description}}
                    </span>
                    <input type = "text" name = "measure" value = "{{$objective->measure->description}}" class = "edit-measure form-control mode2" />
                   
                  </td>

                  

                    {{Form::open(['url' => "objectives/$objective->id/$objective->perspective_id/move", 'class' => 'mode1']) }}
                    <td class = "buttons hidden-print">
                    <div class="btn-group actions">
                      {{HTML::decode (Form::button('<i class="fa fa-pencil fw"></i>', ['class' => 'btn btn-default action-btn hide-button-style allow-edit mode1','title'=>'Edit']))}}
                      {{HTML::decode (link_to("objectives/$objective->id/delete", '<i class="fa fa-times fw"></i>', ['class'=>'btn btn-danger action-btn hide-button-style mode1', 'onclick' => "return confirm('Are you sure?');",'title'=>'Delete']))}}
                      @if(!$first)
                        {{ HTML::decode (Form::button('<i class="fa fa-chevron-up fw"></i>', ['name' => 'move-up', 'class'=>'btn btn-default action-btn hide-button-style mode1','title'=>'Move Up','type'=>'submit']))}}
                      @endif
                      @if($counter != $objectivesLength-1)
                        {{ HTML::decode (Form::button('<i class="fa fa-chevron-down fw"></i>', ['name' => 'move-down', 'class'=>'btn btn-default action-btn hide-button-style mode1','title'=>'Move Down','type'=>'submit']))}}
                      @endif
                    </div>
                      <?php $counter++; ?>
                    {{ Form::close()}}
                    
                    {{ Form::open(['url' => "/objectives/$objective->id/edit", 'class' => 'form-inline mode2', 'role' => 'form']) }}
                      {{Form::hidden("perspective", null, ['class' => 'new_perspective']); }}
                      {{Form::hidden("objective", null, ['class' => 'new_objective','required']); }}
                      {{Form::hidden("measure", null, ['class' => 'new_measure','required']); }}
                      
                      {{Form::button('Save', ['class' => 'btn btn-default btn-xs save-edit mode2'])}}
                      {{Form::button('Cancel', ['class' => 'btn btn-default btn-xs cancel-edit mode2'])}}
                    {{ Form::close() }}
                  </td>
                </tr>
                <?php $first = false; ?>
              @endforeach
            @endforeach
          @else
            <tr>
              <td colspan="5"> <center> <i> No existing goals/objectives </i> </center> </td>
            </tr>
          @endif
          </tbody>
        </table>
    </div>
  </div>
</div>

@stop

@section('cssjs')
<script type = "text/javascript">
  $(document).ready(function() {
    $(".mode2").hide();
    $(".allow-edit").on("click", function() {
      var current = $(this).closest("tr").find(".current-text");
      var textfield = $(this).closest("tr").find(".edit-text");
      var text = current.text().trim();
      
      textfield.val(text);
      textfield.attr({"placeholder": text, "value": text}).show().focus();

      current = $(this).closest("tr").find(".current-measure");
      textfield = $(this).closest("tr").find(".edit-measure");
      text = current.text().trim();
      
      textfield.val(text);
      textfield.attr({"placeholder": text, "value": text}).show().focus();
      
      $(this).closest("tr").find(".mode1").hide();
      $(this).closest("tr").find(".mode2").show();
    });

    $(".save-edit").on("click", function(event) {
      var tr = $(this).closest("tr");
      var form = $(this).closest("form");
      form.find(".new_perspective").val(tr.find(".edit-perspective").val());
      form.find(".new_objective").val(tr.find(".edit-text").val());
      form.find(".new_measure").val(tr.find(".edit-measure").val());
      form.submit();
    });
    
    $(".cancel-edit").on("click", function() {
      $(this).closest("tr").find(".mode2").hide();
      $(this).closest("tr").find(".mode1").show();
    });
  });
</script>
@stop
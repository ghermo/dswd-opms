@extends('layouts.default')
@section('content')
<h2 class="pull-left">{{ HTML::decode (link_to( URL::route('scorecards.index'), '<i class="fa fa-chevron-left fw"></i>', array('class'=>'link-black','title'=>'Scorecard List') )) }} New Scorecard
</h2>
<hr class="clear" />
<div class="col-md-10 col-md-offset-1">
  <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a class="acc-toggler" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
            Step 1 - Scorecard Details
          </a>
        </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse in" data-number="1">
        <div class="panel-body">
            <div class="col-md-6">
              <div class="form-group">
                {{ Form::label('scorecard_name', 'Scorecard Name*'); }}
                <span id='error1' class='errormsg'>Invalid scorecard name.</span>
                {{ Form::text('scorecard_name', null, ['id' => 'input1', 'class' => 'form-control', 'placeholder' => 'Enter scorecard name']) }}
              </div>
              <div class="form-group">
                {{ Form::label('scorecard_desc', 'Description'); }}
                <span id='error2' class='errormsg'>Invalid description.</span>
                {{ Form::textarea('scorecard_desc', null, ['id' => 'input2', 'class' => 'form-control', 'placeholder' => 'Enter description', 'rows' => 4]) }}
              </div>
            </div>
            <div class="col-md-6">
              <?php $years = [] ?>
              @for($year = date("Y")-1; $year < date("Y")+17; $year++)
                <?php array_push($years, $year); ?>
              @endfor
              <table id="years" class='table table-responsive table-center-headers'>
                <th colspan="6"> 
                  Select years to be used to measure targets* <br/>
                  <span id='error3' class='errormsg'>Invalid target year/s.</span> 
                </th>
                @for($i = 0; $i < count($years); $i++)
                  @if($i % 6 == 0)
                    <tr>
                  @endif
                  <td> {{Form::button($years[$i], ['class' => 'btn btn-default btn-xs year'])}} </td>
                  @if($i % 6 == 5)
                    </tr>
                  @endif
                @endfor
                <tr>
                  <td colspan="6">
                    <b> Legend: </b> <br/>
                    <i class="fa fa-circle fa-1 red"></i> Baseline <br/>
                    <i class="fa fa-circle fa-1 blue"></i> Target Year
                  </td>
                </tr>
              </table>
            </div>
        </div>
        <div class="panel-footer">
          <ul class="pager pager2">
            <li class="next"><a class="acc-toggler" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Next &rarr;</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a class="acc-toggler" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
            Step 2 - Perspective
          </a>
        </h4>
      </div>
      <div id="collapseTwo" class="panel-collapse collapse">
        <div class="panel-body">
          <div class="pull-right">
            {{Form::open(['class' => 'form form-inline'])}}
             <span id='error4' class='errormsg'>Perspective name field is required.</span>
            {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'add-perspective-text', 'placeholder' => 'Enter Perspective Name', 'required']) }}
            {{ Form::button('Add', ['class' => 'btn btn-primary', 'id' => 'add-perspective']) }}
            {{Form::close()}}
          </div>
          <br/><br/>
          <table id = "perspective-table" class = "table table-responsive table-fixed">
            <tr>
              <th> No. </th>
              <th> Perspective </th>
              <th> Actions </th>
            </tr>
          </table>
          <center> <small id="no-perspectives"> No existing perspectives. </small></center>
        </div>
        <div class="panel-footer">
          <ul class="pager pager2">
            <li class="previous"><a class="acc-toggler" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">&larr; Back</a></li>
            <li class="next"><a class="acc-toggler" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Next &rarr;</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a class="acc-toggler" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
            Step 3 - Goals/Objectives
          </a>
        </h4>
      </div>
      <div id="collapseThree" class="panel-collapse collapse">
        <div class="panel-body">
          <div class="pull-right">
            {{ Form::open(['class' => 'form-inline', 'role' => 'form']) }}
               {{ Form::select("perspective", [], null, ['class' => 'form-control', 'id' => 'select-perspective'])}}
               {{ Form::textarea('objective', null, ['class' => 'form-control', 'id' => 'add-goal-text', 'rows'=>'2', 'placeholder' => 'Enter Goal/Objective', 'required']) }}
               <span id='error5' class='errormsg'>Goal/objective field is required.</span>
               {{ Form::text('measure', null, ['class' => 'form-control', 'id' => 'add-measure-text', 'placeholder' => 'Enter Measure', 'required']) }}
               <span id='error6' class='errormsg'>Measure field is required.</span>
              {{ Form::button('Add', ['class' => 'btn btn-primary', 'id' => 'add-goal']) }}
            {{ Form::close() }}
          </div>
          <br/><br/>
          <table id = "goal-table" class = "table table-responsive">
            <tr>
              <th> &nbsp;  </th>
              <th> Perspective </th>
              <th> Goals/Objectives </th>
              <th> Measure </th>
              <th width = "300px"> Actions </th>
            </tr>
          </table>
          <center> <small id="no-objectives"> No existing goals/objectives. </small></center>
        </div>
        <div class="panel-footer">
          <ul class="pager pager2">
            <li class="previous"><a class="acc-toggler" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">&larr; Back</a></li>
            <li class="next"><a class="acc-toggler" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Next &rarr;</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a class="acc-toggler" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
            Step 4 - Summary
          </a>
        </h4>
      </div>
      <div id="collapseFour" class="panel-collapse collapse">
        <div class="panel-body">
          <table style="text-align:center;" id="score-sum" class = "table table-bordered table-center-headers font-12">
            <tr>
                <th> P </th>
                <th> &nbsp; </th>
                <th> Objectives/Goals </th>
                <th> # </th>
                <th> Measure </th>
            </tr>
          </table>
        </div>
        <div class="panel-footer">
          <ul class="pager pager2">
            <li class="previous"><a class="acc-toggler" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">&larr; Back</a></li>
              {{ Form::open(['url' => URL::route('scorecards.store')]) }}
                {{ Form::hidden('name', null, ['id'  => 'scorecard_name'])}}
                {{ Form::hidden('description', null, ['id'  => 'scorecard_desc'])}}
                {{ Form::hidden('highest-perspective', 0, ['id' => 'highest'])}}
                <li class="next">
                    <a id="create-scoreboard" class="acc-toggler" data-toggle="collapse" data-parent="#accordion" href="#">Create</a>
                </li>
              {{ Form::close() }}
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

@stop
@section('cssjs')

{{ HTML::script('js/createsc.min.js')}}
@stop
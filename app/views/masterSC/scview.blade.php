@extends('layouts.default')
@section('content')
{{ link_to( '/officesc', '&larr; Back', array('class'=>'btn btn-default') ) }}	
{{ link_to( '/officesc/scedit/1', 'Edit', array('class'=>'btn btn-default') ) }}	
{{ link_to( '/officesc/sc/1', 'Mark as Final', array('class'=>'btn btn-default') ) }}	
<br/><br/>
<div class="panel panel-default">
  <div class="panel-heading">Scoreboard Status Report Template - Central Offices</div>
  <div class="panel-body">
    <dl class="dl-horizontal">
	  <dt>Office:</dt>
	  <dd>DSWD Central Office</dd>
	  <dt>Date:</dt>
	  <dd>10/24/2014</dd>
	  <dt>Strategic Initatives done by CO for the Breathrough Goals:</dt>
	  <dd>jlsdjfkls jfk ljsdfkjdsfl</dd>
	</dl>
	<h4>I. Office Scoreboard for 2014</h4>
	<div class="highlight">
	<h5>1.1. Breakthrough Goal #1</h5>
	<table class="table">
		<thead>
			<tr>
				<th>Breakthrough Goal</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
		</tbody>
	</table>
	<table class="table">
		<thead>
			<tr>
				<th>#</th>
				<th>Lead Measures</th>
				<th>Responsible Person/Division</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>1</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>2</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
		</tbody>
	</table>
	<h5>1.2. Narrative for Goal #1</h5>
	<table class="table">
		<thead>
			<tr>
				<th>Status as of (April,2014)</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Key Accomplishments: Activities completed, Coordination done with Partner OBS and Issues Resolved (if any)</td>
			</tr>
						<tr>
				<td>&nbsp;</td>
			</tr>
		</tbody>
	</table>
</div>
	<h4>III. Issues and Challenges</h4>
	<table class="table">
		<thead>
			<tr>
				<th colspan="3">Issues and Challenges <small>Please report major issues in this format, if applicable</small></th>
			</tr>
			<tr>
				<th>#</th>
				<th>Identifiy issues and challenges encountered<small>Specifiy details under each element, if any</small></th>
				<th>Action being taken by team <small>Summary of the office or team's plan to resolve it, including key resources involved, and indicatie target by when the identified issues shall have been addressed or resolved</small></th>
				<th>Action requested from Management or other OBS (if any) <small>Please indicate desired assistance/intervention from Management or other OBS to facilitate the achievement of the office breakthrough</small></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>1</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>2</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>3</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>4</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>5</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
		</tbody>
	</table>
  </div>
</div>
<div class="row">
	<div class="col-md-12">
		<h6>Legend:</h6>
	</div>
</div>
@stop
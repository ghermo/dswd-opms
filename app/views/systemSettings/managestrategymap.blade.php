@extends('layouts.default')

@section('css')
    <!-- blueimp Gallery styles -->
    <!-- <link rel="stylesheet" href="http://blueimp.github.io/Gallery/css/blueimp-gallery.min.css"> -->
    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
    {{ HTML::style('vendor/jfu/css/jquery.fileupload.css') }}
    {{ HTML::style('vendor/jfu/css/jquery.fileupload-ui.css') }}
    <!-- CSS adjustments for browsers with JavaScript disabled -->
    <!-- <noscript><link rel="stylesheet" href="css/jquery.fileupload-noscript.css"></noscript>
    <noscript><link rel="stylesheet" href="css/jquery.fileupload-ui-noscript.css"></noscript> -->
@stop

@section('content')
<h2>Manage Homepage</h2>
<hr />
    <!-- The file upload form used as target for the file upload widget -->
    <form id="fileupload" action="base_path().'/public/vendor/jfu/server/php/index.php'" method="POST" enctype="multipart/form-data">
        <!-- Redirect browsers with JavaScript disabled to the origin page -->
        <noscript><input type="hidden" name="redirect" value="http://blueimp.github.io/jQuery-File-Upload/"></noscript>
        <input type="hidden" name="example1" value="test">
        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        <div class="row fileupload-buttonbar">
                
            <div class="col-lg-7">
                <!-- <input type="checkbox" class="toggle"> Select All -->
                <span class="btn btn-default fileinput-button">
                            <span><i class="fa fa-plus fw"></i> Choose File(s)</span>
                            <input type="file" name="files[]" multiple>
                        </span><!-- 
                        <button type="submit" class="btn btn-default start">
                            <span><i class="fa fa-upload fw"></i> Start Upload</span>
                        </button>
                        <button type="reset" class="btn btn-default cancel">
                            <span><i class="fa fa-ban fw"></i> Cancel Upload</span>
                        </button>
                        <button type="button" class="btn btn-default delete">
                            <span><i class="fa fa-trash-o fw"></i> Delete</span>
                        </button> -->
                <!-- The global file processing state -->
                <span class="fileupload-process"></span>
            </div>
            <!-- The global progress state -->
            <div class="col-lg-5 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        <!-- The table listing the files available for upload/download -->
        <div class="panel panel-primary">
        <div class="panel-heading">Images</div>
        <div class="panel-body">
            <ul>
                <li>Images listed here will be displayed on the dashboard's image slider.</li>
                <li>You can select multiple file to upload.</li>
                <li>You can drag & drop images here to upload.</li>
            </ul>
        </div>
        <table role="presentation" class="table table-striped">
            <tbody class="files">
                
            </tbody>
        </table>
        </div>
    </form>
@stop

@section('cssjs')
    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
    <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
    {{ HTML::script('/vendor/jfu/js/vendor/jquery.ui.widget.js') }}
    <!-- The Templates plugin is included to render the upload/download listings -->
    {{ HTML::script('/vendor/jfu/js/tmpl.min.js') }}
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    {{ HTML::script('/vendor/jfu/js/load-image.min.js') }}
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    {{ HTML::script('/vendor/jfu/js/canvas-to-blob.min.js') }}
    <!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
    <!-- {{ HTML::script('/vendor/jfu/js/bootstrap.min.js') }} -->
    <!-- blueimp Gallery script -->
    <!-- {{ HTML::script('/vendor/jfu/js/jquery.blueimp-gallery.min.js') }} -->
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    {{ HTML::script('/vendor/jfu/js/jquery.iframe-transport.js') }}
    <!-- The basic File Upload plugin -->
    {{ HTML::script('/vendor/jfu/js/jquery.fileupload.js') }}
    <!-- The File Upload processing plugin -->
    {{ HTML::script('/vendor/jfu/js/jquery.fileupload-process.js') }}
    <!-- The File Upload image preview & resize plugin -->
    <!-- {{ HTML::script('/vendor/jfu/js/jquery.fileupload-image.js') }} -->
    <!-- The File Upload audio preview plugin -->
    {{ HTML::script('/vendor/jfu/js/jquery.fileupload-audio.js') }}
    <!-- The File Upload video preview plugin -->
    {{ HTML::script('/vendor/jfu/js/jquery.fileupload-video.js') }}
    <!-- The File Upload validation plugin -->
    {{ HTML::script('/vendor/jfu/js/jquery.fileupload-validate.js') }}
    <!-- The File Upload user interface plugin -->
    {{ HTML::script('/vendor/jfu/js/jquery.fileupload-ui.js') }}
    <!-- The main application script -->
    {{ HTML::script('/vendor/jfu/js/main.js') }}
    <!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
    <!--[if (gte IE 8)&(lt IE 10)]>
    <script src="js/cors/jquery.xdr-transport.js"></script>
    <![endif]-->
    <script id="template-upload" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
            <tr class="template-upload fade">
                <td>
                    <span class="preview"></span>
                </td>
                <td width="30%">
                    <label class="title">
                        <span>Title:</span><br>
                        <input type="text" name="title[]" class="form-control" required />
                    </label>
                    <label class="description">
                        <span>Description:</span><br>
                        <input type="text" name="description[]" class="form-control" required maxlength="120"/>
                    </label>
                </td>
                <td>
                    <p class="name">{%=file.name%}</p>
                    <strong class="error text-danger"></strong>
                </td>
                <td>
                    <p class="size">Processing...</p>
                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
                </td>
                <td>
                    {% if (!i && !o.options.autoUpload) { %}
                        <button class="btn btn-primary start" disabled>
                            <span><i class="fa fa-upload fw"></i> Start</span>
                        </button>
                    {% } %}
                    {% if (!i) { %}
                        <button class="btn btn-warning cancel">
                            <span><i class="fa fa-ban fw"></i> Cancel</span>
                        </button>
                    {% } %}
                </td>
            </tr>
        {% } %}
</script>
    <!-- The template to display files available for download -->
    <script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-download fade">
            <td>
                <span class="preview pull-right">
                    {% if (file.thumbnailUrl) { %}
                        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                    {% } %}
                </span>
            </td>
            <td width="30%">
                <p class="title"><strong>{%=file.title||''%}</strong></p>
                <p class="description">{%=file.description||''%}</p>
            </td>
            <td>
                <p class="name">
                    {% if (file.url) { %}
                        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                    {% } else { %}
                        <span>{%=file.name%}</span>
                    {% } %}
                </p>
                {% if (file.error) { %}
                    <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                {% } %}
            </td>
            <td>
                <span class="size">{%=o.formatFileSize(file.size)%}</span>
            </td>
            <td>
                {% if (file.deleteUrl) { %}
                    <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                        <span><i class="fa fa-trash-o fw"></i> Delete</span>
                    </button>
                {% } else { %}
                    <button class="btn btn-warning cancel">
                        <span><i class="fa fa-ban fw"></i> Cancel</span>
                    </button>
                {% } %}
            </td>
        </tr>
    {% } %}
    </script>
@stop

@extends('layouts.default')
@section('nav')
    @include('includes.scorecard_menu')
@stop
@section('content')

<h2 class="pull-left">Strategic Goals Accomplishments</h2>

<div class="pull-right btn-option hidden-print">
    <a href="javascript:window.print()"><button class="btn btn-default"><i class="fa fa-print fw"></i> Export as PDF</button></a>
</div>
<hr class="clear hidden-print" />
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading hidden-print">
                <i class="fa fa-table fw"></i> Strategic Goals Accomplishments
            </div>
            <div class="panel-body remove-padding hidden-print">
            {{ Form::open(['url' => URL::route('accomplishments.index'), 'role' => 'form','class='=>'form-inline'])}}
				<div class="table-tools col-md-12 hidden-print">
                    @if(Role::access('0'))
                        <div class="pull-right">
                            {{ link_to( 'systemsettings/aggregate', 'Aggregate Now', array('class'=>'btn btn-primary') ) }} 
                        </div>
                    @endif
				    <div class="form-group col-sm-2" style="padding-left: 0; padding-right: 0;">
				        <div class="input-group">
					      	<span class="input-group-addon">Year</span>
					        {{ Form::select('year', $years, $year, ['class' => 'form-control','style'=>'height: 37px;'])}}
					    </div>
				    </div>
				    <div class="form-group col-sm-3">
				        <div class="input-group">
					      	<span class="input-group-addon">Quarter</span>
					        {{ Form::select('quarter', ['None', '1st quarter', '2nd quarter', '3rd quarter', '4th quarter'], isset($quarter)? $quarter:null, ['class' => 'form-control','style'=>'height: 37px;'])}}
					    </div>
				    </div>
				    {{ Form::submit('Filter', ['class' => 'btn btn-default'])}}
				{{ Form::close()}}
				</div>
            </div>
                <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="5%" rowspan="2"> &nbsp; </th>
                        <th width="15%" rowspan="2" style="vertical-align:middle;"> Strategic Goals </th>
                        <th width="10%" rowspan="2" style="vertical-align:middle;"> Target </th>
                        <th width="25%" rowspan="2" style="vertical-align:middle;"> Measure </th>
                        <th width="35%" rowspan="2" style="vertical-align:middle;"> Indicators </th>

                        @if(isset($quarter))
                            @for($i = ($quarter-1) * 3; $i < $quarter*3; $i++)
                                <th width="10%" colspan="2" style="vertical-align:middle;"> {{$months[$i]}} </th>
                            @endfor
                        @else
                            <th width="10%" colspan="2" style="vertical-align:middle;"> {{$year}} </th>
                        @endif
                    </tr>
                    <tr>
                        @for($i = 0; $i < (isset($quarter)? 3 : 1); $i++)
                            <td style="vertical-align:middle;"> Values </td>
                            <td style="vertical-align:middle;"> Values aggregated to scorecard </td>
                        @endfor
                    </tr>
                </thead>
                <tbody>
                    <?php $id = 'A' ?>
                    @if (count($goals) == 0)
                        <tr>
                            <td colspan="7"><center><b>No data available. </b></center></td>
                        </tr>
                    @else
                    @foreach($goals as $goal)
                        
                        <tr>
                            <td rowspan="{{count($goal->indicatorsMovement)}}"> {{$id++}} </td>
                            <td rowspan="{{count($goal->indicatorsMovement)}}"> {{$goal->objective}} </td>
                            <td rowspan="{{count($goal->indicatorsMovement)}}"> {{$goal->measure->targets->first()->target}} </td>
                            <td rowspan="{{count($goal->indicatorsMovement)}}"> {{$goal->measure->description}} </td>

                            @if( ! count($goal->indicatorsMovement))
                                <td colspan="{{isset($quarter)? 7: 3}}" style="vertical-align:middle;"> <i> Nothing to show </i> </td>
                            @endif

                            <?php $aggregated = 0; ?>
                            <?php $aggregation = 0 ?>

                            @if(isset($quarter))
                                <?php $aggregateds = [] ?>
                                <?php $aggregations = [] ?>
                                
                                @for($i = ($quarter-1) * 3; $i < $quarter*3; $i++)
                                    <?php $aggregateds[$i] = 0; ?>
                                    <?php $aggregations[$i] = 0; ?>
                                @endfor
                            @endif

                            @foreach($goal->indicatorsMovement as $indicator)
                                <?php $totalValue = 0; ?>

                                @if($indicator->affect)
                                    @if(isset($quarter))
                                        @for($i = ($quarter-1) * 3; $i < $quarter*3; $i++)
                                            <?php $totalValue = 0; ?>

                                            @foreach($indicator->accomplishments($i, $year) as $accomplishment)
                                                <?php $totalValue += $accomplishment->value; ?>
                                            @endforeach 
                                            @if($indicator->affect)
                                                <?php $aggregateds[$i] += $totalValue * $indicator->affect ?>
                                                <?php $aggregations[$i]++ ?>
                                            @endif
                                        @endfor
                                    @else
                                        @for($i = 0; $i < 12; $i++)
                                            @foreach($indicator->accomplishments($i, $year) as $accomplishment)
                                                <?php 
                                                    if($accomplishment->scoreboard&&$accomplishment->scoreboard->holder->flag==1)
                                                        $totalValue += $accomplishment->value; 
                                                ?>
                                            @endforeach
                                        @endfor
                                        @if($indicator->affect)
                                            <?php
                                                $aggregated += $totalValue * $indicator->affect ?>
                                            <?php $aggregation++ ?>
                                        @endif
                                    @endif
                                @endif
                            @endforeach

                            <?php $first = true; ?>
                            @foreach($goal->indicatorsMovement as $indicator)
                                @if( ! $first)
                                    </tr>
                                    <tr>
                                @endif

                                <td> {{$indicator->indicator}} </td>

                                @if(isset($quarter))
                                    @for($month = ($quarter-1)*3 ; $month < $quarter*3; $month++)
                                        <?php $totalValue = 0; ?>
                                        @foreach($indicator->accomplishments($month, $year) as $accomplishment)
                                            <?php if($accomplishment->scoreboard->holder->flag==1)
                                            $totalValue += $accomplishment->value; ?>
                                        @endforeach
                                        <td>
                                            {{ $totalValue? number_format($totalValue, 0, '.', ',') : '&nbsp;' }}     
                                        </td>

                                        @if($first)
                                            <td rowspan="{{count($goal->indicatorsMovement)}}">
                                                @if($goal->aggregation_type == 1)
                                                    {{ $aggregateds[$month]? number_format($aggregateds[$month] / ($aggregations[$month] ? $aggregations[$month]: 1), 0, '.', ','): '&nbsp;' }}
                                                @else
                                                    {{ $aggregateds[$month]? number_format($aggregateds[$month], 0, '.', ','): '&nbsp;' }}  
                                                @endif       
                                            </td>
                                        @endif
                                    @endfor
                                @else
                                    <?php $totalValue = 0; ?>
                                    @for($month = 0; $month < 12; $month++)
                                        @foreach($indicator->accomplishments($month, $year) as $accomplishment)
                                            <?php if($accomplishment->scoreboard&&$accomplishment->scoreboard->holder->flag==1)
                                            $totalValue += $accomplishment->value; ?>
                                        @endforeach
                                    @endfor

                                    <td>
                                        {{ $totalValue? number_format($totalValue, 0, '.', ',') : '&nbsp;' }}     
                                    </td>

                                    @if($first)
                                        <td rowspan="{{count($goal->indicatorsMovement)}}">
                                            @if($goal->aggregation_type == 1)
                                                {{ $aggregated? number_format($aggregated / ($aggregation ? $aggregation: 1), 0, '.', ','): '&nbsp;' }}
                                            @else
                                                {{ $aggregated? number_format($aggregated, 0, '.', ','): '&nbsp;' }}  
                                            @endif       
                                        </td>
                                    @endif
                                @endif

                                <?php $first = false; ?>
                            @endforeach
                        </tr>
                    @endforeach
                    @endif
                </tbody>
                </table>
        </div>
    </div>
</div>
@stop
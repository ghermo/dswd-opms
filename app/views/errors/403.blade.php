@extends('layouts.error')
@section('content')
    <h1>Page Forbidden</h1>
    <p>Sorry, but the page you were trying to view is forbidden.</p>
    <br/><hr/><br/>
    {{ link_to('/', 'Go Back Home') }}
@stop
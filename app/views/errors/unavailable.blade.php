@extends('layouts.error')
@section('content')
    <h1>Content Unavailable</h1>
    <p>Sorry, but the page you were trying to view does not exist or has been removed.</p>
    <br/><hr/><br/>
    {{ link_to('/', 'Go Back Home') }}
@stop
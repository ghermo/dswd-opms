@extends('layouts.error')
@section('content')
    <h1>Page Not Found</h1>
    <p>Sorry, but the page you were trying to view does not exist.</p>
    <br/><hr/><br/>
    {{ link_to('/', 'Go Back Home') }}
@stop
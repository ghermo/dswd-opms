@extends('layouts.error')
@section('content')
    <h1>System Error</h1>
    <p>Sorry, but the system encountered an issue. Please contact support immediately.</p>
    <br/><hr/><br/>
    {{ link_to('/', 'Go Back Home') }}
@stop
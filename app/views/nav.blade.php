	<ul class="nav nav-pills">
        <li class="active">{{ link_to('/mastersc  ', 'Enterprise Scorecard') }}</li>
        <li>{{ link_to('/officesc', 'Office Scoreboard') }}</li>
        <li>{{ link_to('/opcr', 'Office Performance Commitment and Review') }}</li>
        <li>{{ link_to('/visionbc', 'Vision Basecamps') }}</li>
	</ul>
	<br>
		<strong>Manage:</strong>
		{{ link_to('/managesc', 'Scorecards') }} |
		{{ link_to('/manageperspective', 'Perspectives') }} |
		{{ link_to('/managestratobj', 'Strategic Goals/Objectives') }} | 
        {{ link_to('/stratInit', 'Initiatives Profile') }} | 
        {{ link_to('/measurelist', 'Measures Profile') }}
	<br><br>
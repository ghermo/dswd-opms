@extends('layouts.default')
@section('nav')
    @include('includes.scorecard_menu')
@stop
@section('content')

<h2>Strategic Goals Accomplishments</h2>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <i class="fa fa-table fw"></i> Strategic Goals Accomplishments
            </div>
            <div class="panel-body remove-padding">
            <div class="pull-right">
            {{ Form::open(['url' => URL::route('accomplishments.index'), 'role' => 'form','class='=>'form-inline'])}}
                <div class="table-tools col-md-12">
                    <div class="form-group col-sm-2">
                        <div class="input-group">
                            <span class="input-group-addon">Year</span>
                            {{ Form::select('year', $years, $year, ['class' => 'form-control'])}}
                        </div>
                    </div>
                    <div class="form-group col-sm-3">
                        <div class="input-group">
                            <span class="input-group-addon">Quarter</span>
                            {{ Form::select('quarter', ['None', '1st quarter', '2nd quarter', '3rd quarter', '4th quarter'], isset($quarter)? $quarter:null, ['class' => 'form-control'])}}
                        </div>
                    </div>
                    {{ Form::submit('Filter', ['class' => 'btn btn-primary'])}}
                </div>
                {{ Form::close()}}
                </div>
                <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="5%" rowspan="2"> &nbsp; </th>
                        <th width="15%" rowspan="2" style="vertical-align:middle;"> Strategic Goals </th>
                        <th width="10%" rowspan="2" style="vertical-align:middle;"> Target </th>
                        <th width="25%" rowspan="2" style="vertical-align:middle;"> Measure </th>
                        <th width="35%" rowspan="2" style="vertical-align:middle;"> Indicators </th>

                        @if(isset($quarter))
                            @for($i = ($quarter-1) * 3; $i < $quarter*3; $i++)
                                <th width="10%" colspan="2" style="vertical-align:middle;"> {{$months[$i]}} </th>
                            @endfor
                        @else
                            <th width="10%" colspan="2" style="vertical-align:middle;"> {{$year}} </th>
                        @endif
                    </tr>
                    <tr>
                        @for($i = 0; $i < (isset($quarter)? 3 : 1); $i++)
                            <td style="vertical-align:middle;"> Values </td>
                            <td style="vertical-align:middle;"> Values aggregated to scorecard </td>
                        @endfor
                    </tr>
                </thead>
                <tbody>
                    <?php $id = 'A' ?>
    
                    @foreach($goals as $goal)
                        <tr>
                            <td rowspan="{{count($goal->indicatorsMovement)}}"> {{$id++}} </td>
                            <td rowspan="{{count($goal->indicatorsMovement)}}"> {{$goal->objective}} </td>
                            <td rowspan="{{count($goal->indicatorsMovement)}}"> {{$goal->measure->targets->first()->target}} </td>
                            <td rowspan="{{count($goal->indicatorsMovement)}}"> {{$goal->measure->description}} </td>

                            @if( ! count($goal->indicatorsMovement))
                                <td colspan="{{isset($quarter)? 7: 3}}" style="vertical-align:middle;"> <i> Nothing to show </i> </td>
                            @endif

                            <?php $aggregated = 0; ?>
                            <?php $aggregation = 0 ?>

            
                          

                            @foreach($goal->indicatorsMovement as $indicator)
                                <?php $totalValue = 0; ?>
                                @if($indicator->affect)
                                    
                                    @for($i = 0; $i < 12; $i++)
                                        @foreach($indicator->accomplishments($i, $year) as $accomplishment)

                                            @if($accomplishment->scoreboard()->get()[0]->holder()->get()[0]->flag==0)
                                                <?php $totalValue += $accomplishment->value; ?>
                                               
                                            @endif
                                        @endforeach
                                    @endfor
                                    @if($indicator->affect)
                                        <?php $aggregated += $totalValue * $indicator->affect ?>
                                        <?php $aggregation++ ?>
                                    @endif
                                 
                                @endif
                               
                            @endforeach

                            <?php $first = true; ?>
                            @foreach($goal->indicatorsMovement as $indicator)
                                @if( ! $first)
                                    </tr>
                                    <tr>
                                @endif

                                <td> {{$indicator->indicator}} </td>

                                
                                    <?php $totalValue = 0; ?>
                                    @for($month = 0; $month < 12; $month++)
                                        @foreach($indicator->accomplishments($month, $year) as $accomplishment)
                                            @if($accomplishment->scoreboard()->get()[0]->holder()->get()[0]->flag==1)
                                                <?php $totalValue += $accomplishment->value; ?>
                                            @endif
                                        @endforeach
                                    @endfor

                                    <td>
                                        {{ $totalValue? $totalValue : '&nbsp;' }}     
                                    </td>

                                    @if($first)
                                        <td rowspan="{{count($goal->indicatorsMovement)}}">
                                            @if($goal->aggregation_type == 1)
                                                {{ $aggregated? ($aggregated / ($aggregation ? $aggregation: 1)): '&nbsp;' }}
                                            @else
                                                {{ $aggregated? $aggregated: '&nbsp;' }}  
                                            @endif    
                                            {{"aggregation: ".$aggregation}}   
                                        </td>
                                    @endif
                                

                                <?php $first = false; ?>
                            @endforeach
                          
                        </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
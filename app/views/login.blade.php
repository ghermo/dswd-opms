@extends('layouts.default')
@section('content')
<div class = "panel panel-default panel-login col-md-6 col-md-offset-3">
    {{ Form::open(['url' => 'login', 'class' => 'form-horizontal form-login', 'role' => 'form']) }}
        <h2>Login</h2>
       

        <!-- Name -->
        <div class="form-group {{{ $errors->has('username') ? 'error' : '' }}}">
            {{ Form::text('username', Input::old('username'), ['class' => 'form-control', 'placeHolder' => 'Username']) }}
            {{ $errors->first('username', '<span class=errormsg>*:message</span>') }}
        </div>

        <!-- Password -->
        <div class="form-group {{{ $errors->has('password') ? 'error' : '' }}}">
            {{ Form::password('password', ['class' => 'form-control', 'placeHolder' => 'Password']) }}
            {{ $errors->first('password', '<span class=errormsg>*:message</span>') }}
        </div>
        <!-- is the account disabled? -->
        <div class="form-group ">
            {{ $errors->first('disable', '<span class=errormsg>*:message</span>') }}
        </div>

        <!-- Login button -->
        <div class="form-group">
            <div class="controls">
                {{ Form::submit('Login', ['class' => 'btn btn-primary pull-right']) }}
            </div>
        </div>
    {{ Form::close() }}
</div>
@stop
@section('cssjs')

@stop
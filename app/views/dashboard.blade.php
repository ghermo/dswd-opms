@extends('layouts.default')
@section('content')

<div class="page-header">
	<h2>Welcome to DSWD Online Perfomance Management System</h2>
</div>
<div class="jumbotron banner">
  <div class="bg">
    <div class="container">
      <!-- Jssor Slider Begin -->
        <div id="slider1_container" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 980px; height: 400px; ">

          <!-- Loading Screen -->
          <!-- <div u="loading" style="position: absolute; top: 0px; left: 0px;">
              <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block;
                  top: 0px; left: 0px; width: 100%; height: 100%;">
              </div>
              <div style="position: absolute; display: block; background: url(../img/loading.gif) no-repeat center center;
                  top: 0px; left: 0px; width: 100%; height: 100%;">
              </div>
          </div> -->

          <!-- Slides Container -->
          <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 980px; height: 400px; overflow: hidden;">
            @if(count($slider)!=0)
              @foreach($slider as $slide)
              <div>
                <div style="position: absolute; width: 480px; height: 300px; top: 10px; left: 10px;
                    text-align: left; line-height: 1.8em; font-size: 12px;">
                    <br />
                    <span style="display: block; line-height: 1em; text-transform: uppercase; font-size: 52px;
                        color: #FFFFFF; font-family: open_sansbold;">{{ $slide->title }}</span>
                    <br />
                    <br />
                    <span style="display: block; line-height: 1.1em; font-size: 2.5em; color: #FFFFFF; font-family: open_sanssemibold; padding-left: 10px; padding-right: 10px;">
                        {{ $slide->description }} </span>
                    <br />
                    <!-- <span style="display: block; line-height: 1.1em; font-size: 1.5em; color: #FFFFFF; padding-right: 10px;">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span> -->
                    <a href="{{ $slide->url }}" data-lightbox="image-1" data-title="{{ $slide->title }}">
                        <button class="btn btn-primary btn-lg">View {{ $slide->title }}</button>
                    </a>
                </div>
                <img class="slider-img" src="{{ $slide->url }}" style="position: absolute; top: 23px; left: 480px; width: 500px; height: 300px; padding-left: 10px;" />
                <img u="thumb" src="{{ $thumbnailUrl . $slide->name }}" />
              </div>
              @endforeach
            @else
            <div>
              <div style="position: absolute; width: 480px; height: 300px; top: 10px; left: 10px;
                  text-align: left; line-height: 1.8em; font-size: 12px;">
                  <br />
                  <span style="display: block; line-height: 1em; text-transform: uppercase; font-size: 52px;
                      color: #FFFFFF; font-family: open_sansbold;">No Image</span>
                  <br />
                  <br />
                  <span style="display: block; line-height: 1.1em; font-size: 2.5em; color: #FFFFFF; font-family: open_sanssemibold; padding-right: 10px;">
                      There is currently no image(s) to show yet. Please upload one now. </span>
                  <br />
                  <!-- <span style="display: block; line-height: 1.1em; font-size: 1.5em; color: #FFFFFF; padding-right: 10px;">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span> -->
              </div>
              <img class="slider-img" src="img/no_image_yet.jpg" style="position: absolute; top: 23px; left: 480px; width: 500px; height: 300px; padding-left: 10px;" />
              <img u="thumb" src="img/no_image_yet.jpg" />
            </div>
            @endif
        </div>

          <!-- Arrow Left -->
          <span u="arrowleft" class="jssora05l slider-arrow sa-left" style="width: 55px; height: 55px; top: 123px; left: -65px;">
          </span>
          <!-- Arrow Right -->
          <span u="arrowright" class="jssora05r slider-arrow sa-right" style="width: 55px; height: 55px; top: 123px; right: -65px">
          </span>
          <!-- Arrow Navigator Skin End -->

          <!-- ThumbnailNavigator Skin Begin -->
          <div u="thumbnavigator" class="jssort04" style="position: absolute; width: 980px;
              height: 60px; right: 0px; bottom: 0px;">
              <!-- <div style=" background-color: #000; filter:alpha(opacity=30); opacity:.3; width: 100%; height:100%;"></div> -->
              <div u="slides" style="bottom: 25px; right: 30px;">
                  <div u="prototype" class="p" style="position: absolute; width: 62px; height: 32px; top: 0; left: 0;">
                      <div class="w">
                          <thumbnailtemplate style="width: 100%; height: 100%; border: none; position: absolute; top: 0; left: 0;"></thumbnailtemplate>
                      </div>
                      <div class="c" style="position: absolute; background-color: #000; top: 0; left: 0"></div>
                  </div>
              </div>
              <!-- Thumbnail Item Skin End -->
          </div>
          <!-- ThumbnailNavigator Skin End -->
            <!-- </div> -->
          </div>
            <!-- Jssor Slider End -->
    </div>
  </div>
</div>

<div class="row">  
      @if(Auth::user())
      <div class="col-md-12">
        <div class="panel panel-primary">
          <!-- Default panel contents -->
        <div class="panel-heading">
          Notifications
          <div class="pull-right">
            <i class="fa fa-table fw"></i>
            {{ link_to( '/notifications', 'View All', array('class'=>'') ) }}
          </div>

        </div>
        <ul class="list-group">
          @if(count(Auth::user()->notifications()->get()))
            @foreach(Auth::user()->notifications()->limit(5)->get() as $notification)

              <a href="{{$notification->link ? $notification->link : ''}}" class="list-group-item {{$notification->link ? '' : 'inactive-link'}}">
                {{'<strong>'.$notification->doer->fullName() . '</strong> ' . $notification->notification . '<br/> <span class="label label-default"><i><small>' . $notification->timeCreated() . '</small></i></span>'}}
              </a>
            @endforeach
          @else
              <li class="list-group-item">
                <i><center>Nothing to show</center></i>
              </li>
          @endif
        </ul>
        </div>
  </div>
      @endif
    
</div>
@stop
@section('cssjs')
{{ HTML::script('vendor/slider.js-master/js/jssor.slider.mini.js') }}
{{ HTML::script('vendor/slider.js-master/js/jssor.core.js') }}
{{ HTML::script('vendor/slider.js-master/js/jssor.utils.js') }}
{{ HTML::script('js/banner.js') }}
{{ HTML::script('vendor/lightbox/js/lightbox.js') }}
{{ HTML::style('vendor/lightbox/css/lightbox.css') }}
@stop
@extends('layouts.default')
@section('nav')
	@include('includes.scorecard_menu')
@stop
@section('content')

<h2>Manage OPCR </h2>
<hr />
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<i class="fa fa-table fw"></i> OPCR
				<div class="pull-right">
				    {{ HTML::decode(link_to_route('opcr.create', '<i class="fa fa-plus fw"></i> Create New', null)) }}
				</div>
			</div>
			<div class="panel-body remove-padding">
				<table class="table table-striped table-hover table-select">
					<thead>
						<tr>
							<th class="width-50">OPCR Name</th>
							<th class="width-20">Date Created</th>
							<th class="width-10">Status</th>
							<th class="width-20">Action</th>
						</tr>
					</thead>
					<tbody>
						@if(count($opcrlist ))
							@foreach($opcrlist as $list)
								<tr>
									<td>{{ link_to("opcr/{$list->id}", $list->name) }}</td>
									<td> {{ $list->created_at }} </td>
									<td>
										@if ($list->flag == 2)
											<span class="label label-success">Final</span>
										@elseif ($list->flag == 1)
											<span class="label label-info">Ready</span>
										@elseif($list->for_approval == 1)
											<span class="label label-warning">Approval</span>
										@else
											<span class="label label-default">Draft</span>
										@endif
									</td>
									<td>
										@if(Role::access('05'))
											<div class="btn-group actions">
											@if ($list->flag == 2)
												
												{{ HTML::decode (link_to( "/opcr/{$list->id}/edit", '<i class="fa fa-pencil fw"></i>', array('class'=>'btn btn-default inline hide-button-style action-btn','title'=>'Edit') )) }}
												{{ HTML::decode (link_to( "/opcr/{$list->id}/mark/1", '<i class="fa fa-flag fw"></i>', array('class'=>'btn btn-info inline hide-button-style action-btn','title'=>'Mark as Ready') )) }}
											@elseif ($list->flag == 1)
												{{ HTML::decode (link_to( "/opcr/{$list->id}/edit", '<i class="fa fa-pencil fw"></i>', array('class'=>'btn btn-default inline hide-button-style action-btn','title'=>'Edit') )) }}
												{{ HTML::decode (link_to( "/opcr/".$list->id."/mark/2", '<i class="fa fa-check fw"></i>', array('class'=>'btn btn-success inline hide-button-style action-btn','title'=>'Mark as Final','onclick'=>"return confirm('Are you sure you want to mark this as final?')") )) }}
												{{ HTML::decode (link_to( "/opcr/{$list->id}/mark/0", '<i class="fa fa-file-text fw"></i>', array('class'=>'btn btn-default inline hide-button-style action-btn','title'=>'Mark as Draft') )) }}
											@else
												{{ HTML::decode (link_to( "/opcr/{$list->id}/edit", '<i class="fa fa-pencil fw"></i>', array('class'=>'btn btn-default inline hide-button-style action-btn','title'=>'Edit') )) }}
												{{ HTML::decode (link_to( "/opcr/".$list->id."/mark/1", '<i class="fa fa-flag fw"></i>', array('class'=>'btn btn-info inline hide-button-style action-btn','title'=>'Mark as Ready','onclick'=>"return confirm('Are you sure you want to mark this as ready?')") )) }}
												{{ HTML::decode (link_to( "/opcr/delete/".$list->id, '<i class="fa fa-times fw"></i>', array('class'=>'btn btn-danger inline hide-button-style action-btn','title'=>'Delete','onclick'=>"return confirm('Are you sure you want to delete this?')") )) }}
												
											@endif
											</div>
										@elseif( ! $list->for_approval || Role::access('34'))
											<div class="btn-group actions">
											@if ( ! $list->flag)
												{{ HTML::decode (link_to( "/opcr/{$list->id}/edit", '<i class="fa fa-pencil fw"></i>', array('class'=>'btn btn-default inline hide-button-style action-btn','title'=>'Edit') )) }}
												@if(Role::access('34'))
												{{ HTML::decode (link_to( "/opcr/".$list->id."/mark/1", '<i class="fa fa-flag fw"></i>', array('class'=>'btn btn-darkgreen inline hide-button-style action-btn','title'=>'Mark as Ready','onclick'=>"return confirm('Are you sure you want to mark this as final?')") )) }}
												
												@elseif( ! $list->for_approval)
												{{ HTML::decode (link_to( "/opcr/".$list->id."/forapproval", '<i class="fa fa-thumbs-up fw"></i>', array('class'=>'btn btn-warning inline hide-button-style action-btn','title'=>'Submit for Approval','onclick'=>"return confirm('Are you sure you want to request for the approval of this?')") )) }}
												
												@endif
												{{ HTML::decode (link_to( "/opcr/delete/".$list->id, '<i class="fa fa-times fw"></i>', array('class'=>'btn btn-danger inline hide-button-style action-btn','title'=>'Delete','onclick'=>"return confirm('Are you sure you want to delete this?')") )) }}
											@endif
											</div>
										@endif
									</td>
								</tr>
							@endforeach
						@else
						<tr>
							<td colspan="4"> <center> <i> No existing OPCRs. </i> </center> </td>
						</tr>
						@endif
					</tbody>
				</table>
				<div class="container">
					<span class="pull-right">
						<?php echo $opcrlist->links(); ?>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
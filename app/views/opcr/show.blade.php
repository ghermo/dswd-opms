@extends('layouts.default')
@section('content')
<h2 class="pull-left">{{ HTML::decode (link_to( "/opcr", '<i class="fa fa-chevron-left fw"></i>', array('class'=>'link-black hidden-print','title'=>'OPCR List') )) }} Office Performance Contract and Review
</h2>
<div class="btn-group pull-right btn-option hidden-print">
    <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown">
    <i class="fa fa-bars fw"></i> Options <span class="caret"></span>
    </button>
    <ul class="dropdown-menu pull-right btn-option-dropdown" style="width:180px;" role="menu">
	    @if ($opcr->flag == 0)
			<li>{{ HTML::decode(link_to('/opcr/'.$opcr->id.'/edit/', '<i class="fa fa-pencil fw"></i> Edit')) }}</li>
			@if( Role::access('34'))
			<li>{{ HTML::decode(link_to( "/opcr/{$opcr->id}/mark/1", '<i class="fa fa-flag fw"></i> Mark as Ready', array('onclick' => "return confirm('Are you sure you want to mark this as ready?')") )) }}</li>
			<li class="divider"></li>
			@endif
		@elseif(Role::access('05') && $opcr->flag == 1)
			<li>{{ HTML::decode(link_to( "/opcr/{$opcr->id}/mark/2", '<i class="fa fa-check fw"></i> Mark as Final', array('onclick' => "return confirm('Are you sure you want to mark this as final?')" ))) }}</li>
			<li>{{ HTML::decode(link_to( "/opcr/{$opcr->id}/mark/0", '<i class="fa fa-file-text fw"></i> Mark as Draft')) }}</li>
			<li class="divider"></li>
		@endif
		<li><a href="javascript:window.print()"><i class="fa fa-print fw"></i> Export as PDF</a></li>
    </ul>
</div>
<hr class="clear" />
<div class="row measure-profile">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<center>
					<h3> OFFICE PERFORMANCE CONTRACT AND REVIEW (OPCR) - CONTRACTING FORM </h3>
				</center>

				<div class="row" style="margin-bottom: 10px;">
					<p class="col-md-offset-1 col-md-10"> 
						The <strong> {{$opcr->office()->first()->office_name}} </strong> commits to deliver and agrees to be rated on the attainment of the following targets in accordance with the indicated measures for the period <strong> {{$opcr->period}} </strong>.
					</p>
				</div>

				@if($opcr->flag == 2 || (Role::access('05') && $opcr->flag))
					<div class="panel panel-default" style="border: 0;">
						<div class="panel-body" style="padding: 0;">
							<table class = "table table-center-headers table-bordered">
								<tr>
									<th width="15%" > Key Result Area/ Key Results </th>
									<th width="32%" > Success/Performance Indicators / Measures </th>
									<th width="18%" > Actual Accomplishment </th>
									<th width="27%" colspan="5"> 
										Rating 
										
									</th>
									<th width="8%" > Remarks/Coaching </th>
								</tr>
								<tr>
									<th width="15%"> &nbsp; </th>
									<th width="32%"> &nbsp; </th>
									<th width="18%"> &nbsp; </th>
								
									<th colspan="1"> Qn</th>
									<th colspan="1"> Ql</th>
									<th colspan="1"> T	</th>
									<th colspan="1">Average Rating</th>
									<th colspan="1">Weighted Rating</th>
									
									<th width="8%"> &nbsp; </th>
									<th> &nbsp; </th>
								</tr>
								<tr class="info">
									<td colspan="8" > I. STRATEGIC PRIORITIES  (50%)  </td>
									<td >
										<?php 
											$strat=0; 
											$counter=0;
										?>
										@foreach($opcr->strategicPriorities as $indicator)
											<?php 
												$strat=$strat + ($indicator->qn + $indicator->q1 + $indicator->t)/3;
												$counter++;
											?>
										@endforeach
										@foreach($objectives as $key=>$objective)
											@foreach($opcr->coreFunctions as $indicator)	
													<?php $strat=$strat+($indicator->qn+$indicator->q1+$indicator->t)/3;
													$counter++;
													?>
											@endforeach
										@endforeach
										{{Form::text('', $strat1=($strat/($counter? $counter : 1))*.50, ['class'=>'form-control','id' => 'strategic', 'disabled'])}} 
										
									</td>
								</tr>
								<tr>
									<td colspan="10"> A. Scoreboard Measures  </td>
								</tr>
								<tr>
									<td colspan="10"><strong> <i>Breakthrough Goals (BG)</i> </strong> </td>
								</tr>
								
								<div>
									@foreach($opcr->strategicPriorities as $indicator)
										@if($indicator->flag == 0)
											<tr>
												<td> &nbsp; </td>
												<td>
													{{$indicator->indicator}} 
												</td>
												<td> 
													{{$indicator->accomplishment}}
												</td>
												<td> 
													{{$indicator->qn}} 
												</td>
												<td> 
													{{$indicator->q1}} 
												</td>
												<td> 
													{{$indicator->t}} 
												</td>
												<td><b>{{($indicator->qn+$indicator->q1+$indicator->t)/3}}</b></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td class = "buttons">
													&nbsp;
								                </td>
											</tr>
										@endif
									@endforeach
								</div>
						
						
						<tr>
							<td colspan="10"><strong> <i>Lead Measures (LM) </i> </strong> </td>
						</tr>
						<tbody id="A-LM">
								<div>
									@foreach($opcr->strategicPriorities as $indicator)
										@if($indicator->flag == 1)
											<tr>
												<td> &nbsp; </td>
												<td> 
														{{$indicator->indicator}} 
												</td>
												<td> 													
													{{$indicator->accomplishment}} 
												</td>
												<td> 
													{{$indicator->qn}} 
												</td>
												<td> 
													{{$indicator->q1}} 
												</td>
												<td> 
													{{$indicator->t}} 
												</td>
												
												<td><b>{{($indicator->qn+$indicator->q1+$indicator->t)/3}}</b></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td class = "button">
													&nbsp;
												</td>
											</tr>
										@endif
									@endforeach
								</div>
								</tbody>
								<tr>
									<td colspan="10">  B. Strategic Outcomes  </td>
								</tr>
								
								
								@foreach($objectives as $key=>$objective)

											<?php $first = true ?>

											@foreach($opcr->coreFunctions as $indicator)
												@if($indicator->objective_id == $key)
													@if($first)
														<tr>
															<td colspan="10"><strong> <i>{{$objective}}</i> </strong> </td>
														</tr>
														<?php $first = false; ?>
													@endif	
													<tr>
														<td> &nbsp; </td>
														<td> 
															{{$indicator->indicator}} 
														</td>
														<td> 
															{{$indicator->accomplishment}} {{$indicator->description}}
														</td>
														<td> 
															{{$indicator->qn}} 
														</td>
														<td> 
															{{$indicator->q1}} 
														</td>
														<td> 
															{{$indicator->t}} 
														</td>
														
														<td><b>{{($indicator->qn+$indicator->q1+$indicator->t)/3}}</b></td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td class = "buttons">
															&nbsp;
										                </td>
													</tr>
												@endif
											@endforeach
										@endforeach
						<tr> 
							<td colspan='8' width="93.25%"> II. Other KRAs (40%)   </td>
							<td>
								<?php 
									$leader=0; 
									$counter=0;
								?>
								@foreach($opcr->leadershipCompetencies as $indicator)
									<?php 
										$leader=$leader+(($indicator->qn+$indicator->q1+$indicator->t)/3);
										$counter++;
									?>
								@endforeach
									{{Form::text('', $leader1=($leader/($counter? $counter : 1))*.40, ['class'=>'form-control','id' => 'strategic', 'disabled'])}} 
							</td>
						</tr>
						<tbody id="D">
							
							
							@foreach($opcr->leadershipCompetencies as $indicator)
									<tr>
										<td> &nbsp; </td>
										<td> 
											{{$indicator->indicator}} 
										</td>
										<td> 
											{{$indicator->accomplishment}} 
										</td>
										<td> 
											{{$indicator->qn}} 
										</td>
										<td> 
											{{$indicator->q1}} 
										</td>
										<td> 
												{{$indicator->t}} 
										</td>
										
										<td><b>{{($indicator->qn+$indicator->q1+$indicator->t)/3}}</b></td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td class = "buttons">
											&nbsp;
						                </td>
									</tr>
								@endforeach
						</tbody>
						<tr>
							<td colspan="8"> III. OTHER FUNCTIONS/DIRECTIVES FROM THE SECRETARY AND CLUSTER HEAD (10%)  </td>
							<td>
								<?php 
									$other=0; 
									$counter=0;
								?>
								@foreach($opcr->otherFunctions as $indicator)
									<?php 
										$other=$other+(($indicator->qn+$indicator->q1+$indicator->t)/3);
										$counter++;
									?>
								@endforeach
								{{Form::text('', $other1=($other/($counter? $counter : 1))*.10, ['class'=>'form-control','id' => 'strategic', 'disabled'])}} 
								
							</td>
						</tr>
						<tbody id="C">
							
							@foreach($opcr->otherFunctions as $indicator)
									<tr>
										<td> &nbsp; </td>
										<td> 
												{{$indicator->indicator}} 
										</td>
										<td> 
												{{$indicator->accomplishment}} 
										</td>
										<td> 
												{{$indicator->qn}} 
										</td>
										<td> 
												{{$indicator->q1}} 
										</td>
										<td> 
												{{$indicator->t}} 
										</td>
										
										<td><b>{{($indicator->qn+$indicator->q1+$indicator->t)/3}}</b></td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td class = "buttons">
											&nbsp;
						                </td>
									</tr>
								@endforeach
							
							</tbody>
							<tr class="opcr-add">
								<td width="15%"> Final Average Rating</td>
								<td width="19%"> &nbsp; </td>
								<td width="16%"> &nbsp; </td>
								<td width="5.5%">  &nbsp; </td>
								<td width="5.5%">  &nbsp;  </td>
								<td width="5.5%">  &nbsp;  </td>
								<td width="6%">&nbsp;</td>
								<td width="8%">&nbsp;</td>
								<td width="13%">&nbsp;</td>
								<td width="%8"> {{Form::text('', $strat1+$other1+$leader1, ['class'=>'form-control','id' => 'final', 'disabled'])}} </td>
							</tr>
							<tr class="opcr-add">
								<td width="15%"> Adjectival Rating</td>
								<td width="19%"> &nbsp; </td>
								<td width="16%"> &nbsp; </td>
								<td width="5.5%">  &nbsp; </td>
								<td width="5.5%">  &nbsp;  </td>
								<td width="5.5%">  &nbsp;  </td>
								<td width="6%">&nbsp;</td>
								<td width="8%">&nbsp;</td>
								<td width="13%">&nbsp;</td>
								<td width="%8">  &nbsp; </td>
						</tr>
						</table>
						</div>
					</div>
				@else
					<div class="panel panel-default" style="border: 0;">
						<div class="panel-body" style="padding: 0;">
							<table class = "table table-center-headers table-bordered">
								<tr>
									<th class="width-20"> Key Result Area/ Key Results </th>
									<th class="width-30"> Success/Performance Indicators / Measures </th>
									<th class="width-40"> Actual Accomplishment </th>
								</tr>
								<tr class="info">
									<td colspan="3" > I. STRATEGIC PRIORITIES  (50%)  </td>
								</tr>
								<tr>
									<td colspan="3"> <b> <i> A. Scoreboard Measures </i> </b> </td>
								</tr>
								@if(count($opcr->strategicPriorities()->whereFlag(0)->get()))
									<?php $first = true ?>
									@foreach($opcr->strategicPriorities()->whereFlag(0)->get() as $indicator)
										<tr>
											@if($first)
												<td rowspan="{{count($opcr->strategicPriorities()->whereFlag(0)->get())}}"> <i>Breakthrough Goals (BG)</i> </td>
											@endif
											<td>
												{{$indicator->indicator}} 
											</td>
											<td> 
												{{$indicator->accomplishment}}
											</td>
										</tr>
										<?php $first = false ?>
									@endforeach
								@else
									<tr>
										<td> <i>Breakthrough Goals (BG)</i> </td>
										<td colspan="2"> &nbsp; </td>
									</tr>
								@endif
								<tbody id="A-LM">
									@if(count($opcr->strategicPriorities()->whereFlag(1)->get()))
										<?php $first = true ?>
										@foreach($opcr->strategicPriorities()->whereFlag(1)->get() as $indicator)
											<tr>
												@if($first)
													<td rowspan="{{count($opcr->strategicPriorities()->whereFlag(1)->get())}}"> <i>Lead Measures (LM)</i> </td>
												@endif
												<td>
													{{$indicator->indicator}} 
												</td>
												<td> 
													{{$indicator->accomplishment}}
												</td>
											</tr>
										<?php $first = false ?>
										@endforeach
									@else
										<tr>
											<td> <i>Lead Measures (LM)</i> </td>
											<td colspan="2"> &nbsp; </td>
										</tr>
									@endif
									</tbody>
									<tr>
										<td colspan="3"> <b> <i> B. Strategic Outcomes </i> </b> </td>
									</tr>
								
									@foreach($objectives as $key=>$objective)
										<?php
											$count = 0;
											foreach($opcr->coreFunctions as $indicator)
												if($indicator->objective_id == $key)
													$count++;
										?>
										@if($count)
											<?php $first = true ?>
											@foreach($opcr->coreFunctions as $indicator)
												@if($indicator->objective_id == $key)
													<tr>
														@if($first)
															<td rowspan="{{$count}}"> <i>{{$objective}}</i> </td>
														@endif	
														<td> 
															{{$indicator->indicator}} 
														</td>
														<td> 
															{{$indicator->accomplishment}} {{$indicator->description}}
														</td>
													</tr>
													<?php $first = false; ?>
												@endif
											@endforeach
										@else
											<tr>
												<td> <i>{{$objective}}</i> </td>
												<td colspan="2"> &nbsp; </td>
											</tr>
										@endif
									@endforeach
						
								<tr class="info">
									<td colspan="3" > II. Other KRAs (40%)  </td>
								</tr>
						<tbody id="D">
							
							@if(count($opcr->leadershipCompetencies))
								@foreach($opcr->leadershipCompetencies as $indicator)
									<tr>
										<td> &nbsp; </td>
										<td> 
											{{$indicator->indicator}} 
										</td>
										<td> 
											{{$indicator->accomplishment}} 
										</td>
								@endforeach
							@else
								<tr> <td colspan="4"> &nbsp; </td> </tr>
							@endif
						</tbody>
						<tr>
							<tr class="info">
								<td colspan="3" > III. OTHER FUNCTIONS/DIRECTIVES FROM THE SECRETARY AND CLUSTER HEAD (10%)  </td>
							</tr>
						</tr>
						<tbody id="C">
							@if(count($opcr->otherFunctions))
								@foreach($opcr->otherFunctions as $indicator)
									<tr>
										<td> &nbsp; </td>
										<td> 
												{{$indicator->indicator}} 
										</td>
										<td> 
												{{$indicator->accomplishment}} 
										</td>
									</tr>
								@endforeach
							@else
								<tr> <td colspan="4"> &nbsp; </td> </tr>
							@endif
							
							</tbody>
						</table>
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>

<div class="panel panel-default remarks">
	<div class="panel-heading">
		Remarks
		<div class="pull-right panel-heading hidden-print">
			{{Form::label('filter', 'Filter:') }} 
			{{ Form::select('filter', ['Comments', 'Suggestions', 'All'], 2, ['class' => 'baseline filter'])}}
		</div>
	</div>
	<div class="panel-body">
		<div class="list-group">
			<div class=" list-group-item hidden-print">
				<div class="row">
					{{Form::open(['url' => URL::route('opcr.remark', ['id' => $opcr->id])])}}
						<div class="col-md-2">
							{{ Form::radio('type', 0, 1)}} Comment <br/>
							{{ Form::radio('type', 1, 0)}} Suggestion
						</div>
						<div class="col-lg-10">
			                <div class="input-group">
			                    {{ Form::text('remark', null, ['class' => 'form-control', 'rows' => 3])}}
				                <span class="input-group-btn">
				                    {{ Form::submit('Add Remark', ['class' => 'btn btn-primary'])}}
				                </span>
			                </div><!-- /input-group -->
			                {{ $errors->first('remark', '<span class=errormsg>*:message</span>') }}
			            </div><!-- /.col-lg-10 -->
					{{ Form::close() }}
				</div>
			</div>
			@if(count($remarks))
				<?php $show_more = true; ?>
				<div class="show-more">
					@foreach($remarks as $key => $remark)
						@if($show_more && ((count($remarks)-3) <= $key))
							</div>
							<?php $show_more = false; ?>
						@endif
						<div class="list-group-item remark{{$remark->type}}">
							<strong> {{ $remark->user->fullName() }} </strong>
							{{ $remark->remark }} <br/>
							<i class="livetimestamp"> 
								{{ $remark->type? "suggested" : "commented"}} {{ $remark->created_at->diffForHumans() }} 
							</i>
						</div>
					@endforeach
				@if(count($remarks) > 3)
					<div class="list-group-item">
						<center>
							{{ Form::button('See All', ['class' => 'btn btn-default remarks-collapse'])}}
						</center>
					</div>
				@endif
			@endif
		</div>
	</div>
</div>

@stop

@section('cssjs')
	{{ HTML::script('js/remarks.js')}}
@stop
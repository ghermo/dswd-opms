@extends('layouts.default')
@section('content')
<h2>Create New OPCR </h2>
<hr />
	{{Form::open(['url' => URL::route("opcr.store"), 'id' => 'form'])}}

		<div class="form-group">
			{{ Form::label('name', 'Name Of OPCR: *') }}{{ $errors->first('name', '<span class=errormsg>*:message</span>') }}
			{{ Form::text('name', null, ['class' => 'form-control'])}} 
		</div>
		<div class = "row">
			<div class="form-group col-md-6">
				{{ Form::label('office_id', 'Office: *') }}
				@if(Role::access('05'))
					{{ Form::select('office_id', $offices, null, ['id' => 'office-select', 'class' => 'form-control'])}} 
				@else
					{{ Form::hidden('office_id', Auth::user()->office_id)}}
					{{ Form::select('office_id', $offices, Auth::user()->office_id, ['id' => 'office-select', 'class' => 'form-control', 'disabled'])}}
					
				@endif
			</div>
			<div class="form-group col-md-6">
				{{ Form::label('period', 'Period: *') }}{{ $errors->first('period', '<span class=errormsg>*:message</span>') }}
				{{ Form::text('period', null, ['id' => 'period-select', 'class' => 'form-control'])}} 
			</div>
		</div>
		<div class="well">
		<p style="padding:20px;"> The <strong id="selected-office"> office </strong> commits to deliver and agrees to be rated on the attainment of the following targets in accordance with the indicated measures for the period <strong id="selected-period"> </strong>. </p>
		</div>

		<div class="box" ng-controller="OpcrController">
			<div class="box-body">
				<table class = "table table-center-headers table-bordered table-hover">
					<tr>
						<th class="width-20"> Key Result Area/ Key Results </th>
						<th class="width-30"> Success/Performance Indicators / Measures </th>
						<th class="width-40"> Actual Accomplishment </th>
						<th class="width-10"> ACTIONS </th>
					</tr>
					<tr class="info">
						<td colspan="4" > I. STRATEGIC PRIORITIES  (50%)  </td>
					</tr>
					<tr>
						<td colspan="4"> <b> <i> A. Scoreboard Measures </i> </b> </td>
					</tr>
					<tbody>
						<tr ng-repeat="indicator in indicators1 | filter:{flag: 0}" ng-mouseleave="indicator.editMode = false">
							<td ng-show="$first" rowspan="<%(indicators1|filter:{flag:0}).length+1%>"> <i>Breakthrough Goals (BG)</i> </td>
							<td> 
								<input type="hidden" name="indicators1_flag[]" ng-value="indicator.flag"> 
								<textarea name="indicators1_indicator[]" ng-show="indicator.editMode" class="form-control" ng-model="indicator.indicator" required></textarea> 
								<span ng-hide="indicator.editMode"> <%indicator.indicator%> </span>
							</td>
							<td> 
								<textarea name="indicators1_actualAccomplishment[]" ng-show="indicator.editMode" class="form-control" ng-model="indicator.actualAccomplishment" required></textarea> 
								<span ng-hide="indicator.editMode"> <%indicator.actualAccomplishment%> </span>
							</td>
							<td>
								<span ng-hide="indicator.editMode">
									<a class="btn btn-default" ng-click="indicator.editMode = true" title="Edit">
										<i class="fa fa-pencil"> </i> 
									</a> 
									<a class="btn btn-danger" ng-click="delIndicator1(indicator)" title="Delete">
										<i class="fa fa-times"> </i> 
									</a>
								</span>
							</td>
						</tr>
						<tr class="opcr-add" ng-init="newIndicator1[0] = {}">
							<td ng-show=" ! (indicators1|filter:{flag:0}).length"> <i>Breakthrough Goals (BG)</i> </td>
							<td ng-show="newIndicator1[0].addMode"> 
								<textarea ng-model="newIndicator1[0].indicator" placeholder="New Success Indicator" class="form-control"> </textarea>
							</td>
							<td ng-show="newIndicator1[0].addMode"> <textarea ng-model="newIndicator1[0].actualAccomplishment" class="form-control" placeholder="New Actual Accomplishment"> </textarea></td>
							<td colspan="2" ng-hide="newIndicator1[0].addMode"> &nbsp; </td>
							<td>
								<div ng-show="newIndicator1[0].addMode"> 
									<a class="btn btn-default" ng-click="addIndicator1(0); newIndicator1[0].addMode=false" title="Save"> <i class="fa fa-save fw"></i> </a>
								</div>
								<div ng-hide="newIndicator1[0].addMode">
									<a class="btn btn-default" ng-click="newIndicator1[0].addMode=true" title="Add"> <i class="fa fa-plus fw"></i> </a>
								</div>
							</td>
						</tr>
					</tbody>
					<tbody>
						<tr ng-repeat="indicator in indicators1 | filter:{flag: 1}" ng-mouseleave="indicator.editMode = false">
							<td ng-show="$first" rowspan="<%(indicators1|filter:{flag:1}).length+1%>"> <i>Lead Measures (LM)</i> </td>
							<td> 
								<input type="hidden" name="indicators1_flag[]" ng-value="indicator.flag"> 
								<textarea name="indicators1_indicator[]" ng-show="indicator.editMode" class="form-control" ng-model="indicator.indicator" required></textarea> 
								<span ng-hide="indicator.editMode"> <%indicator.indicator%> </span>
							</td>
							<td> 
								<textarea name="indicators1_actualAccomplishment[]" ng-show="indicator.editMode" class="form-control" ng-model="indicator.actualAccomplishment" required></textarea> 
								<span ng-hide="indicator.editMode"> <%indicator.actualAccomplishment%> </span>
							</td>
							<td>
								<span ng-hide="indicator.editMode">
									<a class="btn btn-default" ng-click="indicator.editMode = true" title="Edit">
										<i class="fa fa-pencil"> </i> 
									</a> 
									<a class="btn btn-danger" ng-click="delIndicator1(indicator)" title="Delete">
										<i class="fa fa-times"> </i> 
									</a>
								</span>
							</td>
						</tr>
						<tr class="opcr-add" ng-init="newIndicator1[1] = {}">
							<td ng-show=" ! (indicators1|filter:{flag:1}).length"> <i>Lead Measures (LM)</i> </td>
							<td ng-show="newIndicator1[1].addMode"> 
								<textarea ng-model="newIndicator1[1].indicator" placeholder="New Success Indicator" class="form-control"> </textarea>
							</td>
							<td ng-show="newIndicator1[1].addMode"> <textarea ng-model="newIndicator1[1].actualAccomplishment" class="form-control" placeholder="New Actual Accomplishment"> </textarea></td>
							<td colspan="2" ng-hide="newIndicator1[1].addMode"> &nbsp; </td>
							<td>
								<div ng-show="newIndicator1[1].addMode"> 
									<a class="btn btn-default" ng-click="addIndicator1(1); newIndicator1[1].addMode=false" title="Save"> <i class="fa fa-save fw"></i> </a>
								</div>
								<div ng-hide="newIndicator1[1].addMode">
									<a class="btn btn-default" ng-click="newIndicator1[1].addMode=true" title="Add"> <i class="fa fa-plus fw"></i> </a>
								</div>
							</td>
						</tr>
					</tbody>
					<tr>
						<td colspan="4"> <b> <i> B. Strategic Outcomes </i> </b> </td>
					</tr>
					@foreach($objectives as $id => $objective)
						<tbody ng-init="indicators2[{{$objective->id}}] = []">
							<?php $first = true ?>
							@foreach($objective->indicators as $indicator)
								<tr class="disabled-container disabled">
									@if($first)
										<td rowspan="<%indicators2[{{$objective->id}}].length + {{ count($objective->indicators)}} + 1%>"> 
											<i class="default"> {{ $objective->objective }} </i> 
										</td>
									@endif
									<td> 
										<input type="hidden" name="indicators2_indicator[]" value="{{$indicator->indicator}}">
										<input type="checkbox" class="enableSO">
										{{ $indicator->indicator }}
									</td>
									<td>
										<div class="row">
											<div class="col-md-3">
												<input type="text" name="indicators2_actualAccomplishment[]" class="form-control SO" placeholder="Actual*" disabled="disabled" required/>
												<input type="hidden" class="SO" name="indicators2_objective[]" value="{{$objective->id}}" disabled="disabled" />
											</div>
											<input type="hidden" name="indicators2_description[]" value="{{$indicator->description}}">
											<div class="col-md-9"> {{$indicator->description}} </div>
										</div>
									</td>
									<td> &nbsp; </td>
									<?php $first = false ?>
								</tr>
							@endforeach
							<tr ng-repeat="indicator in indicators2[{{$objective->id}}]" ng-mouseleave="indicator.editMode = false">
								<td ng-show="! {{ count($objective->indicators) }} && $first" rowspan="<%indicators2[{{$objective->id}}].length+1%>"> <i> {{ $objective->objective }} </i> </td>
								<td> 
									<input type="hidden" name="indicators2_objective[]" ng-value="indicator.objective">
									<textarea name="indicators2_indicator[]" ng-show="indicator.editMode" class="form-control" ng-model="indicator.indicator" required></textarea> 
									<span ng-hide="indicator.editMode"> <%indicator.indicator%> </span>
								</td>
								<td> 
									<input type="hidden" name="indicators2_description[]" value="">
									<textarea name="indicators2_actualAccomplishment[]" ng-show="indicator.editMode" class="form-control" ng-model="indicator.actualAccomplishment" required></textarea> 
									<span ng-hide="indicator.editMode"> <%indicator.actualAccomplishment%> </span>
								</td>
								<td>
									<span ng-hide="indicator.editMode">
										<a class="btn btn-default" ng-click="indicator.editMode = true" title="Edit">
											<i class="fa fa-pencil"> </i> 
										</a> 
										<a class="btn btn-danger" ng-click="delIndicator2({{$objective->id}}, indicator)" title="Delete">
											<i class="fa fa-times"> </i> 
										</a>
									</span>
								</td>
							</tr>
							<tr class="opcr-add" ng-init="newIndicator2[{{$objective->id}}] = {}; indicators2[{{$objective->id}}] = []	">
								<td ng-show="! {{ count($objective->indicators) }} && ! indicators2[{{$objective->id}}].length"> <i>{{$objective->objective}} (BG)</i> </td>
								<td ng-show="newIndicator2[{{$objective->id}}].addMode"> 
									<textarea ng-model="newIndicator2[{{$objective->id}}].indicator" placeholder="Other Success Indicator" class="form-control"> </textarea>
								</td>
								<td ng-show="newIndicator2[{{$objective->id}}].addMode"> <textarea ng-model="newIndicator2[{{$objective->id}}].actualAccomplishment" class="form-control" placeholder="Other Actual Accomplishment"> </textarea></td>
								<td colspan="2" ng-hide="newIndicator2[{{$objective->id}}].addMode"> &nbsp; </td>
								<td>
									<div ng-show="newIndicator2[{{$objective->id}}].addMode"> 
										<a class="btn btn-default" ng-click="addIndicator2({{$objective->id}}); newIndicator2[{{$objective->id}}].addMode=false" title="Save"> <i class="fa fa-save fw"></i> </a>
									</div>
									<div ng-hide="newIndicator2[{{$objective->id}}].addMode">
										<a class="btn btn-default" ng-click="newIndicator2[{{$objective->id}}].addMode=true" title="Add"> <i class="fa fa-plus fw"></i> </a>
									</div>
								</td>
							</tr>
						</tbody>
					@endforeach
					<tr class="info"> 
						<td colspan="4"> II. Other KRAs (40%)  </td>
					</tr>
					<tr ng-repeat="indicator in indicators3" ng-mouseleave="indicator.editMode = false">
						<td ng-show="$first" rowspan="<%indicators3.length+1%>"> &nbsp; </td>
						<td> 
							<textarea name="indicators3_indicator[]" ng-show="indicator.editMode" class="form-control" ng-model="indicator.indicator" required></textarea> 
							<span ng-hide="indicator.editMode"> <%indicator.indicator%> </span>
						</td>
						<td> 
							<textarea name="indicators3_actualAccomplishment[]" ng-show="indicator.editMode" class="form-control" ng-model="indicator.actualAccomplishment" required></textarea> 
							<span ng-hide="indicator.editMode"> <%indicator.actualAccomplishment%> </span>
						</td>
						<td>
							<span ng-hide="indicator.editMode">
								<a class="btn btn-default" ng-click="indicator.editMode = true" title="Edit">
									<i class="fa fa-pencil"> </i> 
								</a> 
								<a class="btn btn-danger" ng-click="delIndicator3(indicator)" title="Delete">
									<i class="fa fa-times"> </i> 
								</a>
							</span>
						</td>
					</tr>
					<tr class="opcr-add" ng-init="newIndicator3 = {}">
						<td ng-show=" ! indicators3.length"> &nbsp; </td>
						<td ng-show="newIndicator3.addMode"> 
							<textarea ng-model="newIndicator3.indicator" placeholder="New Success Indicator" class="form-control"> </textarea>
						</td>
						<td ng-show="newIndicator3.addMode"> <textarea ng-model="newIndicator3.actualAccomplishment" class="form-control" placeholder="New Actual Accomplishment"> </textarea></td>
						<td colspan="2" ng-hide="newIndicator3.addMode"> &nbsp; </td>
						<td>
							<div ng-show="newIndicator3.addMode"> 
								<a class="btn btn-default" ng-click="addIndicator3(); newIndicator3.addMode=false"> <i class="fa fa-save fw" title="Save"></i> </a>
							</div>
							<div ng-hide="newIndicator3.addMode">
								<a class="btn btn-default" ng-click="newIndicator3.addMode=true" title="Add"> <i class="fa fa-plus fw"></i> </a>
							</div>
						</td>
					</tr>
					<tr class="info">
						<td colspan="9"> III. OTHER FUNCTIONS/DIRECTIVES FROM THE SECRETARY AND CLUSTER HEAD  (10%)  </td>
					</tr>
					<tr ng-repeat="indicator in indicators4" ng-mouseleave="indicator.editMode = false">
						<td ng-show="$first" rowspan="<%indicators4.length+1%>"> &nbsp; </td>
						<td>
							<textarea name="indicators4_indicator[]" ng-show="indicator.editMode" class="form-control" ng-model="indicator.indicator" required></textarea> 
							<span ng-hide="indicator.editMode"> <%indicator.indicator%> </span>
						</td>
						<td> 
							<textarea name="indicators4_actualAccomplishment[]" ng-show="indicator.editMode" class="form-control" ng-model="indicator.actualAccomplishment" required></textarea> 
							<span ng-hide="indicator.editMode"> <%indicator.actualAccomplishment%> </span>
						</td>
						<td>
							<span ng-hide="indicator.editMode">
								<a class="btn btn-default" ng-click="indicator.editMode = true" title="Edit">
									<i class="fa fa-pencil"> </i> 
								</a> 
								<a class="btn btn-danger" ng-click="delIndicator4(indicator)" title="Delete">
									<i class="fa fa-times"> </i> 
								</a>
							</span>
						</td>
					</tr>
					<tr class="opcr-add" ng-init="newIndicator4 = {}">
						<td ng-show=" ! indicators4.length"> &nbsp; </td>
						<td ng-show="newIndicator4.addMode"> 
							<textarea ng-model="newIndicator4.indicator" placeholder="New Success Indicator" class="form-control"> </textarea>
						</td>
						<td ng-show="newIndicator4.addMode"> <textarea ng-model="newIndicator4.actualAccomplishment" class="form-control" placeholder="New Actual Accomplishment"> </textarea></td>
						<td colspan="2" ng-hide="newIndicator4.addMode"> &nbsp; </td>
						<td>
							<div ng-show="newIndicator4.addMode"> 
								<a class="btn btn-default" ng-click="addIndicator4(); newIndicator4.addMode=false"> <i class="fa fa-save fw" title="Save"></i></a>
							</div>
							<div ng-hide="newIndicator4.addMode">
								<a class="btn btn-default" ng-click="newIndicator4.addMode=true" title="Add"> <i class="fa fa-plus fw"></i> </a>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="pull-left">
			<button type="submit" name="create1" class="btn btn-primary" value="create">Create</button>
			{{ link_to( URL::route("opcr.index"), 'Cancel', array('class'=>'btn btn-default') ) }}
		</div>	
	{{Form::close()}}
	</div>
@stop

@section('cssjs')
	<style>
		.disabled { color: #888 ; }
		.default { color: #333; }
	</style>
	<script type="text/javascript"> 

	function stopRKey(evt) { 
	  var evt = (evt) ? evt : ((event) ? event : null); 
	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null); 
	  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;} 
	} 

	document.onkeypress = stopRKey; 

	$(document).ready(function() {
		$('.enableSO').change(function() {

			if($(this).is(":checked"))
				$(this).closest('.disabled-container').removeClass('disabled').find('.SO').removeAttr('disabled');
			else
				$(this).closest('.disabled-container').addClass('disabled').find('.SO').attr('disabled','disabled');
		});
	})

	</script>
	{{HTML::script('js/createopcr.js')}}
	{{HTML::script('js/opcr.js')}}
@stop
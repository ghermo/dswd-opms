@extends('layouts.default')
@section('content')
<h2 class="pull-left">{{ HTML::decode (link_to( "/opcr", '<i class="fa fa-chevron-left fw"></i>', array('class'=>'link-black hidden-print','title'=>'OPCR List') )) }} Office Performance Contract and Review
</h2>
<div class="btn-group pull-right btn-option hidden-print">
    <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown">
    <i class="fa fa-bars fw"></i> Options <span class="caret"></span>
    </button>
    <ul class="dropdown-menu pull-right btn-option-dropdown" style="width:180px;" role="menu">
	    @if ( Role::access('34') && $opcr->flag == 0)
			<li>{{ HTML::decode(link_to( "/opcr/{$opcr->id}/mark/1", '<i class="fa fa-flag fw"></i> Mark as Ready', array('onclick' => "return confirm('Are you sure you want to mark this as ready?')") )) }}</li>
			<li class="divider"></li>
		@elseif(Role::access('05') && $opcr->flag == 1)
			<li>{{ HTML::decode(link_to( "/opcr/{$opcr->id}/mark/2", '<i class="fa fa-check fw"></i> Mark as Final', array('onclick' => "return confirm('Are you sure you want to mark this as final?')" ))) }}</li>
			<li>{{ HTML::decode(link_to( "/opcr/{$opcr->id}/mark/0", '<i class="fa fa-file-text fw"></i> Mark as Draft')) }}</li>
			<li class="divider"></li>
		@endif
		<li><a href="javascript:window.print()"><i class="fa fa-print fw"></i> Export as PDF</a></li>
    </ul>
</div>
<hr class="clear" />
<div class="row measure-profile">
	<div class="col-md-12">
				{{Form::open(['url' => "/opcr/update/{$opcr->id}"])}}
					<div class="form-group">
						{{ Form::label('name', 'Name Of OPCR: *') }}{{ $errors->first('name', '<span class=errormsg>*:message</span>') }}
						{{ Form::text('name', $opcr->name, ['id' => 'name-opcr','class' => 'form-control', Role::access('05') ? '' : 'disabled'])}} 
					</div>
					<div class = "row">
						<div class="form-group col-md-6">
							{{ Form::label('office_id', 'Office: *') }}{{ $errors->first('office_id', '<span class=errormsg>*:message</span>') }}
							{{Form::text('',$opcr->office()->first()->office_name,['class'=>'form-control','disabled'])}}
						</div>
						<div class="form-group col-md-6">
							{{ Form::label('period', 'Period: *') }}{{ $errors->first('period', '<span class=errormsg>*:message</span>') }}
							{{ Form::text('period', $opcr->period, ['id' => 'period-select', 'class' => 'form-control', Role::access('05') ? '' : 'disabled'])}} 
						</div>
					</div>
					{{Form::submit('Save', ['class' => 'btn btn-primary save-button pull-right'])}}
				{{Form::close()}}
				<br><br>
				<hr />
				<div class="well">
					<h3> OFFICE PERFORMANCE CONTRACT AND REVIEW (OPCR) - CONTRACTING FORM </h3>
				</div>
				<div class="row">
					<p class="col-md-offset-1 col-md-10"> 
						<p style="padding:20px;"> The  <strong id="selected-office"> {{$opcr->office()->first()->office_name}} </strong> commits to deliver and agrees to be rated on the attainment of the following targets in accordance with the indicated measures for the period <strong id="selected-period"> {{$opcr->period}} </strong>. </p>
					</p>
				</div>

				@if($opcr->flag == 2 || (Role::access('05') && $opcr->flag))
					<div class="box">
						<div class="box-body">
							<table class = "table table-center-headers table-bordered">
								<tr>
									<th width="15%" > Key Result Area/ Key Results </th>
									<th width="32%" > Success/Performance Indicators / Measures </th>
									<th width="18%" > Actual Accomplishment </th>
									<th width="27%" colspan="5"> 
										Rating 
										
									</th>
									<th width="8%" > Remarks/Coaching </th>
									<th > ACTIONS </th>
								</tr>
								<tr>
									<th width="15%"> &nbsp; </th>
									<th width="32%"> &nbsp; </th>
									<th width="18%"> &nbsp; </th>
								
									<th colspan="1"> Qn</th>
									<th colspan="1"> Ql</th>
									<th colspan="1"> T	</th>
									<th colspan="1">Average Rating</th>
									<th colspan="1">Weighted Rating</th>
									
									<th width="8%"> &nbsp; </th>
									<th> &nbsp; </th>
								</tr>
								<tr class="info">
									<td colspan="9" > I. STRATEGIC PRIORITIES  (50%)  </td>
									<td >
										<?php 
											$strat=0; 
											$counter=0;
										?>
										@foreach($opcr->strategicPriorities as $indicator)
											<?php 
												$strat=$strat + ($indicator->qn + $indicator->q1 + $indicator->t)/3;
												$counter++;
											?>
										@endforeach
										@foreach($objectives as $key=>$objective)
											@foreach($opcr->coreFunctions as $indicator)	
													<?php $strat=$strat+$indicator->averageRating();
													$counter++;
													?>
											@endforeach
										@endforeach
										{{Form::text('', $strat1=($strat/($counter? $counter : 1))*.50, ['class'=>'form-control','id' => 'strategic', 'disabled'])}} 
										
									</td>
								</tr>
								<tr>
									<td colspan="10"> A. Scoreboard Measures  </td>
								</tr>
								<tr>
									<td colspan="10"><strong> <i>Breakthrough Goals (BG)</i> </strong> </td>
								</tr>
								<div>
									@foreach($opcr->strategicPriorities as $indicator)
										@if($indicator->flag == 0)
											<tr>
												<td> &nbsp; </td>
												<td> 
													<span class = "current-text mode1">
														{{$indicator->indicator}} 
													</span>
													<input type = "text" name = "indicator" value = "{{$indicator->indicator}}" class = "edit-indicator form-control mode2" {{Role::access('05') ? '' : 'disabled'}} />
												</td>
												<td> 
													<span class = "current-text mode1">
														{{$indicator->accomplishment}} 
													</span>
													<input type = "text" name = "accomplishment" value="{{$indicator->accomplishment}}" class = "edit-accomplishment form-control mode2"/>
												</td>
												<td> 
													<span class = "current-text mode1">
														{{$indicator->qn}} 
													</span>
													<input type = "text" name = "qn" value="{{$indicator->qn}}" class = "edit-qn form-control mode2" {{Role::access('05') ? '' : 'disabled'}} />
												</td>
												<td> 
													<span class = "current-text mode1">
														{{$indicator->q1}} 
													</span>
													<input type = "text" name = "q1" value="{{$indicator->q1}}" class = "edit-q1 form-control mode2" {{Role::access('05') ? '' : 'disabled'}} />
												</td>
												<td> 
													<span class = "current-text mode1">
														{{$indicator->t}} 
													</span>
													<input type = "text" name = "t" value="{{$indicator->t}}" class = "edit-t form-control mode2" {{Role::access('05') ? '' : 'disabled'}} />
												</td>
												
												<td><b>{{$indicator->averageRating()}}</b></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>

								                  {{Form::open(['class' => 'mode1']) }}
								                    {{Form::button('Edit', ['class' => 'btn btn-default btn-xs allow-edit mode1'])}}
								                    @if(Role::access('05')) 
								                    {{ link_to( "opcr/iDelete/{$indicator->id}/{$opcr->id}", 'Delete', array('class'=>'btn btn-default btn-xs') ) }}
								                    @endif
								                    
								                  {{ Form::close()}}
								                  
								                 {{ Form::open(['url' => "/opcr/iStore/{$opcr->id}", 'class' => 'form-inline mode2', 'role' => 'form']) }}
								                    {{Form::hidden("indicator_id", $indicator->id, ['class' => 'hidden']); }}
								                    {{Form::hidden("indicator", null, ['class' => 'new_indicator']); }}
								                    {{Form::hidden("accomplishment", null, ['class' => 'new_accomplishment']); }}
								                    {{Form::hidden("qn", null, ['class' => 'new_qn']); }}
								                    {{Form::hidden("q1", null, ['class' => 'new_q1']); }}
								                     {{Form::hidden("t", null, ['class' => 'new_t']); }}
								                    {{Form::submit('Save', ['class' => 'btn btn-default btn-xs save-edit mode2', 'name'=>'bgedit'])}}
								                    {{Form::button('Cancel', ['class' => 'btn btn-default btn-xs cancel-edit mode2'])}}
								                  {{ Form::close() }}
								                </td>
											</tr>
										@endif
									@endforeach
								</div>
						
						
						<tr>
							<td colspan="10"><strong> <i>Lead Measures (LM) </i> </strong> </td>
						</tr>
						<tbody id="A-LM">
								<div>
									@foreach($opcr->strategicPriorities as $indicator)
										@if($indicator->flag == 1)
											<tr>
												<td> &nbsp; </td>
												<td> 
													<span class = "current-text mode1">
														{{$indicator->indicator}} 
													</span>
													<input type = "text" name = "indicator" value = "{{$indicator->indicator}}" class = "edit-indicator form-control mode2" {{Role::access('05') ? '' : 'disabled'}} />
												</td>
												<td> 
													<span class = "current-text mode1">
														{{$indicator->accomplishment}} 
													</span>
													<input type = "text" name = "accomplishment" value="{{$indicator->accomplishment}}" class = "edit-accomplishment form-control mode2"/>
												</td>
												<td> 
													<span class = "current-text mode1">
														{{$indicator->qn}} 
													</span>
													<input type = "text" name = "qn" value="{{$indicator->qn}}" class = "edit-qn form-control mode2" {{Role::access('05') ? '' : 'disabled'}} />
												</td>
												<td> 
													<span class = "current-text mode1">
														{{$indicator->q1}} 
													</span>
													<input type = "text" name = "q1" value="{{$indicator->q1}}" class = "edit-q1 form-control mode2" {{Role::access('05') ? '' : 'disabled'}} />
												</td>
												<td> 
													<span class = "current-text mode1">
														{{$indicator->t}} 
													</span>
													<input type = "text" name = "t" value="{{$indicator->t}}" class = "edit-t form-control mode2" {{Role::access('05') ? '' : 'disabled'}} />
												</td>
												
												<td><b>{{$indicator->averageRating()}}</b></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>

								                  {{Form::open(['class' => 'mode1']) }}
								                    {{Form::button('Edit', ['class' => 'btn btn-default btn-xs allow-edit mode1'])}}
								                    @if(Role::access('05')) 
								                    	{{ link_to( "opcr/iDelete/{$indicator->id}/{$opcr->id}", 'Delete', array('class'=>'btn btn-default btn-xs') ) }}
								                    @endif
								                    
								                    
								                  {{ Form::close()}}
								                  
								                 {{ Form::open(['url' => "/opcr/iStore/{$opcr->id}", 'class' => 'form-inline mode2', 'role' => 'form']) }}
								                    {{Form::hidden("indicator_id", $indicator->id, ['class' => 'hidden']); }}
								                    {{Form::hidden("indicator", null, ['class' => 'new_indicator']); }}
								                    {{Form::hidden("accomplishment", null, ['class' => 'new_accomplishment']); }}
								                    {{Form::hidden("qn", null, ['class' => 'new_qn']); }}
								                    {{Form::hidden("q1", null, ['class' => 'new_q1']); }}
								                     {{Form::hidden("t", null, ['class' => 'new_t']); }}
								                    {{Form::submit('Save', ['class' => 'btn btn-default btn-xs save-edit mode2', 'name'=>'lmedit'])}}
								                    {{Form::button('Cancel', ['class' => 'btn btn-default btn-xs cancel-edit mode2'])}}
								                  {{ Form::close() }}
								                </td>
											</tr>
										@endif
									@endforeach
								</div>
								</tbody>
								<tr>
									<td colspan="10">  B. Strategic Outcomes  </td>
								</tr>
								
								@foreach($objectives as $key=>$objective)

											<?php $first = true ?>

											@foreach($opcr->coreFunctions as $indicator)
												@if($indicator->objective_id == $key)
													@if($first)
														<tr>
															<td colspan="10"><strong> <i>{{$objective->objective}}</i> </strong> </td>
														</tr>
														<?php $first = false; ?>
													@endif	
													<tr>
														<td> &nbsp; </td>
														<td> 
															<span class = "current-text mode1">
																{{$indicator->indicator}} 
															</span>
															<input type = "text" name = "indicator" value = "{{$indicator->indicator}}" class = "edit-indicator form-control mode2" {{Role::access('05') && ! $indicator->description ? '' : 'disabled'}}/>
														</td>
														<td> 
															<span class = "current-text mode1">
																{{$indicator->accomplishment}} {{$indicator->description}}
															</span>
															<div class=" mode2">
																	<input type = "text" name = "accomplishment" value="{{$indicator->accomplishment}}" class = "edit-accomplishment form-control"/>
																{{$indicator->description}}
															</div>
														</td>
														<td> 
															<span class = "current-text mode1">
																{{$indicator->qn}} 
															</span>
															<input type = "text" name = "qn" value="{{$indicator->qn}}" class = "edit-qn form-control mode2" {{Role::access('05') ? '' : 'disabled'}}/>
														</td>
														<td> 
															<span class = "current-text mode1">
																{{$indicator->q1}} 
															</span>
															<input type = "text" name = "q1" value="{{$indicator->q1}}" class = "edit-q1 form-control mode2" {{Role::access('05') ? '' : 'disabled'}}/>
														</td>
														<td> 
															<span class = "current-text mode1">
																{{$indicator->t}} 
															</span>
															<input type = "text" name = "t" value="{{$indicator->t}}" class = "edit-t form-control mode2" {{Role::access('05') ? '' : 'disabled'}}/>
														</td>
														
														<td><b>{{$indicator->averageRating()}}</b></td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td>

										                  {{Form::open(['class' => 'mode1']) }}
										                    {{Form::button('Edit', ['class' => 'btn btn-default btn-xs allow-edit mode1'])}}
								                   			@if(Role::access('05')) 
										                    {{ link_to( "opcr/iDelete1/{$indicator->id}/{$opcr->id}", 'Delete', array('class'=>'btn btn-default btn-xs') ) }}
										                    @endif
										                    
										                  {{ Form::close()}}
										                  
										                 {{ Form::open(['url' => "/opcr/iStore/{$opcr->id}", 'class' => 'form-inline mode2', 'role' => 'form']) }}
										                    {{Form::hidden("indicator_id", $indicator->id, ['class' => 'hidden']); }}
										                    {{Form::hidden("indicator", null, ['class' => 'new_indicator']); }}
										                    {{Form::hidden("accomplishment", null, ['class' => 'new_accomplishment']); }}
										                    {{Form::hidden("qn", null, ['class' => 'new_qn']); }}
										                    {{Form::hidden("q1", null, ['class' => 'new_q1']); }}
										                     {{Form::hidden("t", null, ['class' => 'new_t']); }}
										                    {{Form::submit('Save', ['class' => 'btn btn-default btn-xs save-edit mode2', 'name'=>'coreedit'])}}
										                    {{Form::button('Cancel', ['class' => 'btn btn-default btn-xs cancel-edit mode2'])}}
										                  {{ Form::close() }}
										                </td>
													</tr>
												@endif
											@endforeach
										@endforeach
							</table>
					<table class = "table table-center-headers table-bordered">
						<tr> 
							<td colspan="9"> II. Other KRAs (40%)   </td>
							<td>
								<?php 
									$leader=0; 
									$counter=0;
								?>
								
								
								@foreach($opcr->leadershipCompetencies as $indicator)
									<?php 
										$leader=$leader+($indicator->averageRating());
										$counter++;
									?>
								@endforeach
								{{Form::text('', $leader1=($leader/($counter? $counter : 1))*.40, ['class'=>'form-control','id' => 'strategic', 'disabled'])}} 
							</td>
						</tr>
						<tbody id="D">
							@if(Role::access('05'))
							<tr class="opcr-add">
								{{Form::open(['url' => "/opcr/iStore/{$opcr->id}", 'class' => 'mode1']) }}
								<td width="15%"> &nbsp; </td>
								
								<td width="19%"> {{Form::text('indicator', null, ['class' => 'form-control input2'])}} </td>
								<td width="16%"> {{Form::text('accomplishment', null, ['class' => 'form-control input3'])}} </td>
								<td width="5.5%"> {{Form::text('qn', null, ['class' => 'form-control input4'])}} </td>
								<td width="5.5%"> {{Form::text('q1', null, ['class' => 'form-control input5'])}}  </td>
								<td width="5.5%"> {{Form::text('t', null, ['class' => 'form-control input6'])}}  </td>
								<td width="6%">{{Form::text('', 0, ['class' => 'form-control stratAve','disabled'])}}</td>
								<td width="8%">{{Form::text('', null, ['class' => 'form-control'])}} </td>
								<td width="13%">{{Form::text('', null, ['class' => 'form-control'])}} </td>
								<td> {{Form::submit('Add', ['name'=>'leader','class' => 'form-control add-button', 'data-tbody' => 'D'])}} </td>
								{{Form::close()}}
							</tr>
							@endif
							
							@foreach($opcr->leadershipCompetencies as $indicator)
									<tr>
										<td> &nbsp; </td>
										<td> 
											<span class = "current-text mode1">
												{{$indicator->indicator}} 
											</span>
											<input type = "text" name = "indicator" value = "{{$indicator->indicator}}" class = "edit-indicator form-control mode2" {{Role::access('05') ? '' : 'disabled'}}/>
										</td>
										<td> 
											<span class = "current-text mode1">
												{{$indicator->accomplishment}} 
											</span>
											<input type = "text" name = "accomplishment" value="{{$indicator->accomplishment}}" class = "edit-accomplishment form-control mode2"/>
										</td>
										<td> 
											<span class = "current-text mode1">
												{{$indicator->qn}} 
											</span>
											<input type = "text" name = "qn" value="{{$indicator->qn}}" class = "edit-qn form-control mode2" {{Role::access('05') ? '' : 'disabled'}}/>
										</td>
										<td> 
											<span class = "current-text mode1">
												{{$indicator->q1}} 
											</span>
											<input type = "text" name = "q1" value="{{$indicator->q1}}" class = "edit-q1 form-control mode2" {{Role::access('05') ? '' : 'disabled'}}/>
										</td>
										<td> 
											<span class = "current-text mode1">
												{{$indicator->t}} 
											</span>
											<input type = "text" name = "t" value="{{$indicator->t}}" class = "edit-t form-control mode2" {{Role::access('05') ? '' : 'disabled'}}/>
										</td>
										
										<td><b>{{$indicator->averageRating()}}</b></td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>

						                  {{Form::open(['class' => 'mode1']) }}
						                    {{Form::button('Edit', ['class' => 'btn btn-default btn-xs allow-edit mode1'])}}
								            @if(Role::access('05')) 
						                    {{ link_to( "opcr/iDelete3/{$indicator->id}/{$opcr->id}", 'Delete', array('class'=>'btn btn-default btn-xs') ) }}
						                    @endif
						                    
						                  {{ Form::close()}}
						                  
						                 {{ Form::open(['url' => "/opcr/iStore/{$opcr->id}", 'class' => 'form-inline mode2', 'role' => 'form']) }}
						                    {{Form::hidden("indicator_id", $indicator->id, ['class' => 'hidden']); }}
						                    {{Form::hidden("indicator", null, ['class' => 'new_indicator']); }}
						                    {{Form::hidden("accomplishment", null, ['class' => 'new_accomplishment']); }}
						                    {{Form::hidden("qn", null, ['class' => 'new_qn']); }}
						                    {{Form::hidden("q1", null, ['class' => 'new_q1']); }}
						                     {{Form::hidden("t", null, ['class' => 'new_t']); }}
						                    {{Form::submit('Save', ['class' => 'btn btn-default btn-xs save-edit mode2', 'name'=>'leaderedit'])}}
						                    {{Form::button('Cancel', ['class' => 'btn btn-default btn-xs cancel-edit mode2'])}}
						                  {{ Form::close() }}
						                </td>
									</tr>
								@endforeach
						</tbody>
					</table>
					<table class = "table table-center-headers table-bordered">
						<tr>
							<td colspan="9"> III. OTHER FUNCTIONS/DIRECTIVES FROM THE SECRETARY AND CLUSTER HEAD (10%)  </td>
							<td>
								<?php 
									$other=0; 
									$counter=0;
								?>
								@foreach($opcr->otherFunctions as $indicator)
									<?php 
										$other=$other+($indicator->averageRating());
										$counter++;
									?>
								@endforeach
								{{Form::text('', $other1=($other/($counter? $counter : 1))*.10, ['class'=>'form-control','id' => 'strategic', 'disabled'])}} 
								
							</td>
						</tr>
						<tbody id="C">
							@if(Role::access('05'))
							<tr class="opcr-add">
								{{Form::open(['url' => "/opcr/iStore/{$opcr->id}", 'class' => 'mode1']) }}
									<td width="15%"> &nbsp;</td>
									<td width="19%"> 
										{{Form::text('indicator', null, ['class' => 'form-control input2'])}} 
									</td>
									<td width="16%"> 
										{{Form::text('accomplishment', null, ['class' => 'form-control input3'])}} 
									</td>
									<td width="5.5%"> 
										{{Form::text('qn', null, ['class' => 'form-control input4'])}} 
									</td>
									<td width="5.5%"> 
										{{Form::text('q1', null, ['class' => 'form-control input5'])}}  
									</td>
									<td width="5.5%"> 
										{{Form::text('t', null, ['class' => 'form-control input6'])}}  
									</td>
									<td width="6%">{{Form::text('', 0, ['class' => 'form-control stratAve','disabled'])}}</td>
									<td width="8%">{{Form::text('', null, ['class' => 'form-control'])}} </td>
									<td width="12.5%">{{Form::text('', null, ['class' => 'form-control'])}} </td>
									<td width="8.5%"> {{Form::submit('Add', ['name'=>'other','class' => 'form-control add-button', 'data-tbody' => 'A-BM'])}} </td>
								{{Form::close()}}
							</tr>
							@endif
							@foreach($opcr->otherFunctions as $indicator)
									<tr>
										<td> &nbsp; </td>
										<td> 
											<span class = "current-text mode1">
												{{$indicator->indicator}} 
											</span>
											<input type = "text" name = "indicator" value = "{{$indicator->indicator}}" class = "edit-indicator form-control mode2" {{Role::access('05') ? '' : 'disabled'}}/>
										</td>
										<td> 
											<span class = "current-text mode1">
												{{$indicator->accomplishment}} 
											</span>
											<input type = "text" name = "accomplishment" value="{{$indicator->accomplishment}}" class = "edit-accomplishment form-control mode2"/>
										</td>
										<td> 
											<span class = "current-text mode1">
												{{$indicator->qn}} 
											</span>
											<input type = "text" name = "qn" value="{{$indicator->qn}}" class = "edit-qn form-control mode2" {{Role::access('05') ? '' : 'disabled'}}/>
										</td>
										<td> 
											<span class = "current-text mode1">
												{{$indicator->q1}} 
											</span>
											<input type = "text" name = "q1" value="{{$indicator->q1}}" class = "edit-q1 form-control mode2" {{Role::access('05') ? '' : 'disabled'}}/>
										</td>
										<td> 
											<span class = "current-text mode1">
												{{$indicator->t}} 
											</span>
											<input type = "text" name = "t" value="{{$indicator->t}}" class = "edit-t form-control mode2" {{Role::access('05') ? '' : 'disabled'}}/>
										</td>
										
										<td><b>{{$indicator->averageRating()}}</b></td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>

						                  {{Form::open(['class' => 'mode1']) }}
						                    {{Form::button('Edit', ['class' => 'btn btn-default btn-xs allow-edit mode1'])}}
						                    @if(Role::access('05')) 
						                    {{ link_to( "opcr/iDelete2/{$indicator->id}/{$opcr->id}", 'Delete', array('class'=>'btn btn-default btn-xs') ) }}
						                   	@endif
						                    
						                    
						                  {{ Form::close()}}
						                  
						                 {{ Form::open(['url' => "/opcr/iStore/{$opcr->id}", 'class' => 'form-inline mode2', 'role' => 'form']) }}
						                    {{Form::hidden("indicator_id", $indicator->id, ['class' => 'hidden']); }}
						                    {{Form::hidden("indicator", null, ['class' => 'new_indicator']); }}
						                    {{Form::hidden("accomplishment", null, ['class' => 'new_accomplishment']); }}
						                    {{Form::hidden("qn", null, ['class' => 'new_qn']); }}
						                    {{Form::hidden("q1", null, ['class' => 'new_q1']); }}
						                     {{Form::hidden("t", null, ['class' => 'new_t']); }}
						                    {{Form::submit('Save', ['class' => 'btn btn-default btn-xs save-edit mode2', 'name'=>'otheredit'])}}
						                    {{Form::button('Cancel', ['class' => 'btn btn-default btn-xs cancel-edit mode2'])}}
						                  {{ Form::close() }}
						                </td>
									</tr>
								@endforeach
							
							</tbody>
							<tr class="opcr-add">
								<td width="15%"> Final Average Rating</td>
								<td width="19%"> &nbsp; </td>
								<td width="16%"> &nbsp; </td>
								<td width="5.5%">  &nbsp; </td>
								<td width="5.5%">  &nbsp;  </td>
								<td width="5.5%">  &nbsp;  </td>
								<td width="6%">&nbsp;</td>
								<td width="8%">&nbsp;</td>
								<td width="13%">&nbsp;</td>
								<td width="%8"> {{Form::text('', $strat1+$other1+$leader1, ['class'=>'form-control','id' => 'final', 'disabled'])}} </td>
							</tr>
							<tr class="opcr-add">
								<td width="15%"> Adjectival Rating</td>
								<td width="19%"> &nbsp; </td>
								<td width="16%"> &nbsp; </td>
								<td width="5.5%">  &nbsp; </td>
								<td width="5.5%">  &nbsp;  </td>
								<td width="5.5%">  &nbsp;  </td>
								<td width="6%">&nbsp;</td>
								<td width="8%">&nbsp;</td>
								<td width="13%">&nbsp;</td>
								<td width="%8">  &nbsp; </td>
							</tr>
						</table>
					</div>		
				</div>
			@else
					<div class="box">
						<div class="box-body">
							<table class = "table table-center-headers table-bordered">
								<tr>
									<th class="width-20"> Key Result Area/ Key Results </th>
									<th class="width-30"> Success/Performance Indicators / Measures </th>
									<th class="width-40"> Actual Accomplishment </th>
									<th class="width-10"> ACTIONS </th>
								</tr>
								<tr class="info">
									<td colspan="4" > I. STRATEGIC PRIORITIES  (50%)  </td>
								</tr>
								<tr>
									<td colspan="4"> <b> <i> A. Scoreboard Measures </i> </b> </td>
								</tr>
								<?php $first = true ?>
								@foreach($opcr->strategicPriorities()->whereFlag(0)->get() as $indicator)
									<tr>
										@if($first)
											<td rowspan="{{count($opcr->strategicPriorities()->whereFlag(0)->get())+1}}"> <i>Breakthrough Goals (BG)</i> </td>
										@endif
										<td> 
											<span class = "current-text mode1">
												{{$indicator->indicator}} 
											</span>
											<input type = "text" name = "indicator" value = "{{$indicator->indicator}}" class = "edit-indicator form-control mode2" {{Role::access('05') ? '' : 'disabled'}} />
										</td>
										<td> 
											<span class = "current-text mode1">
												{{$indicator->accomplishment}} 
											</span>
											<input type = "text" name = "accomplishment" value="{{$indicator->accomplishment}}" class = "edit-accomplishment form-control mode2"/>
										</td>
										<td>

						                  {{Form::open(['class' => 'mode1']) }}
						                    {{Form::button('<i class="fa fa-pencil"> </i>', ['class' => 'btn btn-default allow-edit mode1'])}}
						                    @if(Role::access('05')) 
						                    {{ HTML::decode(link_to( "opcr/iDelete/{$indicator->id}/{$opcr->id}", '<i class="fa fa-times"> </i>', array('class'=>'btn btn-danger') )) }}
						                    @endif
						                    
						                  {{ Form::close()}}
						                  
						                 {{ Form::open(['url' => "/opcr/iStore/{$opcr->id}", 'class' => 'form-inline mode2', 'role' => 'form']) }}
						                    {{Form::hidden("indicator_id", $indicator->id, ['class' => 'hidden']); }}
						                    {{Form::hidden("indicator", null, ['class' => 'new_indicator']); }}
						                    {{Form::hidden("accomplishment", null, ['class' => 'new_accomplishment']); }}
						                    {{Form::hidden("qn", null, ['class' => 'new_qn']); }}
						                    {{Form::hidden("q1", null, ['class' => 'new_q1']); }}
						                     {{Form::hidden("t", null, ['class' => 'new_t']); }}
						                    {{Form::submit('Save', ['class' => 'btn btn-default btn-xs save-edit mode2', 'name'=>'bgedit'])}}
						                    {{Form::button('Cancel', ['class' => 'btn btn-default btn-xs cancel-edit mode2'])}}
						                  {{ Form::close() }}
						                </td>
									</tr>
									<?php $first = false ?>
								@endforeach
								@if(Role::access('05'))
									<tr class="opcr-add">
										{{Form::open(['url' => "/opcr/iStore/{$opcr->id}", 'class' => 'mode1']) }}
											@if($first)
												<td rowspan="{{count($opcr->strategicPriorities()->whereFlag(0)->get())+1}}"> <i>Breakthrough Goals (BG)</i> </td>
											@endif
											<td > 
												{{Form::textarea('indicator', null, ['class' => 'form-control input2', 'rows' => 3])}} 
											</td>
											<td > 
												{{Form::textarea('accomplishment', null, ['class' => 'form-control input3', 'rows' => 3])}} 
											</td>
											<td> {{Form::button('<i class="fa fa-plus fw"> </i>', ['name'=>'bg','class' => 'btn btn-default add-button', 'data-tbody' => 'A-BM', 'type' => 'submit','title'=>'Add'])}} </td>
										{{Form::close()}}
									</tr>
								@endif

								<?php $first = true ?>
								@foreach($opcr->strategicPriorities()->whereFlag(1)->get() as $indicator)
									<tr>
										@if($first)
											<td rowspan="{{count($opcr->strategicPriorities()->whereFlag(1)->get())+1}}"> <i>Lead Measures (LM)</i> </td>
										@endif
										<td> 
											<span class = "current-text mode1">
												{{$indicator->indicator}} 
											</span>
											<input type = "text" name = "indicator" value = "{{$indicator->indicator}}" class = "edit-indicator form-control mode2" {{Role::access('05') ? '' : 'disabled'}} />
										</td>
										<td> 
											<span class = "current-text mode1">
												{{$indicator->accomplishment}} 
											</span>
											<input type = "text" name = "accomplishment" value="{{$indicator->accomplishment}}" class = "edit-accomplishment form-control mode2"/>
										</td>
										<td>

						                  {{Form::open(['class' => 'mode1']) }}
						                    {{Form::button('<i class="fa fa-pencil"> </i>', ['class' => 'btn btn-default allow-edit mode1'])}}
						                    @if(Role::access('05')) 
						                    {{ HTML::decode(link_to( "opcr/iDelete/{$indicator->id}/{$opcr->id}", '<i class="fa fa-times"> </i>', array('class'=>'btn btn-danger')) ) }}
						                    @endif
						                    
						                  {{ Form::close()}}
						                  
						                 {{ Form::open(['url' => "/opcr/iStore/{$opcr->id}", 'class' => 'form-inline mode2', 'role' => 'form']) }}
						                    {{Form::hidden("indicator_id", $indicator->id, ['class' => 'hidden']); }}
						                    {{Form::hidden("indicator", null, ['class' => 'new_indicator']); }}
						                    {{Form::hidden("accomplishment", null, ['class' => 'new_accomplishment']); }}
						                    {{Form::hidden("qn", null, ['class' => 'new_qn']); }}
						                    {{Form::hidden("q1", null, ['class' => 'new_q1']); }}
						                     {{Form::hidden("t", null, ['class' => 'new_t']); }}
						                    {{Form::submit('Save', ['class' => 'btn btn-default btn-xs save-edit mode2', 'name'=>'bgedit'])}}
						                    {{Form::button('Cancel', ['class' => 'btn btn-default btn-xs cancel-edit mode2'])}}
						                  {{ Form::close() }}
						                </td>
									</tr>
									<?php $first = false ?>
								@endforeach
								@if(Role::access('05'))
									<tr class="opcr-add">
										{{Form::open(['url' => "/opcr/iStore/{$opcr->id}", 'class' => 'mode1']) }}
											@if($first)
												<td rowspan="{{count($opcr->strategicPriorities()->whereFlag(1)->get())+1}}"> <i>Lead Measures (LM)</i> </td>
											@endif
											<td > 
												{{Form::textarea('indicator', null, ['class' => 'form-control input2', 'rows' => 3])}} 
											</td>
											<td > 
												{{Form::textarea('accomplishment', null, ['class' => 'form-control input3', 'rows' => 3])}} 
											</td>
											<td> {{ Form::button("<i class='fa fa-plus fw'></i>", ['name'=>'lm','class' => 'btn btn-default add-button', 'data-tbody' => 'A-BM', 'type' => 'submit', 'title'=>'Add'])}} </td>
										{{Form::close()}}
									</tr>
								@endif
								<tr>
									<td colspan="4"> <b> <i> B. Strategic Outcomes </i> </b> </td>
								</tr>
								
								@foreach($objectives as $key=>$objective)
									<?php $first = true ?>

									@foreach($opcr->coreFunctions()->whereObjectiveId($key)->get() as $indicator)
										<tr>
											@if($first)
												<td rowspan="{{count($opcr->coreFunctions()->whereObjectiveId($key)->get())+1}}"> <i>{{$objective->objective}}</i> </td>
											@endif
											<td> 
												<span class = "current-text mode1">
													{{$indicator->indicator}} 
												</span>
												<input type = "text" name = "indicator" value = "{{$indicator->indicator}}" class = "edit-indicator form-control mode2" {{Role::access('05') && ! $indicator->description ? '' : 'disabled'}}/>
											</td>
											<td> 
												<span class = "current-text mode1">
													{{$indicator->accomplishment}} {{$indicator->description}}
												</span>
												<div class="row mode2">
													<div class="col-md-3">
														<input type = "text" name = "accomplishment" value="{{$indicator->accomplishment}}" class = "edit-accomplishment form-control"/>
													</div>
													<div class="col-md-9"> {{$indicator->description}} </div>
												</div>
											</td>
											<td>

							                  {{Form::open(['class' => 'mode1']) }}
							                    {{Form::button('<i class="fa fa-pencil"> </i>', ['class' => 'btn btn-default allow-edit mode1'])}}
					                   			@if(Role::access('05') && ! $indicator->description)
							                    {{ HTML::decode(link_to( "opcr/iDelete1/{$indicator->id}/{$opcr->id}", '<i class="fa fa-times"> </i>', array('class'=>'btn btn-danger') )) }}
							                    @endif
							                    
							                  {{ Form::close()}}
							                  
							                 {{ Form::open(['url' => "/opcr/iStore/{$opcr->id}", 'class' => 'form-inline mode2', 'role' => 'form']) }}
							                    {{Form::hidden("indicator_id", $indicator->id, ['class' => 'hidden']); }}
							                    {{Form::hidden("indicator", null, ['class' => 'new_indicator']); }}
							                    {{Form::hidden("accomplishment", null, ['class' => 'new_accomplishment']); }}
							                    {{Form::hidden("qn", null, ['class' => 'new_qn']); }}
							                    {{Form::hidden("q1", null, ['class' => 'new_q1']); }}
							                     {{Form::hidden("t", null, ['class' => 'new_t']); }}
							                    {{Form::submit('Save', ['class' => 'btn btn-default btn-xs save-edit mode2', 'name'=>'coreedit'])}}
							                    {{Form::button('Cancel', ['class' => 'btn btn-default btn-xs cancel-edit mode2'])}}
							                  {{ Form::close() }}
							                </td>
										</tr>
										<?php $first =false ?>
									@endforeach
									@if(Role::access('05'))
										<tr class="opcr-add">
											{{Form::open(['url' => "/opcr/iStore/{$opcr->id}", 'class' => 'mode1']) }}
											@if($first)
												<td rowspan="{{count($opcr->coreFunctions()->whereObjectiveId($key)->get())+1}}"> <i>{{$objective->objective}}</i> </td>
											@endif
											<td> 
												{{Form::hidden('KRA', $objective->id)}} 
												{{Form::textarea('indicator', null, ['class' => 'form-control input2', 'rows' => 3])}} 
											</td>
											<td> {{Form::textarea('accomplishment', null, ['class' => 'form-control input3', 'rows' => 3])}} </td>
											<td> {{Form::button('<i class="fa fa-plus fw"> </i>', ['name'=>'core','class' => 'btn btn-default add-button', 'data-tbody' => 'A-BM', 'type'=>'submit', 'title'=>'Add'])}}  </td>
											{{Form::close()}}
										</tr>
									@endif
								@endforeach
						<tr> 
							<td colspan="4"> II. Other KRAs (40%)   </td>
						</tr>
							@foreach($opcr->leadershipCompetencies as $indicator)
								<tr>
									<td> &nbsp; </td>
									<td> 
										<span class = "current-text mode1">
											{{$indicator->indicator}} 
										</span>
										<input type = "text" name = "indicator" value = "{{$indicator->indicator}}" class = "edit-indicator form-control mode2" {{Role::access('05') ? '' : 'disabled'}}/>
									</td>
									<td> 
										<span class = "current-text mode1">
											{{$indicator->accomplishment}} 
										</span>
										<input type = "text" name = "accomplishment" value="{{$indicator->accomplishment}}" class = "edit-accomplishment form-control mode2"/>
									</td>
									<td>

					                  {{Form::open(['class' => 'mode1']) }}
					                    {{Form::button('<i class="fa fa-pencil"> </i>', ['class' => 'btn btn-default allow-edit mode1'])}}
							            @if(Role::access('05')) 
					                    {{ HTML::decode( link_to( "opcr/iDelete3/{$indicator->id}/{$opcr->id}", '<i class="fa fa-times"> </i>', array('class'=>'btn btn-danger') )) }}
					                    @endif
					                    
					                  {{ Form::close()}}
					                  
					                 {{ Form::open(['url' => "/opcr/iStore/{$opcr->id}", 'class' => 'form-inline mode2', 'role' => 'form']) }}
					                    {{Form::hidden("indicator_id", $indicator->id, ['class' => 'hidden']); }}
					                    {{Form::hidden("indicator", null, ['class' => 'new_indicator']); }}
					                    {{Form::hidden("accomplishment", null, ['class' => 'new_accomplishment']); }}
					                    {{Form::hidden("qn", null, ['class' => 'new_qn']); }}
					                    {{Form::hidden("q1", null, ['class' => 'new_q1']); }}
					                     {{Form::hidden("t", null, ['class' => 'new_t']); }}
					                    {{Form::submit('Save', ['class' => 'btn btn-default btn-xs save-edit mode2', 'name'=>'leaderedit'])}}
					                    {{Form::button('Cancel', ['class' => 'btn btn-default btn-xs cancel-edit mode2'])}}
					                  {{ Form::close() }}
					                </td>
								</tr>
							@endforeach
							@if(Role::access('05'))
								<tr class="opcr-add">
									{{Form::open(['url' => "/opcr/iStore/{$opcr->id}", 'class' => 'mode1']) }}
									<td> &nbsp; </td>
									<td> {{Form::textarea('indicator', null, ['class' => 'form-control input2', 'rows' => 3])}} </td>
									<td> {{Form::textarea('accomplishment', null, ['class' => 'form-control input3', 'rows' => 3])}} </td>
									<td> {{Form::button('<i class="fa fa-plus fw"> </i>', ['name'=>'leader','class' => 'btn btn-default add-button', 'data-tbody' => 'D', 'type' => 'submit', 'title'=>'Add'])}} </td>
									{{Form::close()}}
								</tr>
							@endif
							

						<tr>
							<td colspan="4"> III. OTHER FUNCTIONS/DIRECTIVES FROM THE SECRETARY AND CLUSTER HEAD (10%)  </td>
						</tr>
						<tbody id="C">
							@foreach($opcr->otherFunctions as $indicator)
									<tr>
										<td> &nbsp; </td>
										<td> 
											<span class = "current-text mode1">
												{{$indicator->indicator}} 
											</span>
											<input type = "text" name = "indicator" value = "{{$indicator->indicator}}" class = "edit-indicator form-control mode2" {{Role::access('05') ? '' : 'disabled'}}/>
										</td>
										<td> 
											<span class = "current-text mode1">
												{{$indicator->accomplishment}} 
											</span>
											<input type = "text" name = "accomplishment" value="{{$indicator->accomplishment}}" class = "edit-accomplishment form-control mode2"/>
										</td>
										
										<td>

						                  {{Form::open(['class' => 'mode1']) }}
						                    {{Form::button('<i class="fa fa-pencil"> </i>', ['class' => 'btn btn-default allow-edit mode1'])}}
						                    @if(Role::access('05')) 
						                    {{ HTML::decode(link_to( "opcr/iDelete2/{$indicator->id}/{$opcr->id}", '<i class="fa fa-times fw"> </i>', array('class'=>'btn btn-danger') )) }}
						                   	@endif
						                    
						                    
						                  {{ Form::close()}}
						                  
						                 {{ Form::open(['url' => "/opcr/iStore/{$opcr->id}", 'class' => 'form-inline mode2', 'role' => 'form']) }}
						                    {{Form::hidden("indicator_id", $indicator->id, ['class' => 'hidden']); }}
						                    {{Form::hidden("indicator", null, ['class' => 'new_indicator']); }}
						                    {{Form::hidden("accomplishment", null, ['class' => 'new_accomplishment']); }}
						                    {{Form::hidden("qn", null, ['class' => 'new_qn']); }}
						                    {{Form::hidden("q1", null, ['class' => 'new_q1']); }}
						                     {{Form::hidden("t", null, ['class' => 'new_t']); }}
						                    {{Form::submit('Save', ['class' => 'btn btn-default btn-xs save-edit mode2', 'name'=>'otheredit'])}}
						                    {{Form::button('Cancel', ['class' => 'btn btn-default btn-xs cancel-edit mode2'])}}
						                  {{ Form::close() }}
						                </td>
									</tr>
								@endforeach
								@if(Role::access('05'))
									<tr class="opcr-add">
										{{Form::open(['url' => "/opcr/iStore/{$opcr->id}", 'class' => 'mode1']) }}
											<td> &nbsp;</td>
											<td> 
												{{Form::textarea('indicator', null, ['class' => 'form-control input2', 'rows' => 3])}} 
											</td>
											<td> 
												{{Form::textarea('accomplishment', null, ['class' => 'form-control input3', 'rows' => 3])}} 
											</td>
											<td width="8.5%"> {{Form::button('<i class="fa fa-plus fw"> </i>', ['name'=>'other','class' => 'btn btn-default add-button', 'data-tbody' => 'A-BM', 'type' => 'submit', 'title'=>'Add'])}} </td>
										{{Form::close()}}
									</tr>
								@endif
							</tbody>
						</table>
					</div>		
				</div>
			@endif
</div>
</div>

<div class="box remarks">
	<div class="box-header box-header-default">
		Remarks
		<div class="pull-right">
			{{Form::label('filter', 'Filter:') }} 
			{{ Form::select('filter', ['Comments', 'Suggestions', 'All'], 2, ['class' => 'baseline filter'])}}
		</div>
	</div>
	<div class="box-body">
		<div class="list-group">
			<div class=" list-group-item">
				<div class="row">
					{{Form::open(['url' => URL::route('opcr.remark', ['id' => $opcr->id])])}}
						<div class="col-md-2">
							{{ Form::radio('type', 0, 1)}} Comment <br/>
							{{ Form::radio('type', 1, 0)}} Suggestion
						</div>
						<div class="col-lg-10">
		                <div class="input-group">
			                {{ Form::text('remark', null, ['class' => 'form-control', 'rows' => 3, 'placeholder' => 'Enter remarks (if any)'])}}
			                <span class="input-group-btn">
			                  {{ Form::submit('Add Remark', ['class' => 'btn btn-primary'])}}
			                </span>
		                </div><!-- /input-group -->
		            </div><!-- /.col-lg-10 -->
					{{ Form::close() }}
				</div>
			</div>
			@if(count($remarks))
				<?php $show_more = true; ?>
				<div class="show-more">
					@foreach($remarks as $key => $remark)
						@if($show_more && ((count($remarks)-3) <= $key))
							</div>
							<?php $show_more = false; ?>
						@endif
						<div class="list-group-item remark{{$remark->type}}">
							<strong> {{ $remark->user->fullName() }} </strong>
							{{ $remark->remark }} <br/>
							<i class="livetimestamp"> 
								{{ $remark->type? "suggested" : "commented"}} {{ $remark->created_at->diffForHumans() }} 
							</i>
						</div>
					@endforeach
				@if(count($remarks) > 3)
					<div class="list-group-item">
						<center>
							{{ Form::button('See All', ['class' => 'btn btn-default remarks-collapse'])}}
						</center>
					</div>
				@endif
			@endif
		</div>
	</div>
</div>

@stop

@section('cssjs')
	{{ HTML::script('js/remarks.js')}}

<script type = "text/javascript">
  $(document).ready(function() {
    $(".mode2").hide();
    $(".save-button").hide();
    $('#selected-office').text($('#office-select option:selected').text());
	$('#office-select').change(function() {
		$('#selected-office').text($('#office-select option:selected').text());
		$('.save-button').show();
	});
	$('#name-opcr').keyup(function() {
		$('#name-opcr').text($('#name-opcr').text());
		$('.save-button').show();
	});
	$('#period-select').keyup(function() {
		$('#selected-period').text($('#period-select').val());
		$('.save-button').show();

	});
	$(".input4").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $(".input5").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $(".input6").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    //stack overflow end
	$('.input4').keyup(function(){
		var form = $(this).closest(".opcr-add");
		$input4=form.find('.input4').val();
		$input5=form.find('.input5').val();
		$input6=form.find('.input6').val();
		if($input4=='')
			$input4=0;
		if($input5=='')
			$input5=0;
		if($input6=='')
			$input6=0;

		form.find('.stratAve').val((parseFloat($input4)+parseFloat($input5)+parseFloat($input6))/3);
    }); 
    $('.input5').keyup(function(){
    	var form = $(this).closest(".opcr-add");
		$input4=form.find('.input4').val();
		$input5=form.find('.input5').val();
		$input6=form.find('.input6').val();
		if($input4=='')
			$input4=0;
		if($input5=='')
			$input5=0;
		if($input6=='')
			$input6=0;

		form.find('.stratAve').val((parseFloat($input4)+parseFloat($input5)+parseFloat($input6))/3);
    }); 
    $('.input6').keyup(function(){
    	var form = $(this).closest(".opcr-add");
		$input4=form.find('.input4').val();
		$input5=form.find('.input5').val();
		$input6=form.find('.input6').val();
		if($input4=='')
			$input4=0;
		if($input5=='')
			$input5=0;
		if($input6=='')
			$input6=0;

		form.find('.stratAve').val((parseFloat($input4)+parseFloat($input5)+parseFloat($input6))/3);
    }); 
    $(".allow-edit").on("click", function() {
      var current = $(this).closest("tr").find(".current-text");
      var textfield = $(this).closest("tr").find(".edit-text");
      var text = current.text().trim();
      current.hide();
      textfield.attr({"placeholder": text, "value": text}).show().focus();
      $(".mode2").hide();
      $(".mode1").show();
      $(this).closest("tr").find(".mode1").hide();
      $(this).closest("tr").find(".mode2").show();
    });

    $(".save-edit").on("click", function(event) {
      var tr = $(this).closest("tr");
      var form = $(this).closest("form");
      form.find(".new_indicator").val(tr.find(".edit-indicator").val());
      form.find(".new_accomplishment").val(tr.find(".edit-accomplishment").val());
      form.find(".new_qn").val(tr.find(".edit-qn").val());
      form.find(".new_q1").val(tr.find(".edit-q1").val());
      form.find(".new_t").val(tr.find(".edit-t").val());
      form.submit();
    });
    
    $(".cancel-edit").on("click", function() {
      $(this).closest("tr").find(".mode2").hide();
      $(this).closest("tr").find(".mode1").show();
    });
    function addIssue(event)  {
      var issues = 0;
      var id = issues++;
      var newIssue = $("<tr><td><input type='text' class='form-control' name='remark"+id+"' clearable></td><td><br><a href='#' class='btn btn-default add' title='Add'><i class='fa fa-plus fw'></i></a> <a href='#' class='btn btn-default remove' title='Delete'><i class='fa fa-times fw'></i></a></td></tr>");
    
      newIssue.find(".add").click(addIssue);
      newIssue.find(".remove").click(removeIssue);
      
      $('#rem').append(newIssue);

      event.preventDefault();
    }

    function removeIssue(event) {      
      $('#rem tr:last').remove();
       sub();
      event.preventDefault();
    }

    $(".add").click(addIssue);
    $(".remove").click(removeIssue);
  });
</script>
@stop
@extends('layouts.default')
@section('content')

<div class="page-header">
	<h2>Notifications</h2>
</div>
<div class="panel panel-default">
    <div class="panel-heading">Notifications</div>
    <ul class="list-group">
      @if(count($notifs))
        @foreach($notifs as $notification)

          <a href="{{$notification->link ? $notification->link : ''}}" class="list-group-item {{$notification->link ? '' : 'inactive-link'}}">
            {{'<strong>'.$notification->doer->fullName() . '</strong> ' . $notification->notification . '<br/>  - <i><small>' . $notification->timeCreated() . '</small> </i>'}}
          </a>
        @endforeach
      @else
          <li class="list-group-item">
            <i><center>Nothing to show</center></i>
          </li>
      @endif
    </ul>
</div>
    <div class="container">
            <span class="pull-right">
              <?php echo $notifs->links(); ?>
            </span>
          </div>
@stop

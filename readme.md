### DSWD Online Performance Management System

Online Performance Management System, in general, aims to better facilitate the monitoring of scoreboard and office performance in contributing to the Enterprise Scorecards by building an integrated web-based information system.
 
The Online Performance Management System aims to help the organization review the performance the various programs within the department based on the current manual process of using the balanced scorecard framework.

The system will enable users to specify the business goal, solutions and measures in order to avoid missing important details as well as view the trends by generating reports and graphs
